# Northwind Truckers Operating System (NTOS)

The Northwind Truckers Operating System is a combined group of applications based off of my ideas for Northwind Traders Basic Edition. However, Northwind Traders Basic Edition was quickly coming to a point where it was overwhelming to incorporate all of the planned functionality in one application. Therefore, I had the idea to create it as multiple smaller applications that work cooperatively and rename the system to Northwind Truckers Operating System (NTOS).

## Description
While NTOS will now be many small applications, the overall plan is still the same. NTOS will provide a means of tracking the following items for owner/operator truck drivers:

1. Loads &mdash; The Load Booking Wizard will still be a part of NTOS, but will be its own application that has the sole purpose of getting loads into the owner/operator's load tracking database. This application will work cooperatively with the employees and customers management applications.
1. Vehichles &mdash; Maintains records on all trucks and/or trailers owned or leased by the owner/operator. Trucks/trailers can be assigned to an employee.
1. Service/Repair &mdash; Maintains all maintenance and repair records for trucks and trailers. Furthermore, through this application, an individual mechanic may be assigned as primary responsibility holder on any given vehicle.
1. Per Diem &mdash; Maintains the timestamp (down to the millisecond) of the start and end of a tour of duty, which is the time spent away from home. This application then calculates the amount of time away from home using both the standard deduction and the exact time deduction calculation methods. Furthermore, this application allows the owner/operator to generate a report using each of these two calculation methods in order to use the one that provides the largest per diem deduction on their tax returns.
1. Customers &mdash; Maintains records for every customer from which loads may be picked up or to which loads may be delivered. These records maintain the name and address of the company, as well as contact information and notes regarding the customer and/or location.
1. Employees &mdash; Maintains all employee-related records, including on-boarding, off-boarding, training, discipline, etc.
1. Fuel Purchases &mdash; Since the largest expense for an owner/operator is the cost of fuel, we have given it its own application. This application manages all fuel purchases, as well as fuel economy when used consistently.
1. Expenses &mdash; Maintains all expenses incurred in the course of doing business.
1. Settlements &mdash; An application that is separate from, but closely linked to, the Loads Management System. This application provides a wizard for settling loads that have been delivered and paid.
1. Reporting &mdash; Each of the applications have their own reporting contained within them. For most of the applications, starting the application with either the `-r` or `--report` command line switch will bring up the reports tool.

While this list is not yet complete, it does give a decent overview of the functionality of the NTOS system and how these programs will be related to one another.

## Roadmap
At this point (March 10, 2022), NTOS is in its infancy, as we start to move the code from the NTA Project to this one. Within the next few days, however, some of the initial ported code will start to materialize in this repository. For now, our schedule is to have a beta of at least a couple of the applications ready within the next few months (hopefully by the end of June, 2022).

## Contributing
Contributions are welcomed! We would love anyone to help us out with the NTOS Project. We do not care if your experience is minimal (we'll guide and mentor you), or if you are extremely experienced. If you want to help us out, we will truly appreciate you! If you are wanting to volunteer Java code, feel free to fork this repository and contribute.

NTOS is being written 100% in Java 11 (either OpenJDK or the Oracle product). We are most likely going to need someone who can do some JavaFX in the future because of how we want NTOS to be launched...A dock similar to Cairo-Dock on *nix or the Mac Dock:

![image.png](./image.png)

By using such a dock, we will be able to keep all of our application icons together and handy for the user. In the image above, the only icons that are related to NTOS are the second group of three, which are the icons to Start and End a Tour of Duty, as well as to open the dialog to choose a per diem report. If we have a dock like this, we will be able to group applications of the NTOS together with their related application, such as the Load Tracking System and the Settlement Wizard. In this example image, the three NTOS icons are all launchers for the same application, just with different command line switches to activate the appropriate part of the application.

We are planning and hoping that this application will spread internationally, so if you would like to contribute transalations (as *.properties files), that will be very welcome.

Artwork is another great way to contribute. At this time we are using copy/paste and rescaling of various free icon sets included with Linux distributions to make our icons. While this works in the short-term, better quality and matched artwork would be great!

## Authors and acknowledgment
At this time, we have three contributors on the NTOS Project:

| Contributor | Email Address | Position |
| :---        | :---          | :---     |
| Sean Carrick | sean at gs-unitedlabs dot com | Original Founder of GS United Labs and Lead Developer/Planner |
| Jiří Kovalský | jiri at gs-unitedlabs dot com | Original Founder of GS United Labs and Lead Tester/Team Developer |
| Kevin Nathan | knathan at gs-unitedlabs dot com| Contributing Developer and Tester |

## License
The NTOS Project is being released under the GNU General Public License (GPL) version 3.0.

## Project status
The current Project status is: infancy. We are working to port source code from the NTA Project (which was not very far along due to confusion of how things were supposed to work together). This phase of the Project may take a while since all three contributors have full-time employment that does not involve this Project and only work on it when they have a little free time. We also (Sean & Jiří) have family needs that need to have time made available, so that reduces the amount of time available for the Project as well. Work will continue, but may be slow at times...
