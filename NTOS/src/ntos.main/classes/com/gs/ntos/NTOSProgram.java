/*
 * Copyright (C) 2022 GS United Labs
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * *****************************************************************************
 *  Project    :   LoadAccounting
 *  Class      :   NTOSProgram.java
 *  Author     :   Sean Carrick
 *  Created    :   Mar 29, 2022
 *  Modified   :   Mar 29, 2022
 *  
 *  Purpose: See class JavaDoc for explanation
 *  
 *  Revision History:
 *  
 *  WHEN          BY                   REASON
 *  ------------  -------------------  -----------------------------------------
 *  Mar 29, 2022  Sean Carrick         Initial creation.
 * *****************************************************************************
 */

package com.gs.ntos;

import com.gs.ntos.api.DesktopProgram;
import com.gs.ntos.api.Properties;
import com.gs.ntos.enums.Level;
import com.gs.ntos.logging.Logger;
import com.gs.ntos.support.ArgumentParser;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;

/**
 *
 * @author Sean Carrick &lt;sean at gs-unitedlabs dot com&gt;
 * 
 * @version 1.0
 * @since 1.0
 */
public class NTOSProgram extends DesktopProgram {
    
    
    private final Logger logger;
    private final Properties props;
    
    public NTOSProgram () {
        logger = new Logger(this, getClass().getSimpleName(), Level.ALL);
        props = new Properties(this);
    }

    @Override
    public String getProgramName() {
        return "NTOS";
    }

    @Override
    public String getProgramTitle() {
        return "Northwind Truckers' Operating System (" + getProgramName() + ")";
    }

    @Override
    public String getProgramId() {
        return "ntos";
    }

    @Override
    public String getVendorName() {
        return "GS United Labs";
    }

    @Override
    public String getVendorId() {
        return "GS";
    }

    @Override
    public String getVersion() {
        String version = props.getSystemProperty("program.major") + ".";
        version += props.getSystemProperty("program.minor") + ".";
        version += props.getSystemProperty("program.revision") + " build ";
        version += props.getSystemProperty("program.build");
        
        return version;
    }

    @Override
    public void initialize(String[] args) {
        // Override the default look and feel if the look and feel property is
        //+ set in the user properties file.
        Logger logger = new Logger(this, getClass().getSimpleName());
        String osName = System.getProperty("os.name").toLowerCase();

        String className = props.getPropertyAsString("program.lookAndFeel", null);
        
        installLookAndFeels();
        
        if (className == null) {
            if (osName.contains("win") || osName.contains("mac")) {
                className = UIManager.getSystemLookAndFeelClassName();
            } else {
                className = "com.sun.java.swing.plaf.gtk.GTKLookAndFeel";
            }
        }
        
        try {
            UIManager.setLookAndFeel(className);
        } catch (ClassNotFoundException
                | UnsupportedLookAndFeelException e) {
            // This is most likely caused by the GTK+ look and feel, so we will
            //+ just use the cross-platform look and feel of Nimbus.
            try {
                UIManager.setLookAndFeel("javax.swing.plaf.nimbus.NimbusLookAndFeel");
            } catch (ClassNotFoundException
                    | InstantiationException 
                    | IllegalAccessException 
                    | UnsupportedLookAndFeelException ex) {
                logger.log(Level.WARNING, "Unable to set look and feel to Nimbus");
            }
        } catch (InstantiationException
                | IllegalAccessException e) {
            logger.log(Level.WARNING, "Unable to set look and feel to {0}", className);
        }
        
        ArgumentParser parser = new ArgumentParser(args);
        
        if (parser.isSwitchPresent("--ide") || parser.isSwitchPresent("-i")) {
            logger.setLevel(Level.ALL);
            calculateVersion();
        } else {
            logger.setLevel(Level.INFO);
        }
    }
    
    private void installLookAndFeels() {
        UIManager.installLookAndFeel("JTattoo Acryl", "com.jtattoo.plaf.acryl.AcrylLookAndFeel");
        UIManager.installLookAndFeel("JTattoo Aero", "com.jtattoo.plaf.aero.AeroLookAndFeel");
        UIManager.installLookAndFeel("JTattoo Aluminium", "com.jtattoo.plaf.aluminium.AluminiumLookAndFeel");
        UIManager.installLookAndFeel("JTattoo Bernstein", "com.jtattoo.plaf.bernstein.BernsteinLookAndFeel");
        UIManager.installLookAndFeel("JTattoo Fast", "com.jtattoo.plaf.fast.FastLookAndFeel");
        UIManager.installLookAndFeel("JTattoo Graphite", "com.jtattoo.plaf.graphite.GraphiteLookAndFeel");
        UIManager.installLookAndFeel("JTattoo HiFi", "com.jtattoo.plaf.hifi.HiFiLookAndFeel");
        UIManager.installLookAndFeel("JTattoo Luna", "com.jtattoo.plaf.luna.LunaLookAndFeel");
        UIManager.installLookAndFeel("JTattoo McWin", "com.jtattoo.plaf.mcwin.McWinLookAndFeel");
        UIManager.installLookAndFeel("JTattoo Mint", "com.jtattoo.plaf.mint.MintLookAndFeel");
        UIManager.installLookAndFeel("JTattoo Noire", "com.jtattoo.plaf.noire.NoireLookAndFeel");
        UIManager.installLookAndFeel("JTattoo Smart", "com.jtattoo.plaf.smart.SmartLookAndFeel");
    }

    @Override
    public void startup() {
        MainFrame frame = new MainFrame(this);
        frame.setName("LoadAccountingMainFrame");
        setMainFrame(frame);
        show(getMainFrame());
    }

    @Override
    public void shutdown() {
        super.shutdown(); // Call super.shudown() in case any changes are made
        
        save(getMainFrame());
    }
    
    private void calculateVersion() {
        logger.entering("LAProgram.calculateVersion()");
        
        long build = Long.parseLong(props.getSystemProperty("program.build", 1503L).toString());
        int revision = Integer.parseInt(props.getSystemProperty("program.revision", 10).toString());
        int minor = Integer.parseInt(props.getSystemProperty("program.minor", 0).toString());
        int major = Integer.parseInt(props.getSystemProperty("program.major", 1).toString());
        logger.log(Level.TRACE, "Current program version: {0}.{1}.{2} build {3}",
                major, minor, revision, build);
        
        // The build number should continuously increment, but each time fifty
        //+ builds have been created, we should increment the revision value.
        if ((build % 50) == 0) {
            revision++;
        }
        build++;
        
        // The revision number should only run between 10 and 99. Once revision
        //+ gets to 100, we increment the minor value and reset revision to 10.
        if (revision > 99) {
            minor++;
            revision = 10;
        }
        
        // The minor number should never be higher than 9. On the tenth increase,
        //+ the major should be incremented, which will continue forever.
        if (minor > 9) {
            major++;
            minor = 0;
        }
        
        // Now that the version has been calculated, store the values in the
        //+ program properties file.
        props.setSystemProperty("program.build", build);
        props.setSystemProperty("program.revision", revision);
        props.setSystemProperty("program.minor", minor);
        props.setSystemProperty("program.major", major);
        
        logger.exiting("LAProgram.calculateVersion(): Application version: "
                + "{0}.{1}.{2} build {3}", major, minor, revision, build);
    }

}
