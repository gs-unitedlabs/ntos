/*
 * Copyright (C) 2022 GS United Labs
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * *****************************************************************************
 *  Project    :   NTOS
 *  Class      :   OptionsDialog.java
 *  Author     :   Sean Carrick
 *  Created    :   Mar 31, 2022
 *  Modified   :   Mar 31, 2022
 *  
 *  Purpose: See class JavaDoc for explanation
 *  
 *  Revision History:
 *  
 *  WHEN          BY                   REASON
 *  ------------  -------------------  -----------------------------------------
 *  Mar 31, 2022  Sean Carrick         Initial creation.
 * *****************************************************************************
 */

package com.gs.ntos;

import com.gs.ntos.api.DesktopProgram;
import com.gs.ntos.api.ExitListener;
import com.gs.ntos.api.Properties;
import com.gs.ntos.enums.Level;
import com.gs.ntos.logging.Logger;
import com.gs.ntos.support.MessageBox;
import com.gs.ntos.utils.FileUtils;
import com.gs.ntos.utils.ScreenUtils;
import com.gs.ntos.validators.EmailValidator;
import com.gs.ntos.validators.PhoneNumberValidator;
import com.gs.ntos.validators.PostalCodeValidator;
import com.gs.ntos.validators.StateOrProvinceValidator;
import java.awt.CardLayout;
import java.awt.Dimension;
import java.awt.GraphicsDevice;
import java.awt.GraphicsEnvironment;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.EventObject;
import java.util.ResourceBundle;
import javax.swing.AbstractAction;
import static javax.swing.Action.DISPLAYED_MNEMONIC_INDEX_KEY;
import static javax.swing.Action.SHORT_DESCRIPTION;
import static javax.swing.Action.SMALL_ICON;
import javax.swing.ActionMap;
import javax.swing.ImageIcon;
import javax.swing.InputMap;
import javax.swing.JFrame;
import javax.swing.KeyStroke;
import javax.swing.LookAndFeel;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import javax.swing.UIManager.LookAndFeelInfo;
import javax.swing.UnsupportedLookAndFeelException;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.text.JTextComponent;

/**
 *
 * @author Sean Carrick
 */
public class OptionsDialog extends javax.swing.JDialog
        implements ExitListener, DocumentListener, PropertyChangeListener {
    
    private final OptionsDialog instance;
    private final DesktopProgram program;
    private final Logger logger;
    private final ResourceBundle bundle;
    
    private String lafName;
    private int currentPanel;
    private boolean dirty;
    private boolean loading;
    private boolean zipIsValid;
    private boolean stateIsValid;
    private boolean emailIsValid;
    private boolean phoneIsValid;
    private boolean faxIsValid;

    /** Creates new form OptionsDialog */
    public OptionsDialog(DesktopProgram program, java.awt.Frame parent) {
        super(parent, false);
        logger = new Logger(program, getClass().getSimpleName(), Level.ALL);
        this.program = program;
        bundle = ResourceBundle.getBundle("com/gs/ntos/resources/Bundle");
        
        saveAction = new SaveAction();
        applyAction = new ApplyAction();
        cancelAction = new CancelAction();
        helpAction = new HelpAction();
        
        currentPanel = 0;
        
        loading = true;
        dirty = false;
        stateIsValid = true;
        zipIsValid = true;
        emailIsValid = true;
        phoneIsValid = true;
        faxIsValid = true;
        
        initComponents();
        
        instance = this;
        loading = false;
    }
    
    public boolean isSavable() {
        return dirty && stateIsValid && zipIsValid && emailIsValid
                && phoneIsValid && faxIsValid;
    }
    
    public void setSavable(boolean savable) {
        boolean oldValue = isSavable();
        this.dirty = savable;
        
        program.firePropertyChange("savable", oldValue, isSavable());
    }

    @Override
    public boolean canExit(EventObject event) {
        if (dirty) {
            int choice = MessageBox.askQuestion("You have unsaved changes.\n"
                    + "Close anyway?", "Confirm Close", false);
            
            if (choice == MessageBox.NO_OPTION) {
                return false;
            }
        }
        return true;
    }

    @Override
    public void willExit(EventObject event) {
        WindowEvent e = new WindowEvent(instance, event.hashCode());
        for (WindowListener l : getWindowListeners()) {
            if (l instanceof OptionsDialogAdapter) {
                l.windowClosing(e);
            }
        }
    }

    @Override
    public void insertUpdate(DocumentEvent e) {
        changedUpdate(e);
    }

    @Override
    public void removeUpdate(DocumentEvent e) {
        changedUpdate(e);
    }

    @Override
    public void changedUpdate(DocumentEvent e) {
        if (!loading) {
            setSavable(true);
        }
        
        String owner = e.getDocument().getProperty("owner").toString();
        switch (owner) {
            case "stateField":
                if (stateField.getText() != null
                        && !stateField.getText().isBlank()
                        && !stateField.getText().isEmpty()) {
                    StateOrProvinceValidator validator = new StateOrProvinceValidator();
                    stateIsValid = validator.validate(stateField.getText());
                }
                break;
            case "zipField":
                if (zipField.getText() != null
                        && !zipField.getText().isBlank()
                        && !zipField.getText().isEmpty()) {
                    PostalCodeValidator validator = new PostalCodeValidator();
                    zipIsValid = validator.validate(zipField.getText());
                }
                break;
            case "emailField":
                if (emailField.getText() != null
                        && !emailField.getText().isBlank()
                        && !emailField.getText().isEmpty()) {
                    EmailValidator validator = new EmailValidator();
                    emailIsValid = validator.validate(emailField.getText());
                }
                break;
            case "phoneField":
                if (phoneField.getText() != null
                        && !phoneField.getText().equals("(   )    -    ")
                        && !phoneField.getText().equals("(   )    -    ")) {
                    PhoneNumberValidator validator = new PhoneNumberValidator();
                    phoneIsValid = validator.validate(phoneField.getText());
                }
                break;
            case "faxField":
                if (faxField.getText() != null
                        && !faxField.getText().equals("(   )    -    ")
                        && !faxField.getText().equals("(   )    -    ")) {
                    PhoneNumberValidator validator = new PhoneNumberValidator();
                    faxIsValid = validator.validate(faxField.getText());
                    break;
                }
        }
    }

    @Override
    public void propertyChange(PropertyChangeEvent evt) {
        if ("savable".equals(evt.getPropertyName())) {
            saveAction.setEnabled((Boolean) evt.getNewValue());
            applyAction.setEnabled((Boolean) evt.getNewValue());
//            saveButton.setEnabled(dirty);
//            applyButton.setEnabled((Boolean) evt.getNewValue());
        }
    }

    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        categoriesButtonGroup = new javax.swing.ButtonGroup();
        buttonGroup1 = new javax.swing.ButtonGroup();
        bottomPanel = new javax.swing.JPanel();
        jSeparator1 = new javax.swing.JSeparator();
        commandPanel = new javax.swing.JPanel();
        jButton2 = new javax.swing.JButton();
        jButton3 = new javax.swing.JButton();
        jButton4 = new javax.swing.JButton();
        jButton5 = new javax.swing.JButton();
        categoriesBar = new javax.swing.JToolBar();
        generalButton = new javax.swing.JToggleButton();
        accountingButton = new javax.swing.JToggleButton();
        miscellaneousButton = new javax.swing.JToggleButton();
        contentPanel = new javax.swing.JPanel();
        generalTabs = new javax.swing.JTabbedPane();
        companyInfoTab = new javax.swing.JPanel();
        companyLabel = new javax.swing.JLabel();
        companyField = new javax.swing.JTextField();
        streetLabel = new javax.swing.JLabel();
        streetField = new javax.swing.JTextField();
        suiteLabel = new javax.swing.JLabel();
        suiteField = new javax.swing.JTextField();
        cityLabel = new javax.swing.JLabel();
        cityField = new javax.swing.JTextField();
        stateLabel = new javax.swing.JLabel();
        stateField = new javax.swing.JTextField();
        zipLabel = new javax.swing.JLabel();
        zipField = new javax.swing.JTextField();
        contactLabel = new javax.swing.JLabel();
        contactField = new javax.swing.JTextField();
        emailLabel = new javax.swing.JLabel();
        emailField = new javax.swing.JTextField();
        phoneLabel = new javax.swing.JLabel();
        phoneField = new javax.swing.JFormattedTextField();
        faxLabel = new javax.swing.JLabel();
        faxField = new javax.swing.JFormattedTextField();
        authorityCheckbox = new javax.swing.JCheckBox();
        dotPanel = new javax.swing.JPanel();
        dotNumberField = new javax.swing.JTextField();
        dotNumberLabel = new javax.swing.JLabel();
        companyHintLabel = new javax.swing.JLabel();
        accountingTabs = new javax.swing.JTabbedPane();
        miscellaneousTabs = new javax.swing.JTabbedPane();
        lafTab = new javax.swing.JPanel();
        lafLabel = new javax.swing.JLabel();
        lafCombobox = new javax.swing.JComboBox<>();
        jSeparator2 = new javax.swing.JSeparator();
        previewDescriptionPanel = new javax.swing.JPanel();
        previewPanel = new javax.swing.JPanel();
        label1 = new javax.swing.JLabel();
        jTextField1 = new javax.swing.JTextField();
        jButton1 = new javax.swing.JButton();
        jToggleButton1 = new javax.swing.JToggleButton();
        jToggleButton2 = new javax.swing.JToggleButton();
        jCheckBox1 = new javax.swing.JCheckBox();
        jCheckBox2 = new javax.swing.JCheckBox();
        jRadioButton1 = new javax.swing.JRadioButton();
        jRadioButton2 = new javax.swing.JRadioButton();
        jComboBox1 = new javax.swing.JComboBox<>();
        jScrollPane1 = new javax.swing.JScrollPane();
        jList1 = new javax.swing.JList<>();
        jScrollPane2 = new javax.swing.JScrollPane();
        jTextArea1 = new javax.swing.JTextArea();
        jSlider1 = new javax.swing.JSlider();
        jProgressBar1 = new javax.swing.JProgressBar();
        jPasswordField1 = new javax.swing.JPasswordField();
        jLabel1 = new javax.swing.JLabel();
        jSpinner1 = new javax.swing.JSpinner();
        jScrollPane3 = new javax.swing.JScrollPane();
        jTable1 = new javax.swing.JTable();
        jScrollPane4 = new javax.swing.JScrollPane();
        jTree1 = new javax.swing.JTree();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setName("OptionsDialog"); // NOI18N

        bottomPanel.setName("bottomPanel"); // NOI18N
        bottomPanel.setLayout(new java.awt.BorderLayout());

        jSeparator1.setName("jSeparator1"); // NOI18N
        bottomPanel.add(jSeparator1, java.awt.BorderLayout.PAGE_START);

        commandPanel.setName("commandPanel"); // NOI18N

        jButton2.setAction(helpAction);
        jButton2.setName("jButton2"); // NOI18N

        jButton3.setAction(saveAction);
        jButton3.setName("jButton3"); // NOI18N

        jButton4.setAction(applyAction);
        jButton4.setName("jButton4"); // NOI18N

        jButton5.setAction(cancelAction);
        jButton5.setName("jButton5"); // NOI18N

        javax.swing.GroupLayout commandPanelLayout = new javax.swing.GroupLayout(commandPanel);
        commandPanel.setLayout(commandPanelLayout);
        commandPanelLayout.setHorizontalGroup(
            commandPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(commandPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jButton2, javax.swing.GroupLayout.PREFERRED_SIZE, 80, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 395, Short.MAX_VALUE)
                .addComponent(jButton3, javax.swing.GroupLayout.PREFERRED_SIZE, 80, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jButton4, javax.swing.GroupLayout.PREFERRED_SIZE, 80, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jButton5, javax.swing.GroupLayout.PREFERRED_SIZE, 106, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        commandPanelLayout.setVerticalGroup(
            commandPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, commandPanelLayout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(commandPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jButton2)
                    .addComponent(jButton3)
                    .addComponent(jButton5)
                    .addComponent(jButton4))
                .addContainerGap())
        );

        bottomPanel.add(commandPanel, java.awt.BorderLayout.CENTER);

        getContentPane().add(bottomPanel, java.awt.BorderLayout.PAGE_END);

        categoriesBar.setFloatable(false);
        categoriesBar.setRollover(true);
        categoriesBar.setName("categoriesBar"); // NOI18N

        generalButton.setBackground(javax.swing.UIManager.getDefaults().getColor("Button.shadow"));
        categoriesButtonGroup.add(generalButton);
        generalButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/gs/ntos/resources/icons/OptionsToolbar/applications-accessories.png"))); // NOI18N
        generalButton.setMnemonic(bundle.getString("OptionsDialog.generalButton.mnemonic").charAt(0));
        generalButton.setSelected(true);
        generalButton.setText(bundle.getString("OptionsDialog.generalButton.text")); // NOI18N
        generalButton.setFocusable(false);
        generalButton.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        generalButton.setName("generalButton"); // NOI18N
        generalButton.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        generalButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                generalButtonActionPerformed(evt);
            }
        });
        categoriesBar.add(generalButton);

        categoriesButtonGroup.add(accountingButton);
        accountingButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/gs/ntos/resources/icons/OptionsToolbar/applications-mathematics.png"))); // NOI18N
        accountingButton.setMnemonic(bundle.getString("OptionsDialog.accountingButton.mnemonic").charAt(0));
        accountingButton.setText(bundle.getString("OptionsDialog.accountingButton.text")); // NOI18N
        accountingButton.setFocusable(false);
        accountingButton.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        accountingButton.setName("accountingButton"); // NOI18N
        accountingButton.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        accountingButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                accountingButtonActionPerformed(evt);
            }
        });
        categoriesBar.add(accountingButton);

        categoriesButtonGroup.add(miscellaneousButton);
        miscellaneousButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/gs/ntos/resources/icons/OptionsToolbar/applications-internet.png"))); // NOI18N
        miscellaneousButton.setMnemonic(bundle.getString("OptionsDialog.miscellaneousButton.mnemonic").charAt(0));
        miscellaneousButton.setText(bundle.getString("OptionsDialog.miscellaneousButton.text")); // NOI18N
        miscellaneousButton.setFocusable(false);
        miscellaneousButton.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        miscellaneousButton.setName("miscellaneousButton"); // NOI18N
        miscellaneousButton.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        miscellaneousButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                miscellaneousButtonActionPerformed(evt);
            }
        });
        categoriesBar.add(miscellaneousButton);

        getContentPane().add(categoriesBar, java.awt.BorderLayout.PAGE_START);

        contentPanel.setName("contentPanel"); // NOI18N
        contentPanel.setLayout(new java.awt.CardLayout());

        generalTabs.setName("generalTabs"); // NOI18N

        companyInfoTab.setName("companyInfoTab"); // NOI18N

        companyLabel.setDisplayedMnemonic(bundle.getString("OptionsDialog.companyLabel.mnemonic").charAt(0));
        companyLabel.setLabelFor(companyField);
        companyLabel.setText(bundle.getString("OptionsDialog.companyLabel.text")); // NOI18N
        companyLabel.setName("companyLabel"); // NOI18N

        companyField.setText(program.getProperties().getSystemProperty("company.name") == null ? "" : program.getProperties().getSystemProperty("company.name").toString());
        companyField.setToolTipText(bundle.getString("OptionsDialog.companyField.toolTipText")); // NOI18N
        companyField.setName("companyField"); // NOI18N
        companyField.getDocument().putProperty("owner", companyField.getName());
        companyField.getDocument().addDocumentListener(this);
        companyField.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                selectText(evt);
            }
        });

        streetLabel.setDisplayedMnemonic(bundle.getString("OptionsDialog.streetLabel.mnemonic").charAt(0));
        streetLabel.setLabelFor(streetField);
        streetLabel.setText(bundle.getString("OptionsDialog.streetLabel.text")); // NOI18N
        streetLabel.setName("streetLabel"); // NOI18N

        streetField.setText(program.getProperties().getSystemProperty("company.street") == null ? "" : program.getProperties().getSystemProperty("company.street").toString());
        streetField.setToolTipText(bundle.getString("OptionsDialog.streetField.toolTipText")); // NOI18N
        streetField.setName("streetField"); // NOI18N
        streetField.getDocument().putProperty("owner", streetField.getName());
        streetField.getDocument().addDocumentListener(this);
        streetField.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                selectText(evt);
            }
        });

        suiteLabel.setDisplayedMnemonic(bundle.getString("OptionsDialog.suiteLabel.mnemonic").charAt(0));
        suiteLabel.setLabelFor(suiteField);
        suiteLabel.setText(bundle.getString("OptionsDialog.suiteLabel.text")); // NOI18N
        suiteLabel.setName("suiteLabel"); // NOI18N

        suiteField.setText(program.getProperties().getSystemProperty("company.suite") == null ? "" : program.getProperties().getSystemProperty("company.suite").toString());
        suiteField.setToolTipText(bundle.getString("OptionsDialog.suiteField.toolTipText")); // NOI18N
        suiteField.setName("suiteField"); // NOI18N
        suiteField.getDocument().putProperty("owner", suiteField.getName());
        suiteField.getDocument().addDocumentListener(this);
        suiteField.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                selectText(evt);
            }
        });

        cityLabel.setDisplayedMnemonic(bundle.getString("OptionsDialog.cityLabel.mnemonic").charAt(0));
        cityLabel.setLabelFor(cityField);
        cityLabel.setText(bundle.getString("OptionsDialog.cityLabel.text")); // NOI18N
        cityLabel.setName("cityLabel"); // NOI18N

        cityField.setText(program.getProperties().getSystemProperty("company.city") == null ? "" : program.getProperties().getSystemProperty("company.city").toString());
        cityField.setToolTipText(bundle.getString("OptionsDialog.cityField.toolTipText")); // NOI18N
        cityField.setName("cityField"); // NOI18N
        cityField.getDocument().putProperty("owner", cityField.getName());
        cityField.getDocument().addDocumentListener(this);
        cityField.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                selectText(evt);
            }
        });

        stateLabel.setDisplayedMnemonic(bundle.getString("OptionsDialog.stateLabel.mnemonic").charAt(0));
        stateLabel.setLabelFor(stateField);
        stateLabel.setText(bundle.getString("OptionsDialog.stateLabel.text")); // NOI18N
        stateLabel.setName("stateLabel"); // NOI18N

        stateField.setText(program.getProperties().getSystemProperty("company.state") == null ? "" : program.getProperties().getSystemProperty("company.state").toString());
        stateField.setToolTipText(bundle.getString("OptionsDialog.stateField.toolTipText")); // NOI18N
        stateField.setName("stateField"); // NOI18N
        stateField.getDocument().putProperty("owner", stateField.getName());
        stateField.getDocument().addDocumentListener(this);
        stateField.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                selectText(evt);
            }
        });

        zipLabel.setDisplayedMnemonic(bundle.getString("OptionsDialog.zipLabel.mnemonic").charAt(0));
        zipLabel.setLabelFor(zipField);
        zipLabel.setText(bundle.getString("OptionsDialog.zipLabel.text")); // NOI18N
        zipLabel.setName("zipLabel"); // NOI18N

        zipField.setText(program.getProperties().getSystemProperty("company.zip") == null ? "" : program.getProperties().getSystemProperty("company.zip").toString());
        zipField.setToolTipText(bundle.getString("OptionsDialog.zipField.toolTipText")); // NOI18N
        zipField.setName("zipField"); // NOI18N
        zipField.getDocument().putProperty("owner", zipField.getName());
        zipField.getDocument().addDocumentListener(this);
        zipField.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                selectText(evt);
            }
        });

        contactLabel.setDisplayedMnemonic(bundle.getString("OptionsDialog.contactLabel.mnemonic").charAt(0));
        contactLabel.setLabelFor(contactField);
        contactLabel.setText(bundle.getString("OptionsDialog.contactLabel.text")); // NOI18N
        contactLabel.setName("contactLabel"); // NOI18N

        contactField.setText(program.getProperties().getSystemProperty("company.contact") == null ? "" : program.getProperties().getSystemProperty("company.contact").toString());
        contactField.setToolTipText(bundle.getString("OptionsDialog.contactField.toolTipText")); // NOI18N
        contactField.setName("contactField"); // NOI18N
        contactField.getDocument().putProperty("owner", contactField.getName());
        contactField.getDocument().addDocumentListener(this);
        contactField.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                selectText(evt);
            }
        });

        emailLabel.setDisplayedMnemonic(bundle.getString("OptionsDialog.emailLabel.mnemonic").charAt(0));
        emailLabel.setLabelFor(emailField);
        emailLabel.setText(bundle.getString("OptionsDialog.emailLabel.text")); // NOI18N
        emailLabel.setName("emailLabel"); // NOI18N

        emailField.setText(program.getProperties().getSystemProperty("company.email") == null ? "" : program.getProperties().getSystemProperty("company.email").toString());
        emailField.setToolTipText(bundle.getString("OptionsDialog.emailField.toolTipText")); // NOI18N
        emailField.setName("emailField"); // NOI18N
        emailField.getDocument().putProperty("owner", emailField.getName());
        emailField.getDocument().addDocumentListener(this);
        emailField.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                selectText(evt);
            }
        });

        phoneLabel.setDisplayedMnemonic(bundle.getString("OptionsDialog.phoneLabel.mnemonic").charAt(0));
        phoneLabel.setLabelFor(phoneField);
        phoneLabel.setText(bundle.getString("OptionsDialog.phoneLabel.text")); // NOI18N
        phoneLabel.setName("phoneLabel"); // NOI18N

        try {
            phoneField.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.MaskFormatter("(###) ###-####")));
        } catch (java.text.ParseException ex) {
            ex.printStackTrace();
        }
        phoneField.setText(program.getProperties().getSystemProperty("company.phone") == null ? "" : program.getProperties().getSystemProperty("company.phone").toString());
        phoneField.setToolTipText(bundle.getString("OptionsDialog.phoneField.toolTipText")); // NOI18N
        phoneField.setName("phoneField"); // NOI18N
        phoneField.getDocument().putProperty("owner", phoneField.getName());
        phoneField.getDocument().addDocumentListener(this);
        phoneField.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                selectText(evt);
            }
        });

        faxLabel.setDisplayedMnemonic(bundle.getString("OptionsDialog.faxLabel.mnemonic").charAt(0));
        faxLabel.setLabelFor(faxField);
        faxLabel.setText(bundle.getString("OptionsDialog.faxLabel.text")); // NOI18N
        faxLabel.setName("faxLabel"); // NOI18N

        try {
            faxField.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.MaskFormatter("(###) ###-####")));
        } catch (java.text.ParseException ex) {
            ex.printStackTrace();
        }
        faxField.setText(program.getProperties().getSystemProperty("company.fax") == null ? "" : program.getProperties().getSystemProperty("company.fax").toString());
        faxField.setToolTipText(bundle.getString("OptionsDialog.faxField.toolTipText")); // NOI18N
        faxField.setName("faxField"); // NOI18N
        faxField.getDocument().putProperty("owner", faxField.getName());
        faxField.getDocument().addDocumentListener(this);
        faxField.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                selectText(evt);
            }
        });

        authorityCheckbox.setMnemonic(bundle.getString("OptionsDialog.authorityCheckbox.mnemonic").charAt(0));
        authorityCheckbox.setSelected(program.getProperties().getSystemProperty("company.name") == null ? false : Boolean.parseBoolean(program.getProperties().getSystemProperty("company.name").toString()));
        authorityCheckbox.setText(bundle.getString("OptionsDialog.authorityCheckbox.text")); // NOI18N
        authorityCheckbox.setToolTipText(bundle.getString("OptionsDialog.authorityCheckbox.toolTipText")); // NOI18N
        authorityCheckbox.setName("authorityCheckbox"); // NOI18N
        authorityCheckbox.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                authorityCheckboxItemStateChanged(evt);
            }
        });
        authorityCheckbox.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                authorityCheckboxFocusGained(evt);
            }
        });

        dotPanel.setName("dotPanel"); // NOI18N

        dotNumberField.setText(program.getProperties().getSystemProperty("company.dotNumber") == null ? "" : program.getProperties().getSystemProperty("company.dotNumber").toString());
        dotNumberField.setToolTipText(bundle.getString("OptionsDialog.dotNumberField.toolTipText")); // NOI18N
        dotNumberField.setName("dotNumberField"); // NOI18N
        dotNumberField.getDocument().putProperty("owner", dotNumberField.getName());
        dotNumberField.getDocument().addDocumentListener(this);
        dotNumberField.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                selectText(evt);
            }
        });

        dotNumberLabel.setDisplayedMnemonic(bundle.getString("OptionsDialog.dotNumberLabel.mnemonic").charAt(0));
        dotNumberLabel.setLabelFor(dotNumberField);
        dotNumberLabel.setText(bundle.getString("OptionsDialog.dotNumberLabel.text")); // NOI18N
        dotNumberLabel.setName("dotNumberLabel"); // NOI18N

        javax.swing.GroupLayout dotPanelLayout = new javax.swing.GroupLayout(dotPanel);
        dotPanel.setLayout(dotPanelLayout);
        dotPanelLayout.setHorizontalGroup(
            dotPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, dotPanelLayout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(dotNumberLabel)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(dotNumberField, javax.swing.GroupLayout.PREFERRED_SIZE, 97, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        dotPanelLayout.setVerticalGroup(
            dotPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(dotPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(dotPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(dotNumberField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(dotNumberLabel))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        companyHintLabel.setFont(new java.awt.Font("Noto Sans", 0, 13)); // NOI18N
        companyHintLabel.setForeground(new java.awt.Color(0, 0, 255));
        companyHintLabel.setText(bundle.getString("OptionsDialog.companyHintLabel.text")); // NOI18N
        companyHintLabel.setName("companyHintLabel"); // NOI18N

        javax.swing.GroupLayout companyInfoTabLayout = new javax.swing.GroupLayout(companyInfoTab);
        companyInfoTab.setLayout(companyInfoTabLayout);
        companyInfoTabLayout.setHorizontalGroup(
            companyInfoTabLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(companyInfoTabLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(companyInfoTabLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(companyInfoTabLayout.createSequentialGroup()
                        .addGroup(companyInfoTabLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(phoneLabel)
                            .addComponent(contactLabel)
                            .addComponent(cityLabel)
                            .addComponent(streetLabel)
                            .addComponent(companyLabel))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(companyInfoTabLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(companyField)
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, companyInfoTabLayout.createSequentialGroup()
                                .addGroup(companyInfoTabLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addGroup(companyInfoTabLayout.createSequentialGroup()
                                        .addComponent(cityField)
                                        .addGap(18, 18, 18)
                                        .addComponent(stateLabel)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(stateField, javax.swing.GroupLayout.PREFERRED_SIZE, 37, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(18, 18, 18)
                                        .addComponent(zipLabel))
                                    .addGroup(companyInfoTabLayout.createSequentialGroup()
                                        .addComponent(streetField)
                                        .addGap(18, 18, 18)
                                        .addComponent(suiteLabel)))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(companyInfoTabLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(suiteField, javax.swing.GroupLayout.DEFAULT_SIZE, 90, Short.MAX_VALUE)
                                    .addComponent(zipField, javax.swing.GroupLayout.PREFERRED_SIZE, 1, Short.MAX_VALUE)))
                            .addGroup(companyInfoTabLayout.createSequentialGroup()
                                .addComponent(contactField, javax.swing.GroupLayout.PREFERRED_SIZE, 228, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(emailLabel)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(emailField))
                            .addGroup(companyInfoTabLayout.createSequentialGroup()
                                .addComponent(phoneField, javax.swing.GroupLayout.PREFERRED_SIZE, 134, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(faxLabel)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(faxField, javax.swing.GroupLayout.PREFERRED_SIZE, 134, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 205, Short.MAX_VALUE)
                                .addComponent(authorityCheckbox))))
                    .addComponent(companyHintLabel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, companyInfoTabLayout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(dotPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap())
        );
        companyInfoTabLayout.setVerticalGroup(
            companyInfoTabLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(companyInfoTabLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(companyInfoTabLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(companyLabel)
                    .addComponent(companyField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(companyInfoTabLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(streetLabel)
                    .addComponent(streetField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(suiteLabel)
                    .addComponent(suiteField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(companyInfoTabLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cityLabel)
                    .addComponent(cityField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(stateLabel)
                    .addComponent(stateField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(zipLabel)
                    .addComponent(zipField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(companyInfoTabLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(contactLabel)
                    .addComponent(contactField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(emailLabel)
                    .addComponent(emailField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(companyInfoTabLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(phoneLabel)
                    .addComponent(phoneField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(faxLabel)
                    .addComponent(faxField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(authorityCheckbox))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(dotPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(companyHintLabel)
                .addContainerGap())
        );

        dotPanel.setVisible(authorityCheckbox.isSelected());
        companyHintLabel.setVisible(false);

        generalTabs.addTab(bundle.getString("OptionsDialog.companyInfoTab.TabConstraints.tabTitle"), companyInfoTab); // NOI18N

        contentPanel.add(generalTabs, "general");

        accountingTabs.setName("accountingTabs"); // NOI18N
        contentPanel.add(accountingTabs, "accounting");

        miscellaneousTabs.setName("miscellaneousTabs"); // NOI18N
        miscellaneousTabs.addComponentListener(new java.awt.event.ComponentAdapter() {
            public void componentShown(java.awt.event.ComponentEvent evt) {
                miscellaneousTabsComponentShown(evt);
            }
        });

        lafTab.setName("lafTab"); // NOI18N

        lafLabel.setText(bundle.getString("OptionsDialog.lafLabel.text")); // NOI18N
        lafLabel.setName("lafLabel"); // NOI18N

        lafCombobox.setName("lafCombobox"); // NOI18N
        lafCombobox.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                lafComboboxItemStateChanged(evt);
            }
        });

        jSeparator2.setName("jSeparator2"); // NOI18N

        previewDescriptionPanel.setBorder(javax.swing.BorderFactory.createTitledBorder(bundle.getString("OptionsDialog.previewDescriptionPanel.border.title"))); // NOI18N
        previewDescriptionPanel.setName("previewDescriptionPanel"); // NOI18N

        previewPanel.setName("previewPanel"); // NOI18N

        label1.setText(bundle.getString("OptionsDialog.label1.text")); // NOI18N
        label1.setName("label1"); // NOI18N

        jTextField1.setText(bundle.getString("OptionsDialog.jTextField1.text")); // NOI18N
        jTextField1.setName("jTextField1"); // NOI18N

        jButton1.setText(bundle.getString("OptionsDialog.jButton1.text_1")); // NOI18N
        jButton1.setName("jButton1"); // NOI18N

        buttonGroup1.add(jToggleButton1);
        jToggleButton1.setSelected(true);
        jToggleButton1.setText(bundle.getString("OptionsDialog.jToggleButton1.text")); // NOI18N
        jToggleButton1.setName("jToggleButton1"); // NOI18N

        buttonGroup1.add(jToggleButton2);
        jToggleButton2.setText(bundle.getString("OptionsDialog.jToggleButton2.text")); // NOI18N
        jToggleButton2.setName("jToggleButton2"); // NOI18N

        jCheckBox1.setText(bundle.getString("OptionsDialog.jCheckBox1.text")); // NOI18N
        jCheckBox1.setName("jCheckBox1"); // NOI18N

        jCheckBox2.setText(bundle.getString("OptionsDialog.jCheckBox2.text")); // NOI18N
        jCheckBox2.setName("jCheckBox2"); // NOI18N

        jRadioButton1.setText(bundle.getString("OptionsDialog.jRadioButton1.text")); // NOI18N
        jRadioButton1.setName("jRadioButton1"); // NOI18N

        jRadioButton2.setText(bundle.getString("OptionsDialog.jRadioButton2.text")); // NOI18N
        jRadioButton2.setName("jRadioButton2"); // NOI18N

        jComboBox1.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Fruit", "Sports", "Computers" }));
        jComboBox1.setName("jComboBox1"); // NOI18N

        jScrollPane1.setName("jScrollPane1"); // NOI18N

        jList1.setModel(new javax.swing.AbstractListModel<String>() {
            String[] strings = { "Apple", "Banana", "Cherry", "Date", "Elderberry", "Fig", "Grape", "Jackfruit", "Kiwi", "Lime", "Mango", "Nectarine", "Orange", "Pineapple", "Quince", "Raspberry", "Strawberry", "Tangarine", "Ugni", "Voavanga", "Watermelon", "Xigua (Chinese Watermelon)", "Yangmei", "Zucchini" };
            public int getSize() { return strings.length; }
            public String getElementAt(int i) { return strings[i]; }
        });
        jList1.setName("jList1"); // NOI18N
        jScrollPane1.setViewportView(jList1);

        jScrollPane2.setName("jScrollPane2"); // NOI18N

        jTextArea1.setColumns(20);
        jTextArea1.setLineWrap(true);
        jTextArea1.setRows(5);
        jTextArea1.setText(bundle.getString("OptionsDialog.jTextArea1.text")); // NOI18N
        jTextArea1.setWrapStyleWord(true);
        jTextArea1.setName("jTextArea1"); // NOI18N
        jScrollPane2.setViewportView(jTextArea1);

        jSlider1.setName("jSlider1"); // NOI18N
        jSlider1.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                jSlider1StateChanged(evt);
            }
        });

        jProgressBar1.setValue(50);
        jProgressBar1.setName("jProgressBar1"); // NOI18N

        jPasswordField1.setText(bundle.getString("OptionsDialog.jPasswordField1.text")); // NOI18N
        jPasswordField1.setName("jPasswordField1"); // NOI18N

        jLabel1.setText(bundle.getString("OptionsDialog.jLabel1.text")); // NOI18N
        jLabel1.setName("jLabel1"); // NOI18N

        jSpinner1.setModel(new javax.swing.SpinnerNumberModel(0, 0, 100, 1));
        jSpinner1.setName("jSpinner1"); // NOI18N
        jSpinner1.setValue(50);
        jSpinner1.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                jSpinner1StateChanged(evt);
            }
        });

        jScrollPane3.setName("jScrollPane3"); // NOI18N

        jTable1.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jTable1.setName("jTable1"); // NOI18N
        jScrollPane3.setViewportView(jTable1);

        jScrollPane4.setName("jScrollPane4"); // NOI18N

        jTree1.setName("jTree1"); // NOI18N
        jScrollPane4.setViewportView(jTree1);

        javax.swing.GroupLayout previewPanelLayout = new javax.swing.GroupLayout(previewPanel);
        previewPanel.setLayout(previewPanelLayout);
        previewPanelLayout.setHorizontalGroup(
            previewPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(previewPanelLayout.createSequentialGroup()
                .addGap(18, 18, 18)
                .addComponent(label1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(previewPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(previewPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                        .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 256, Short.MAX_VALUE)
                        .addComponent(jTextField1, javax.swing.GroupLayout.Alignment.TRAILING)
                        .addGroup(previewPanelLayout.createSequentialGroup()
                            .addGroup(previewPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                .addComponent(jCheckBox1)
                                .addComponent(jCheckBox2))
                            .addGap(18, 18, 18)
                            .addGroup(previewPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addComponent(jRadioButton1)
                                .addComponent(jRadioButton2, javax.swing.GroupLayout.PREFERRED_SIZE, 123, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addComponent(jComboBox1, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGroup(previewPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(previewPanelLayout.createSequentialGroup()
                        .addGap(49, 49, 49)
                        .addComponent(jProgressBar1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(previewPanelLayout.createSequentialGroup()
                        .addGap(18, 18, 18)
                        .addGroup(previewPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, 312, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(previewPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                .addGroup(previewPanelLayout.createSequentialGroup()
                                    .addGroup(previewPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                        .addGroup(previewPanelLayout.createSequentialGroup()
                                            .addComponent(jButton1, javax.swing.GroupLayout.PREFERRED_SIZE, 76, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addGap(18, 18, 18)
                                            .addComponent(jToggleButton1))
                                        .addComponent(jSlider1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                    .addGroup(previewPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addComponent(jToggleButton2)
                                        .addComponent(jSpinner1, javax.swing.GroupLayout.PREFERRED_SIZE, 57, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                .addGroup(previewPanelLayout.createSequentialGroup()
                                    .addComponent(jLabel1)
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                    .addComponent(jPasswordField1))))))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane4, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE))
        );
        previewPanelLayout.setVerticalGroup(
            previewPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(previewPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(previewPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(previewPanelLayout.createSequentialGroup()
                        .addGroup(previewPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addGroup(previewPanelLayout.createSequentialGroup()
                                .addGroup(previewPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(label1)
                                    .addComponent(jTextField1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(jButton1)
                                    .addComponent(jToggleButton1)
                                    .addComponent(jToggleButton2))
                                .addGroup(previewPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(previewPanelLayout.createSequentialGroup()
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addGroup(previewPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                            .addComponent(jCheckBox1)
                                            .addComponent(jRadioButton1))
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addGroup(previewPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                            .addComponent(jRadioButton2)
                                            .addComponent(jCheckBox2)))
                                    .addGroup(previewPanelLayout.createSequentialGroup()
                                        .addGap(19, 19, 19)
                                        .addComponent(jSpinner1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))))
                            .addComponent(jSlider1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(previewPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jComboBox1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jProgressBar1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(previewPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(previewPanelLayout.createSequentialGroup()
                                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 37, Short.MAX_VALUE))
                            .addGroup(previewPanelLayout.createSequentialGroup()
                                .addGroup(previewPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(jLabel1)
                                    .addComponent(jPasswordField1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE))))
                    .addComponent(jScrollPane4, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE))
                .addContainerGap())
        );

        javax.swing.GroupLayout previewDescriptionPanelLayout = new javax.swing.GroupLayout(previewDescriptionPanel);
        previewDescriptionPanel.setLayout(previewDescriptionPanelLayout);
        previewDescriptionPanelLayout.setHorizontalGroup(
            previewDescriptionPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(previewDescriptionPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(previewPanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );
        previewDescriptionPanelLayout.setVerticalGroup(
            previewDescriptionPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(previewDescriptionPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(previewPanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );

        javax.swing.GroupLayout lafTabLayout = new javax.swing.GroupLayout(lafTab);
        lafTab.setLayout(lafTabLayout);
        lafTabLayout.setHorizontalGroup(
            lafTabLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(lafTabLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(lafTabLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jSeparator2)
                    .addGroup(lafTabLayout.createSequentialGroup()
                        .addComponent(lafLabel)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(lafCombobox, javax.swing.GroupLayout.PREFERRED_SIZE, 221, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addComponent(previewDescriptionPanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        lafTabLayout.setVerticalGroup(
            lafTabLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(lafTabLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(lafTabLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lafLabel)
                    .addComponent(lafCombobox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jSeparator2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(previewDescriptionPanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );

        lafCombobox.removeAllItems();

        for (LookAndFeelInfo i : UIManager.getInstalledLookAndFeels()) {
            lafCombobox.addItem(i.getName());
        }

        LookAndFeel laf = UIManager.getLookAndFeel();
        String lafName = laf.getDescription();
        for (LookAndFeelInfo i : UIManager.getInstalledLookAndFeels()) {
            if (UIManager.getLookAndFeel().getClass().getCanonicalName()
                .contains(i.getClassName())) {
                lafName = i.getName();
                break;
            }
        }

        for (int x = 0; x < lafCombobox.getItemCount(); x++) {
            if (lafName.equals(lafCombobox.getItemAt(x))) {
                lafCombobox.setSelectedIndex(x);
                break;
            }
        }

        miscellaneousTabs.addTab(bundle.getString("OptionsDialog.lafTab.TabConstraints.tabTitle"), lafTab); // NOI18N

        contentPanel.add(miscellaneousTabs, "misc");
        program.addPropertyChangeListener("savable", this);
        addWindowListener(new OptionsDialogAdapter());
        setTitle(bundle.getString("OptionsDialog.title"));
        setIconImage(new ImageIcon(getClass().getResource(bundle.getString("OptionsDialog.icon"))).getImage());
        //getRootPane().setDefaultButton(applyButton);

        getContentPane().add(contentPanel, java.awt.BorderLayout.CENTER);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void selectText(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_selectText
        if (evt.getSource() instanceof JTextComponent) {
            JTextComponent txt = (JTextComponent) evt.getSource();
            
            if (!txt.getName().equals("notesField")) {
                txt.selectAll();
            } else {
                txt.select(txt.getText().length(), txt.getText().length());
            }
            
            
            switch (txt.getName()) {
                case "companyField":
                    companyHintLabel.setText(bundle.getString("OptionsDialog.companyField.toolTipText"));
                    companyHintLabel.setVisible(true);
                    break;
                case "streetField":
                    companyHintLabel.setText(bundle.getString("OptionsDialog.streetField.toolTipText"));
                    companyHintLabel.setVisible(true);
                    break;
                case "suiteField":
                    companyHintLabel.setText(bundle.getString("OptionsDialog.suiteField.toolTipText"));
                    companyHintLabel.setVisible(true);
                    break;
                case "cityField":
                    companyHintLabel.setText(bundle.getString("OptionsDialog.cityField.toolTipText"));
                    companyHintLabel.setVisible(true);
                    break;
                case "stateField":
                    companyHintLabel.setText(bundle.getString("OptionsDialog.stateField.toolTipText"));
                    companyHintLabel.setVisible(true);
                    break;
                case "zipField":
                    companyHintLabel.setText(bundle.getString("OptionsDialog.zipField.toolTipText"));
                    companyHintLabel.setVisible(true);
                    break;
                case "contactField":
                    companyHintLabel.setText(bundle.getString("OptionsDialog.contactField.toolTipText"));
                    companyHintLabel.setVisible(true);
                    break;
                case "phoneField":
                    companyHintLabel.setText(bundle.getString("OptionsDialog.phoneField.toolTipText"));
                    companyHintLabel.setVisible(true);
                    break;
                case "emailField":
                    companyHintLabel.setText(bundle.getString("OptionsDialog.emailField.toolTipText"));
                    companyHintLabel.setVisible(true);
                    break;
                case "faxField":
                    companyHintLabel.setText(bundle.getString("OptionsDialog.faxField.toolTipText"));
                    companyHintLabel.setVisible(true);
                    break;
                case "dotNumberField":
                    companyHintLabel.setText(bundle.getString("OptionsDialog.dotNumberField.toolTipText"));
                    companyHintLabel.setVisible(true);
                    break;
                default:
                    companyHintLabel.setVisible(false);
                    companyHintLabel.setText("");
            }
        }
    }//GEN-LAST:event_selectText

    private void authorityCheckboxFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_authorityCheckboxFocusGained
        companyHintLabel.setText(bundle.getString("OptionsDialog.authorityCheckbox.toolTipText"));
    }//GEN-LAST:event_authorityCheckboxFocusGained

    private void authorityCheckboxItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_authorityCheckboxItemStateChanged
        dotPanel.setVisible(authorityCheckbox.isSelected());
        if (!loading) {
            setSavable(true);
        }
    }//GEN-LAST:event_authorityCheckboxItemStateChanged

    private void generalButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_generalButtonActionPerformed
        ((CardLayout) contentPanel.getLayout()).show(contentPanel, "general");
        currentPanel = 0;
    }//GEN-LAST:event_generalButtonActionPerformed

    private void accountingButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_accountingButtonActionPerformed
        ((CardLayout) contentPanel.getLayout()).show(contentPanel, "accounting");
        currentPanel = 1;
    }//GEN-LAST:event_accountingButtonActionPerformed

    private void miscellaneousButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_miscellaneousButtonActionPerformed
        ((CardLayout) contentPanel.getLayout()).show(contentPanel, "misc");
        currentPanel = 2;
    }//GEN-LAST:event_miscellaneousButtonActionPerformed

    private void jSlider1StateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_jSlider1StateChanged
        jProgressBar1.setValue(jSlider1.getValue());
        jSpinner1.setValue(jSlider1.getValue());
    }//GEN-LAST:event_jSlider1StateChanged

    private void jSpinner1StateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_jSpinner1StateChanged
        jProgressBar1.setValue((Integer) jSpinner1.getValue());
        jSlider1.setValue((Integer) jSpinner1.getValue());
    }//GEN-LAST:event_jSpinner1StateChanged

    private void miscellaneousTabsComponentShown(java.awt.event.ComponentEvent evt) {//GEN-FIRST:event_miscellaneousTabsComponentShown
        if (evt.getComponent().getName().equals("lafTab")) {
            for (LookAndFeelInfo i : UIManager.getInstalledLookAndFeels()) {
                lafCombobox.addItem(i.getName());
            }
        }
    }//GEN-LAST:event_miscellaneousTabsComponentShown

    private void lafComboboxItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_lafComboboxItemStateChanged
        lafName = null;
        
        if (!loading) {
            for (LookAndFeelInfo i : UIManager.getInstalledLookAndFeels()) {
                if (i.getName().equals(lafCombobox.getSelectedItem().toString())) {
                    lafName = i.getClassName();
                    program.setUserProperty("program.lookAndFeel", i.getClassName());
                    break;
                }
            }
            try {
                UIManager.setLookAndFeel(lafName);
                SwingUtilities.updateComponentTreeUI(previewPanel);
            } catch (ClassNotFoundException
                    | UnsupportedLookAndFeelException e) {
            } catch (InstantiationException
                    | IllegalAccessException e) {
                logger.log(Level.WARNING, "Unable to set look and feel to {0}", 
                        lafName);
            }
        } 
    }//GEN-LAST:event_lafComboboxItemStateChanged

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JToggleButton accountingButton;
    private javax.swing.JTabbedPane accountingTabs;
    private javax.swing.JCheckBox authorityCheckbox;
    private javax.swing.JPanel bottomPanel;
    private javax.swing.ButtonGroup buttonGroup1;
    private javax.swing.JToolBar categoriesBar;
    private javax.swing.ButtonGroup categoriesButtonGroup;
    private javax.swing.JTextField cityField;
    private javax.swing.JLabel cityLabel;
    private javax.swing.JPanel commandPanel;
    private javax.swing.JTextField companyField;
    private javax.swing.JLabel companyHintLabel;
    private javax.swing.JPanel companyInfoTab;
    private javax.swing.JLabel companyLabel;
    private javax.swing.JTextField contactField;
    private javax.swing.JLabel contactLabel;
    private javax.swing.JPanel contentPanel;
    private javax.swing.JTextField dotNumberField;
    private javax.swing.JLabel dotNumberLabel;
    private javax.swing.JPanel dotPanel;
    private javax.swing.JTextField emailField;
    private javax.swing.JLabel emailLabel;
    private javax.swing.JFormattedTextField faxField;
    private javax.swing.JLabel faxLabel;
    private javax.swing.JToggleButton generalButton;
    private javax.swing.JTabbedPane generalTabs;
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton2;
    private javax.swing.JButton jButton3;
    private javax.swing.JButton jButton4;
    private javax.swing.JButton jButton5;
    private javax.swing.JCheckBox jCheckBox1;
    private javax.swing.JCheckBox jCheckBox2;
    private javax.swing.JComboBox<String> jComboBox1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JList<String> jList1;
    private javax.swing.JPasswordField jPasswordField1;
    private javax.swing.JProgressBar jProgressBar1;
    private javax.swing.JRadioButton jRadioButton1;
    private javax.swing.JRadioButton jRadioButton2;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JScrollPane jScrollPane4;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JSeparator jSeparator2;
    private javax.swing.JSlider jSlider1;
    private javax.swing.JSpinner jSpinner1;
    private javax.swing.JTable jTable1;
    private javax.swing.JTextArea jTextArea1;
    private javax.swing.JTextField jTextField1;
    private javax.swing.JToggleButton jToggleButton1;
    private javax.swing.JToggleButton jToggleButton2;
    private javax.swing.JTree jTree1;
    private javax.swing.JLabel label1;
    private javax.swing.JComboBox<String> lafCombobox;
    private javax.swing.JLabel lafLabel;
    private javax.swing.JPanel lafTab;
    private javax.swing.JToggleButton miscellaneousButton;
    private javax.swing.JTabbedPane miscellaneousTabs;
    private javax.swing.JFormattedTextField phoneField;
    private javax.swing.JLabel phoneLabel;
    private javax.swing.JPanel previewDescriptionPanel;
    private javax.swing.JPanel previewPanel;
    private javax.swing.JTextField stateField;
    private javax.swing.JLabel stateLabel;
    private javax.swing.JTextField streetField;
    private javax.swing.JLabel streetLabel;
    private javax.swing.JTextField suiteField;
    private javax.swing.JLabel suiteLabel;
    private javax.swing.JTextField zipField;
    private javax.swing.JLabel zipLabel;
    // End of variables declaration//GEN-END:variables

    private final SaveAction saveAction;
    private class SaveAction extends AbstractAction {

        private static final long serialVersionUID = 7149780887303574384L;
//        private static final ResourceBundle resMap = ResourceBundle.getBundle("com/gs/ntos/resources/Bundle");
        
        public SaveAction() {
            super(bundle.getString("OptionsDialog.SaveAction.text"));
            
            initAction();
        }
        
        private void initAction() {
            putValue(SMALL_ICON, new ImageIcon(getClass().getResource(
                    bundle.getString("OptionsDialog.SaveAction.smallIcon"))));
            putValue(DISPLAYED_MNEMONIC_INDEX_KEY, Integer.valueOf(
                    bundle.getString("OptionsDialog.SaveAction.mnemonicIndex")));
            putValue(SHORT_DESCRIPTION, 
                    bundle.getString("OptionsDialog.SaveAction.shortDescription"));
        }
        
        @Override
        public void actionPerformed(ActionEvent e) {
            applyAction.actionPerformed(e);
            cancelAction.actionPerformed(e);
        }
        
    }
    
    private final ApplyAction applyAction;
    private class ApplyAction extends AbstractAction {

        private static final long serialVersionUID = 5314732521006093469L;

        public ApplyAction() {
            super(bundle.getString("OptionsDialog.ApplyAction.text"));
            
            initAction();
        }
        
        private void initAction() {
            putValue(SMALL_ICON, new ImageIcon(getClass().getResource(
                    bundle.getString("OptionsDialog.ApplyAction.smallIcon"))));
            putValue(DISPLAYED_MNEMONIC_INDEX_KEY, Integer.valueOf(
                    bundle.getString("OptionsDialog.ApplyAction.mnemonicIndex")));
            putValue(SHORT_DESCRIPTION, 
                    bundle.getString("OptionsDialog.ApplyAction.shortDescription"));
        }
        
        @Override
        public void actionPerformed(ActionEvent e) {
            Properties p = program.getProperties();
            
            // Company Information
            p.setSystemProperty("company.name", companyField.getText());
            p.setSystemProperty("company.street", streetField.getText());
            p.setSystemProperty("company.suite", suiteField.getText());
            p.setSystemProperty("company.city", cityField.getText());
            p.setSystemProperty("company.state", stateField.getText());
            p.setSystemProperty("company.zip", zipField.getText());
            p.setSystemProperty("company.contact", contactField.getText());
            p.setSystemProperty("company.email", emailField.getText());
            p.setSystemProperty("company.phone", phoneField.getText());
            p.setSystemProperty("company.fax", faxField.getText());
            p.setSystemProperty("company.authority", authorityCheckbox.isSelected());
            p.setSystemProperty("company.dotNumber", dotNumberField.getText());
            
            // Accounting Information
            
            // Miscellaneous Information
            p.setProperty("program.lookAndFeel", lafName);
            
            // Update the look and feel.
            SwingUtilities.updateComponentTreeUI(instance.getParent());
            
            setSavable(false);
        }
        
    }
    
    private final CancelAction cancelAction;
    private class CancelAction extends AbstractAction {

        private static final long serialVersionUID = -6089139508449217744L;
        
        public CancelAction() {
            super(bundle.getString("OptionsDialog.CancelAction.text"));
            
            initAction();
        }
        
        private void initAction() {
            putValue(SMALL_ICON, new ImageIcon(getClass().getResource(
                    bundle.getString("OptionsDialog.CancelAction.smallIcon"))));
            putValue(DISPLAYED_MNEMONIC_INDEX_KEY, Integer.valueOf(
                    bundle.getString("OptionsDialog.CancelAction.mnemonicIndex")));
            putValue(SHORT_DESCRIPTION, 
                    bundle.getString("OptionsDialog.CancelAction.shortDescription"));
        }

        @Override
        public void actionPerformed(ActionEvent e) {
            willExit(e);
        }
        
    }
    
    private final HelpAction helpAction;
    private class HelpAction extends AbstractAction {

        private static final long serialVersionUID = 9195556010988315538L;
        
        public HelpAction() {
            super(bundle.getString("OptionsDialog.HelpAction.text"));
            
            initAction();
        }
        
        private void initAction() {
            putValue(SMALL_ICON, new ImageIcon(getClass().getResource(
                    bundle.getString("OptionsDialog.HelpAction.smallIcon"))));
            putValue(DISPLAYED_MNEMONIC_INDEX_KEY, Integer.valueOf(
                    bundle.getString("OptionsDialog.HelpAction.mnemonicIndex")));
            putValue(SHORT_DESCRIPTION, 
                    bundle.getString("OptionsDialog.HelpAction.shortDescription"));
        }

        @Override
        public void actionPerformed(ActionEvent e) {
            throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        }
        
    }
    
    private class OptionsDialogAdapter extends WindowAdapter {
        
        @Override
        public void windowClosing(WindowEvent e) {
            java.util.Properties props = new java.util.Properties();
            
            props.setProperty("dialog.x", String.valueOf(instance.getLocationOnScreen().x));
            props.setProperty("dialog.y", String.valueOf(instance.getLocationOnScreen().y));
            props.setProperty("dialog.width", String.valueOf(instance.getWidth()));
            props.setProperty("dialog.height", String.valueOf(instance.getHeight()));
            props.setProperty("dialog.selectedPanel", String.valueOf(currentPanel));
            
            switch (currentPanel) {
                case 0:
                    props.put("dialog.general.selectedTab", 
                            generalTabs.getSelectedIndex());
                    break;
                case 1:
                    props.put("dialog.accounting.selectedTab", 
                            accountingTabs.getSelectedIndex());
                    break;
                case 2:
                    props.put("dialog.misc.selectedTab", 
                            miscellaneousTabs.getSelectedIndex());
                    break;
            }
            
            File sessionFile = new File(FileUtils.getApplicationConfigDirectory(),
                        instance.getClass().getSimpleName() + "session.xml");
            try {
                try (FileOutputStream fos = new FileOutputStream(sessionFile)) {
                    props.storeToXML(fos, "Storing Session State for Options Dialog");
                }
            } catch (IOException ex) {
                logger.error(ex, "Attempting to store session settings to {0}", 
                        sessionFile);
            }
            
            dispose();
        }

        @Override
        public void windowOpened(WindowEvent e) {
            java.util.Properties props = new java.util.Properties();
            
            File sessionFile = new File(FileUtils.getApplicationConfigDirectory(),
                        instance.getClass().getSimpleName() + "session.xml");
            try {
                try (FileInputStream fis = new FileInputStream(sessionFile)) {
                    props.loadFromXML(fis);
                }
            } catch (IOException ex) {
                logger.error(ex, "Attempting to store session settings to {0}", 
                        sessionFile);
            }
            
            lafName = props.getProperty("program.lookAndFeel",
                    UIManager.getLookAndFeel().getClass().getCanonicalName());
            
            Point p = ScreenUtils.getCenterPoint(instance.getParent(), instance, false);
            int x = Integer.valueOf(props.getProperty("dialog.x", String.valueOf(p.x)));
            int y = Integer.valueOf(props.getProperty("dialog.y", String.valueOf(p.y)));
            instance.setLocation(x, y);
            
            Dimension size = instance.getSize();
            int width = Integer.valueOf(
                    props.getProperty("dialog.width", String.valueOf(size.width)));
            int height = Integer.valueOf(
                    props.getProperty("dialog.height", String.valueOf(size.height)));
            instance.setSize(width, height);
            
            currentPanel = Integer.valueOf(
                    props.getProperty("dialog.selectedPanel", String.valueOf(0)));
            
            switch (currentPanel) {
                case 0:
                    generalButton.setSelected(true);
                    
                    ((CardLayout) contentPanel.getLayout()).show(contentPanel, 
                            "general");
                    
                    if (generalTabs.getTabCount() > 0) {
                        generalTabs.setSelectedIndex(Integer.valueOf(
                                props.getProperty("dialog.general.selectedTab", 
                                        String.valueOf(0))));
                    }
                    break;
                case 1:
                    accountingButton.setSelected(true);
                    
                    ((CardLayout) contentPanel.getLayout()).show(contentPanel, 
                            "accounting");
                    
                    if (accountingTabs.getTabCount() > 0) {
                        accountingTabs.setSelectedIndex(Integer.valueOf(
                                props.getProperty("dialog.accounting.selectedTab", 
                                        String.valueOf(0))));
                    }
                    break;
                case 2:
                    miscellaneousButton.setSelected(true);
                    
                    ((CardLayout) contentPanel.getLayout()).show(contentPanel, 
                            "misc");
                    
                    if (miscellaneousTabs.getTabCount() > 0) {
                        miscellaneousTabs.setSelectedIndex(Integer.valueOf(
                                props.getProperty("dialog.misc.selectedTab", 
                                        String.valueOf(0))));
                    }
            }
            
            companyHintLabel.setVisible(false);
            companyField.requestFocusInWindow();
        }
        
    }
    
    private void showOnScreen(int screen, JFrame frame ) {
        GraphicsEnvironment ge = GraphicsEnvironment.getLocalGraphicsEnvironment();
        GraphicsDevice[] gd = ge.getScreenDevices();
        int width = 0, height = 0;
        if( screen > -1 && screen < gd.length ) {
            width = gd[screen].getDefaultConfiguration().getBounds().width;
            height = gd[screen].getDefaultConfiguration().getBounds().height;
            frame.setLocation(
                ((width / 2) - (frame.getSize().width / 2)) + gd[screen].getDefaultConfiguration().getBounds().x, 
                ((height / 2) - (frame.getSize().height / 2)) + gd[screen].getDefaultConfiguration().getBounds().y
            );
            frame.setVisible(true);
        } else {
            throw new RuntimeException( "No Screens Found" );
        }
    }
    
}
