/*
 * Copyright (C) 2022 GS United Labs
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * *****************************************************************************
 *  Project    :   NTOS
 *  Class      :   LoadWizardAction.java
 *  Author     :   Sean Carrick
 *  Created    :   Apr 3, 2022
 *  Modified   :   Apr 3, 2022
 *  
 *  Purpose: See class JavaDoc for explanation
 *  
 *  Revision History:
 *  
 *  WHEN          BY                   REASON
 *  ------------  -------------------  -----------------------------------------
 *  Apr 3, 2022  Sean Carrick         Initial creation.
 * *****************************************************************************
 */

package com.gs.ntos.actions;

import com.gs.ntos.MainFrame;
import com.gs.ntos.api.DesktopProgram;
import com.gs.ntos.enums.Level;
import com.gs.ntos.loads.LoadWizardPanelProvider;
import com.gs.ntos.logging.Logger;
import com.gs.ntos.utils.ScreenUtils;
import java.awt.Container;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.util.ResourceBundle;
import javax.imageio.ImageIO;
import javax.swing.AbstractAction;
import static javax.swing.Action.ACCELERATOR_KEY;
import static javax.swing.Action.DISPLAYED_MNEMONIC_INDEX_KEY;
import static javax.swing.Action.LARGE_ICON_KEY;
import static javax.swing.Action.SHORT_DESCRIPTION;
import static javax.swing.Action.SMALL_ICON;
import javax.swing.ImageIcon;
import javax.swing.KeyStroke;
import javax.swing.UIManager;
import org.netbeans.api.wizard.WizardDisplayer;
import org.netbeans.spi.wizard.Wizard;

/**
 *
 * @author Sean Carrick &lt;sean at gs-unitedlabs dot com&gt;
 * 
 * @version 1.0
 * @since 1.0
 */
public class LoadWizardAction extends AbstractAction {

        private static final long serialVersionUID = 3478528720301140513L;
        
        private static final ResourceBundle bundle = ResourceBundle.getBundle("com/gs/ntos/resources/Bundle");
        private final Logger logger;
        private final DesktopProgram program;
        private final MainFrame parent;
//        private BufferedImage sidebarImage;
        
        public LoadWizardAction(DesktopProgram program, MainFrame parent) {
            super(bundle.getString("LoadWizardAction.text"));
            logger = new Logger(program, getClass().getSimpleName(), (Level)
                    program.getProperties().getSystemProperty("logging.level", 
                            Level.ALL));
            this.program = program;
            this.parent = parent;
            
            initAction();
        }
        
        private void initAction() {
            putValue(SMALL_ICON, new ImageIcon(getClass().getResource(bundle.getString(
                    "LoadWizardAction.smallIcon"))));
            putValue(LARGE_ICON_KEY, new ImageIcon(getClass().getResource(
                    bundle.getString("LoadWizardAction.largeIcon"))));
            putValue(ACCELERATOR_KEY, KeyStroke.getKeyStroke(bundle.getString(
                    "LoadWizardAction.accelerator")));
            putValue(SHORT_DESCRIPTION, bundle.getString("LoadWizardAction.shortDescription"));
            putValue(DISPLAYED_MNEMONIC_INDEX_KEY, Integer.valueOf(bundle.getString(
                    "LoadWizardAction.mnemonicIndex")));
            
//            try {
//                sidebarImage = ImageIO.read(getClass().getResource("/com/gs/ntos/resources/icons/xlarge/LoadWizard.png"));
//            } catch (IOException ex) {
//                logger.error(ex, "Attempting to load the BufferedImage.");
//            }
//            UIManager.put("wizard.sidebar.image", sidebarImage);
        }

        @Override
        public void actionPerformed(ActionEvent e) {
            try {
                BufferedImage img = ImageIO.read(new File(getClass().getResource(
                        bundle.getString("LoadWizard.sidebar.image")).toURI()));
                UIManager.put("wizard.sidebar.image", img);
            } catch (URISyntaxException
                    | IOException ex) {
                logger.error(ex, "Attempting to load the sidebar image.");
            }
            Wizard wizard = new LoadWizardPanelProvider(program, 
                    new String[] {
                        "welcome",
                        "load",
                        "broker",
                        "stops",
                        "summary"
                    }, new String[] {
                        "Welcome and Instructions",
                        "General Load Information",
                        "Broker/Agent Information",
                        "Stop/Customer Information",
                        "Load Summary"
                    }, "New Load Wizard").createWizard();
            Container throwAway = new Container();
            throwAway.setSize(900, 475);
            Point loc = ScreenUtils.getCenterPoint(parent, throwAway, false);
            
            WizardDisplayer.showWizard(wizard, new Rectangle(loc.x, loc.y, 
                    throwAway.getWidth(), throwAway.getHeight()));
        }
        
}
