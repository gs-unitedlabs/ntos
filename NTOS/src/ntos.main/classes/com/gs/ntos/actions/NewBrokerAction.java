/*
 * Copyright (C) 2022 GS United Labs
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * *****************************************************************************
 *  Project    :   NTOS
 *  Class      :   NewBrokerAction.java
 *  Author     :   Sean Carrick
 *  Created    :   Mar 30, 2022
 *  Modified   :   Mar 30, 2022
 *  
 *  Purpose: See class JavaDoc for explanation
 *  
 *  Revision History:
 *  
 *  WHEN          BY                   REASON
 *  ------------  -------------------  -----------------------------------------
 *  Mar 30, 2022  Sean Carrick         Initial creation.
 * *****************************************************************************
 */

package com.gs.ntos.actions;

import com.gs.ntos.MainFrame;
import com.gs.ntos.api.DesktopProgram;
import com.gs.ntos.broker.NewBrokerDialog;
import java.awt.event.ActionEvent;
import java.util.ResourceBundle;
import javax.swing.AbstractAction;
import javax.swing.ImageIcon;
import javax.swing.KeyStroke;

/**
 *
 * @author Sean Carrick &lt;sean at gs-unitedlabs dot com&gt;
 * 
 * @version 1.0
 * @since 1.0
 */
public class NewBrokerAction extends AbstractAction {

    private static final long serialVersionUID = 1L;
    
    private static final ResourceBundle map = ResourceBundle.getBundle("com/gs/ntos/resources/Bundle");
    private final DesktopProgram program;
    private final MainFrame parent;
    
    public NewBrokerAction (DesktopProgram program, MainFrame parent) {
        super(map.getString("NewBrokerAction.text"));
        this.program = program;
        this.parent = parent;
        
        initAction();
    }
    
    private void initAction() {
        putValue(SMALL_ICON, new ImageIcon(getClass().getResource(map.getString(
                "NewBrokerAction.smallIcon"))));
        putValue(LARGE_ICON_KEY, new ImageIcon(getClass().getResource(
                map.getString("NewBrokerAction.largeIcon"))));
        putValue(ACCELERATOR_KEY, KeyStroke.getKeyStroke(map.getString(
                "NewBrokerAction.accelerator")));
        putValue(SHORT_DESCRIPTION, map.getString("NewBrokerAction.shortDescription"));
        putValue(DISPLAYED_MNEMONIC_INDEX_KEY, Integer.valueOf(map.getString(
                "NewBrokerAction.mnemonicIndex")));
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        NewBrokerDialog dlg = new NewBrokerDialog(program, null, true);
        program.show(dlg);
    }

}
