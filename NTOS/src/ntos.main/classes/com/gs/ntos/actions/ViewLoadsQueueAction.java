/*
 * Copyright (C) 2022 GS United Labs
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * *****************************************************************************
 *  Project    :   NTOS
 *  Class      :   ViewLoadsQueueAction.java
 *  Author     :   Sean Carrick
 *  Created    :   Mar 31, 2022
 *  Modified   :   Mar 31, 2022
 *  
 *  Purpose: See class JavaDoc for explanation
 *  
 *  Revision History:
 *  
 *  WHEN          BY                   REASON
 *  ------------  -------------------  -----------------------------------------
 *  Mar 31, 2022  Sean Carrick         Initial creation.
 * *****************************************************************************
 */

package com.gs.ntos.actions;

import com.gs.ntos.MainFrame;
import com.gs.ntos.api.DesktopProgram;
import com.gs.ntos.broker.BrokerViewer;
import com.gs.ntos.loads.LoadsQueue;
import java.awt.event.ActionEvent;
import java.util.ResourceBundle;
import javax.swing.AbstractAction;
import static javax.swing.Action.ACCELERATOR_KEY;
import static javax.swing.Action.DISPLAYED_MNEMONIC_INDEX_KEY;
import static javax.swing.Action.LARGE_ICON_KEY;
import static javax.swing.Action.SHORT_DESCRIPTION;
import static javax.swing.Action.SMALL_ICON;
import javax.swing.ImageIcon;
import javax.swing.KeyStroke;

/**
 *
 * @author Sean Carrick &lt;sean at gs-unitedlabs dot com&gt;
 * 
 * @version 1.0
 * @since 1.0
 */
public class ViewLoadsQueueAction extends AbstractAction {

        private static final long serialVersionUID = 6415813141952195693L;
        private static final ResourceBundle bundle = ResourceBundle.getBundle("com/gs/ntos/resources/Bundle");
        
        private final DesktopProgram program;
        private final MainFrame parent;
        
        public ViewLoadsQueueAction(DesktopProgram program, MainFrame parent) {
            super(bundle.getString("ViewLoadsQueueAction.text"));
            
            this.program = program;
            this.parent = parent;
            
            initAction();
        }
        
        private void initAction() {
        putValue(SMALL_ICON, new ImageIcon(getClass().getResource(bundle.getString(
                "ViewLoadsQueueAction.smallIcon"))));
        putValue(LARGE_ICON_KEY, new ImageIcon(getClass().getResource(
                bundle.getString("ViewLoadsQueueAction.largeIcon"))));
        putValue(ACCELERATOR_KEY, KeyStroke.getKeyStroke(bundle.getString(
                "ViewLoadsQueueAction.accelerator")));
        putValue(SHORT_DESCRIPTION, bundle.getString("ViewLoadsQueueAction.shortDescription"));
        putValue(DISPLAYED_MNEMONIC_INDEX_KEY, Integer.valueOf(bundle.getString(
                "ViewLoadsQueueAction.mnemonicIndex")));
        }

        @Override
        public void actionPerformed(ActionEvent e) {
            LoadsQueue queue = new LoadsQueue();
            program.show(queue);
        }
        
}
