/*
 * Copyright (C) 2022 GS United Labs
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * *****************************************************************************
 *  Project    :   LoadAccounting
 *  Class      :   NewCustomerDialog.java
 *  Author     :   Sean Carrick
 *  Created    :   Mar 30, 2022
 *  Modified   :   Mar 30, 2022
 *  
 *  Purpose: See class JavaDoc for explanation
 *  
 *  Revision History:
 *  
 *  WHEN          BY                   REASON
 *  ------------  -------------------  -----------------------------------------
 *  Mar 30, 2022  Sean Carrick         Initial creation.
 * *****************************************************************************
 */
package com.gs.ntos.customer;

import com.gs.ntos.api.DesktopProgram;
import com.gs.ntos.api.ExitListener;
import com.gs.ntos.database.errors.DataStoreException;
import com.gs.ntos.database.models.Customer;
import com.gs.ntos.database.tables.CustomersTable;
import com.gs.ntos.enums.Level;
import com.gs.ntos.logging.Logger;
import com.gs.ntos.support.MessageBox;
import com.gs.ntos.utils.NetUtils;
import com.gs.ntos.validators.EmailValidator;
import com.gs.ntos.validators.PhoneNumberValidator;
import com.gs.ntos.validators.PostalCodeValidator;
import com.gs.ntos.validators.StateOrProvinceValidator;
import com.gs.ntos.verifiers.NotEmptyInputVerifier;
import com.gs.ntos.verifiers.PostalCodeInputVerifier;
import com.gs.ntos.verifiers.StateProvinceInputVerifier;
import java.awt.Color;
import java.awt.Desktop;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.EventObject;
import java.util.ResourceBundle;
import javax.swing.AbstractAction;
import javax.swing.ImageIcon;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.text.JTextComponent;

/**
 *
 * @author Sean Carrick
 */
public class NewCustomerDialog extends javax.swing.JDialog
        implements ExitListener, PropertyChangeListener, DocumentListener {
    
    private final NewCustomerDialog instance;
    private final DesktopProgram program;
    private final WindowAdapter listener;
    private final ResourceBundle bundle;
    private final SaveAction save;
    private final Logger logger;
    
    private boolean savable;
    private boolean added;
    private boolean dirty;
    private boolean loading;
    
    /**
     * Creates new form NewCustomerDialog
     */
    public NewCustomerDialog(DesktopProgram program, java.awt.Frame parent, 
            boolean modal) {
        super(parent, modal);
        this.program = program;
        bundle = ResourceBundle.getBundle("com/gs/ntos/customer/resources/Bundle");
        logger = new Logger(program, getClass().getSimpleName(), Level.ALL);
        listener = new CustomerDialogAdapter();
        save = new SaveAction();
        dirty = false;
        added = false;
        loading = true;
        
        initComponents();
        
        instance = this;
        
        loading = false;
    }
    
    public NewCustomerDialog(DesktopProgram program, java.awt.Dialog parent, 
            boolean modal) {
        super(parent, modal);
        this.program = program;
        bundle = ResourceBundle.getBundle("com/gs/ntos/customer/resources/Bundle");
        logger = new Logger(program, getClass().getSimpleName(), Level.ALL);
        listener = new CustomerDialogAdapter();
        save = new SaveAction();
        dirty = false;
        added = false;
        loading = true;
        
        initComponents();
        
        instance = this;
        
        loading = false;
    }
    
    public boolean isSavable() {
        return savable;
    }
    
    public void setSavable(boolean savable) {
        logger.entering("setSavable(savable = {0})", savable);
        
        boolean oldValue = isSavable();
        this.savable = savable;
        
        logger.debug("Firing a property change. oldValue = {0}, newValue = {1}",
                oldValue, isSavable());
        program.firePropertyChange("savable", oldValue, isSavable());
        
        logger.exiting("setSavable(savable)");
    }

    @Override
    public boolean canExit(EventObject event) {
        if (dirty) {
            int choice = MessageBox.askQuestion("You have unsaved changes.\n\n"
                    + "Do you really want to close?", "Confirm Close", false);
            
            if (choice == MessageBox.NO_OPTION) {
                return false;
            }
        } else if (!dirty && added) {
            int choice = MessageBox.askQuestion("Would you like to enter "
                    + "another?", "Confirm Close", false);
            
            if (choice == MessageBox.YES_OPTION) {
                added = false;
                clearFields();
                return false;
            }
        }
        return true;    // For now...
    }

    @Override
    public void willExit(EventObject event) {
        listener.windowClosing(new WindowEvent(this, 
                WindowEvent.WINDOW_CLOSING));
    }

    @Override
    public void insertUpdate(DocumentEvent e) {
        logger.entering("insertUpdate(e = {0})", e);
        if (!loading) {
            dirty = true;
        }
        setSavable(validateFields());
        logger.exiting("insertUpdate(e)");
    }

    @Override
    public void removeUpdate(DocumentEvent e) {
        logger.entering("removeUpdate(e = {0})", e);
        if (!loading) {
            dirty = true;
        }
        setSavable(validateFields());
        logger.exiting("removeUpdate(e)");
    }

    @Override
    public void changedUpdate(DocumentEvent e) {
        logger.entering("changedUpdate(e = {0})", e);
        if (!loading) {
            dirty = true;
        }
        setSavable(validateFields());
        logger.exiting("changedUpdate(e)");
    }

    @Override
    public void propertyChange(PropertyChangeEvent evt) {
        logger.entering("propertyChange(evt = {0})", evt);
        
        if ("savable".equals(evt.getPropertyName())) {
            logger.debug("Setting the enabled state of the save event to {0}", 
                    evt.getNewValue());
            save.setEnabled((Boolean) evt.getNewValue());
        }
        
        logger.exiting("propertyChange(evt)");
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        bottomPanel = new javax.swing.JPanel();
        dialogSeparator = new javax.swing.JSeparator();
        commandPanel = new javax.swing.JPanel();
        helpButton = new javax.swing.JButton();
        saveButton = new javax.swing.JButton();
        cancelButton = new javax.swing.JButton();
        detailsPanel = new javax.swing.JPanel();
        idLabel = new javax.swing.JLabel();
        idField = new javax.swing.JTextField();
        jSeparator1 = new javax.swing.JSeparator();
        companyLabel = new javax.swing.JLabel();
        companyField = new javax.swing.JTextField();
        hintLabel = new javax.swing.JLabel();
        streetLabel = new javax.swing.JLabel();
        streetField = new javax.swing.JTextField();
        suiteLabel = new javax.swing.JLabel();
        suiteField = new javax.swing.JTextField();
        cityLabel = new javax.swing.JLabel();
        cityField = new javax.swing.JTextField();
        stateLabel = new javax.swing.JLabel();
        stateField = new javax.swing.JTextField();
        zipLabel = new javax.swing.JLabel();
        zipField = new javax.swing.JTextField();
        contactLabel = new javax.swing.JLabel();
        contactField = new javax.swing.JTextField();
        emailLabel = new javax.swing.JLabel();
        emailField = new javax.swing.JTextField();
        phoneLabel = new javax.swing.JLabel();
        phoneField = new javax.swing.JFormattedTextField();
        faxField = new javax.swing.JFormattedTextField();
        faxLabel = new javax.swing.JLabel();
        notesLabel = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        notesField = new javax.swing.JTextArea();

        setDefaultCloseOperation(javax.swing.WindowConstants.DO_NOTHING_ON_CLOSE);
        setName("NewCustomerDialog"); // NOI18N
        addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                captureEscape(evt);
            }
        });

        bottomPanel.setName("bottomPanel"); // NOI18N
        bottomPanel.setLayout(new java.awt.BorderLayout());

        dialogSeparator.setName("dialogSeparator"); // NOI18N
        bottomPanel.add(dialogSeparator, java.awt.BorderLayout.PAGE_START);

        commandPanel.setName("commandPanel"); // NOI18N

        helpButton.setAction(new HelpAction());
        helpButton.setName("helpButton"); // NOI18N
        helpButton.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                captureEscape(evt);
            }
        });

        saveButton.setAction(save);
        saveButton.setName("saveButton"); // NOI18N
        saveButton.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                captureEscape(evt);
            }
        });

        cancelButton.setAction(new CancelAction());
        cancelButton.setName("cancelButton"); // NOI18N
        cancelButton.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                captureEscape(evt);
            }
        });

        javax.swing.GroupLayout commandPanelLayout = new javax.swing.GroupLayout(commandPanel);
        commandPanel.setLayout(commandPanelLayout);
        commandPanelLayout.setHorizontalGroup(
            commandPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(commandPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(helpButton)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 631, Short.MAX_VALUE)
                .addComponent(saveButton)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(cancelButton)
                .addContainerGap())
        );
        commandPanelLayout.setVerticalGroup(
            commandPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(commandPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(commandPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(helpButton)
                    .addComponent(saveButton)
                    .addComponent(cancelButton))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        bottomPanel.add(commandPanel, java.awt.BorderLayout.CENTER);

        getContentPane().add(bottomPanel, java.awt.BorderLayout.PAGE_END);

        detailsPanel.setName("detailsPanel"); // NOI18N

        idLabel.setFont(new java.awt.Font("Noto Sans", 1, 13)); // NOI18N
        idLabel.setText(bundle.getString("NewCustomerDialog.idLabel.text_1")); // NOI18N
        idLabel.setName("idLabel"); // NOI18N

        idField.setEditable(false);
        idField.setFocusable(false);
        idField.setName("idField"); // NOI18N

        jSeparator1.setName("jSeparator1"); // NOI18N

        companyLabel.setDisplayedMnemonic(bundle.getString("NewCustomerDialog.companyLabel.mnemonic").charAt(0));
        companyLabel.setFont(new java.awt.Font("Noto Sans", 1, 13)); // NOI18N
        companyLabel.setLabelFor(companyField);
        companyLabel.setText(bundle.getString("NewCustomerDialog.companyLabel.text_1")); // NOI18N
        companyLabel.setName("companyLabel"); // NOI18N

        companyField.setName("companyField"); // NOI18N
        companyField.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                selectText(evt);
            }
        });
        companyField.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                captureEscape(evt);
            }
        });
        companyField.setInputVerifier(new NotEmptyInputVerifier());
        companyField.getDocument().addDocumentListener(this);

        hintLabel.setFont(new java.awt.Font("Noto Sans", 1, 13)); // NOI18N
        hintLabel.setForeground(new java.awt.Color(255, 0, 0));
        hintLabel.setText(bundle.getString("NewCustomerDialog.hintLabel.text_1")); // NOI18N
        hintLabel.setName("hintLabel"); // NOI18N

        streetLabel.setDisplayedMnemonic(bundle.getString("NewCustomerDialog.streetLabel.mnemonic").charAt(0));
        streetLabel.setFont(new java.awt.Font("Noto Sans", 1, 13)); // NOI18N
        streetLabel.setLabelFor(streetField);
        streetLabel.setText(bundle.getString("NewCustomerDialog.streetLabel.text_1")); // NOI18N
        streetLabel.setName("streetLabel"); // NOI18N

        streetField.setName("streetField"); // NOI18N
        streetField.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                selectText(evt);
            }
        });
        streetField.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                captureEscape(evt);
            }
        });
        streetField.setInputVerifier(new NotEmptyInputVerifier());
        streetField.getDocument().addDocumentListener(this);

        suiteLabel.setDisplayedMnemonic(bundle.getString("NewCustomerDialog.suiteLabel.mnemonic").charAt(0));
        suiteLabel.setFont(new java.awt.Font("Noto Sans", 0, 13)); // NOI18N
        suiteLabel.setLabelFor(suiteField);
        suiteLabel.setText(bundle.getString("NewCustomerDialog.suiteLabel.text_1")); // NOI18N
        suiteLabel.setName("suiteLabel"); // NOI18N

        suiteField.setName("suiteField"); // NOI18N
        suiteField.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                selectText(evt);
            }
        });
        suiteField.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                captureEscape(evt);
            }
        });
        suiteField.getDocument().addDocumentListener(this);

        cityLabel.setDisplayedMnemonic(bundle.getString("NewCustomerDialog.cityLabel.mnemonic").charAt(0));
        cityLabel.setFont(new java.awt.Font("Noto Sans", 1, 13)); // NOI18N
        cityLabel.setLabelFor(cityField);
        cityLabel.setText(bundle.getString("NewCustomerDialog.cityLabel.text_1")); // NOI18N
        cityLabel.setName("cityLabel"); // NOI18N

        cityField.setName("cityField"); // NOI18N
        cityField.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                selectText(evt);
            }
        });
        cityField.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                captureEscape(evt);
            }
        });
        cityField.setInputVerifier(new NotEmptyInputVerifier());
        cityField.getDocument().addDocumentListener(this);

        stateLabel.setDisplayedMnemonic(bundle.getString("NewCustomerDialog.stateLabel.mnemonic").charAt(0));
        stateLabel.setFont(new java.awt.Font("Noto Sans", 1, 13)); // NOI18N
        stateLabel.setLabelFor(stateField);
        stateLabel.setText(bundle.getString("NewCustomerDialog.stateLabel.text_1")); // NOI18N
        stateLabel.setName("stateLabel"); // NOI18N

        stateField.setName("stateField"); // NOI18N
        stateField.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                selectText(evt);
            }
        });
        stateField.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                captureEscape(evt);
            }
        });
        stateField.setInputVerifier(new StateProvinceInputVerifier());
        stateField.getDocument().addDocumentListener(this);

        zipLabel.setDisplayedMnemonic(bundle.getString("NewCustomerDialog.zipLabel.mnemonic").charAt(0));
        zipLabel.setFont(new java.awt.Font("Noto Sans", 1, 13)); // NOI18N
        zipLabel.setLabelFor(zipField);
        zipLabel.setText(bundle.getString("NewCustomerDialog.zipLabel.text_1")); // NOI18N
        zipLabel.setName("zipLabel"); // NOI18N

        zipField.setName("zipField"); // NOI18N
        zipField.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                selectText(evt);
            }
        });
        zipField.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                captureEscape(evt);
            }
        });
        zipField.setInputVerifier(new PostalCodeInputVerifier());
        zipField.getDocument().addDocumentListener(this);

        contactLabel.setDisplayedMnemonic(bundle.getString("NewCustomerDialog.contactLabel.mnemonic").charAt(0));
        contactLabel.setFont(new java.awt.Font("Noto Sans", 0, 13)); // NOI18N
        contactLabel.setLabelFor(contactField);
        contactLabel.setText(bundle.getString("NewCustomerDialog.contactLabel.text_1")); // NOI18N
        contactLabel.setName("contactLabel"); // NOI18N

        contactField.setName("contactField"); // NOI18N
        contactField.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                selectText(evt);
            }
        });
        contactField.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                captureEscape(evt);
            }
        });
        contactField.getDocument().addDocumentListener(this);

        emailLabel.setDisplayedMnemonic(bundle.getString("NewCustomerDialog.emailLabel.mnemonic").charAt(0));
        emailLabel.setFont(new java.awt.Font("Noto Sans", 0, 13)); // NOI18N
        emailLabel.setLabelFor(emailField);
        emailLabel.setText(bundle.getString("NewCustomerDialog.emailLabel.text_1")); // NOI18N
        emailLabel.setName("emailLabel"); // NOI18N

        emailField.setName("emailField"); // NOI18N
        emailField.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                selectText(evt);
            }
        });
        emailField.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                captureEscape(evt);
            }
        });
        emailField.getDocument().addDocumentListener(this);

        phoneLabel.setDisplayedMnemonic(bundle.getString("NewCustomerDialog.phoneLabel.mnemonic").charAt(0));
        phoneLabel.setFont(new java.awt.Font("Noto Sans", 0, 13)); // NOI18N
        phoneLabel.setLabelFor(phoneField);
        phoneLabel.setText(bundle.getString("NewCustomerDialog.phoneLabel.text_1")); // NOI18N
        phoneLabel.setName("phoneLabel"); // NOI18N

        try {
            phoneField.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.MaskFormatter("(###) ###-####")));
        } catch (java.text.ParseException ex) {
            ex.printStackTrace();
        }
        phoneField.setName("phoneField"); // NOI18N
        phoneField.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                selectText(evt);
            }
        });
        phoneField.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                captureEscape(evt);
            }
        });
        phoneField.getDocument().addDocumentListener(this);

        try {
            faxField.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.MaskFormatter("(###) ###-####")));
        } catch (java.text.ParseException ex) {
            ex.printStackTrace();
        }
        faxField.setName("faxField"); // NOI18N
        faxField.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                selectText(evt);
            }
        });
        faxField.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                captureEscape(evt);
            }
        });
        faxField.getDocument().addDocumentListener(this);

        faxLabel.setDisplayedMnemonic(bundle.getString("NewCustomerDialog.faxLabel.mnemonic").charAt(0));
        faxLabel.setFont(new java.awt.Font("Noto Sans", 0, 13)); // NOI18N
        faxLabel.setLabelFor(faxField);
        faxLabel.setText(bundle.getString("NewCustomerDialog.faxLabel.text_1")); // NOI18N
        faxLabel.setName("faxLabel"); // NOI18N

        notesLabel.setDisplayedMnemonic(bundle.getString("NewCustomerDialog.notesLabel.mnemonic").charAt(0));
        notesLabel.setLabelFor(notesField);
        notesLabel.setText(bundle.getString("NewCustomerDialog.notesLabel.text_1")); // NOI18N
        notesLabel.setName("notesLabel"); // NOI18N

        jScrollPane1.setName("jScrollPane1"); // NOI18N

        notesField.setColumns(20);
        notesField.setRows(5);
        notesField.setName("notesField"); // NOI18N
        notesField.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                selectText(evt);
            }
        });
        notesField.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                captureEscape(evt);
            }
        });
        notesField.getDocument().addDocumentListener(this);
        jScrollPane1.setViewportView(notesField);

        javax.swing.GroupLayout detailsPanelLayout = new javax.swing.GroupLayout(detailsPanel);
        detailsPanel.setLayout(detailsPanelLayout);
        detailsPanelLayout.setHorizontalGroup(
            detailsPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(detailsPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(detailsPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jSeparator1, javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(detailsPanelLayout.createSequentialGroup()
                        .addGroup(detailsPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(hintLabel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addGroup(detailsPanelLayout.createSequentialGroup()
                                .addGroup(detailsPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addComponent(companyLabel)
                                    .addComponent(idLabel)
                                    .addComponent(streetLabel)
                                    .addComponent(cityLabel)
                                    .addComponent(contactLabel)
                                    .addComponent(phoneLabel)
                                    .addComponent(notesLabel))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(detailsPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(companyField)
                                    .addGroup(detailsPanelLayout.createSequentialGroup()
                                        .addGroup(detailsPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addComponent(idField, javax.swing.GroupLayout.PREFERRED_SIZE, 208, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addGroup(detailsPanelLayout.createSequentialGroup()
                                                .addComponent(streetField)
                                                .addGap(18, 18, 18)
                                                .addComponent(suiteLabel)))
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(suiteField, javax.swing.GroupLayout.PREFERRED_SIZE, 86, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addGroup(detailsPanelLayout.createSequentialGroup()
                                        .addComponent(cityField, javax.swing.GroupLayout.DEFAULT_SIZE, 139, Short.MAX_VALUE)
                                        .addGap(18, 18, 18)
                                        .addComponent(stateLabel)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(stateField, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                        .addComponent(zipLabel)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(zipField, javax.swing.GroupLayout.PREFERRED_SIZE, 104, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addGroup(detailsPanelLayout.createSequentialGroup()
                                        .addComponent(contactField, javax.swing.GroupLayout.PREFERRED_SIZE, 225, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(18, 18, 18)
                                        .addComponent(emailLabel)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(emailField))
                                    .addGroup(detailsPanelLayout.createSequentialGroup()
                                        .addComponent(phoneField, javax.swing.GroupLayout.PREFERRED_SIZE, 123, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                        .addComponent(faxLabel)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(faxField, javax.swing.GroupLayout.PREFERRED_SIZE, 123, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addComponent(jScrollPane1))))
                        .addContainerGap())))
        );
        detailsPanelLayout.setVerticalGroup(
            detailsPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(detailsPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(detailsPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(idLabel)
                    .addComponent(idField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jSeparator1, javax.swing.GroupLayout.PREFERRED_SIZE, 5, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addGroup(detailsPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(companyLabel)
                    .addComponent(companyField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(detailsPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(streetLabel)
                    .addComponent(streetField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(suiteLabel)
                    .addComponent(suiteField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(detailsPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cityLabel)
                    .addComponent(cityField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(stateLabel)
                    .addComponent(stateField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(zipLabel)
                    .addComponent(zipField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(detailsPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(contactLabel)
                    .addComponent(contactField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(emailLabel)
                    .addComponent(emailField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(detailsPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(phoneLabel)
                    .addComponent(phoneField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(faxField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(faxLabel))
                .addGap(18, 18, 18)
                .addGroup(detailsPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(detailsPanelLayout.createSequentialGroup()
                        .addComponent(notesLabel)
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 202, Short.MAX_VALUE))
                .addGap(18, 18, 18)
                .addComponent(hintLabel))
        );

        addWindowListener(new CustomerDialogAdapter());

        getContentPane().add(detailsPanel, java.awt.BorderLayout.CENTER);
        getRootPane().setDefaultButton(saveButton);
        program.addPropertyChangeListener("savable", this);
        setTitle(bundle.getString("NewCustomerDialog.title"));
        setIconImage(new ImageIcon(getClass().getResource(bundle.getString("Customer.icon"))).getImage());

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void selectText(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_selectText
        if (evt.getSource() instanceof JTextComponent) {
            JTextComponent txt = (JTextComponent) evt.getSource();
            
            if (!txt.getName().equals("notesField")) {
                txt.selectAll();
            } else {
                txt.select(txt.getText().length(), txt.getText().length());
            }
            
            switch (txt.getName()) {
                case "companyField":
                    hintLabel.setForeground(Color.red);
                    hintLabel.setText(bundle.getString("company.hint"));
                    hintLabel.setVisible(true);
                    break;
                case "streetField":
                    hintLabel.setForeground(Color.red);
                    hintLabel.setText(bundle.getString("street.hint"));
                    hintLabel.setVisible(true);
                    break;
                case "suiteField":
                    hintLabel.setForeground(Color.blue);
                    hintLabel.setText(bundle.getString("suite.hint"));
                    hintLabel.setVisible(true);
                    break;
                case "cityField":
                    hintLabel.setForeground(Color.red);
                    hintLabel.setText(bundle.getString("city.hint"));
                    hintLabel.setVisible(true);
                    break;
                case "stateField":
                    hintLabel.setForeground(Color.red);
                    hintLabel.setText(bundle.getString("state.hint"));
                    hintLabel.setVisible(true);
                    break;
                case "zipField":
                    hintLabel.setForeground(Color.red);
                    hintLabel.setText(bundle.getString("zip.hint"));
                    hintLabel.setVisible(true);
                    break;
                case "contactField":
                    hintLabel.setForeground(Color.blue);
                    hintLabel.setText(bundle.getString("contact.hint"));
                    hintLabel.setVisible(true);
                    break;
                case "phoneField":
                    hintLabel.setForeground(Color.blue);
                    hintLabel.setText(bundle.getString("phone.hint"));
                    hintLabel.setVisible(true);
                    break;
                case "emailField":
                    hintLabel.setForeground(Color.blue);
                    hintLabel.setText(bundle.getString("email.hint"));
                    hintLabel.setVisible(true);
                    break;
                case "faxField":
                    hintLabel.setForeground(Color.blue);
                    hintLabel.setText(bundle.getString("fax.hint"));
                    hintLabel.setVisible(true);
                    break;
                case "notesField":
                    hintLabel.setForeground(Color.blue);
                    hintLabel.setText(bundle.getString("notes.hint"));
                    hintLabel.setVisible(true);
                    break;
                default:
                    hintLabel.setVisible(false);
                    hintLabel.setText("");
            }
        }
    }//GEN-LAST:event_selectText

    private void captureEscape(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_captureEscape
        if (evt.getKeyCode() == KeyEvent.VK_ESCAPE) {
            cancelButton.getAction().actionPerformed(
                    new ActionEvent(this, evt.getID(), evt.toString()));
        }
    }//GEN-LAST:event_captureEscape
    
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JPanel bottomPanel;
    private javax.swing.JButton cancelButton;
    private javax.swing.JTextField cityField;
    private javax.swing.JLabel cityLabel;
    private javax.swing.JPanel commandPanel;
    private javax.swing.JTextField companyField;
    private javax.swing.JLabel companyLabel;
    private javax.swing.JTextField contactField;
    private javax.swing.JLabel contactLabel;
    private javax.swing.JPanel detailsPanel;
    private javax.swing.JSeparator dialogSeparator;
    private javax.swing.JTextField emailField;
    private javax.swing.JLabel emailLabel;
    private javax.swing.JFormattedTextField faxField;
    private javax.swing.JLabel faxLabel;
    private javax.swing.JButton helpButton;
    private javax.swing.JLabel hintLabel;
    private javax.swing.JTextField idField;
    private javax.swing.JLabel idLabel;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JTextArea notesField;
    private javax.swing.JLabel notesLabel;
    private javax.swing.JFormattedTextField phoneField;
    private javax.swing.JLabel phoneLabel;
    private javax.swing.JButton saveButton;
    private javax.swing.JTextField stateField;
    private javax.swing.JLabel stateLabel;
    private javax.swing.JTextField streetField;
    private javax.swing.JLabel streetLabel;
    private javax.swing.JTextField suiteField;
    private javax.swing.JLabel suiteLabel;
    private javax.swing.JTextField zipField;
    private javax.swing.JLabel zipLabel;
    // End of variables declaration//GEN-END:variables
   
    private void clearFields() {
        loading = true;
        
        idField.setText(String.valueOf(System.currentTimeMillis()));
        companyField.setText("");
        streetField.setText("");
        suiteField.setText("");
        cityField.setText("");
        stateField.setText("");
        zipField.setText("");
        contactField.setText("");
        emailField.setText("");
        phoneField.setText("");
        faxField.setText("");
        notesField.setText("");
        
        loading = false;
    }
    
    private boolean validateFields() {
        logger.entering("validateFields()");
        
        boolean results = companyProvided() && streetProvided() && cityProvided()
                && stateIsValid() && zipIsValid() && emailIsValid()
                && phoneIsValid() && faxIsValid();
        
        logger.exiting("validateFields() RETURNING: {0}", results);
        return results;
    }
    
    private boolean emailIsValid() {
        if (emailField != null && emailField.getText().length() > 0) {
            EmailValidator validator = new EmailValidator();
            return validator.validate(emailField.getText());
        } else {
            return true;
        }
    }
    
    private boolean phoneIsValid() {
        if (phoneField != null 
                && !phoneField.getText().equals("(   )    -    ")) {
            PhoneNumberValidator validator = new PhoneNumberValidator();
            return validator.validate(phoneField.getText());
        } else {
            return true;
        }
    }
    
    private boolean faxIsValid() {
        if (faxField != null && !faxField.getText().equals("(   )    -    ")) {
            PhoneNumberValidator validator = new PhoneNumberValidator();
            return validator.validate(faxField.getText());
        } else {
            return true;
        }
    }
    
    private boolean companyProvided() {
        return companyField.getText() != null  && companyField.getText().length() > 0;
    }
    
    private boolean streetProvided() {
        return streetField.getText() != null && streetField.getText().length() > 0;
    }
    
    private boolean cityProvided() {
        return cityField.getText() != null && cityField.getText().length() > 0;
    }
    
    private boolean stateIsValid() {
        StateOrProvinceValidator validator = new StateOrProvinceValidator();
        return stateField.getText() != null && stateField.getText().length() > 0
                && validator.validate(stateField.getText());
    }
    
    private boolean zipIsValid() {
        PostalCodeValidator validator = new PostalCodeValidator();
        return zipField.getText() != null && zipField.getText().length() > 0
                && validator.validate(zipField.getText());
    }
    
    private class SaveAction extends AbstractAction {

        private static final long serialVersionUID = -7232581254088203628L;

        public SaveAction() {
            super(bundle.getString("SaveAction.text"));
            
            initAction();
        }
        
        private void initAction() {
            putValue(SMALL_ICON, new ImageIcon(getClass().getResource(
                    bundle.getString("SaveAction.smallIcon"))));
            putValue(DISPLAYED_MNEMONIC_INDEX_KEY, Integer.valueOf(
                    bundle.getString("SaveAction.mnemonicIndex")));
            putValue(SHORT_DESCRIPTION, 
                    bundle.getString("SaveAction.shortDescription"));
        }
        
        @Override
        public void actionPerformed(ActionEvent e) {
            logger.entering("NewCustomerDialog.SaveAction.actionPerformed(e = {0})", e);
            
            logger.debug("Creating a new Customer object...");
            Customer customer = new Customer();
            
            logger.debug("Adding the data to the customer instance...");
            customer.setCompany(companyField.getText());
            customer.setStreet(streetField.getText());
            customer.setSuite(suiteField.getText());
            customer.setCity(cityField.getText());
            customer.setStateOrProvince(stateField.getText());
            customer.setPostalCode(zipField.getText());
            customer.setContact(contactField.getText());
            customer.setEmail(emailField.getText());
            customer.setPhone(phoneField.getText());
            customer.setFax(faxField.getText());
            customer.setNotes(notesField.getText());
            logger.log(Level.TRACE, "Data added. customer = {0}", customer);
            
            logger.debug("Creating the CustomersTable instance...");
            CustomersTable table = new CustomersTable();
            logger.log(Level.TRACE, "CustomersTable created: {0}\nOpening it.", table);
            boolean tableOpen = false;
            
            try {
                table.connect();
                tableOpen = true;
            } catch (DataStoreException ex) {
                logger.error(ex, "Attempting to open the table {0}", table);
            }
            logger.log(Level.TRACE, "Table open? {0}", tableOpen);
            
            if (tableOpen) {
                logger.debug("Adding the customer instance to the table.");
                table.add(customer);
                dirty = false;
                added = true;
                setSavable(dirty);
                logger.log(Level.TRACE, "Saved? {0}", added);
            }
            
            logger.debug("Closing the table...");
            try {
                table.close();
                logger.log(Level.TRACE, "Table closed.");
            } catch (DataStoreException ex) {
                logger.error(ex, "Attempting to close the table ({0}) after "
                        + "adding the data: {1}", table, customer);
            }
            
            table = null;
            customer = null;
            Runtime.getRuntime().gc();
            
            logger.exiting("NewCustomerDialog.SaveAction.actionPerformed(e)");
            if (canExit(e)) {
                willExit(e);
            }
        }
        
    }
    
    private class CancelAction extends AbstractAction {

        private static final long serialVersionUID = -6089139508449217744L;
        
        public CancelAction() {
            super(bundle.getString("CancelAction.text"));
            
            initAction();
        }
        
        private void initAction() {
            putValue(SMALL_ICON, new ImageIcon(getClass().getResource(
                    bundle.getString("CancelAction.smallIcon"))));
            putValue(DISPLAYED_MNEMONIC_INDEX_KEY, Integer.valueOf(
                    bundle.getString("CancelAction.mnemonicIndex")));
            putValue(SHORT_DESCRIPTION, 
                    bundle.getString("CancelAction.shortDescription"));
        }

        @Override
        public void actionPerformed(ActionEvent e) {
            willExit(e);
        }
        
    }
    
    private class HelpAction extends AbstractAction {

        private static final long serialVersionUID = 9195556010988315538L;
        
        public HelpAction() {
            super(bundle.getString("HelpAction.text"));
            
            initAction();
        }
        
        private void initAction() {
            putValue(SMALL_ICON, new ImageIcon(getClass().getResource(
                    bundle.getString("HelpAction.smallIcon"))));
            putValue(DISPLAYED_MNEMONIC_INDEX_KEY, Integer.valueOf(
                    bundle.getString("HelpAction.mnemonicIndex")));
            putValue(SHORT_DESCRIPTION, 
                    bundle.getString("HelpAction.shortDescription"));
        }

        @Override
        public void actionPerformed(ActionEvent e) {
            if (NetUtils.isNetworkConnectionAvailable()) {
                try {
                    Desktop.getDesktop().browse(new URI(bundle.getString("HelpAction.url")));
                } catch (URISyntaxException | IOException ex) {
                    logger.error(ex, "Unable to create URI for {0}", 
                            bundle.getString("HelpAction.url"));
                    MessageBox.showWarning("Unable to load help.", "Help Unavailable");
                }
            } else {
                // TODO: Need to create JavaHelp system.
            }
        }
        
    }
    
    private class CustomerDialogAdapter extends WindowAdapter {
        
        @Override
        public void windowClosing(WindowEvent e) {
            if (canExit(e)) {
                program.save(instance);

                dispose();
            }
        }

        @Override
        public void windowOpened(WindowEvent e) {
            logger.entering("CustomerDialogAdapter.windowOpened(e = {0})", e);
            
            idField.setText(String.valueOf(System.currentTimeMillis()));
            hintLabel.setVisible(false);
            companyField.requestFocusInWindow();
            setSavable(validateFields());
            save.setEnabled(savable);
            
            logger.exiting("CustomerDialogAdapter.windowOpened(e)");
        }
        
    }
}
