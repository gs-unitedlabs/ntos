/*
 * Copyright (C) 2022 GS United Labs
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * *****************************************************************************
 *  Project    :   ntos.database
 *  Class      :   AccountValidator.java
 *  Author     :   Sean Carrick
 *  Created    :   Mar 23, 2022
 *  Modified   :   Mar 23, 2022
 *  
 *  Purpose: See class JavaDoc for explanation
 *  
 *  Revision History:
 *  
 *  WHEN          BY                   REASON
 *  ------------  -------------------  -----------------------------------------
 *  Mar 23, 2022  Sean Carrick         Initial creation.
 * *****************************************************************************
 */

package com.gs.ntos.database.validators;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 *
 * @author Sean Carrick &lt;sean at gs-unitedlabs dot com&gt;
 * 
 * @version 0.1.0
 * @since 0.1.0
 */
public class VehicleNumberValidator {
    
    private String vehicleNumber;
    
    public VehicleNumberValidator (String vehicleNumber) {
        this.vehicleNumber = vehicleNumber;
    }
    
    public boolean isValid() {
        boolean result = false; // Default to not valid.
        int dashes = 0;
        
        for (int x = 0; x < vehicleNumber.length(); x++) {
            if (vehicleNumber.charAt(x) == '-') {
                dashes++;
            }
        }
        
        if (dashes > 1) {
            result = false;
        } else {
            Pattern validate = Pattern.compile("^[A-Z0-9-]+$");
            Matcher matcher = validate.matcher(vehicleNumber);
            result = matcher.matches();
        }
        
        return result;
    }

}
