/*
 * Copyright (C) 2022 GS United Labs
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * *****************************************************************************
 *  Project    :   NTOS
 *  Class      :   LoadsTable.java
 *  Author     :   Sean Carrick
 *  Created    :   Apr 13, 2022
 *  Modified   :   Apr 13, 2022
 *  
 *  Purpose: See class JavaDoc for explanation
 *  
 *  Revision History:
 *  
 *  WHEN          BY                   REASON
 *  ------------  -------------------  -----------------------------------------
 *  Apr 13, 2022  Sean Carrick         Initial creation.
 * *****************************************************************************
 */

package com.gs.ntos.database.tables;

import com.gs.ntos.database.errors.DataStoreException;
import com.gs.ntos.database.errors.InvalidDataException;
import com.gs.ntos.database.errors.ReferentialIntegrityException;
import com.gs.ntos.database.models.Broker;
import com.gs.ntos.database.models.Load;

/**
 *
 * @author Sean Carrick &lt;sean at gs-unitedlabs dot com&gt;
 * 
 * @version 1.0
 * @since 1.0
 */
public class LoadsTable extends GenericTable<Load> {
    
    public LoadsTable () {
        super("loads", Load.class);
    }

    @Override
    public boolean add(Load record) {
        if (record == null) {
            return false;
        }
        if (doesRecordExistInTable(record)) {
            throw new InvalidDataException("The record with the Order # " 
                    + record.getOrderNumber()+ " and Trip # " 
                    + record.getTripNumber() + " already exists in the table. "
                            + "Call update(Load) instead.");
        }
        
        boolean result = records.add(record);
        
        try {
            save();
        } catch (DataStoreException ex) {
            logger.error(ex, "Attempting to save data after new record added: "
                    + "{0}", record);
        }
        
        return result;
    }

    @Override
    public boolean delete(Load record) throws ReferentialIntegrityException {
        if (record == null) {
            return false;
        }
        if (!doesRecordExistInTable(record)) {
            return false;
        }
        if (isRecordReferenced(record)){
            throw new ReferentialIntegrityException("Load, "
                    + record.getOrderNumber()+ ", has foreign key references.\n\n"
                    + "These references must be deleted before this account "
                    + "type record can be deleted.");
        }
        
        int idx = records.indexOf(record);
        boolean result = records.remove(record);
        
        if (idx >= records.size() && idx == getCurrentRecordNumber()){
            setCurrentRecordNumber(records.size() - 1);
        } else {
            setCurrentRecordNumber(idx);
        }
        
        return result;
    }

    @Override
    protected boolean doesRecordExistInTable(Load record) {
        for (Load l : records) {
            if (l.getId() == record.getId()
                    || l.getOrderNumber().equals(record.getOrderNumber())) {
                return true;
            }
        }
        
        return false;
    }
    
    public Load get(long id) {
        for (Load l : records) {
            if (l.getId() == id) {
                return l;
            }
        }
        
        return null;
    }

    public long getIdForLoadOrderNumber(String order) {
        for (Load l : records) {
            if (l.getOrderNumber().equals(order)) {
                return l.getId();
            }
        }
        
        return -1L;
    }

    @Override
    protected boolean isRecordReferenced(Load record) {
        StopsTable table = new StopsTable();
        boolean referenced = false;
        try {
            table.connect();
            referenced = table.isLoadReferenced(record);
            table.close();
        } catch (DataStoreException ex) {
            logger.error(ex, "Attempting to connect to the Stops table {0}", 
                    table);
        }
        
        return referenced;
    }
    
    public boolean isBrokerReferenceByLoad(Broker broker) {
        for (Load l : records) {
            if (l.getBrokerId() == broker.getId()) {
                return true;
            }
        }
        
        return false;
    }

    @Override
    protected void save() throws DataStoreException {
        super.save();
        program.firePropertyChange("loadsAvailable", !encryptedFile.exists(), 
                encryptedFile.exists());
    }

    @Override
    public boolean update(Load record) {
        if (record == null) {
            return false;
        }
        if (!doesRecordExistInTable(record)) {
            throw new InvalidDataException("Load with Order # " 
                    + record.getOrderNumber() + " does not exist in the table. "
                    + "Call add(Load) instead.");
        }
        
        for (Load l : records) {
            if (l.getId() == record.getId()) {
                int idx = records.indexOf(l);
                records.remove(idx);
                records.add(idx, record);
                return true;
            }
        }
        
        return false;
    }

}
