/*
 * Copyright (C) 2022 GS United Labs
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * *****************************************************************************
 *  Project    :   ntos.database
 *  Class      :   Account.java
 *  Author     :   Sean Carrick
 *  Created    :   Mar 19, 2022
 *  Modified   :   Mar 19, 2022
 *  
 *  Purpose: See class JavaDoc for explanation
 *  
 *  Revision History:
 *  
 *  WHEN          BY                   REASON
 *  ------------  -------------------  -----------------------------------------
 *  Mar 19, 2022  Sean Carrick         Initial creation.
 * *****************************************************************************
 */

package com.gs.ntos.database.models;

import java.io.Serializable;

/**
 * This Java Bean defines the data that is required to be collected for accounts
 * for the accounting system.
 * <p>
 * The data fields in this Bean are declared as } protected} in case the
 * situation arises in the future that this Bean needs to be extended. In that
 * situation, the subclass will be able to access the data fields directly, and
 * the documentation should describe the length and requirements of the data.</p>
 * 
 * @author Sean Carrick &lt;sean at gs-unitedlabs dot com&gt;
 * 
 * @version 0.1.0
 * @since 0.1.0
 */
public class Account implements SelfStorage<Account>, Serializable {

    private static final long serialVersionUID = -7741376601192378337L;
    
    /** Required. Must be unique. <strong>Read-Only</strong>*/
    private long id;
    /** Required. <em>Should be</em> unique. Maximum length: 30 characters. */
    private String name;
    /** Optional. `null` is OK. Indeterminate length. */
    private String description;
    /** Required. <em>Foreign Key</em>: } AccountType.getId()}. */
    private long typeId;
    
    public Account () {
        id = System.currentTimeMillis();
    }

    /**
     * Retrieves the unique ID for this } Account} object. 
     * 
     * @return the unique ID
     */
    public long getId() {
        return id;
    }

    /**
     * Sets the ID for the } Account}. This is typically only used when
     * loading a record from a data source.
     * 
     * @param id the unique ID
     * @throws IllegalArgumentException in the event the supplied `id` is
     *          less than or equal to zero
     */
    private void setId(long id) {
        if (id <= 0) {
            throw new IllegalArgumentException(id + " <= 0");
        }
        this.id = id;
    }

    /**
     * Retrieves the name of this } Account}.
     * 
     * @return the account name
     */
    public String getName() {
        return name;
    }

    /**
     * Sets the name of this } Account}. This field is <strong>required
     * </strong> and <em>should be</em> unique, but does not have to be. This
     * field has a maximum length of thirty (30) characters. If the specified
     * } typeName} is greater than thirty characters in length, it will be
     * truncated.
     * 
     * @param name the account name
     * @throws IllegalArgumentException in the event } typeName} is blank,
     *          empty, or `null`
     */
    public void setName(String name) {
        if (name == null || name.isBlank() || name.isEmpty()) {
            throw new IllegalArgumentException("blank, empty, or null name");
        } else if (name.length() > 30) {
            name = name.substring(0, 30);
        }
        this.name = name;
    }

    /**
     * Retrieves the description of this } Account}.
     * 
     * @return the account's description
     */
    public String getDescription() {
        return description;
    }

    /**
     * Sets the description for this } Account}. This field is <em>
     * optional</em>, but is recommended. A value of `null` is OK. There 
     * is a maximum length of three hundred (300) characters set for this field.
     * 
     * @param description the account's description
     */
    public void setDescription(String description) {
        if (description == null) {
            description = "";
        }
        this.description = description;
    }

    /**
     * Retrieves the unique ID for the `AccountType` of which this
     * } Account} is.
     * 
     * @return the type ID
     */
    public long getTypeId() {
        return typeId;
    }

    /**
     * Sets the unique ID for the `AccountType` of which this } 
     * Account} is. The } typeId} value supplied must have a corresponding
     * record in the `AccountType` data store.
     * 
     * @param typeId the type ID
     */
    public void setTypeId(long typeId) {
        this.typeId = typeId;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        
        sb.append(getName()).append("@").append(hashCode());
        sb.append(": [id=").append(getId()).append("]");
        
        return sb.toString();
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null || !(obj instanceof Account)) {
            return false;
        }
        
        Account other = (Account) obj;
        
        return (getId() == other.getId())
                && (getName().equals(other.getName()))
                && (hashCode() == other.hashCode());
    }

    @Override
    public int hashCode() {
        int hash = Long.valueOf(getId()).hashCode();
        hash += (getName() == null) ? 255 : getName().hashCode();
        hash += (getDescription() == null) ? 255 : getDescription().hashCode();
        
        return hash;
    }

    @Override
    public String store() {
        StringBuilder sb = new StringBuilder();
        
        sb.append(getId()).append("~").append(getName()).append("~");
        sb.append(getTypeId()).append("~").append(getDescription());
        
        return sb.toString() + "\n";
    }

    @Override
    public Account restore(String line) {
        String[] fields = line.split("~");
        
        setId(Long.valueOf(fields[0]));
        setName(fields[1]);
        setTypeId(Long.valueOf(fields[2]));
        
        if (fields.length == 4) {
            setDescription(fields[3]);
        } else {
            setDescription("");
        }
        
        return this;
    }

}
