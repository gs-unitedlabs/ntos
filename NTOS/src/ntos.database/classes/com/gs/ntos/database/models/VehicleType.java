/*
 * Copyright (C) 2022 GS United Labs
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * *****************************************************************************
 *  Project    :   ntos.database
 *  Class      :   VehicleType.java
 *  Author     :   Sean Carrick
 *  Created    :   Mar 25, 2022
 *  Modified   :   Mar 25, 2022
 *  
 *  Purpose: See class JavaDoc for explanation
 *  
 *  Revision History:
 *  
 *  WHEN          BY                   REASON
 *  ------------  -------------------  -----------------------------------------
 *  Mar 25, 2022  Sean Carrick         Initial creation.
 * *****************************************************************************
 */

package com.gs.ntos.database.models;

/**
 * This Java Bean defines the data that is required to be collected for a vehicle
 * type.
 * <p>
 * The data fields in this Bean are declared as } protected} in case the
 * situation arises in the future that this Bean needs to be extended. In that
 * situation, the subclass will be able to access the data fields directly, and
 * the documentation should describe the length and requirements of the data.</p>
 *
 * @author Sean Carrick &lt;sean at gs-unitedlabs dot com&gt;
 * 
 * @version 0.1.0
 * @since 0.1.0
 */
public class VehicleType {
    
    private long id;
    private String type;
    private String description;
    
    public VehicleType () {
        id = System.currentTimeMillis();
    }

    /**
     * Retrieves the type of vehicle, such as tractor, trailer, etc.
     * 
     * @return the vehicle type
     */
    public String getType() {
        return type;
    }

    /**
     * Sets the type of vehicle, such as tractor, trailer, etc. This field is
     * required and has a maximum length of thirty (30) characters. If the
     * specified } type} is longer than the maximum length, it will be
     * truncated.
     * 
     * @param type 
     * @throws IllegalArgumentException if } type} is blank, empty, or
     *          `null`
     */
    public void setType(String type) {
        if (type == null || type.isBlank() || type.isEmpty()) {
            throw new IllegalArgumentException("blank, empty, or null type");
        } else if (type.length() > 30) {
            type = type.substring(0, 30);
        }
        
        this.type = type;
    }

    /**
     * Retrieves the description of this } VehicleType}.
     * 
     * @return the vehicle type description
     */
    public String getDescription() {
        return description;
    }

    /**
     * Sets the description of this } VehicleType}. This field is optional,
     * but `null` values are not allowed. If a `null` value is 
     * supplied, it will be converted to an empty string and no exception will 
     * be thrown.
     * 
     * @param description the vehicle type description
     */
    public void setDescription(String description) {
        if (description == null) {
            description = "";
        }
        
        this.description = description;
    }

    public long getId() {
        return id;
    }

}
