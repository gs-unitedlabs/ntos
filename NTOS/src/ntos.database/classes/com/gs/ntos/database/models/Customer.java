/*
 * Copyright (C) 2022 GS United Labs
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * *****************************************************************************
 *  Project    :   ntos.database
 *  Class      :   Customer.java
 *  Author     :   Sean Carrick
 *  Created    :   Mar 19, 2022
 *  Modified   :   Mar 19, 2022
 *  
 *  Purpose: See class JavaDoc for explanation
 *  
 *  Revision History:
 *  
 *  WHEN          BY                   REASON
 *  ------------  -------------------  -----------------------------------------
 *  Mar 19, 2022  Sean Carrick         Initial creation.
 * *****************************************************************************
 */

package com.gs.ntos.database.models;

import com.gs.ntos.database.errors.InvalidEmailException;
import com.gs.ntos.database.errors.InvalidPhoneNumberException;
import com.gs.ntos.database.errors.InvalidPostalCodeException;
import com.gs.ntos.database.errors.InvalidStateOrProvinceException;
import com.gs.ntos.validators.EmailValidator;
import com.gs.ntos.validators.PhoneNumberValidator;
import com.gs.ntos.validators.PostalCodeValidator;
import com.gs.ntos.validators.StateOrProvinceValidator;
import java.io.Serializable;

/**
 * This Java Bean defines the data that is required to be collected for customers.
 * <p>
 * The data fields in this Bean are declared as `protected in case the
 * situation arises in the future that this Bean needs to be extended. In that
 * situation, the subclass will be able to access the data fields directly, and
 * the documentation should describe the length and requirements of the data.</p>
 *
 * @author Sean Carrick &lt;sean at gs-unitedlabs dot com&gt;
 * 
 * @version 0.1.0
 * @since 0.1.0
 */
public class Customer implements SelfStorage<Customer>, Serializable {

    private static final long serialVersionUID = 6271140995703122837L;
    
    /** Required. Must be unique. */
    private long id;
    /** Required. `null` is not allowed. Length is 40 characters max. */
    private String company;
    /** Required. `null` is not allowed. Length is 50 characters max. */
    private String street;
    /** Optional. `null` is OK. Length is 15 characters max. */
    private String suite;
    /** Required. `null` is not allowed. Length is 50 characters max. */
    private String city;
    /** Required. `null` is not allowed. Length is 2 characters max. */
    private String stateOrProvince;
    /** Required. `null` is not allowed. Length is 10 characters max. */
    private String postalCode;
    /** Optional. `null` is OK. Length is 40 characters max. */ 
    private String contact;
    /** Optional. `null` is OK. Length is 14 characters max. */
    private String phone;
    /** Optional. `null` is OK. Length is 40 characters max. */
    private String email;
    /** Optional. `null` is OK. Length is 14 characters max. */
    private String fax;
    /** Optional. `null` is not allowed. Length is indeterminate. */
    private String notes;
    
    /**
     * Constructs a new, default `Customer` object. This constructor
     * automatically assigns a unique ID to the object.
     */
    public Customer () {
        id = System.currentTimeMillis();
    }

    /**
     * Retrieves the unique ID for this `Customer` object. 
     * 
     * @return the unique ID
     */
    public long getId() {
        return id;
    }

    /**
     * Sets the ID for the `Customer`. This is typically only used when
     * loading a record from a data source.
     * 
     * @param id the unique ID
     * @throws IllegalArgumentException in the event the supplied `id` is
     *          less than or equal to zero
     */
    private void setId(long id) {
        if (id <= 0L) {
            throw new IllegalArgumentException("invalid id");
        }
        this.id = id;
    }

    /**
     * Retrieves the name of the company for this `Customer`.
     * 
     * @return the company name
     */
    public String getCompany() {
        return company;
    }

    /**
     * Sets the name of the company for this `Customer`. This is a <strong>
     * required</strong> field and has a maximum length of forty (40)
     * characters. If the supplied `company` is longer than the allowed
     * length, it will be truncated.
     * 
     * @param company the company name
     * @throws IllegalArgumentException in the event the specified `company`
     *          is blank, empty, or `null`
     */
    public void setCompany(String company) {
        if (company == null || company.isBlank() || company.isEmpty()) {
            throw new IllegalArgumentException("blank, empty, or null company");
        } else if (company.length() > 40) {
            company = company.substring(0, 39);
        }
        this.company = company;
    }

    /**
     * Retrieves the street address for this `Customer`.
     * 
     * @return the street address
     */
    public String getStreet() {
        return street;
    }

    /**
     * Sets the street address for this `Customer`. This is a <strong>
     * required</strong> field and has a maximum length of fifty (50) characters.
     * If the specified `street` is longer than the maximum length, it
     * will be truncated.
     * 
     * @param street the street address
     * @throws IllegalArgumentException in the event the specified `street`
     *          is blank, empty, or `null`
     */
    public void setStreet(String street) {
        if (street == null || street.isBlank() || street.isEmpty()) {
            throw new IllegalArgumentException("blank, empty, or null street");
        } else if (street.length() > 50) {
            street = street.substring(0, 49);
        }
        this.street = street;
    }

    /**
     * Retrieves the suite portion of the address for this `Customer`.
     * 
     * @return the suite number
     */
    public String getSuite() {
        return suite;
    }

    /**
     * Sets the suite portion of the address for this `Customer`.
     * This field is <em>optional</em>, `null`s are OK, and it has a 
     * maximum length of fifteen (15) characters. If a `null` value is
     * passed it, it will be converted to an empty string. If the value of 
     * `suite` is longer than fifteen, it will be truncated.
     * 
     * @param suite the suite number
     */
    public void setSuite(String suite) {
        if (suite == null) {
            suite = "";
        } else if (suite.length() > 15) {
            suite = suite.substring(0, 14);
        }
        this.suite = suite;
    }

    /**
     * Retrieves the name of the city for this `Customer`.
     * 
     * @return the city name
     */
    public String getCity() {
        return city;
    }

    /**
     * Sets the name of the city for this `Customer`. This is a <strong>
     * required</strong> field and has a maximum length of fifty (50)
     * characters. If the specified `city` has a length longer than fifty,
     * it will be truncated.
     * 
     * @param city the city name
     * @throws IllegalArgumentException in the event that the specified `city`
     *          is blank, empty, or `null`
     */
    public void setCity(String city) {
        if (city == null || city.isBlank() || city.isEmpty()) {
            throw new IllegalArgumentException("blank, empty, or null city");
        } else if (city.length() >= 50) {
            city = city.substring(0, 49);
        }
        this.city = city;
    }

    /**
     * Retrieves the US State or Canadian Province for this `Customer`.
     * 
     * @return the state or province
     */
    public String getStateOrProvince() {
        return stateOrProvince;
    }

    /**
     * Sets the US State or Canadian Province abbreviation for this `Customer`.
     * This is a <strong>required</strong> field and has a maximum
     * length of two (2) characters. Furthermore, the `stateOrProvince`
     * supplied as an argument <em>must be</em> a valid US State or Canadian
     * Province abbreviation.
     * 
     * @param stateOrProvince the state or province abbreviation
     * @throws IllegalArgumentException in the event `stateOrProvince` is
     *          blank, empty, or `null`
     * @throws InvalidStateOrProvinceException in the event that } 
     *          stateOrProvince} is not a valid US State or Canadian Province
     */
    public void setStateOrProvince(String stateOrProvince) {
        if (stateOrProvince == null || stateOrProvince.isBlank()
                || stateOrProvince.isEmpty()) {
            throw new IllegalArgumentException("blank, empty, or null "
                    + "stateOrProvince");
        } else if (!(new StateOrProvinceValidator().validate(stateOrProvince))) {
            throw new InvalidStateOrProvinceException(stateOrProvince + " is "
                    + "not a valid US State or Canadian Province abbreviation.");
        }
        this.stateOrProvince = stateOrProvince;
    }

    /**
     * Retrieves the postal code for this `Customer`.
     * 
     * @return the postal code
     */
    public String getPostalCode() {
        return postalCode;
    }

    /**
     * Sets the postal code for this `Customer`. This is a <strong>required
     * </strong> field and has a maximum length of ten (10) characters. 
     * Furthermore, the supplied `postalCode` must be a valid US Zip Code
     * or Canadian Postal Code.
     * 
     * @param postalCode the postal code
     * @throws IllegalArgumentException in the event `postalCode` is blank,
     *          empty, or `null`
     * @throws InvalidPostalCodeException in the event `postalCode` is not
     *          a valid US Zip Code or Canadian Postal Code
     */
    public void setPostalCode(String postalCode) {
        if (postalCode == null || postalCode.isBlank() || postalCode.isEmpty()) {
            throw new IllegalArgumentException("blank, empty, or null postalCode");
        } else if (!(new PostalCodeValidator().validate(postalCode))) {
            throw new InvalidPostalCodeException(postalCode + " is not a valid "
                    + "US Zip Code nor Canadian Postal Code.");
        }
        this.postalCode = postalCode;
    }

    /**
     * Retrieves the name of the point of contact at this `Customer`.
     * 
     * @return the point of contact's name
     */
    public String getContact() {
        return contact;
    }

    /**
     * Sets the name of the point of contact at this `Customer`. This is
     * an <em>optional</em> field and has a maximum length of forty (40) 
     * characters, and `null` is OK. If `contact` has a length 
     * greater than forty, it will be truncated. If `null` is passed in,
     * it will be converted to an empty string.
     * 
     * @param contact the point of contact's name
     */
    public void setContact(String contact) {
        if (contact == null) {
            contact = "";
        } else if (contact.length() > 40) {
            contact = contact.substring(0, 39);
        }
        this.contact = contact;
    }

    /**
     * Retrieves the phone number for this `Customer` in the same format
     * in which it was stored.
     * 
     * @return the customer's phone number
     */
    public String getPhone() {
        return phone;
    }

    /**
     * Sets the phone number for this `Customer`. No formatting is 
     * erformed on supplied phone numbers. This is an <em>optional</em> field 
     * and has a maximum length of fourteen (14) characters and `null` is
     * OK. The specified `phone`, if any, <em>must be</em> a valid phone 
     * number.
     * 
     * @param phone the phone number
     * @throws InvalidPhoneNumberException in the event a phone number is
     *          specified, but is not a valid phone number
     */
    public void setPhone(String phone) {
        if (phone == null) {
            phone = "";
        } else if (!phone.equals("(   )    -    ") 
                && !(new PhoneNumberValidator().validate(phone))) {
            throw new InvalidPhoneNumberException(phone + " is not a valid "
                    + "phone number");
        }
        this.phone = phone;
    }

    /**
     * Retrieves the email address for this `Customer`.
     * 
     * @return the email address
     */
    public String getEmail() {
        return email;
    }

    /**
     * Sets the email address for this `Customer`. This is an <em>optional
     * </em> field with a maximum length of forty (40) characters, and `null`
     * is OK. If an email address is provided, it will be validated.
     * 
     * @param email the email address
     * @throws InvalidEmailException in the event the specified `email` is
     *          not a valid email address
     */
    public void setEmail(String email) {
        if (email == null) {
            email = "";
        } else if (!email.isBlank() && !email.isEmpty() 
                && !(new EmailValidator().validate(email))) {
            throw new InvalidEmailException(email + " is not a valid email "
                    + "address.");
        }
        this.email = email;
    }

    /**
     * Retrieves the fax number for this `Customer` in the same format in
     * which it was stored.
     * 
     * @return the fax number
     */
    public String getFax() {
        return fax;
    }

    /**
     * Sets the fax number for this `Customer`. No formatting is 
     * erformed on supplied phone numbers. This is an <em>optional</em> field 
     * and has a maximum length of fourteen (14) characters and `null` is
     * OK. The specified `fax`, if any, <em>must be</em> a valid phone 
     * number.
     * 
     * @param fax the fax number
     * @throws InvalidPhoneNumberException in the event a fax number is
     *          specified, but is not a valid fax number
     */
    public void setFax(String fax) {
        if (fax == null) {
            fax = "";
        } else if (!fax.equals("(   )    -    ")    
                && !(new PhoneNumberValidator().validate(fax))) {
            throw new InvalidPhoneNumberException(fax + " is not a valid "
                    + "fax number");
        }
        this.fax = fax;
    }

    /**
     * Retrieves any notes that have been stored regarding this `Customer`.
     * 
     * @return any notes
     */
    public String getNotes() {
        return notes;
    }

    /**
     * Sets any notes that need to be made about this `Customer`. This is
     * an <em>optional</em> field and `null` is OK. There is no set
     * maximum length for this field.
     * 
     * @param notes any notes
     */
    public void setNotes(String notes) {
        if (notes == null) {
            notes = "";
        }
        this.notes = notes;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        
        sb.append(getCompany()).append("@").append(hashCode());
        sb.append("[id=").append(getId()).append(";city=");
        sb.append(getCity()).append(";state=").append(getStateOrProvince());
        sb.append("]").append("@").append(Integer.toHexString(hashCode()));
        
        return sb.toString();
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null || !(obj instanceof Customer)) {
            return false;
        }
        
        Customer other = (Customer) obj;
        
        return (getId() == other.getId())
                && (getCompany().equals(other.getCompany()))
                && (getStreet().equals(other.getStreet()))
                && (getCity().equals(other.getCity()))
                && (getStateOrProvince().equals(other.getStateOrProvince()))
                && (getPostalCode().equals(other.getPostalCode()))
                && (hashCode() == other.hashCode());
    }

    @Override
    public int hashCode() {
        int hash = Long.valueOf(getId()).hashCode();
        hash += getCompany() != null ? getCompany().hashCode() : 1234;
        hash += getStreet() != null ? getStreet().hashCode() : 4321;
        hash += getSuite() != null ? getSuite().hashCode() : 9854;
        hash += getCity() != null ? getCity().hashCode() : 5764;
        hash += getStateOrProvince() != null ? getStateOrProvince().hashCode() : 8735;
        hash += getPostalCode() != null ? getPostalCode().hashCode() : 4185;
        hash += getContact() != null ? getContact().hashCode() : 1568;
        hash += getEmail() != null ? getEmail().hashCode() : 7463;
        hash += getPhone().hashCode();
        hash += getFax().hashCode();
        hash += getNotes() != null ? getNotes().hashCode() : 4836;
        hash /= 12;
        
        return hash;
    }
    
    /**
     * Retrieves the entire address for the customer. The address will be returned 
     * as:
     * <pre>
     * Contact Name
     * Company Name
     * Street Address[, SuiteNumber]
     * City, ST Postal
     * </pre>
     * The value returned from this method is formatted just right for use on
     * envelopes or letterhead.
     * 
     * @return the customer's address (formatted as above) or an empty string
     */
    public String getPrintableAddress() {
        if (id <= 0L) {
            throw new IllegalArgumentException(id + " <= 0");
        }
        
        StringBuilder sb = new StringBuilder();
        
        sb.append(getContact()).append("\n");
        sb.append(getCompany()).append("\n");
        sb.append(getStreet());
        
        if (getSuite() == null || getSuite().isBlank() || getSuite().isEmpty()) {
            sb.append("\n");
        } else {
            sb.append(", ").append(getSuite()).append("\n");
        }
        
        sb.append(getCity()).append(", ").append(getStateOrProvince()).append(" ");
        sb.append(getPostalCode());
        
        if (sb.toString() == null) {
            sb = new StringBuilder();
            sb.append("");
        }
        
        return sb.toString();
    }

    @Override
    public String store() {
        StringBuilder sb = new StringBuilder();
        
        sb.append(getId()).append("~").append(getCompany()).append("~");
        sb.append(getStreet()).append("~").append(getSuite()).append("~");
        sb.append(getCity()).append("~").append(getStateOrProvince()).append("~");
        sb.append(getPostalCode()).append("~").append(getContact()).append("~");
        sb.append(getEmail()).append("~").append(getPhone()).append("~");
        sb.append(getFax()).append("~").append(getNotes());
        
        return sb.toString() + "\n";
    }

    @Override
    public Customer restore(String line) {
        String[] fields = line.split("~");
        
        setId(Long.valueOf(fields[0]));
        setCompany(fields[1]);
        setStreet(fields[2]);
        setSuite(fields[3]);
        setCity(fields[4]);
        setStateOrProvince(fields[5]);
        setPostalCode(fields[6]);
        setContact(fields[7]);
        setEmail(fields[8]);
        setPhone(fields[9]);
        setFax(fields[10]);
        if (fields.length == 12) {
            setNotes(fields[11]);
        } else {
            setNotes("");
        }
        
        return this;
    }

}
