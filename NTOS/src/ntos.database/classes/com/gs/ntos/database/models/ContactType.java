/*
 * Copyright (C) 2022 GS United Labs
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * *****************************************************************************
 *  Project    :   ntos.database
 *  Class      :   ContactTypes.java
 *  Author     :   Sean Carrick
 *  Created    :   Mar 19, 2022
 *  Modified   :   Mar 19, 2022
 *  
 *  Purpose: See class JavaDoc for explanation
 *  
 *  Revision History:
 *  
 *  WHEN          BY                   REASON
 *  ------------  -------------------  -----------------------------------------
 *  Mar 19, 2022  Sean Carrick         Initial creation.
 * *****************************************************************************
 */

package com.gs.ntos.database.models;

import java.io.Serializable;

/**
 * This Java Bean defines the data that is required to be collected for contact
 * types.
 * <p>
 * The data fields in this Bean are declared as } protected} in case the
 * situation arises in the future that this Bean needs to be extended. In that
 * situation, the subclass will be able to access the data fields directly, and
 * the documentation should describe the length and requirements of the data.</p>
 *
 * @author Sean Carrick &lt;sean at gs-unitedlabs dot com&gt;
 * 
 * @version 0.1.0
 * @since 0.1.0
 */
public class ContactType implements Serializable {

    private static final long serialVersionUID = -7172811676508455396L;
    
    /** Required. Must be unique. */
    private long id;
    /** Required. <em>Should be</em> unique, but not a requirement. Length is a
        maximum of fifteen (15) characters. */
    private String typeName;
    /** Optional. Length is a maximum of three hundred (300) characters.
        `null` is OK. */
    private String description;
    
    public ContactType () {
        id = System.currentTimeMillis();
    }

    /**
     * Retrieves the unique ID for this } ContactType}.
     * 
     * @return the unique ID
     */
    public long getId() {
        return id;
    }

    /**
     * Sets the unique ID for this } ContactType}. This method is typically
     * used for initializing a } ContactType} from the data store.
     * 
     * @param id the unique ID
     * @throws IllegalArgumentException in the event the specified `id` is
     *          less than or equal to zero
     */
    public void setId(long id) {
        if (id <= 0) {
            throw new IllegalArgumentException("invalid id");
        }
        this.id = id;
    }

    /**
     * Retrieves the name of this } ContactType}.
     * 
     * @return the type name
     */
    public String getTypeName() {
        return typeName;
    }

    /**
     * Sets the name of this } ContactType}. This field is <strong>required
     * </strong> and <em>should be</em> unique, but does not have to be. This
     * field has a maximum length of fifteen (15) characters. If the specified
     * } typeName} is greater than fifteen characters in length, it will be
     * truncated.
     * 
     * @param typeName the type name
     * @throws IllegalArgumentException in the event } typeName} is blank,
     *          empty, or `null`
     */
    public void setTypeName(String typeName) {
        if (typeName == null || typeName.isBlank() || typeName.isEmpty()) {
            throw new IllegalArgumentException("blank, empty, or null typeName");
        } else if (typeName.length() > 15) {
            typeName = typeName.substring(0, 15);
        }
        this.typeName = typeName;
    }

    /**
     * Retrieves the description of this } ContactType}.
     * 
     * @return the type's description
     */
    public String getDescription() {
        return description;
    }

    /**
     * Sets the description for this } ContactType}. This field is <em>
     * optional</em>, but is recommended. A value of `null` is OK. There 
     * is a maximum length of three hundred (300) characters set for this field.
     * 
     * @param description the type's description
     */
    public void setDescription(String description) {
        if (description == null) {
            description = "";
        }
        this.description = description;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        
        sb.append(getTypeName()).append("@").append(hashCode());
        sb.append("[id=").append(getId()).append("]");
        
        return sb.toString();
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null || !(obj instanceof ContactType)) {
            return false;
        }
        
        ContactType other = (ContactType) obj;
        
        return (getId() == other.getId())
                && (getTypeName().equals(other.getTypeName()))
                && (getDescription().equals(other.getDescription()))
                && (hashCode() == other.hashCode());
    }

    @Override
    public int hashCode() {
        int hash = Long.valueOf(getId()).hashCode();
        hash += getTypeName().hashCode();
        hash += getDescription().hashCode();
        
        return hash;
    }

}
