/*
 * Copyright (C) 2022 GS United Labs
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * *****************************************************************************
 *  Project    :   ntos.database
 *  Class      :   TransactionCode.java
 *  Author     :   Sean Carrick
 *  Created    :   Mar 19, 2022
 *  Modified   :   Mar 19, 2022
 *  
 *  Purpose: See class JavaDoc for explanation
 *  
 *  Revision History:
 *  
 *  WHEN          BY                   REASON
 *  ------------  -------------------  -----------------------------------------
 *  Mar 19, 2022  Sean Carrick         Initial creation.
 * *****************************************************************************
 */

package com.gs.ntos.database.models;

import java.io.Serializable;

/**
 * This Java Bean defines the data that is required to be collected for 
 * transaction codes.
 * <p>
 * The data fields in this Bean are declared as } protected} in case the
 * situation arises in the future that this Bean needs to be extended. In that
 * situation, the subclass will be able to access the data fields directly, and
 * the documentation should describe the length and requirements of the data.</p>
 *
 * @author Sean Carrick &lt;sean at gs-unitedlabs dot com&gt;
 * 
 * @version 0.1.0
 * @since 0.1.0
 */
public class TransactionCode implements SelfStorage<TransactionCode>, Serializable {

    private static final long serialVersionUID = 226594396702670320L;
    
    /** Required. Must be unique. */
    private long id;
    /** Required. <strong><em>Should be</em></strong> unique. Max length is 50. */
    private String txCode;
    /** Optional. Length is a maximum of three hundred (300) characters.
        `null` is OK. */
    private String description;
    
    /**
     * Constructs a new } TransactionCode} object. This constructor
     * establishes a unique ID for this object.
     */
    public TransactionCode () {
        id = System.currentTimeMillis();
    }

    /**
     * Retrieves the unique ID for this } TransactionCode} object. 
     * 
     * @return the unique ID
     */
    public long getId() {
        return id;
    }

    /**
     * Sets the ID for the } TransactionCode}. This is typically only used 
     * when loading a record from a data source.
     * 
     * @param id the unique ID
     * @throws IllegalArgumentException in the event the supplied `id` is
     *          less than or equal to zero
     */
    public void setId(long id) {
        if (id <= 0) {
            throw new IllegalArgumentException(id + " <= 0");
        }
        this.id = id;
    }

    /**
     * Retrieves the name of the } TransactionCode} object.
     * 
     * @return the code's name
     */
    public String getTxCode() {
        return txCode;
    }

    /**
     * Sets the name of the } TransactionCode} object. This field is 
     * <strong>required</strong> and <em>should be</em> unique, though it does
     * not have to be. The maximum length for this field is fifty (50)
     * characters. If the specified } txCode.length()} is longer than
     * fifty characters, it will be truncated.
     * 
     * @param txCode the code's name
     * @throws IllegalArgumentException in the event that } txCode} is 
     *          blank, empty, or `null`
     */
    public void setTxCode(String txCode) {
        if (txCode == null || txCode.isBlank() || txCode.isEmpty()) {
            throw new IllegalArgumentException("blank, empty, or null txCode");
        } else if(txCode.length() > 50) {
            txCode = txCode.substring(0, 50);
        }
        this.txCode = txCode;
    }

    /**
     * Retrieves the description of this } TransactionCode}.
     * 
     * @return the code's description
     */
    public String getDescription() {
        return description;
    }

    /**
     * Sets the description for this } TransactionCode}. This field is <em>
     * optional</em>, but is recommended. A value of `null` is OK. There 
     * is a maximum length of three hundred (300) characters set for this field.
     * 
     * @param description the code's description
     */
    public void setDescription(String description) {
        if (description == null) {
            description = "";
        }
        this.description = description;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        
        sb.append(getTxCode()).append("@").append(hashCode());
        sb.append("[id=").append(getId()).append("]");
        
        return sb.toString();
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null || !(obj instanceof TransactionCode)) {
            return false;
        }
        
        TransactionCode other = (TransactionCode) obj;
        
        return (getId() == other.getId())
                && (getTxCode().equals(other.getTxCode()))
                && (getDescription().equals(other.getDescription()))
                && (hashCode() == other.hashCode());
    }

    @Override
    public native int hashCode();

    @Override
    public String store() {
        StringBuilder sb = new StringBuilder();
        
        sb.append(getId()).append("~").append(getTxCode()).append("~");
        sb.append(getDescription());
        
        return sb.toString() + "\n";
    }

    @Override
    public TransactionCode restore(String line) {
        String[] fields = line.split("~");
        
        setId(Long.valueOf(fields[0]));
        setTxCode(fields[1]);
        if (fields.length == 3) {
            setDescription(fields[2]);
        } else {
            setDescription("");
        }
        
        return this;
    }

}
