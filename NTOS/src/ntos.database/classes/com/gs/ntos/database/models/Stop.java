/*
 * Copyright (C) 2022 GS United Labs
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * *****************************************************************************
 *  Project    :   ntos.database
 *  Class      :   Stop.java
 *  Author     :   Sean Carrick
 *  Created    :   Mar 21, 2022
 *  Modified   :   Mar 21, 2022
 *  
 *  Purpose: See class JavaDoc for explanation
 *  
 *  Revision History:
 *  
 *  WHEN          BY                   REASON
 *  ------------  -------------------  -----------------------------------------
 *  Mar 21, 2022  Sean Carrick         Initial creation.
 * *****************************************************************************
 */

package com.gs.ntos.database.models;

import com.gs.ntos.database.errors.DataStoreException;
import com.gs.ntos.database.errors.ReferentialIntegrityException;
import com.gs.ntos.database.errors.InvalidDataException;
import com.gs.ntos.database.tables.CustomersTable;
import com.gs.ntos.database.tables.LoadsTable;
import com.gs.ntos.database.validators.StopNumberValidator;
import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.Locale;

/**
 * This Java Bean defines the data that is required to be collected for a stop.
 * <p>
 * The data fields in this Bean are declared as } protected} in case the
 * situation arises in the future that this Bean needs to be extended. In that
 * situation, the subclass will be able to access the data fields directly, and
 * the documentation should describe the length and requirements of the data.</p>
 *
 * @author Sean Carrick &lt;sean at gs-unitedlabs dot com&gt;
 * 
 * @version 0.1.0
 * @since 0.1.0
 */
public class Stop implements SelfStorage<Stop>, Serializable {

    private static final long serialVersionUID = -5864394216487825387L;
    
    public static final String LOAD_DATE_FORMAT = "yyyy/MM/dd";
    private static final DateTimeFormatter dateFormat 
            = DateTimeFormatter.ofPattern("MM/dd/yyyy");
    private static final DateTimeFormatter dateParser
            = DateTimeFormatter.ofPattern(LOAD_DATE_FORMAT)
            .withLocale(Locale.getDefault());
    private static final DateTimeFormatter timeFormat 
            = DateTimeFormatter.ofPattern("HH:mm");
    
    private long id;
    private String orderNumber;
    private int stopNumber;
    private long customerId;
    private String stopReferenceNumber;
    private LocalDate earlyDate;
    private LocalTime earlyTime;
    private LocalDate lateDate;
    private LocalTime lateTime;
    private LocalDate arrivalDate;
    private LocalTime arrivalTime;
    private String notes;
    
    public Stop () {
        id = System.currentTimeMillis();
    }

    /**
     * Retrieves the unique ID for this `Stop`.
     * 
     * @return the unique ID
     */
    public long getId() {
        return id;
    }

    /**
     * Sets the unique ID for this `Stop`. Typically this method is only 
     * used when loading data from the data store, as the unique ID is 
     * automatically generated when the instance object is created and is 
     * guaranteed to be unique in the table.
     * 
     * @param id the unique ID
     * @throws IllegalArgumentException if the `id` is less than or equal
     *          to zero, or already appears in the table
     */
    public void setId(long id) {
        if (id <= 0L) {
            throw new IllegalArgumentException(id + " <= 0");
        }
        // Validation of whether the id already exists in the table will have to
        //+ be performed in the DataTable subclass when the record is being 
        //+ added to the table.
        
        this.id = id;
    }

    /**
     * Retrieves the order number for this `Stop`.
     * 
     * @return the order number
     */
    public String getOrderNumber() {
        return orderNumber;
    }

    /**
     * Sets the order number for this `Stop`. This value must exist in the
     * loads table of the data store, as a `Stop` cannot exist independent
     * of a load.
     * 
     * @param orderNumber the order number
     * @throws IllegalArgumentException if `orderNumber` is `null`,
     *          blank, or empty
     * @throws ReferentialIntegrityException if the specified `orderNumber`
     *          does not exist in the loads table
     */
    public void setOrderNumber(String orderNumber) {
        if (orderNumber == null || orderNumber.isBlank() || orderNumber.isEmpty()) {
            throw new IllegalArgumentException("empty, blank, or null orderNumber");
        } else if (!isOrderNumberPresent(orderNumber)) {
            throw new ReferentialIntegrityException(orderNumber + " does not "
                    + "exist as an order number or as a trip number in the "
                    + "loads table. orderNumber must exist in the loads "
                    + "table.");
        }
        
        this.orderNumber = orderNumber;
    }

    /**
     * Retrieves the stop number for this `Stop`.
     * 
     * @return the stop number
     */
    public int getStopNumber() {
        return stopNumber;
    }

    /**
     * Sets the stop number for this `Stop`. The stop number determines
     * the order in which stops are shown in the load window, as well as in the
     * loads queue. The order determines to which customer the driver must go
     * first, second, etc. This is a required field and must contain a value
     * greater than zero and not already assigned for the `orderNumber` to
     * which this `Stop` belongs.
     * 
     * @param stopNumber the stop number
     * @throws IllegalArgumentException if `stopNumber` is less than or
     *          equal to zero
     * @throws InvalidDataException if `stopNumber` has already been used
     *          for this `orderNumber`
     */
    public void setStopNumber(int stopNumber) {
        if (stopNumber <= 0) {
            throw new IllegalArgumentException(stopNumber + " <= 0");
        } else {
            StopNumberValidator validator = new StopNumberValidator(stopNumber, 
                    orderNumber);
            if (validator.isUsed()) {
                throw new InvalidDataException(stopNumber + " has already been "
                        + "used for order " + orderNumber);
            }
        }
        
        this.stopNumber = stopNumber;
    }

    /**
     * Retrieves the customer ID value for this `Stop`.
     * 
     * @return the customer ID
     */
    public long getCustomerId() {
        return customerId;
    }

    /**
     * Sets the customer id for this `Stop`. This is a required field and
     * the } customerId} must exist in the customers table of the data
     * store.
     * 
     * @param customerId the customer ID
     * @throws IllegalArgumentException if } customerId} is less than or
     *          equal to zero
     * @throws ReferentialIntegrityException if } customerId} does not
     *          exist in the customers table
     */
    public void setCustomerId(long customerId) {
        if (customerId <= 0) {
            throw new IllegalArgumentException(customerId + " <= 0");
        }
        if (!isCustomerPresent(customerId)) {
            throw new ReferentialIntegrityException(customerId + " does not "
                    + "exist in the customers table. customerId must exist "
                    + "in the customers table.");
        }
        
        this.customerId = customerId;
    }

    /**
     * Retrieves the early date for arrival at this `Stop`.
     * 
     * @return the early arrival date
     */
    public LocalDate getEarlyDate() {
        return earlyDate;
    }
    
    public String getEarlyDateAsString() {
        return dateFormat.format(earlyDate);
    }

    /**
     * Sets the early time for arrival at this `Stop`. This is a required
     * field and must be a valid date.
     * 
     * @param earlyDate the early arrival date
     * @throws IllegalArgumentException if the specified } earlyDate} is
     *          `null` or prior to the current date or if } earlyDate}
     *          is after } lateDate}
     */
    public void setEarlyDate(LocalDate earlyDate) {
        if (earlyDate == null) {
            throw new IllegalArgumentException("null, blank, or empty earlyDate");
        }
        
        this.earlyDate = earlyDate;
    }
    
    public void setEarlyDate(String earlyDate) {
        if (earlyDate == null) {
            throw new IllegalArgumentException("null earlyDate");
        }
        this.earlyDate = LocalDate.parse(earlyDate, dateParser);
    }

    /**
     * Retrieves the early time for arrival at this `Stop`
     * 
     * @return the early arrival time
     */
    public LocalTime getEarlyTime() {
        return earlyTime;
    }
    
    public String getEarlyTimeAsString() {
        return timeFormat.format(earlyTime);
    }

    /**
     * Sets the early time for arrival at this `Stop`. This is a required
     * field and must be a valid time.
     * 
     * @param earlyTime the early arrival time
     * @throws IllegalArgumentException if the specified } earlyTime} is
     *          `null`, blank, or empty, or if it is not a valid time
     */
    public void setEarlyTime(LocalTime earlyTime) {
        if (earlyTime == null) {
            throw new IllegalArgumentException("null, blank, or empty earlyTime");
        }
        
        this.earlyTime = earlyTime;
    }
    
    public void setEarlyTime(String earlyTime) {
        if (earlyTime == null || earlyTime.isBlank() || earlyTime.isEmpty()) {
            throw new IllegalArgumentException("blank, empty, or null earlyTime");
        }
        
        this.earlyTime = LocalTime.parse(earlyTime, timeFormat);
    }

    /**
     * Retrieves the early date for arrival at this `Stop`.
     * 
     * @return the late arrival date
     */
    public LocalDate getLateDate() {
        return lateDate;
    }
    
    public String getLateDateAsString() {
        return dateFormat.format(lateDate);
    }

    /**
     * Sets the late date for arrival at this `Stop`. This is a required
     * field and must be a valid date.
     * 
     * @param lateDate the late arrival date
     * @throws IllegalArgumentException if the specified } lateDate} is
     *          `null` or prior to the current date or earlier than the
     *          } earlyDate}
     */
    public void setLateDate(LocalDate lateDate) {
        if (lateDate == null) {
            throw new IllegalArgumentException("null, blank, or empty lateDate");
        }
        
        this.lateDate = lateDate;
    }
    
    public void setLateDate(String lateDate) {
        if (lateDate == null) {
            throw new IllegalArgumentException("null lateDate");
        }
        
        this.lateDate = LocalDate.parse(lateDate, dateParser);
    }

    /**
     * Retrieves the late time for arrival at this `Stop`.
     * 
     * @return the late arrival time
     */
    public LocalTime getLateTime() {
        return lateTime;
    }
    
    public String getLateTimeAsString() {
        return timeFormat.format(lateTime);
    }

    /**
     * Sets the late time for arrival at this `Stop`. This is a required
     * field and must be a valid time.
     * 
     * @param lateTime the late arrival time
     * @throws IllegalArgumentException if the specified } lateTime} is
     *          `null`, blank, or empty, or if it is not a valid time
     */
    public void setLateTime(LocalTime lateTime) {
        if (lateTime == null) {
            throw new IllegalArgumentException("null, blank, or empty lateTime");
        }
        
        this.lateTime = lateTime;
    }
    
    public void setLateTime(String lateTime) {
        if (lateTime == null || lateTime.isBlank() || lateTime.isEmpty()) {
            throw new IllegalArgumentException("null, blank, or empty lateTime");
        }
        
        this.lateTime = LocalTime.parse(lateTime, timeFormat);
    }

    /**
     * Retrieves the actual date of arrival at this `Stop`.
     * 
     * @return the arrival date
     */
    public LocalDate getArrivalDate() {
        return arrivalDate;
    }
    
    public String getArrivalDateAsString() {
        return dateFormat.format(arrivalDate);
    }

    /**
     * Sets the actual arrival date at this `Stop`. This is a required
     * field and must be a valid date. This field's value must be supplied once
     * arrived on site at this `Stop`.
     * 
     * @param arrivalDate the actual arrival date
     * @throws IllegalArgumentException if the specified } arrivalDate} is
     *          `null`, blank, or empty, or prior to the current date, or
     *          is not a valid date
     */
    public void setArrivalDate(LocalDate arrivalDate) {
        if (arrivalDate == null) {
            throw new IllegalArgumentException("null, blank, or empty arrivalDate");
        }
        
        this.arrivalDate = arrivalDate;
    }
    
    public void setArrivalDate(String arrivalDate) {
        if (arrivalDate == null || arrivalDate.isBlank() || arrivalDate.isEmpty()) {
            throw new IllegalArgumentException("null, blank, or empty arrivalDate");
        }
        
        this.arrivalDate = LocalDate.parse(arrivalDate, dateParser);
    }

    /**
     * Retrieves the actual arrival time at this `Stop`
     * @return 
     */
    public LocalTime getArrivalTime() {
        return arrivalTime;
    }
    
    public String getArrivalTimeAsString() {
        return timeFormat.format(arrivalTime);
    }

    /**
     * Sets the actual arrival time at this `Stop`. This is a required
     * field and must be a valid time. This field's value must be supplied once
     * arrived on site at this `Stop`
     * 
     * @param arrivalTime the actual arrival time
     * @throws IllegalArgumentException if } arrivalTime} is `null`,
     *          blank, or empty, or is not a valid time
     */
    public void setArrivalTime(LocalTime arrivalTime) {
        if (arrivalTime == null) {
            throw new IllegalArgumentException("null, blank, or empty arrivalTime");
        }
        
        this.arrivalTime = arrivalTime;
    }
    
    public void setArrivalTime(String arrivalTime) {
        if (arrivalTime == null || arrivalTime.isBlank() || arrivalTime.isEmpty()) {
            throw new IllegalArgumentException("null, blank, or empty arrivalTime");
        }
        
        this.arrivalTime = LocalTime.parse(arrivalTime, timeFormat);
    }

    /**
     * Retrieves the notes regarding this `Stop`.
     * 
     * @return stop notes
     */
    public String getNotes() {
        return notes;
    }

    /**
     * Sets the notes regarding this `Stop`. This field is optional, but
     * `null` is not allowed. If a `null` value is supplied, it will
     * be converted to an empty string. The maximum length of this field is
     * one thousand (1,000) characters. If } notes} is longer than this, it
     * will be truncated.
     * 
     * @param notes stop notes
     */
    public void setNotes(String notes) {
        if (notes == null) {
            notes = "";
        } else if (notes.length() > 1000) {
            notes = notes.substring(0, 1000);
        }
        
        this.notes = notes;
    }

    public String getStopReferenceNumber() {
        return stopReferenceNumber;
    }

    public void setStopReferenceNumber(String stopReferenceNumber) {
        this.stopReferenceNumber = stopReferenceNumber;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        
        sb.append("Stop #").append(getStopNumber()).append("@").append(hashCode());
        sb.append("[id=").append(getId()).append(";earlyDate=");
        sb.append(getEarlyDate()).append(";lateDate=").append(getLateDate());
        sb.append("]");
        
        return sb.toString();
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null || !(obj instanceof Stop)) {
            return false;
        }
        
        Stop other = (Stop) obj;
        
        return (getId() == other.getId())
                && (getCustomerId() == other.getCustomerId())
                && (getEarlyDate().equals(other.getEarlyDate()))
                && (getEarlyTime().equals(other.getEarlyTime()))
                && (getLateDate().equals(other.getLateDate()))
                && (getLateTime().equals(other.getLateTime()))
                && (hashCode() == other.hashCode());
    }
    
    @Override
    public int hashCode() {
        int hash = Long.valueOf(getId()).hashCode();
        hash += getEarlyDate() == null ? 3985 : getEarlyDate().hashCode();
        hash += getEarlyTime() == null ? 3498 : getEarlyTime().hashCode();
        hash += getLateDate() == null ? 5484 : getLateDate().hashCode();
        hash += getLateTime() == null ? 3209 : getLateTime().hashCode();
        hash += getArrivalDate() == null ? 7856 : getArrivalDate().hashCode();
        hash += getArrivalTime() == null ? 4938 : getArrivalTime().hashCode();
        hash += Long.valueOf(getCustomerId()).hashCode();
        hash += Integer.valueOf(getStopNumber()).hashCode();
        hash += getStopReferenceNumber() == null ? 7834 
                : getStopReferenceNumber().hashCode();
        hash += getOrderNumber() == null ? 9484 : getOrderNumber().hashCode();
        hash += getNotes() == null ? 2340 : getNotes().hashCode();
        hash /= 12;
        
        return hash;
    }

    @Override
    public String store() {
        StringBuilder sb = new StringBuilder();
        
        sb.append(getId()).append("~").append(getOrderNumber()).append("~");
        sb.append(getStopNumber()).append("~").append(getCustomerId()).append("~");
        sb.append(getEarlyDate()).append("~").append(getEarlyTime()).append("~");
        sb.append(getLateDate()).append("~").append(getLateTime()).append("~");
        sb.append(getArrivalDate()).append("~").append(getArrivalTime()).append("~");
        sb.append(getStopReferenceNumber()).append("~").append(getNotes());
        
        return sb.toString() + "\n";
    }

    @Override
    public Stop restore(String line) {
        String[] fields = line.split("~");
        
        setId(Long.valueOf(fields[0]));
        setOrderNumber(fields[1]);
        setStopNumber(Integer.valueOf(fields[2]));
        setCustomerId(Long.valueOf(fields[3]));
        setEarlyDate(fields[4]);
        setEarlyTime(fields[5]);
        setLateDate(fields[6]);
        setLateTime(fields[7]);
        setArrivalDate(fields[8]);
        setArrivalTime(fields[9]);
        setStopReferenceNumber(fields[10]);
        if (fields.length == 12) {
            setNotes(fields[11]);
        } else {
            setNotes("");
        }
        
        return this;
    }
    
    private boolean isCustomerPresent(long id) {
        CustomersTable table = new CustomersTable();
        
        try {
            table.connect();
        } catch (DataStoreException ex) {
            return false;
        }
        
        return table.doesCustomerExist(id);
    }
    
    private boolean isOrderNumberPresent(String orderNumber) {
        LoadsTable table = new LoadsTable();
        
        Load[] loads = null;
        
        try {
            table.connect();
            
            loads = table.getAllRecords();
            
            table.close();
        } catch (DataStoreException ex) {
            return false;
        }
        
        if (loads != null) {
            for (Load l : loads) {
                if (orderNumber.equals(l.getOrderNumber())
                        || orderNumber.equals(l.getTripNumber())) {
                    return true;
                }
            }
        }
        
        return false;
    }

}
