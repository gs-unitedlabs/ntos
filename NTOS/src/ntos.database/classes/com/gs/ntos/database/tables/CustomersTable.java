/*
 * Copyright (C) 2022 GS United Labs
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * *****************************************************************************
 *  Project    :   NTOS
 *  Class      :   CustomersTable.java
 *  Author     :   Sean Carrick
 *  Created    :   Apr 13, 2022
 *  Modified   :   Apr 13, 2022
 *  
 *  Purpose: See class JavaDoc for explanation
 *  
 *  Revision History:
 *  
 *  WHEN          BY                   REASON
 *  ------------  -------------------  -----------------------------------------
 *  Apr 13, 2022  Sean Carrick         Initial creation.
 * *****************************************************************************
 */

package com.gs.ntos.database.tables;

import com.gs.ntos.database.errors.DataStoreException;
import com.gs.ntos.database.errors.InvalidDataException;
import com.gs.ntos.database.errors.ReferentialIntegrityException;
import com.gs.ntos.database.models.Customer;
import com.gs.ntos.database.models.Stop;

/**
 *
 * @author Sean Carrick &lt;sean at gs-unitedlabs dot com&gt;
 * 
 * @version 1.0
 * @since 1.0
 */
public class CustomersTable extends GenericTable<Customer> {
    
    public CustomersTable () {
        super("custs", Customer.class);
    }

    @Override
    public boolean add(Customer record) {
        if (record == null) {
            return false;
        }
        if (doesRecordExistInTable(record)) {
            throw new InvalidDataException("The record with the ID " + record.getId()
                    + " already exists in the table. Call update(Account) "
                            + "instead.");
        }
        
        boolean result = records.add(record);
        
        try {
            save();
        } catch (DataStoreException ex) {
            logger.error(ex, "Attempting to save data after new record added: "
                    + "{0}", record);
        }
        
        return result;
    }

    @Override
    public boolean delete(Customer record) throws ReferentialIntegrityException {
        if (record == null) {
            return false;
        }
        if (!doesRecordExistInTable(record)) {
            return false;
        }
        if (isRecordReferenced(record)){
            throw new ReferentialIntegrityException("Customer, "
                    + record.getCompany() + ", has foreign key references.\n\n"
                    + "These references must be deleted before this account "
                    + "type record can be deleted.");
        }
        
        int idx = records.indexOf(record);
        boolean result = records.remove(record);
        
        if (idx >= records.size() && idx == getCurrentRecordNumber()){
            setCurrentRecordNumber(records.size() - 1);
        } else {
            setCurrentRecordNumber(idx);
        }
        
        return result;
    }
    
    public boolean doesCustomerExist(long id) {
        for (Customer c : records) {
            if (c.getId() == id) {
                return true;
            }
        }
        
        return false;
    }

    @Override
    protected boolean doesRecordExistInTable(Customer record) {
        for (Customer c : records) {
            if (c.getId() == record.getId()) {
                return true;
            }
        }
        
        return false;
    }
    
    public Customer get(long id) {
        for (Customer c : records) {
            if (c.getId() == id) {
                return c;
            }
        }
        
        return null;
    }

    @Override
    protected boolean isRecordReferenced(Customer record) {
        StopsTable table = new StopsTable();
        boolean referenced = false;
        try {
            table.connect();
            referenced = table.isCustomerReferenced(record);
            table.close();
        } catch (DataStoreException ex) {
            logger.error(ex, "Attempting to retrieves the stops from the Stops "
                    + "table: {0}", table);
        }
        
        return referenced;
    }

    @Override
    protected void save() throws DataStoreException {
        super.save();
        program.firePropertyChange("customersAvailable", !encryptedFile.exists(), 
                encryptedFile.exists());
    }

    @Override
    public boolean update(Customer record) {
        if (record == null) {
            return false;
        }
        if (!doesRecordExistInTable(record)) {
            throw new InvalidDataException("Record with ID " + record.getId()
                    + " does not exist in the table. Call add(Customer) "
                    + "instead.");
        }
        
        for (Customer c : records) {
            if (c.getId() == record.getId()) {
                int idx = records.indexOf(c);
                records.remove(idx);
                records.add(idx, record);
                return true;
            }
        }
        
        return false;
    }

}
