/*
 * Copyright (C) 2022 GS United Labs
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * *****************************************************************************
 *  Project    :   ntos.database
 *  Class      :   Transaction.java
 *  Author     :   Sean Carrick
 *  Created    :   Mar 23, 2022
 *  Modified   :   Mar 23, 2022
 *  
 *  Purpose: See class JavaDoc for explanation
 *  
 *  Revision History:
 *  
 *  WHEN          BY                   REASON
 *  ------------  -------------------  -----------------------------------------
 *  Mar 23, 2022  Sean Carrick         Initial creation.
 * *****************************************************************************
 */

package com.gs.ntos.database.models;

import com.gs.ntos.database.errors.ReferentialIntegrityException;
import com.gs.ntos.database.validators.AccountValidator;
import com.gs.ntos.database.validators.TransactionCodeValidator;
import java.io.Serializable;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * This Java Bean defines the data that is required to be collected for a ledger
 * entry.
 * <p>
 * The data fields in this Bean are declared as } protected} in case the
 * situation arises in the future that this Bean needs to be extended. In that
 * situation, the subclass will be able to access the data fields directly, and
 * the documentation should describe the length and requirements of the data.</p>
 *
 * @author Sean Carrick &lt;sean at gs-unitedlabs dot com&gt;
 * 
 * @version 0.1.0
 * @since 0.1.0
 */
public class Transaction implements SelfStorage<Transaction>, Serializable {

    private static final long serialVersionUID = 7769585609560451468L;
    
    private final SimpleDateFormat sdf;
    
    private long id;
    private Date date;
    private long txCode;
    private String description;
    private long fromAccount;
    private long toAccount;
    private double amount;
    private boolean taxDeductible;
    private boolean balanced;
    
    public Transaction () {
        id = System.currentTimeMillis();
        sdf = new SimpleDateFormat("MM/dd/yyyy");
    }

    /**
     * Retrieves the unique, internal ID for this } Transaction}.
     * 
     * @return the unique ID
     */
    public long getId() {
        return id;
    }

    /**
     * Sets the unique, internal ID for this } Transaction}. The unique ID
     * is automatically generated at the time an object is created, so this is
     * typically only used when loading a } Transaction} from the data
     * store.
     * 
     * @param id the unique ID
     * @throws IllegalArgumentException if `id` is less than or equal to
     *          zero
     */
    public void setId(long id) {
        if (id <= 0) {
            throw new IllegalArgumentException(id + " <= 0");
        }
        this.id = id;
    }

    /**
     * Retrieves the } Transaction} date as a string, in the format
     * } MM/dd/yyyy}.
     * 
     * @return the transaction date
     * 
     * @see #setDate(java.util.Date) 
     * @see #setDate(java.lang.String) 
     */
    public String getDate() {
        return sdf.format(date);
    }

    /**
     * Sets the } Transaction} date. This is a required field and must be
     * a valid date.
     * 
     * @param date the transaction date
     * @throws IllegalArgumentException if } date} is `null`
     * 
     * @see #setDate(java.lang.String) 
     * @see #getDate() 
     */
    public void setDate(Date date) {
        if (date == null) {
            throw new IllegalArgumentException("null date");
        }
        this.date = date;
    }
    
    /**
     * A convenience method for setting the } Transaction} date from a date
     * string. This date string must be in the format } MM/dd/yyyy}.
     * 
     * @param date the date string that represents the transaction date
     * @throws IllegalArgumentException if the } date} string is not a valid
     *          } java.util.Date} representation
     * 
     * @see #setDate(java.util.Date) 
     * @see #getDate() 
     */
    public void setDate(String date) {
        if (date == null || date.isBlank() || date.isEmpty()) {
            throw new IllegalArgumentException("null, blank, or empty date");
        }
        
        try {
            this.date = sdf.parse(date);
        } catch (ParseException ex) {
            throw new IllegalArgumentException("date is not valid");
        }
    }

    /**
     * Retrieves the unique ID value for the } TransactionCode} for this
     * } Transaction}.
     * 
     * @return the transaction code ID for this transaction
     */
    public long getTxCode() {
        return txCode;
    }

    /**
     * Sets the } TransactionCode} unique ID for this } Transaction}.
     * This is a required field and the supplied } txCode} <em>must</em>
     * exist in the } TransactionCodesTable}.
     * 
     * @param txCode the ID of the } TransactionCode} for this transaction
     * @throws IllegalArgumentException if } txCode} is less than or equal
     *          to zero
     * @throws ReferentialIntegrityException if } txCode} does not exist in
     *          the } TransactionCodesTable}
     */
    public void setTxCode(long txCode) {
        if (txCode <= 0) {
            throw new IllegalArgumentException(txCode + " <= 0");
        }
        TransactionCodeValidator validator = new TransactionCodeValidator(txCode);
        if (!validator.isPresent()) {
            throw new ReferentialIntegrityException("txCode does not exist");
        }
        
        this.txCode = txCode;
    }

    /**
     * Retrieves the description of this } Transaction}.
     * 
     * @return the transaction's description
     */
    public String getDescription() {
        return description;
    }

    /**
     * Sets the description for this } Transaction}. While the description
     * field is not required, it is a best practice to always provide a 
     * description for each transaction. This way, you will have a historical 
     * reference as to where the money came from or went, depending upon the
     * transaction. `null` is <em>not</em> allowed. If a `null`
     * value is provided, it will be converted to an empty string.
     * 
     * @param description the transaction's description
     */
    public void setDescription(String description) {
        if (description == null) {
            description = "";
        }
        this.description = description;
    }

    /**
     * Retrieves the unique ID of the } Account} from which the money on
     * this } Transaction} is to be taken.
     * 
     * @return the from account unique ID
     */
    public long getFromAccount() {
        return fromAccount;
    }
    
    /**
     * Sets the } Account} from which the monies on this } Transaction}
     * will be paid out. The value supplied for } fromAccount} must be
     * greater than zero and must exist in the accounts table.
     * 
     * @param fromAccount the } Account} from which monies will be paid out
     * @throws IllegalArgumentException if } fromAccount} is less than or
     *          equal to zero
     * @throws ReferentialIntegrityException if } fromAccount} value does
     *          not exist in the accounts table
     */
    public void setFromAccount(long fromAccount) {
        if (fromAccount <= 0L) {
            throw new IllegalArgumentException(fromAccount + " <= 0");
        }
        AccountValidator validator = new AccountValidator(fromAccount);
        if (!validator.isPresent()) {
            throw new ReferentialIntegrityException(fromAccount + " is not a "
                    + "valid account in the accounts table.");
        }
        
        this.fromAccount = fromAccount;
    }

    /**
     * Retrieves the } Account} ID for the account to which the monies on
     * this transaction are to be paid.
     * 
     * @return the } Account} into which monies will be paid
     */
    public long getToAccount() {
        return toAccount;
    }

    /**
     * Sets the } Account} to which monies on this } Transaction} will
     * be paid. The value supplied for } toAccount} must be greater than
     * zero and must exist in the accounts table.
     * 
     * @param toAccount the } Account} into which the monies will be paid
     * @throws IllegalArgumentException if } toAccount} is less than or
     *          equal to zero
     * @throws ReferentialIntegrityException if } toAccount} value does not
     *          exist in the accounts table
     */
    public void setToAccount(long toAccount) {
        if (toAccount <= 0L) {
            throw new IllegalArgumentException(toAccount + " <= 0");
        }
        AccountValidator validator = new AccountValidator(toAccount);
        if (!validator.isPresent()) {
            throw new ReferentialIntegrityException(toAccount + " is not a "
                    + "valid account in the accounts table.");
        }
        
        this.toAccount = toAccount;
    }

    /**
     * Retrieves the amount of this } Transaction}.
     * 
     * @return the transaction amount
     */
    public double getAmount() {
        return amount;
    }

    /**
     * Sets the amount of this } Transaction}. The } amount} must be a
     * positive value and greater than zero.
     * 
     * @param amount the amount of this transaction
     * @throws IllegalArgumentException if } amount} is less than or equal
     *          to zero
     */
    public void setAmount(double amount) {
        if (amount <= 0D) {
            throw new IllegalArgumentException(amount + " <= 0");
        }
        
        this.amount = amount;
    }

    /**
     * Checks if this } Transaction} is for a tax deductible expense.
     * 
     * @return `true` if tax deductible; `false` otherwise
     */
    public boolean isTaxDeductible() {
        return taxDeductible;
    }

    /**
     * Sets whether this } Transaction} is for a tax deductible expense.
     * 
     * @param taxDeductible `true` if tax deductible; `false` if not
     */
    public void setTaxDeductible(boolean taxDeductible) {
        this.taxDeductible = taxDeductible;
    }

    /**
     * Checks whether this } Transaction} has been balanced with the bank
     * account statement.
     * 
     * @return `true` if balanced; `false` if not
     */
    public boolean isBalanced() {
        return balanced;
    }

    /**
     * Sets whether this } Transaction} has been balanced with the bank
     * account statement.
     * 
     * @param balanced `true` if balanced; `false` if not
     */
    public void setBalanced(boolean balanced) {
        this.balanced = balanced;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        
        sb.append((getDescription() != null) ? getDescription() : "[NoDescription]");
        sb.append(" on ");
        sb.append((getDate() != null) ? getDate() : "[NoDate]");
        sb.append(" for ");
        sb.append(DecimalFormat.getCurrencyInstance().format(getAmount()));
        sb.append("@[").append(hashCode()).append("]");
        
        return sb.toString();
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null || !(obj instanceof Transaction)) {
            return false;
        }
        
        Transaction other = (Transaction) obj;
        
        return getId() == other.getId()
                && getDate().equals(other.getDate())
                && getDescription().equalsIgnoreCase(other.getDescription())
                && getAmount() == other.getAmount()
                && getFromAccount() == other.getFromAccount()
                && getToAccount() == other.getToAccount()
                && isBalanced() == other.isBalanced()
                && isTaxDeductible() == other.isTaxDeductible()
                && getTxCode() == other.getTxCode()
                && hashCode() == other.hashCode();
    }

    @Override
    public int hashCode() {
        int hash = Long.valueOf(getId()).hashCode();
        hash += Double.valueOf(getAmount()).hashCode();
        hash += Long.valueOf(getFromAccount()).hashCode();
        hash += Long.valueOf(getToAccount()).hashCode();
        hash += Long.valueOf(getTxCode()).hashCode();
        hash += getDate().hashCode();
        hash += getDescription().hashCode();
        hash /= 9;
        
        return hash;
    }

    @Override
    public String store() {
        StringBuilder sb = new StringBuilder();
        
        sb.append(getId()).append("~").append(getTxCode()).append("~");
        sb.append(getDate()).append("~");
        sb.append((getDescription() == null || getDescription().isBlank()) 
                ? "[NoDescription]" : getDescription()).append("~");
        sb.append(getFromAccount()).append("~").append(getToAccount()).append("~");
        sb.append(getAmount()).append("~").append(isTaxDeductible()).append("~");
        sb.append(isBalanced());
        
        return sb.toString() + "\n";
    }

    @Override
    public Transaction restore(String line) {
        String[] fields = line.split("~");
        
        setId(Long.valueOf(fields[0]));
        setTxCode(Long.valueOf(fields[1]));
        setDate(fields[2]);
        setDescription(fields[3]);
        setFromAccount(Long.valueOf(fields[4]));
        setToAccount(Long.valueOf(fields[5]));
        setAmount(Double.valueOf(fields[6]));
        setTaxDeductible(Boolean.parseBoolean(fields[7]));
        setBalanced(Boolean.parseBoolean(fields[8]));
        
        return this;
    }

}
