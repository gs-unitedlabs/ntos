/*
 * Copyright (C) 2022 GS United Labs
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * *****************************************************************************
 *  Project    :   ntos.database
 *  Class      :   LedgerTable.java
 *  Author     :   Sean Carrick
 *  Created    :   Mar 25, 2022
 *  Modified   :   Mar 25, 2022
 *  
 *  Purpose: See class JavaDoc for explanation
 *  
 *  Revision History:
 *  
 *  WHEN          BY                   REASON
 *  ------------  -------------------  -----------------------------------------
 *  Mar 25, 2022  Sean Carrick         Initial creation.
 * *****************************************************************************
 */

package com.gs.ntos.database.tables;

import com.gs.ntos.database.errors.DataStoreException;
import com.gs.ntos.database.errors.InvalidDataException;
import com.gs.ntos.database.errors.ReferentialIntegrityException;
import com.gs.ntos.database.models.Account;
import com.gs.ntos.database.models.Transaction;
import com.gs.ntos.database.models.TransactionCode;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 *
 * @author Sean Carrick &lt;sean at gs-unitedlabs dot com&gt;
 * 
 * @version 0.1.0
 * @since 0.1.0
 */
public class LedgerTable extends GenericTable<Transaction> {
    
    public LedgerTable () {
        super ("genledg", Transaction.class);
    }

    @Override
    public boolean add(Transaction record) {
        if (record == null) {
            return false;
        }
        if (doesRecordExistInTable(record)) {
            throw new InvalidDataException("Transaction with ID " + record.getId()
                    + " on " + record.getDate() + " for "
                    + DecimalFormat.getCurrencyInstance().format(record.getAmount())
                    + " already exists in the table. Call update(Transaction) "
                    + "instead.");
        }
        
        boolean result = records.add(record);
        
        try {
            save();
        } catch (DataStoreException ex) {
            logger.error(ex, "Attempting to save data after new record added: "
                    + "{0}", record);
        }
        
        return result;
    }

    @Override
    public boolean delete(Transaction record) throws ReferentialIntegrityException {
        if (record == null) {
            return false;
        }
        if (!doesRecordExistInTable(record)) {
            return false;
        }
        
        Transaction reverse = new Transaction();
        reverse.setDate(record.getDate());
        reverse.setTxCode(record.getTxCode());
        reverse.setAmount(record.getAmount());
        reverse.setBalanced(record.isBalanced());
        reverse.setDescription("[REVERSAL] " + record.getDescription());
        reverse.setFromAccount(record.getToAccount());
        reverse.setTaxDeductible(record.isTaxDeductible());
        reverse.setToAccount(record.getFromAccount());
        
        return add(reverse);
    }

    @Override
    protected boolean doesRecordExistInTable(Transaction record) {
        for (Transaction t : records) {
            if (t.equals(record)) {
                return true;
            }
        }
        
        return false;
    }

    @Override
    public boolean update(Transaction record) {
        if (record == null) {
            return false;
        }
        if (!doesRecordExistInTable(record)) {
            throw new InvalidDataException("Record with ID " + record.getId()
                    + " does not exist in the table. Call add(AccountType) "
                    + "instead.");
        }
        
        for (Transaction t : records) {
            if (t.getId() == record.getId()) {
                int idx = records.indexOf(t);
                records.remove(idx);
                records.add(idx, record);
                return true;
            }
        }
        
        return false;
    }
    
    public Transaction[] getTxForAccount(Account record) {
        List<Transaction> tx = new ArrayList<>();
        
        for (Transaction t : records) {
            if (t.getFromAccount() == record.getId()
                    || t.getToAccount() == record.getId()) {
                tx.add(t);
            }
        }
        
        return tx.toArray(new Transaction[tx.size()]);
    }
    
    public boolean isTxCodeReferenced(TransactionCode code) {
        for (Transaction t : records) {
            if (t.getTxCode() == code.getId()) {
                return true;
            }
        }
        
        return false;
    }
    
    public List<Transaction> getTransactionsBetween(String start, String end) {
        List<Transaction> found = new ArrayList<>();
        SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
        
        Date startDate = null;
        Date endDate = null;
        try {
            startDate = sdf.parse(start);
            endDate = sdf.parse(end);
        } catch (ParseException ex) {
            System.err.println("Error parsing date");
        }
        
        for (Transaction t : records) {
            if (startDate != null && endDate != null) {
                Date date = null;
                try {
                    date = sdf.parse(t.getDate());
                } catch (ParseException ex) {
                    System.err.println("Error parsing transaction date.");
                }
                
                if (date != null) {
                    if ((date.equals(startDate) || date.equals(endDate))
                            || (date.after(startDate) && date.before(endDate))) {
                        found.add(t);
                    }
                }
            }
        }
        
        return found;
    }

}
