/*
 * Copyright (C) 2022 GS United Labs
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * *****************************************************************************
 *  Project    :   NTOS
 *  Class      :   FuelPurchase.java
 *  Author     :   Sean Carrick
 *  Created    :   Mar 27, 2022
 *  Modified   :   Mar 27, 2022
 *  
 *  Purpose: See class JavaDoc for explanation
 *  
 *  Revision History:
 *  
 *  WHEN          BY                   REASON
 *  ------------  -------------------  -----------------------------------------
 *  Mar 27, 2022  Sean Carrick         Initial creation.
 * *****************************************************************************
 */

package com.gs.ntos.database.models;

import com.gs.ntos.database.errors.InvalidDataException;
import com.gs.ntos.database.validators.DateStringValidator;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * The `FuelPurchase` Java Bean defines the properties of a fuel purchase.
 * 
 * This class provides a means for a driver to store critical information about
 * each fuel purchase as a means of maintaining a history of fuel purchases and
 * understanding how driving conditions, speeds, etc., affect fuel economy. By
 * having all of this information stored in the Fuel Account Journal,
 * `10040.njrl`, the driver will be able to make informed decisions regarding
 * his/her driving practices in order to reduce their fuel expense over time.
 *
 * @author Sean Carrick &lt;sean at gs-unitedlabs dot com&gt;
 * 
 * @version 1.0
 * @since 1.0
 */
public class FuelPurchase {
    
    private final SimpleDateFormat sdf;
    
    private long id;
    private Date date;
    private int odometer;
    private String location;
    private double gallonsOfDiesel;
    private double pricePerGallonDiesel;
    private double gallonsOfDef;
    private double pricePerGallonDef;
    private String notes;
    
    public FuelPurchase () {
        id = System.currentTimeMillis();
        date = new Date();
        sdf = new SimpleDateFormat("MM/dd/yyyy");
    }

    /**
     * Retrieves the date of this `FuelPurchase`.
     * 
     * @return the purchase date
     */
    public Date getDate() {
        return date;
    }
    
    /**
     * Convenience method  to retrieve the date of this `FuelPurchase` as a 
     * string value, in the format "MM/dd/yyyy".
     * 
     * @return the purchase date as a string.
     */
    public String getDateAsString() {
        return sdf.format(getDate());
    }
    
    /**
     * Convenience method to retrieve the date of this `FuelPurchase` as a 
     * string in the specified `format`. The value of `format` must be a valid
     * date formatting string, in accordance with the <a href="https://docs.oracle.com/en/java/javase/11/docs/api/java.base/java/text/SimpleDateFormat.html#%3Cinit%3E(java.lang.String)>Java Specifications</a>.
     * 
     * @param format the format string to use to format the date
     * @return the purchase date as a string in the specified `format`
     * @throws IllegalArgumentException if `format` is blank, empty, or `null`
     */
    public String getDateAsString(String format) {
        if (format == null || format.isBlank() || format.isEmpty()) {
            throw new IllegalArgumentException("blank, empty, or null format");
        }
        SimpleDateFormat fmt = new SimpleDateFormat(format);
        return fmt.format(getDate());
    }

    /**
     * Sets the date of this `FuelPurchase`.
     * 
     * @param date the purchase date
     * @throws IllegalArgumentException if `date` is `null`
     */
    public void setDate(Date date) {
        if (date == null) {
            throw new IllegalArgumentException("null date");
        }
        this.date = date;
    }
    
    /**
     * Convenience method to set the date of this `FuelPurchase` from a date
     * string. This method only works with date strings in the format "MM/dd/yyyy".
     * 
     * @param date the purchase date as a string
     * @throws IllegalArgumentException if `date` is blank, empty, or `null`
     * @throws InvalidDataException if `date` is not a valid date string or is
     *          not able to be parsed
     */
    public void setDate(String date) {
        if (date == null || date.isBlank() || date.isEmpty()) {
            throw new IllegalArgumentException("blank, empty, or null date");
        }
        if (!new DateStringValidator(date).isValid()) {
            throw new InvalidDataException("invalid date string value");
        }
        
        try {
            this.date = sdf.parse(date);
        } catch (ParseException ex) {
            System.err.println("Error parsing the date \"" + date + "\"");
            throw new InvalidDataException("Error parsing the date \"" + date
                    + "\"");
        }
    }
    
    public void setDate(String date, String format) {
        if (date == null || date.isBlank() || date.isEmpty()) {
            throw new IllegalArgumentException("blank, empty, or null date");
        }
        if (format == null || date.isBlank() || date.isEmpty()) {
            throw new IllegalArgumentException("blank, empty, or null format");
        }
        if (!new DateStringValidator(date, format).isValid()) {
            throw new InvalidDataException("invalid date or format string value");
        }
        
        try {
            SimpleDateFormat fmt = new SimpleDateFormat(format);
            this.date = fmt.parse(date);
        } catch (ParseException ex) {
            System.err.println("Error parsing the date \"" + date + "\" using "
                    + "the format \"" + format + "\"");
            throw new InvalidDataException("Error parsing the date \"" + date
                    + "\" using " + "the format \"" + format + "\"");
        }
    }

    /**
     * Retrieves the odometer reading for this `FuelPurchase`.
     * 
     * @return 
     */
    public int getOdometer() {
        return odometer;
    }

    /**
     * Sets the odometer reading for this `FuelPurchase`.
     * 
     * @param odometer 
     */
    public void setOdometer(int odometer) {
        this.odometer = odometer;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public double getGallonsOfDiesel() {
        return gallonsOfDiesel;
    }

    public void setGallonsOfDiesel(double gallonsOfDiesel) {
        this.gallonsOfDiesel = gallonsOfDiesel;
    }

    public double getPricePerGallonDiesel() {
        return pricePerGallonDiesel;
    }

    public void setPricePerGallonDiesel(double pricePerGallonDiesel) {
        this.pricePerGallonDiesel = pricePerGallonDiesel;
    }

    public double getGallonsOfDef() {
        return gallonsOfDef;
    }

    public void setGallonsOfDef(double gallonsOfDef) {
        this.gallonsOfDef = gallonsOfDef;
    }

    public double getPricePerGallonDef() {
        return pricePerGallonDef;
    }

    public void setPricePerGallonDef(double pricePerGallonDef) {
        this.pricePerGallonDef = pricePerGallonDef;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public long getId() {
        return id;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        
        sb.append(sdf.format(getDate())).append("@[id=");
        sb.append(getId()).append(";location=").append(getLocation());
        sb.append(";diesel=").append(getGallonsOfDiesel()).append(" ($");
        sb.append(getPricePerGallonDiesel()).append(");def=");
        sb.append(getGallonsOfDef()).append(" ($");
        sb.append(getPricePerGallonDef()).append(")");
        sb.append("]");
        
        return sb.toString();
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null || !(obj instanceof FuelPurchase)) {
            return false;
        }
        FuelPurchase other = (FuelPurchase) obj;
        return (getId() == other.getId())
                && (getDate().equals(other.getDate()))
                && (getGallonsOfDef() == other.getGallonsOfDef())
                && (getPricePerGallonDef() == other.getPricePerGallonDef())
                && (getGallonsOfDiesel() == other.getGallonsOfDiesel())
                && (getPricePerGallonDiesel() == other.getPricePerGallonDiesel())
                && (getLocation().equals(other.getLocation()))
                && (hashCode() == other.hashCode());
    }

    @Override
    public int hashCode() {
        int hash = Long.valueOf(getId()).hashCode();
        hash += Double.valueOf(getGallonsOfDef()).hashCode();
        hash += Double.valueOf(getGallonsOfDiesel()).hashCode();
        hash += Double.valueOf(getPricePerGallonDef()).hashCode();
        hash += Double.valueOf(getPricePerGallonDiesel()).hashCode();
        hash += getLocation().hashCode();
        
        return hash;
    }

}
