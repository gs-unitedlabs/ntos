/*
 * Copyright (C) 2022 GS United Labs
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * *****************************************************************************
 *  Project    :   ntos.database
 *  Class      :   Vehicle.java
 *  Author     :   Sean Carrick
 *  Created    :   Mar 25, 2022
 *  Modified   :   Mar 25, 2022
 *  
 *  Purpose: See class JavaDoc for explanation
 *  
 *  Revision History:
 *  
 *  WHEN          BY                   REASON
 *  ------------  -------------------  -----------------------------------------
 *  Mar 25, 2022  Sean Carrick         Initial creation.
 * *****************************************************************************
 */

package com.gs.ntos.database.models;

import com.gs.ntos.database.errors.InvalidDataException;
import com.gs.ntos.database.validators.VehicleNumberValidator;
import java.io.Serializable;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

/**
 * This Java Bean defines the data that is required to be collected for a vehicle.
 * <p>
 * The data fields in this Bean are declared as } protected} in case the
 * situation arises in the future that this Bean needs to be extended. In that
 * situation, the subclass will be able to access the data fields directly, and
 * the documentation should describe the length and requirements of the data.</p>
 *
 * @author Sean Carrick &lt;sean at gs-unitedlabs dot com&gt;
 * 
 * @version 0.1.0
 * @since 0.1.0
 */
public class Vehicle implements SelfStorage<Vehicle>, Serializable {

    private static final long serialVersionUID = 2732905463557517305L;
    
    private final DateTimeFormatter fmt = DateTimeFormatter.ofPattern("MM/dd/yyyy");
    
    private long id;
    private String vin;
    private String make;
    private String model;
    private String color;
    private String number;
    private LocalDate purchaseDate;
    private LocalDate salesDate;
    private long vehicleType;
    
    public Vehicle () {
        id = System.currentTimeMillis();
    }

    /**
     * Retrieves the VIN for this } Vehicle}.
     * 
     * @return the vehicle's VIN
     */
    public String getVin() {
        return vin;
    }

    /**
     * Sets the VIN for this } Vehicle}. This is a required field.
     * 
     * @param vin the vehicle's VIN
     * @throws IllegalArgumentException if } vin} is blank, empty, or
     *          `null`
     */
    public void setVin(String vin) {
        if (vin == null || vin.isBlank() || vin.isEmpty()) {
            throw new IllegalArgumentException("blank, empty, or null vin");
        }
        
        this.vin = vin;
    }

    /**
     * Retrieves the make (manufacturer) of this } Vehicle}.
     * 
     * @return the vehicle's make
     */
    public String getMake() {
        return make;
    }

    /**
     * Sets the make (manufacturer) of this } Vehicle}. This is a required
     * field and has a maximum length of twenty (20) characters. If the specified
     * } make} is longer than the maximum length, it will be truncated.
     * 
     * @param make the vehicle's make
     * @throws IllegalArgumentException if } make} is blank, empty, or 
     *          `null`
     */
    public void setMake(String make) {
        if (make == null || make.isBlank() || make.isEmpty()) {
            throw new IllegalArgumentException("blank, empty, or null make");
        } else if (make.length() > 20) {
            make = make.substring(0, 20);
        }
        
        this.make = make;
    }

    /**
     * Retrieves the model (design) of this } Vehicle}.
     * 
     * @return the vehicle's model
     */
    public String getModel() {
        return model;
    }

    /**
     * Sets the model (design) of this } Vehicle}. This is a required field
     * and has a maximum length of fifteen (15) characters. If the specified 
     * model is longer than the maximum length, it will be truncated.
     * 
     * @param model the vehicle's model
     * @throws IllegalArgumentException if } model} is blank, empty, or
     *          `null`
     */
    public void setModel(String model) {
        if (model == null || model.isBlank() || model.isEmpty()) {
            throw new IllegalArgumentException("blank, empty, or null model");
        } else if (model.length() > 15) {
            model = model.substring(0, 15);
        }
        
        this.model = model;
    }

    /**
     * Retrieves the color of this } Vehicle} as a string value.
     * 
     * @return the vehicle's color
     */
    public String getColor() {
        return color;
    }

    /**
     * Sets the color of this } Vehicle} as a string value. This field is
     * optional, but `null` values are not allowed. If the specified
     * } color} has a `null` value, it will be converted to an empty
     * string and no exception will be thrown.
     * 
     * @param color the vehicle's color
     */
    public void setColor(String color) {
        if (color == null) {
            color = "";
        }
        
        this.color = color;
    }

    /**
     * Retrieves the number assigned to this } Vehicle}.
     * 
     * @return the vehicle's number
     */
    public String getNumber() {
        return number;
    }

    /**
     * Sets the number assigned to this } Vehicle}. This is a required 
     * field and has a maximum length of fifteen (15) characters. This field
     * maintains the company-assigned vehicle identification number and may
     * contain letters and numbers, as well as not more than one dash. The data
     * supplied via the } number} parameter will be validated and an 
     * `IllegalArgumentException` will be thrown if the value is blank,
     * empty, or `null`. An } InvalidDataException} will be thrown if
     * the specified value contains anything other than letters, numbers, or a
     * single dash character. All letters in the vehicle number should be upper
     * case. If there are lower case letters within the number, they will be
     * converted to upper case by this setter.
     * 
     * @param number the vehicle's number
     * @throws IllegalArgumentException
     * @throws InvalidDataException
     */
    public void setNumber(String number) {
        if (number == null || number.isBlank() || number.isEmpty()) {
            throw new IllegalArgumentException("blank, empty, or null number");
        } else {
            number = replaceLowerCaseWithUpperCase(number);
        }
        
        VehicleNumberValidator validator = new VehicleNumberValidator(number);
        if (!validator.isValid()) {
            throw new InvalidDataException(number + " is not a valid vehicle "
                    + "number");
        }
        
        this.number = number;
    }

    public LocalDate getPurchaseDate() {
        return purchaseDate;
    }
    
    public String getPurchaseDateAsString() {
        return fmt.format(purchaseDate);
    }

    public void setPurchaseDate(LocalDate purchaseDate) {
        this.purchaseDate = purchaseDate;
    }
    
    public void setPurchaseDate(String purchaseDate) {
        this.purchaseDate = LocalDate.parse(purchaseDate, fmt);
    }

    public LocalDate getSalesDate() {
        return salesDate;
    }
    
    public String getSalesDateAsString() {
        return fmt.format(salesDate);
    }

    public void setSalesDate(LocalDate salesDate) {
        this.salesDate = salesDate;
    }
    
    public void setSalesDate(String salesDate) {
        this.salesDate = LocalDate.parse(salesDate, fmt);
    }

    public long getVehicleType() {
        return vehicleType;
    }

    public void setVehicleType(long vehicleType) {
        this.vehicleType = vehicleType;
    }

    public long getId() {
        return id;
    }
    
    private void setId(long id) {
        this.id = id;
    }
    
    private String replaceLowerCaseWithUpperCase(String number) {
        String temp = number;
        
        for (int x = 0; x < number.length(); x++) {
            if (Character.isLetter(number.charAt(x))
                    && Character.isLowerCase(number.charAt(x))) {
                char ucase = Character.toUpperCase(number.charAt(x));
                temp = number.replace(number.charAt(x), ucase);
            }
        }
        
        return temp;
    }

    @Override
    public String store() {
        StringBuilder sb = new StringBuilder();
        
        sb.append(getId()).append("~").append(getMake()).append("~");
        sb.append(getModel()).append("~").append(getVehicleType()).append("~");
        sb.append(getColor()).append("~").append(getNumber()).append("~");
        sb.append(getPurchaseDate()).append("~").append(getSalesDate());
        
        return sb.toString() + "\n";
    }

    @Override
    public Vehicle restore(String line) {
        String[] fields = line.split("~");
        
        setId(Long.valueOf(fields[0]));
        setMake(fields[1]);
        setModel(fields[2]);
        setVehicleType(Long.valueOf(fields[3]));
        setColor(fields[4]);
        setNumber(fields[5]);
        setPurchaseDate(fields[6]);
        setSalesDate(fields[7]);
        
        return this;
    }

}
