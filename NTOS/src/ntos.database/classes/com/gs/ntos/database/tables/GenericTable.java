/*
 * Copyright (C) 2022 GS United Labs
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * *****************************************************************************
 *  Project    :   NTOS
 *  Class      :   GenericTable.java
 *  Author     :   Sean Carrick
 *  Created    :   Apr 13, 2022
 *  Modified   :   Apr 13, 2022
 *  
 *  Purpose: See class JavaDoc for explanation
 *  
 *  Revision History:
 *  
 *  WHEN          BY                   REASON
 *  ------------  -------------------  -----------------------------------------
 *  Apr 13, 2022  Sean Carrick         Initial creation.
 * *****************************************************************************
 */

package com.gs.ntos.database.tables;

import com.gs.ntos.api.DesktopProgram;
import com.gs.ntos.database.errors.DataStoreException;
import com.gs.ntos.database.errors.ReferentialIntegrityException;
import com.gs.ntos.database.models.SelfStorage;
import com.gs.ntos.enums.Level;
import com.gs.ntos.errors.CryptoException;
import com.gs.ntos.logging.Logger;
import com.gs.ntos.utils.CryptoUtils;
import com.gs.ntos.utils.FileUtils;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.lang.reflect.Array;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;

/**
 * The `GenericTable` class is an abstract class that provides the majority of
 * the common functionality of data tables, such as navigation, loading, storing,
 * adding batched records, deleting batched records, and updating batched records.
 * Subclasses only need to provide implementations for adding, deleting, and
 * updating individual records, as well as any specialized search functionality
 * required by the specific table for which the subclass is created. 
 * 
 * This class provides the following fields for the the subclasses:
 
 * * `DesktopProgram program`: the `DesktopProgram` in which the table is used
 * * `Logger logger`: the `Logger` object for logging messages to log files
 * * `List&lt;T&gt; records`: the records for the table
 * 
 * 
 * @param <T> class type for the subclass
 *
 * @author Sean Carrick &lt;sean at gs-unitedlabs dot com&gt;
 * 
 * @version 1.0
 * @since 1.0
 */
public abstract class GenericTable<T> {
    
    protected final DesktopProgram program = DesktopProgram.getInstance();
    protected final Logger logger;
    protected final List<T> records;
    
    private final File dataFile;
    protected final File encryptedFile;
    private final Class<T> cls;
    
    private boolean newFile;
    private int currentRecord;
    
    /**
     * Constructs a new `GenericTable`, for the specified class type (`&lt;T&gt;`),
     * with the data file base name `fileBaseName`.
     * 
     * @param fileBaseName the base name for the data file. This is the file name
     *          with neither the path nor the extension
     * @param cls the class type for the underlying table data model
     */
    protected GenericTable (String fileBaseName, Class<T> cls) {
        logger = new Logger(program, getClass().getSimpleName(), (Level)
                program.getProperties().getSystemProperty("logging.level",
                        Level.ALL));
        
        this.cls = cls;
        
        dataFile = new File(FileUtils.getApplicationDataDirectory(), 
                fileBaseName + ".dat");
        encryptedFile = new File(FileUtils.getApplicationDataDirectory(),
                fileBaseName + ".ntbl");
        
        newFile = !dataFile.exists();
        
        records = new ArrayList<>();
    }
    
    /**
     * Adds the new `record` to the table. This method returns a boolean value
     * based on the success of the record's addition. If the specified `record`
     * is `null`, no exception is thrown and no action is taken. In that case,
     * this method simply returns `false`.
     * 
     * @param record the record of type `&lt;T&gt;` to add to the table
     * @return `true` upon successful addition; `false` upon failure
     * 
     * @see #add(java.util.List) 
     */
    public abstract boolean add(T record);
    
    /**
     * Adds the specified list of records of type `&lt;T&gt;` to the table. This
     * method returns the count of successfully added records from the list of
     * new records. The default implementation of this method simply loops through
     * the specified list of records and calls `add(T)` for each one.
     * 
     * @param recordsToAdd the list of records of type `&lt;T&gt;` to add
     * @return the count of records successfully added from the given list
     * 
     * @see #add(java.lang.Object) 
     */
    public int add(List<T> recordsToAdd) {
        if (recordsToAdd == null || recordsToAdd.isEmpty()) {
            return 0;
        }
        
        int counter = 0;
        
        for (T r : recordsToAdd) {
            if (add(r)) {
                counter++;
            }
        }
        
        return counter;
    }
    
    /**
     * Closes the connection to the table.
     * 
     * @throws DataStoreException if any errors occur while closing down the
     *          table
     * 
     * @see #close() 
     */
    public void close() throws DataStoreException {
        save();
    }
    
    /**
     * Connects to the table.
     * 
     * @throws DataStoreException if any errors occur while connecting to the
     *          table
     * 
     * @see #connect() 
     */
    public void connect() throws DataStoreException {
        load();
    }
    
    /**
     * Deletes the specified `record` from the table. If the specified `record`
     * is `null`, or does not exist in the table, no exception is thrown, no
     * action is taken, and `false` is returned.
     * 
     * @param record the record to delete from the table
     * @return `true` if successfully deleted; `false` otherwise
     * @throws ReferentialIntegrityException if the record is subject to any
     *          foreign key relationships and is referenced by any other table(s)
     * 
     * @see #delete(java.util.List) 
     */
    public abstract boolean delete(T record) throws ReferentialIntegrityException;
    
    /**
     * Deletes each of the records in the specified list of records from the
     * table. If the list is `null` or contains any `null` records, no action is
     * taken, no exception is thrown, and the returned count of deleted records
     * will be zero for a `null` list, or reduced by one for each `null` record
     * in the list.
     * 
     * @param recordsToDelete the list of records to be deleted from the table
     * @return the count of records successfully deleted
     * @throws ReferentialIntegrityException for any record in the list that is
     *          subject to any foreign key relationships and is referenced by 
     *          any other table(s)
     * 
     * @see #delete(java.lang.Object) 
     */
    public int delete(List<T> recordsToDelete) 
            throws ReferentialIntegrityException {
        int counter = 0; 
        
        for (T r : recordsToDelete) {
            if (delete(r)) {
                counter++;
            }
        }
        
        return counter;
    }
    
    /**
     * A record validation method that ***must be*** implemented by subclasses
     * to maintain data normalization and integrity. This method should be used
     * to verify that a new record does not already exist in the table and that
     * a record to delete or update does. Furthermore, this method should be used
     * to be sure a given value is not stored in the database more than once.
     * 
     * @param record the record to check for existence
     * @return `true` if exists; `false` if it does not
     */
    protected abstract boolean doesRecordExistInTable(T record);
    
    /**
     * Moves the record pointer to the first record in the table. This method is
     * guaranteed to never move the record pointer to a point *before* the first
     * record.
     * 
     * @see #last() 
     * @see #next() 
     * @see #previous() 
     */
    public void first() {
        currentRecord = 0;
    }
    
    /**
     * Retrieves the current record from the table.
     * 
     * @return the current record
     * 
     * @see #get(int) 
     * @see #getAllRecords(java.lang.Class) 
     */
    public T get() {
        return records.get(currentRecord);
    }
    
    /**
     * Retrieves the record at the specified `index` from the table.
     * 
     * @param index the index from which to retrieve the record
     * @return the specified record
     * 
     * @see #get() 
     * @see #getAllRecords(java.lang.Class) 
     */
    public T get(int index) {
        return records.get(index);
    }
    
    /**
     * Retrieves all of the records in the table as an array of objects.
     * 
     * @return an array of all of the records
     * 
     * @see #get() 
     * @see #get(int) 
     */
    public T[] getAllRecords() {
        T[] elements = (T[]) Array.newInstance(cls, records.size());
        
        for (int i = 0; i < elements.length; i++) {
            elements[i] = records.get(i);
        }
        
        return elements;
    }
    
    /**
     * Retrieves the index location of the record pointer in the table.
     * 
     * @return the current index position of the record pointer
     */
    public int getCurrentRecordNumber() {
        return currentRecord;
    }
    
    /**
     * Retrieves the total number of records the table contains.
     * 
     * @return the total number of records
     */
    public int getRecordCount() {
        return records.size();
    }
    
    /**
     * Determines if there are more records in the table past the current index
     * of the record pointer.
     * 
     * @return `true` if more records exist; `false` otherwise
     */
    public boolean hasNext() {
        return currentRecord < records.size() - 1;
    }
    
    /**
     * A record validation method that ***may be*** implemented by subclasses to
     * check if a record that is being deleted is referenced by any foreign
     * tables. The default implementation simply returns `false`.
     * 
     * @param record the record to check for relationships
     * @return `true` if referenced from foreign table(s); `false` if not
     */
    protected boolean isRecordReferenced(T record) {
        return false;
    }
    
    /**
     * Moves the record pointer to the last record in the table. This method is
     * guaranteed to never move the record pointer *beyond* the last record in
     * the table.
     * 
     * @see #first() 
     * @see #next() 
     * @see #previous() 
     */
    public void last() {
        currentRecord = records.size() - 1;
    }
    
    /**
     * This method loads the records from the underlying data store. The default
     * implementation creates a new instance of the class `&lt;T&ct;` and checks
 to make sure it implements `SelfStorage`. If it does, then the object is
 initialized by sending the read-in line from the data file to the class'
 `restore` method to allow it to construct itself.
 
 <dl><strong><em>Developer's Note</em></strong>:</td>
     * <dd>This method traps for the exceptions involved with instantiating the
     * generic class type that defines this table's underlying data model, but
     * not any of the file operations.</dd></dl>
     * 
     * @throws DataStoreException if any type of error occurs while reading in
     *          the data
     * 
     * @see com.gs.ntos.database.models.SelfStorage
     * @see #connect() 
     * @see #save() 
     */
    protected void load() throws DataStoreException {
        if (!newFile) {
            // Decrypt the data file to make it readable.
//            try {
//                CryptoUtils.decrypt(dataFile.getAbsolutePath(), encryptedFile, 
//                        dataFile);
//                encryptedFile.delete();
//            } catch (CryptoException ex) {
//                throw new DataStoreException("Data File Preparation Error: " 
//                        + ex.getMessage(), ex);
//            }

            FileReader fr = null;
            BufferedReader in = null;

            try {
                fr = new FileReader(dataFile);
                in = new BufferedReader(fr);

                String line = null;
                while ((line = in.readLine()) != null) {
                    T r = null;
                    try {
                        Constructor ctor = cls.getConstructor();
                        if (!ctor.canAccess(null)) {
                            ctor.setAccessible(true);
                        }

                        r = (T) ctor.newInstance();
                    } catch (NoSuchMethodException
                            | SecurityException
                            | InstantiationException
                            | InvocationTargetException
                            | IllegalAccessException
                            | IllegalArgumentException ex) {
                        logger.error(ex, "Error creating a new T object: {0}", 
                                ex.getMessage());
                    }

                    if (r != null && r instanceof SelfStorage) {
                        records.add((T) ((SelfStorage) r).restore(line));
                    }

                   }
            } catch (IOException e) {
                throw new DataStoreException("Read Error: " + e.getMessage(), e);
            } finally {
                try {
                    if (in != null) {
                        in.close();
                    }
                    if (fr != null) {
                        fr.close();
                    }
                } catch (IOException ex) {
                    throw new DataStoreException("Close Error: " + ex.getMessage(), ex);
                }
            }

            // Encrypt the data file to make it unreadable.
//            try {
//                CryptoUtils.encrypt(dataFile.getAbsolutePath(), dataFile, 
//                        encryptedFile);
//                dataFile.delete();
//            } catch (CryptoException ex) {
//                throw new DataStoreException("Data File Finalization Error: " 
//                        + ex.getMessage(), ex);
//            }
        }
    }
    
    /**
     * Moves the record pointer to the next record in the table. This method is
     * guaranteed to never move the record pointer *beyond* the last record in
     * the table.
     * 
     * @see #first() 
     * @see #last() 
     * @see #previous() 
     */
    public void next() {
        currentRecord++;
        
        if (currentRecord >= records.size()) {
            last();
        }
    }
    
    /**
     * Moves the record pointer to the previous record in the table. This method
     * is guaranteed to never move the record pointer *before* the first record
     * in the table.
     * 
     * @see #first() 
     * @see #last() 
     * @see #next() 
     */
    public void previous() {
        currentRecord--;
        
        if (currentRecord < 0) {
            first();
        }
    }
    
    /**
     * Saves the table data to a file on disk. This method assumes that the data
     * model underlying this table implements the `SelfStorage` interface, allowing
     * each record to serialize themselves for file storage. If the underlying
     * data model does not implement this interface, then this method should be
     * overridden by the subclass.
     * 
     * @throws DataStoreException if any file operations throw an error
     * 
     * @see com.gs.ntos.database.models.SelfStorage
     * @see #close() 
     * @see #load() 
     */
    protected void save() throws DataStoreException {
//        if (!newFile) {
//            // Decrypt the data file to make it readable.
//            try {
//                CryptoUtils.decrypt(dataFile.getAbsolutePath(), encryptedFile, 
//                        dataFile);
//                encryptedFile.delete();
//            } catch (CryptoException ex) {
//                throw new DataStoreException("Data File Preparation Error: " 
//                        + ex.getMessage(), ex);
//            }
//        }
        
        FileWriter fw = null;
        BufferedWriter out = null;
        
        try {
            fw = new FileWriter(dataFile);
            out = new BufferedWriter(fw);
            
            for (T r : records) {
                if (r instanceof SelfStorage) {
                    out.write(((SelfStorage) r).store());
                }
            }
            
            out.flush();
        } catch (IOException e) {
            throw new DataStoreException("Read Error: " + e.getMessage(), e);
        } finally {
            try {
                if (out != null) {
                    out.close();
                }
                if (fw != null) {
                    fw.close();
                }
            } catch (IOException ex) {
                throw new DataStoreException("Close Error: " + ex.getMessage(), ex);
            }
        }
        
        // Encrypt the data file to make it unreadable.
//        try {
//            CryptoUtils.encrypt(dataFile.getAbsolutePath(), dataFile, 
//                    encryptedFile);
//            dataFile.delete();
//        } catch (CryptoException ex) {
//            throw new DataStoreException("Data File Finalization Error: " 
//                    + ex.getMessage(), ex);
//        }
        
        newFile = encryptedFile.exists();
    }

    /**
     * Sets the current record index value for the record pointer into this 
     * table. This method is provided for internal use only by this class and
     * its subclasses to use if needed.
     * 
     * @param index the new record pointer index location
     */
    protected void setCurrentRecordNumber(int index) {
        if (index < 0 || index > getRecordCount()) {
            throw new IndexOutOfBoundsException(index + " is outside of valid "
                    + "range");
        }
        
        currentRecord = index;
    }
    
    /**
     * Updates the specified record in the table. This method assumes that an
     * internal, read-only unique ID value exists in the record, as that is the
     * value that is checked to update the proper record in the table.
     * 
     * @param record the updated record data
     * @return `true` upon successful update; `false` otherwise
     * 
     * @see #update(java.util.List) 
     */
    public abstract boolean update(T record);
    
    /**
     * Updates the specified list of records. If the specified list is `null` or
     * empty, no exception is thrown, no action is taken, and zero is returned.
     * If any of the records in the specified list are null, no exception is
     * thrown and no action is taken, the value returned by this method will be
     * one less for each `null` record.
     * 
     * @param recordsToUpdate the list of modified records to update
     * @return the count of successful updates
     * 
     * @see #update(java.lang.Object) 
     */
    public int update(List<T> recordsToUpdate) {
        if (recordsToUpdate == null || recordsToUpdate.isEmpty()) {
            return 0;
        }
        
        int counter = 0;
        
        for (T r : recordsToUpdate) {
            if (r != null) {
                if (update(r)) {
                    counter++;
                }
            }
        }
        
        return counter;
    }
    
}
