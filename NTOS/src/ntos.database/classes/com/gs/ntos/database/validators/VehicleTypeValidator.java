/*
 * Copyright (C) 2022 GS United Labs
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * *****************************************************************************
 *  Project    :   ntos.database
 *  Class      :   AccountValidator.java
 *  Author     :   Sean Carrick
 *  Created    :   Mar 23, 2022
 *  Modified   :   Mar 23, 2022
 *  
 *  Purpose: See class JavaDoc for explanation
 *  
 *  Revision History:
 *  
 *  WHEN          BY                   REASON
 *  ------------  -------------------  -----------------------------------------
 *  Mar 23, 2022  Sean Carrick         Initial creation.
 * *****************************************************************************
 */

package com.gs.ntos.database.validators;

import com.gs.ntos.database.errors.DataStoreException;
import com.gs.ntos.database.models.VehicleType;
import com.gs.ntos.database.tables.VehicleTypesTable;
import com.gs.ntos.support.TerminalErrorPrinter;

/**
 *
 * @author Sean Carrick &lt;sean at gs-unitedlabs dot com&gt;
 * 
 * @version 0.1.0
 * @since 0.1.0
 */
public class VehicleTypeValidator {
    
    private final VehicleTypesTable table;
    private final long vehicleTypeId;
    private VehicleType[] allTypes;
    
    public VehicleTypeValidator (long vehicleTypeId) {
        this.vehicleTypeId = vehicleTypeId;
        table = new VehicleTypesTable();
        
        try {
            table.connect();
            allTypes = table.getAllRecords();
            table.close();
        } catch (DataStoreException ex) {
            TerminalErrorPrinter.print(ex, "Attempting to get all vehicle types.");
        }
    }
    
    public boolean isPresent() {
        for (VehicleType v : allTypes) {
            if (v.getId() == vehicleTypeId) {
                return true;
            }
        }
        
        return false;
    }

}
