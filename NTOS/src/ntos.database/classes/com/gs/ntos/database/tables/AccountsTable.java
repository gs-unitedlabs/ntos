/*
 * Copyright (C) 2022 GS United Labs
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * *****************************************************************************
 *  Project    :   ntos.database
 *  Class      :   AccountsTable.java
 *  Author     :   Sean Carrick
 *  Created    :   Mar 23, 2022
 *  Modified   :   Mar 23, 2022
 *  
 *  Purpose: See class JavaDoc for explanation
 *  
 *  Revision History:
 *  
 *  WHEN          BY                   REASON
 *  ------------  -------------------  -----------------------------------------
 *  Mar 23, 2022  Sean Carrick         Initial creation.
 * *****************************************************************************
 */

package com.gs.ntos.database.tables;

import com.gs.ntos.database.errors.DataStoreException;
import com.gs.ntos.database.errors.InvalidDataException;
import com.gs.ntos.database.errors.ReferentialIntegrityException;
import com.gs.ntos.database.models.Account;
import com.gs.ntos.database.models.AccountType;
import com.gs.ntos.database.models.Transaction;
import java.util.ArrayList;
import java.util.List;

/**
 * An implementation of the `GenericTable` specifically for `Account`s. This
 * table maintains the *Chart of Accounts* for the accounting system.
 *
 * @author Sean Carrick &lt;sean at gs-unitedlabs dot com&gt;
 * 
 * @version 0.1.0
 * @since 0.1.0
 */
public class AccountsTable extends GenericTable<Account> {
    
    public AccountsTable () {
        super("accounts", Account.class);
    }

    @Override
    public boolean add(Account record) {
        if (record == null) {
            return false;
        }
        if (doesRecordExistInTable(record)) {
            throw new InvalidDataException("The record with the ID " + record.getId()
                    + " already exists in the table. Call update(Account) "
                            + "instead.");
        }
        
        boolean result = records.add(record);
        
        try {
            save();
        } catch (DataStoreException ex) {
            logger.error(ex, "Attempting to save data after new record added: "
                    + "{0}", record);
        }
        
        return result;
    }

    @Override
    public boolean delete(Account record) throws ReferentialIntegrityException {
        if (record == null) {
            return false;
        }
        if (!doesRecordExistInTable(record)) {
            return false;
        }
        if (isRecordReferenced(record)){
            throw new ReferentialIntegrityException("Account, "
                    + record.getName() + ", has foreign key references.\n\n"
                    + "These references must be deleted before this account "
                    + "type record can be deleted.");
        }
        
        int idx = records.indexOf(record);
        boolean result = records.remove(record);
        
        if (idx >= records.size() && idx == getCurrentRecordNumber()){
            setCurrentRecordNumber(records.size() - 1);
        } else {
            setCurrentRecordNumber(idx);
        }
        
        return result;
    }

    @Override
    protected boolean doesRecordExistInTable(Account record) {
        for (Account t : records) {
            if (t.getId() == record.getId()) {
                return true;
            }
        }
        
        return false;
    }

    @Override
    protected boolean isRecordReferenced(Account record) {
        LedgerTable entries = new LedgerTable();
        Transaction[] trans = null;
        try {
            entries.connect();
            trans = entries.getTxForAccount(record);
            entries.close();
        } catch (DataStoreException ex) {
            logger.error(ex, "Attempting to connect to the Accounts table {0}", 
                    entries);
        }
        
        return trans.length > 0;
    }

    @Override
    public boolean update(Account record) {
        if (record == null) {
            return false;
        }
        if (!doesRecordExistInTable(record)) {
            throw new InvalidDataException("Record with ID " + record.getId()
                    + " does not exist in the table. Call add(AccountType) "
                    + "instead.");
        }
        
        for (Account t : records) {
            if (t.getId() == record.getId()) {
                int idx = records.indexOf(t);
                records.remove(idx);
                records.add(idx, record);
                return true;
            }
        }
        
        return false;
    }
    
    public Account[] getAccountsForType(AccountType type) {
        List<Account> accounts = new ArrayList<>();
        for (Account a : records) {
            if (a.getTypeId() == type.getId()) {
                accounts.add(a);
            }
        }
        
        return accounts.toArray(new Account[accounts.size()]);
    }

}
