/*
 * Copyright (C) 2022 GS United Labs
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * *****************************************************************************
 *  Project    :   ntos.loads.wizard
 *  Class      :   Broker.java
 *  Author     :   Sean Carrick
 *  Created    :   Mar 16, 2022 @ 8:19:19 PM
 *  Modified   :   Mar 16, 2022
 *  
 *  Purpose: See JavaDoc comments.
 *  
 *  Revision History:
 *  
 *  WHEN          BY                   REASON
 *  ------------  -------------------  ------------------------------------------
 *  Mar 16, 2022  Sean Carrick         Initial creation.
 * *****************************************************************************
 */

package com.gs.ntos.database.models;

import com.gs.ntos.database.errors.InvalidEmailException;
import com.gs.ntos.database.errors.InvalidPhoneNumberException;
import com.gs.ntos.database.errors.InvalidPostalCodeException;
import com.gs.ntos.database.errors.InvalidStateOrProvinceException;
import com.gs.ntos.validators.EmailValidator;
import com.gs.ntos.validators.PhoneNumberValidator;
import com.gs.ntos.validators.PostalCodeValidator;
import com.gs.ntos.validators.StateOrProvinceValidator;
import java.io.Serializable;

/**
 * This Java Bean defines the data that needs to be collected for a broker or
 * agent for the Northwind Truckers' Operating System (NTOS).
 * <p>
 * The data fields defined in this Bean are declared as `protected` in
 * case a situation arises in the future that this Bean needs to be extended. In
 * that situation, the data definitions in this Bean will be directly accessible
 * by the subclass, and the documentation should describe the length and the
 * requirements of the data.</p>
 *
 * @author Sean Carrick &lt;sean at gs-unitedlabs dot com&gt;
 * 
 * @version 0.1.0
 * @since 0.1.0
 */
public class Broker implements SelfStorage<Broker>, Serializable {

    private static final long serialVersionUID = 5381576619391858394L;
    
    /** Required! Must be unique! */
    protected long id;
    /** Required. Null is not allowed. Max length is 50. */ 
    protected String company = "[Unspecified]";
    /** Optional. Null is not allowed. Max length is 50. */
    protected String street = "[Unspecified]";
    /** Optional. Null IS allowed. Max length is 15. */
    protected String suite;
    /** Optional. Null is not allowed. Max length is 50. */
    protected String city = "[Unspecified]";
    /** Optional. Null is not allowed. Max length is 2. */
    protected String state = "[Unspecified]";
    /** Optional. Null is not allowed. Max length is 10. */
    protected String postalCode = "[Unspecified]";
    /** Required! Max length is 40. */
    protected String contact = "[Unspecified]";
    /** Required! If no phone is provided. Max length is 50. */
    protected String email = "[Unspecified]";
    /** Required! If no email is provided. Max length is 14. */
    protected String phone = "(   )    -    ";
    /** Optional. Null is not allowed. Max length is 14. */
    protected String fax = "(   )    -    ";
    /** Optional. Null is not allowed. Max length is 2000. */
    protected String notes;
    
    /**
     * Creates a new default `Broker` instance. This constructor 
     * automatically assigns the ID as a unique `long` value.
     */
    public Broker () {
        id = System.currentTimeMillis();
    }

    /**
     * Retrieves the unique ID value for this `Broker`.
     * 
     * @return the broker's ID
     */
    public long getId() {
        return id;
    }

    /**
     * Sets the unique ID for this `Broker`. This is a <strong>required
     * </strong> field and <em>must be</em> unique within the table.
     * 
     * @param id the new unique ID
     */
    private void setId(long id) {
        this.id = id;
    }

    /**
     * Retrieves the company name for this `Broker`.
     * 
     * @return the broker's company name
     */
    public String getCompany() {
        return company;
    }

    /**
     * Sets the company name for this `Broker`. This is a <em>required
     * </em> field. It maximum length is 50 characters. May not be `null`.
     * 
     * @param company the new company name
     * @throws IllegalArgumentException if `company` is blank, empty, or `null`
     */
    public void setCompany(String company) {
        if (company == null || company.isBlank() || company.isEmpty()) {
            throw new IllegalArgumentException("blank, empty, or null company");
        }
        
        this.company = company.length() <= 50 ? company : company.substring(0, 49);
    }

    /**
     * Retrieves the street address for this `Broker`.
     * 
     * @return the broker's street address
     */
    public String getStreet() {
        return street;
    }

    /**
     * Sets the street address for this `Broker`. This is an <em>optional
     * </em> field. The maximum length is 50 characters. May not be `null`.
     * 
     * @param street the new street address
     */
    public void setStreet(String street) {
        if (street != null && street.length() >= 50) {
            street = street.substring(0, 49);
        }
        this.street = street == null ? "" : street;
    }

    /**
     * Retrieves the suite number for this `Broker`.
     * 
     * @return the broker's suite number
     */
    public String getSuite() {
        return suite;
    }

    /**
     * Sets the suite number for this `Broker`. This is an <em>optional
     * </em> field. The maximum length is 15 characters and `null` is 
     * allowed.
     * 
     * @param suite the new suite number
     */
    public void setSuite(String suite) {
        if (suite != null && suite.length() >= 15) {
            suite = suite.substring(0, 14);
        }
        this.suite = suite == null ? null : suite;
    }

    /**
     * Retrieves the city in which this `Broker` is located.
     * 
     * @return the broker's city
     */
    public String getCity() {
        return city;
    }

    /**
     * Sets the city in which this `Broker` is located. This is an <em>
     * optional</em> field. The maximum length is 50 characters. May not be
     * `null`
     * 
     * @param city the new city
     */
    public void setCity(String city) {
        if (city != null && city.length() >= 50) {
            city = city.substring(0, 49);
        }
        this.city = city ==  null ? "" : city;
    }

    /**
     * Retrieves the state in which this `Broker` is located.
     * 
     * @return the broker's state
     */
    public String getState() {
        return state;
    }

    /**
     * Sets the state in which this `Broker` is located. This is an <em>
     * optional</em> field. The maximum length is 2 characters. May not be 
     * `null`.
     * 
     * @param state the new state
     * @throws InvalidStateOrProvinceException in the event the specified 
     *          `state` is not `null`, blank, or empty, and not a valid US State
     *          or Canadian Province
     */
    public void setState(String state) throws InvalidStateOrProvinceException {
        if (state != null && !state.isBlank() && !state.isEmpty()) {
            if (!(new StateOrProvinceValidator().validate(state))) {
                throw new InvalidStateOrProvinceException(state + " is not a valid "
                        + "US State or Canadian Province.");
            }
        }
        this.state = state;
    }

    /**
     * Retrieves the Postal Code for this `Broker`.
     * 
     * @return the broker's Postal Code
     */
    public String getPostalCode() {
        return postalCode;
    }

    /**
     * Sets the Postal Code for this `Broker`. This is an <em>optional</em>
     * field. The maximum length is 10 characters. May not be `null`.
     * 
     * @param postalCode the new Postal Code
     * @throws InvalidPostalCodeException in the event the specified  
     *          `postalCode` is not `null`, blank, or empty, and is not a valid 
     *          US Zip or Canadian Postal Code
     */
    public void setPostalCode(String postalCode) throws InvalidPostalCodeException {
        if (postalCode != null && !postalCode.isBlank() && !postalCode.isEmpty()) {
            if (!(new PostalCodeValidator().validate(postalCode))) {
                throw new InvalidPostalCodeException(postalCode + " is not a valid "
                        + "US Zip or Canadian Postal Code.");
            }
        }
        this.postalCode = postalCode;
    }

    /**
     * Retrieves the name of the contact person at this `Broker`.
     * 
     * @return the contact person's name
     */
    public String getContact() {
        return contact;
    }

    /**
     * Sets the name of the contact person at this `Broker`. This is a
     * <strong>required</strong> field. The maximum length is 40 characters.
     * `null` is not allowed.
     * 
     * @param contact the new contact person's name
     * @throws IllegalArgumentException in the event `contact is `null`
     */
    public void setContact(String contact) {
        if (contact == null || contact.isBlank() || contact.isEmpty()) {
            throw new IllegalArgumentException("Blank, empty, or null contact");
        } else if (contact.length() >= 40) {
            contact = contact.substring(0, 39);
        }
        this.contact = contact;
    }

    /**
     * Retrieves the email address for this `Broker`.
     * 
     * @return the broker's email address
     */
    public String getEmail() {
        return email;
    }

    /**
     * Sets the email address for this `Broker`. This is one of two fields
     * that store contact information, of which one must be supplied. The 
     * maximum length for this value is 50 characters and `null` is not
     * allowed.
     * 
     * @param email the broker's new email address
     * @throws InvalidEmailException in the event `email` is not a validly
     *          formatted email address
     */
    public void setEmail(String email) throws InvalidEmailException {
        if (email == null) {
            email = "";
        }
        if (!email.isBlank() && !email.isEmpty()) {
            if (!(new EmailValidator().validate(email))) {
                throw new InvalidEmailException(email + " is not in a proper format");
            }
        }
        if (email.length() >= 50) {
            email = email.substring(0, 49);
        }
        this.email = email;
    }

    /**
     * Retrieves the phone number for this `Broker`.
     * 
     * @return the broker's phone number
     */
    public String getPhone() {
        return phone;
    }

    /**
     * Sets the phone number for this `Broker`. This is one of two fields
     * that store contact information, of which one must be supplied. The 
     * maximum length for this value is 19 characters (to allow for international
     * dialing prefix to be added). `null` is not allowed.
     * 
     * @param phone the broker's new phone number
     * @throws InvalidPhoneNumberException in the event `phone` is not in
     *          a valid phone number format
     */
    public void setPhone(String phone) throws InvalidPhoneNumberException {
        if (phone == null) {
            phone = "";
        }
        if (!phone.equals("(   )    -    ") && !phone.isBlank() && !phone.isEmpty()) {
            if (!(new PhoneNumberValidator().validate(phone))) {
                throw new InvalidPhoneNumberException(phone + " is not a valid "
                        + "phone number.");
            }
        }
        
        this.phone = phone;
    }

    /**
     * Retrieves the fax number for this `Broker`.
     * 
     * @return the broker's fax number
     */
    public String getFax() {
        return fax;
    }

    /**
     * Sets the fax number for this `Broker`. This field is <em>optional
     * </em> and may not be `null`. Also, if a value is provided, it must
     * be in a valid phone number format.
     * 
     * @param fax the broker's new fax number
     * @throws InvalidPhoneNumberException in the event `fax` is not in a
     *          valid format for phone numbers
     */
    public void setFax(String fax) throws InvalidPhoneNumberException {
        if (fax == null) {
            fax = "";
        }
        if (!fax.equals("(   )    -    ") && !fax.isBlank() && !fax.isEmpty()) {
            if (!(new PhoneNumberValidator().validate(fax))) {
                throw new InvalidPhoneNumberException(fax + " is not a valid "
                        + "fax number.");
            }
        }
        
        this.fax = fax;
    }

    /**
     * Retrieves any notes regarding this `Broker`.
     * 
     * @return the notes about the broker
     */
    public String getNotes() {
        return notes;
    }

    /**
     * Sets any notes regarding this `Broker`.
     * 
     * @param notes the notes about the broker
     */
    public void setNotes(String notes) {
        if (notes == null) {
            notes = "";
        } else if (notes.length() >= 2000) {
            notes = notes.substring(0, 1999);
        }
        this.notes = notes;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null || !(obj instanceof Broker)) {
            return false;
        }
        
        return getId() == ((Broker) obj).getId()
                && getCompany().equals(((Broker) obj).getCompany())
                && getContact().equals(((Broker) obj).getContact())
                && getEmail().equals(((Broker) obj).getEmail())
                && getPhone().equals(((Broker) obj).getPhone())
                && hashCode() == obj.hashCode();
    }

    @Override
    public int hashCode() {
        int hash = getCompany().hashCode();
        hash += getContact().hashCode();
        hash += (getEmail() != null) ? getEmail().hashCode() : 575;
        hash += (!getPhone().equals("(   )    -    ")) ? getPhone().hashCode()
                : 575;
        hash *= 12;
        return hash;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getContact()).append(" [").append(getId()).append("] of ");
        sb.append(getCompany());
        sb.append((getCity() != null || getState() != null) ? " (" : "");
        sb.append(getCity() != null ? getCity() : "");
        sb.append(getState() != null ? ", " : "");
        sb.append(getState() != null ? getState() : "");
        sb.append((getCity() != null || getState() != null) ? ")" : "");
        sb.append("@[").append(Integer.toHexString(hashCode())).append("]");
        
        return sb.toString();
    }
    
    /**
     * Retrieves the entire address for the broker. The address will be returned 
     * as:
     * <pre>
     * Contact Name
     * Company Name
     * Street Address[, SuiteNumber]
     * City, ST Postal
     * </pre>
     * The value returned from this method is formatted just right for use on
     * envelopes or letterhead.
     * 
     * @return the broker's address (formatted as above) or an empty string
     */
    public String getPrintableAddress() {
        if (id <= 0L) {
            throw new IllegalArgumentException(id + " <= 0");
        }
        
        StringBuilder sb = new StringBuilder();
        
        sb.append(getContact()).append("\n");
        sb.append(getCompany()).append("\n");
        sb.append(getStreet());
        
        if (getSuite() == null || getSuite().isBlank() || getSuite().isEmpty()) {
            sb.append("\n");
        } else {
            sb.append(", ").append(getSuite()).append("\n");
        }
        
        sb.append(getCity()).append(", ").append(getState()).append(" ");
        sb.append(getPostalCode());
        
        if (sb.toString() == null) {
            sb = new StringBuilder();
            sb.append("");
        }
        
        return sb.toString();
    }

    @Override
    public String store() {
        StringBuilder sb = new StringBuilder();
        
        sb.append(getId()).append("~").append(getCompany()).append("~");
        sb.append(getStreet()).append("~").append(getSuite()).append("~");
        sb.append(getCity()).append("~").append(getState()).append("~");
        sb.append(getPostalCode()).append("~").append(getContact()).append("~");
        sb.append(getEmail()).append("~").append(getPhone()).append("~");
        sb.append(getFax()).append("~").append(getNotes());
        
        return sb.toString() + "\n";
    }

    @Override
    public Broker restore(String line) {
        String[] fields = line.split("~");
        
        setId(Long.valueOf(fields[0]));
        setCompany(fields[1]);
        setStreet(fields[2]);
        setSuite(fields[3]);
        setCity(fields[4]);
        setState(fields[5]);
        setPostalCode(fields[6]);
        setContact(fields[7]);
        setEmail(fields[8]);
        setPhone(fields[9]);
        setFax(fields[10]);
        if (fields.length == 12) {
            setNotes(fields[11]);
        } else {
            setNotes("");
        }
        
        return this;
    }

}
