/*
 * Copyright (C) 2022 GS United Labs
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * *****************************************************************************
 *  Project    :   ntos.database
 *  Class      :   InvalidPostalCodeException.java
 *  Author     :   Sean Carrick
 *  Created    :   Mar 16, 2022 @ 9:38:41 PM
 *  Modified   :   Mar 16, 2022
 *  
 *  Purpose: See JavaDoc comments.
 *  
 *  Revision History:
 *  
 *  WHEN          BY                   REASON
 *  ------------  -------------------  ------------------------------------------
 *  Mar 16, 2022  Sean Carrick         Initial creation.
 * *****************************************************************************
 */

package com.gs.ntos.database.errors;

/**
 *
 * @author Sean Carrick &lt;sean at gs-unitedlabs dot com&gt;
 * 
 * @version 0.1.0
 * @since 0.1.0
 */
public class InvalidPostalCodeException extends RuntimeException {

    /**
     * Creates a new instance of <code>InvalidPostalCodeException</code> without detail message.
     */
    public InvalidPostalCodeException() {
    }


    /**
     * Constructs an instance of <code>InvalidPostalCodeException</code> with the specified detail message.
     * @param msg the detail message.
     */
    public InvalidPostalCodeException(String msg) {
        super(msg);
    }
}
