/*
 * Copyright (C) 2022 GS United Labs
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * *****************************************************************************
 *  Project    :   NTOS
 *  Class      :   SelfStorage.java
 *  Author     :   Sean Carrick
 *  Created    :   Apr 13, 2022
 *  Modified   :   Apr 13, 2022
 *  
 *  Purpose: See class JavaDoc for explanation
 *  
 *  Revision History:
 *  
 *  WHEN          BY                   REASON
 *  ------------  -------------------  -----------------------------------------
 *  Apr 13, 2022  Sean Carrick         Initial creation.
 * *****************************************************************************
 */

package com.gs.ntos.database.models;

/**
 * The `SelfStorage` interface provides a method by which individual models may
 * determine how they will be written to disk and read back from disk.
 * 
 * When the implementing model class is being written to disk, the writing method
 * may simply call `serialize` to get the serialization string the model class
 * decides is best for its data.
 * 
 * Likewise, when the implementing model class is being read from disk, the
 * reading method may simply call `deserialize` to allow the model to build an
 * instance of itself from the data read from file.
 * 
 * @see #store() 
 * @see #restore(java.lang.String) 
 *
 * @author Sean Carrick &lt;sean at gs-unitedlabs dot com&gt;
 * 
 * @version 1.0
 * @since 1.0
 */
public interface SelfStorage<T> {
    
    /**
     * Since a data model class knows its own data best, it is best to let it
     * determine how its data will be stored on disk. The `store` method
     * provides the mechanism for the model class to dictate how it wants its
     * data stored.
     * 
     * @return a string that best represents the data model for file storage
     * 
     * @see #restore(java.lang.String) 
     */
    public String store();
    
    /**
     * Since a data model class knows its own data best, it is best to let it
     * build itself from the data retrieved from disk. The `restore` method
     * provides the mechanism for the model class to construct itself from the
     * stored data, based on the way in which the data was stored via the 
     * `store` method.
     * 
     * @param line the line of data read in from disk
     * @return the constructed model class instance
     * 
     * @see #store() 
     */
    public T restore(String line);
    
}
