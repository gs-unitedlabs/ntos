/*
 * Copyright (C) 2022 GS United Labs
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * *****************************************************************************
 *  Project    :   ntos.database
 *  Class      :   StopNumberValidator.java
 *  Author     :   Sean Carrick
 *  Created    :   Mar 21, 2022
 *  Modified   :   Mar 21, 2022
 *  
 *  Purpose: See class JavaDoc for explanation
 *  
 *  Revision History:
 *  
 *  WHEN          BY                   REASON
 *  ------------  -------------------  -----------------------------------------
 *  Mar 21, 2022  Sean Carrick         Initial creation.
 * *****************************************************************************
 */

package com.gs.ntos.database.validators;

import com.gs.ntos.database.errors.DataStoreException;
import com.gs.ntos.database.models.Stop;
import com.gs.ntos.database.tables.StopsTable;
import com.gs.ntos.support.TerminalErrorPrinter;

/**
 *
 * @author Sean Carrick &lt;sean at gs-unitedlabs dot com&gt;
 * 
 * @version 0.1.0
 * @since 0.1.0
 */
public class StopNumberValidator {
    
    private final StopsTable table;
    private final int stopNumber;
    private final String orderNumber;
    
    private Stop[] allStops;
    
    public StopNumberValidator (int stopNumber, String orderNumber) {
        this.orderNumber = orderNumber;
        this.stopNumber = stopNumber;
        this.table = new StopsTable();
        
        try {
            table.connect();
            allStops = table.getAllRecords();
            table.close();
        } catch (DataStoreException ex) {
            TerminalErrorPrinter.print(ex, "Attempting to get all stops.");
        }
    }
    
    public boolean isUsed() {
        for (Stop s : allStops) {
            if (s.getStopNumber() == stopNumber && orderNumber.equals(s.getOrderNumber())) {
                return true;
            }
        }
        
        return false;
    }

}
