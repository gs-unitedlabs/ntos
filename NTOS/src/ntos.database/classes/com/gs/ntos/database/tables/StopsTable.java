/*
 * Copyright (C) 2022 GS United Labs
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * *****************************************************************************
 *  Project    :   NTOS
 *  Class      :   StopsTable.java
 *  Author     :   Sean Carrick
 *  Created    :   Apr 13, 2022
 *  Modified   :   Apr 13, 2022
 *  
 *  Purpose: See class JavaDoc for explanation
 *  
 *  Revision History:
 *  
 *  WHEN          BY                   REASON
 *  ------------  -------------------  -----------------------------------------
 *  Apr 13, 2022  Sean Carrick         Initial creation.
 * *****************************************************************************
 */

package com.gs.ntos.database.tables;

import com.gs.ntos.database.errors.DataStoreException;
import com.gs.ntos.database.errors.InvalidDataException;
import com.gs.ntos.database.errors.ReferentialIntegrityException;
import com.gs.ntos.database.models.Customer;
import com.gs.ntos.database.models.Load;
import com.gs.ntos.database.models.Stop;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 *
 * @author Sean Carrick &lt;sean at gs-unitedlabs dot com&gt;
 * 
 * @version 1.0
 * @since 1.0
 */
public class StopsTable extends GenericTable<Stop> {
    
    public StopsTable () {
        super("stops", Stop.class);
    }

    @Override
    public boolean add(Stop record) {
        if (record == null) {
            return false;
        }
        if (doesRecordExistInTable(record)) {
            throw new InvalidDataException("The record with the ID " 
                    + record.getId() + " already exists in the table. Call "
                    + "update(Stop) instead.");
        }
        
        boolean result = records.add(record);
        
        try {
            save();
        } catch (DataStoreException ex) {
            logger.error(ex, "Attempting to save data after new record added: "
                    + "{0}", record);
        }
        
        return result;
    }

    @Override
    public boolean delete(Stop record) throws ReferentialIntegrityException {
        if (record == null) {
            return false;
        }
        if (!doesRecordExistInTable(record)) {
            return false;
        }
        
        int idx = records.indexOf(record);
        boolean result = records.remove(record);
        
        if (idx >= records.size() && idx == getCurrentRecordNumber()){
            setCurrentRecordNumber(records.size() - 1);
        } else {
            setCurrentRecordNumber(idx);
        }
        
        return result;
    }

    @Override
    protected boolean doesRecordExistInTable(Stop record) {
        for (Stop s : records) {
            if (s.getId() == record.getId()) {
                return true;
            }
        }
        
        return false;
    }
    
    /**
     * Retrieves all customer records that are connected to the specified `load`
     * due to being referenced by a stop on the load. This method is guaranteed
     * to never return `null`. Either an array of all of the customers found, or
     * an empty `Customer` array if none were found, will be returned.
     * 
     * @param load the `Load` record of interest
     * @return an array of `Customer` records, or an empty `Customer` array
     */
    public Customer[] getCustomersForLoad(Load load) {
        CustomersTable table = new CustomersTable();
        Customer[] customers = null;
        List<Customer> found = new ArrayList<>();
        
        try {
            table.connect();
            customers = table.getAllRecords();
            table.close();
        } catch (DataStoreException ex) {
            logger.error(ex, "Attempting to get the customers from the "
                    + "Customers table: {0}", table);
        }
        
        if (customers != null) {
            for (Customer c : customers) {
                for (Stop s : records) {
                    if (s.getCustomerId() == c.getId()) {
                        if (s.getOrderNumber().equals(load.getOrderNumber())) {
                            found.add(c);
                            break;
                        }
                    }
                }
            }
            
            return found.toArray(new Customer[found.size()]);
        }
        
        return Collections.<Customer>emptyList().toArray(new Customer[0]);
    }
    
    /**
     * Retrieves an array of all `Stop` records related to the specified `Load`
     * record. This method is guaranteed to never return `null`.
     * 
     * @param load the `Load` of interest
     * @return an array of all `Stop` records related to the given `load`, or an
     *          empty `Stop` array
     */
    public Stop[] getStopsForLoad(Load load) {
        List<Stop> found = new ArrayList<>();
        
        for (Stop s : records) {
            if (s.getOrderNumber().equals(load.getOrderNumber())) {
                found.add(s);
            }
        }
        
        return found.toArray(new Stop[found.size()]);
    }
    
    /**
     * Determines if the given `Customer` record has any references in this
     * table. This method is used for referential integrity checks.
     * 
     * @param customer the `Customer` sought, most likely being deleted
     * @return `true` if the `Customer`'s ID is referenced in this table;
     *          `false` if it is not
     */
    public boolean isCustomerReferenced(Customer customer) {
        for (Stop s : records) {
            if (s.getCustomerId() == customer.getId()) {
                return true;
            }
        }
        
        return false;
    }
    
    /**
     * Determines if the specified `Load` record has any references in this
     * table. This method is used for referential integrity checks.
     * 
     * @param load the `Load` sought, most likely being deleted
     * @return `true` if the `Load`'s order number is referenced in this table;
     *          `false` if it is not
     */
    public boolean isLoadReferenced(Load load) {
        for (Stop s : records) {
            if (s.getOrderNumber().equals(load.getOrderNumber())) {
                return true;
            }
        }
        
        return false;
    }

    @Override
    public boolean update(Stop record) {
        if (record == null) {
            return false;
        }
        if (!doesRecordExistInTable(record)) {
            throw new InvalidDataException("Record with ID " + record.getId()
                    + " does not exist in the table. Call add(Stop) "
                    + "instead.");
        }
        
        for (Stop s : records) {
            if (s.getId() == record.getId()) {
                int idx = records.indexOf(s);
                records.remove(idx);
                records.add(idx, record);
                return true;
            }
        }
        
        return false;
    }

}
