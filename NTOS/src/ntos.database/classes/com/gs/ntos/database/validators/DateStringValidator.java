/*
 * Copyright (C) 2022 GS United Labs
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * *****************************************************************************
 *  Project    :   NTOS
 *  Class      :   DateStringValidator.java
 *  Author     :   Sean Carrick
 *  Created    :   Mar 27, 2022
 *  Modified   :   Mar 27, 2022
 *  
 *  Purpose: See class JavaDoc for explanation
 *  
 *  Revision History:
 *  
 *  WHEN          BY                   REASON
 *  ------------  -------------------  -----------------------------------------
 *  Mar 27, 2022  Sean Carrick         Initial creation.
 * *****************************************************************************
 */

package com.gs.ntos.database.validators;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 *
 * @author Sean Carrick &lt;sean at gs-unitedlabs dot com&gt;
 * 
 * @version 1.0
 * @since 1.0
 */
public class DateStringValidator {
    
    private final SimpleDateFormat sdf;
    private String dateString;
    
    public DateStringValidator (String dateString) {
        this(dateString, "MM/dd/yyyy");
    }
    
    public DateStringValidator (String dateString, String dateFormat) {
        this.dateString = dateString;
        sdf = new SimpleDateFormat(dateFormat);
    }
    
    public boolean isValid() {
        try {
            Date test = sdf.parse(dateString);
        } catch (ParseException ex) {
            return false;
        }
        
        return true;
    }

}
