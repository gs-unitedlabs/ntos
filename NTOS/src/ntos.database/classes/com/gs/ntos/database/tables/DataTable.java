/*
 * Copyright (C) 2022 GS United Labs
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * *****************************************************************************
 *  Project    :   ntos.database
 *  Class      :   DataTable.java
 *  Author     :   Sean Carrick
 *  Created    :   Mar 17, 2022 @ 8:24:28 AM
 *  Modified   :   Mar 17, 2022
 *  
 *  Purpose: See JavaDoc comments.
 *  
 *  Revision History:
 *  
 *  WHEN          BY                   REASON
 *  ------------  -------------------  ------------------------------------------
 *  Mar 17, 2022  Sean Carrick         Initial creation.
 * *****************************************************************************
 */

package com.gs.ntos.database.tables;

import com.gs.ntos.utils.FileUtils;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;

/**
 * The abstract `DataTable<T>` class is the basis for classes that provide
 * data access to the tables in the Northwind Truckers Operating System (NTOS)
 * database. All of the common functionality between all tables is handled within
 * this class. Any functionality that relies specifically on the underlying data
 * model is exposed by `abstract` methods that must be implemented by the
 * subclass to be able to properly load the data into the data object model.
 * 
 * @param <T> the class type of the data model for the records of this table
 *
 * @author Sean Carrick &lt;sean at gs-unitedlabs dot com&gt;
 * 
 * @version 0.1.0
 * @since 0.1.0
 */
public abstract class DataTable<T> {
    
    protected final File dataFile;
    protected final List<T> data;
    
    protected int currentRecord;
    
    public DataTable(String dataFileName) {
        if (dataFileName == null || dataFileName.isBlank() || dataFileName.isEmpty()) {
            throw new IllegalArgumentException("null, blank, or empty dataFileName");
        }
        
        if (!FileUtils.getApplicationDataDirectory().exists()) {
            FileUtils.getApplicationDataDirectory().mkdirs();
        }
        dataFile = new File(FileUtils.getApplicationDataDirectory(), dataFileName);
        
        data = new ArrayList<>();
        currentRecord = 0;
    }
    
    /**
     * Adds a new record to the table. This method requires knowledge of the
     * underlying table model, so must be implemented by the subclass.
     * <p>
     * <strong><em>Note</em></strong>: Data validation should be performed by
     * the subclass so that no `null` records are added to the table.</p>
     * <dl>
     * <dt><strong><em>Developer's Note</em></strong>:</dt>
     * <dd>If desired by the subclass, the protected `store` method may be
     * called from this method to store the data immediately to disk. If this
     * action is taken, the data file will need to be created within this method
     * and passed to the `store` method.</dd></dl>
     * 
     * @param record the new record to add
     * @return `true` upon successful addition; `false upon failure
     * @throws IllegalArgumentException in the event the specified `record`
     *          is `null`
     */
    public boolean add(T record) {
        if (record == null) {
            throw new IllegalArgumentException("null record");
        }
        
        return data.add(record);
    }
    
    /**
     * Adds a list of new records to the table. This method requires knowledge of
     * the underlying table model, so must be implemented by the subclass.
     * <p>
     * <strong><em>Note</em></strong>: Data validation should be performed by
     * the subclass so that the list of records is not a `null` or empty
     * list, and that no individual record in the list is `null`.</p>
     * <dl>
     * <dt><strong><em>Developer's Note</em></strong>:</dt>
     * <dd>If desired by the subclass, the protected `store` method may be
     * called from this method to store the data immediately to disk. If this
     * action is taken, the data file will need to be created within this method
     * and passed to the `store` method.</dd></dl>
     * 
     * @param records the list of new records to add
     * @return the count of successful additions to the table
     * @throws IllegalArgumentException in the event that the list of records or
     *          any individual record is `null`
     */
    public final int add(List<T> records) {
        if (records == null) {
            throw new IllegalArgumentException("null record");
        }
        
        int count = 0;
        for (T record : records) {
            if (add(record)) {
                count++;
            }
        }
        
        return count;
    }
    
    /**
     * Archives a table. This method backs up the data table to the specified
     * `archivePath` and deletes the original table.
     * <p>
     * This method is useful for accounting systems to backup the prior fiscal
     * year's books in order to start the books for a new fiscal year. In these
     * types of systems, the application developer will want to call this method,
     * then `open` the `DataTable` data connection again to create a
     * new table. Once the new table is created, the developer will want to make
     * a new record in that table that provides the beginning balance for the
     * new fiscal year, which should be equal to the ending balance for the 
     * prior fiscal year.</p>
     * <p>
     * When used in this capacity, the developer will need to repeat this same
     * process for each account table in the database in order to have the 
     * accounting system begin the fiscal year with fresh books and only those
     * transactions for that fiscal year in the table. In order to accomplish
     * this, it would be advised to have a &quot;Close Books&quot; utility that
     * takes care of all of this in a single place.</p>
     * 
     * @param archivePath the path and file name to which the current table 
     *          should be backed up
     * @throws IOException in the event an error occurs performing the backup
     */
    public void archive(String archivePath) throws IOException {
        backup(archivePath);
        
        if (dataFile.exists()) {
            dataFile.delete();
        }
    }
    
    /**
     * Performs a backup of the table. This method allows for backups to be made
     * of the data table. This may be useful for backing up the data for a 
     * certain period of time to allow a new, empty table to be created.
     * 
     * @param backupPath the path and file name to which the table should be
     *          backed up
     * @throws IOException in the event an error occurs while performing the 
     *          backup
     */
    public void backup(String backupPath) throws IOException {
        if (backupPath == null || backupPath.isBlank() || backupPath.isEmpty()) {
            throw new IllegalArgumentException("null, blank, or empty backupPath");
        }
        
        File original = new File(dataFile.getAbsolutePath());
        File backup = new File(backupPath);
        
        BufferedReader in = new BufferedReader(new FileReader(original));
        BufferedWriter out = new BufferedWriter(new FileWriter(backup));
        
        String inLine = null;
        while ((inLine = in.readLine()) != null) {
            if (!inLine.endsWith("\n")) {
                inLine += "\n";
            }
            out.write(inLine);
        }
        
        in.close();
        out.flush();
        out.close();
    }
    
    /**
     * Closes the data access connection. This method should be called prior to
     * the application exiting (in other words, during `DesktopProgram.shutdown`
     * at the latest). Typically, the data connection will be closed as soon as
     * it is no longer needed, such as after a transaction has been conducted.
     * 
     * @throws IOException in the event an input/output error occurs
     */
    public void close() throws IOException {
        // We are going to delete the original file, if it exists, then create
        //+ the file again. This way we can be absolutely sure that the data in
        //+ the file matches the data in the array list exactly.
        if (dataFile.exists()) {
            dataFile.delete();
        }
        dataFile.createNewFile();
        
        store();
    }
    
    /**
     * Deletes a record from the table.
     * 
     * @param record the record to delete
     * @return `true` if the record was deleted; `false if not
     * @throws IllegalArgumentException in the event the specified `record`
     *          is `null`
     */
    public boolean delete(T record) {
        if (record == null) {
            throw new IllegalArgumentException("null record");
        }
        return data.remove(record);
    }
    
    /**
     * Deletes the record with the specified `id` value. This method 
     * requires knowledge of the underlying table model, so must be implemented
     * by the subclass.
     * 
     * @param id the unique ID for the record to delete
     * @return `true` if successful deleted; `false if not
     * @throws IllegalArgumentException if the specified `id` is less than
     *          or equal to zero
     */
    public abstract boolean delete(int id);
    
    /**
     * Deletes the specified list of records from the table.
     * 
     * @param records the records to delete
     * @return the count of successful deletions
     * @throws IllegalArgumentException in the event the list of records is
     *          `null`, or if any single record in the list is `null`
     */
    public int delete(List<T> records) {
        if (records == null) {
            throw new IllegalArgumentException("null records list");
        }
        
        int count = 0;
        for (T record : records) {
            if (delete(record)) {
                count++;
            }
        }
        
        return count;
    }
    
    /**
     * Moves the record pointer to the first record in the table and retrieves
     * the record located at that position.
     * 
     * @return the first record in the table
     */
    public T first() {
        currentRecord = 0;
        return data.get(currentRecord);
    }
    
    /**
     * Retrieves the current record.
     * 
     * @return the current record
     */
    public T get() {
        return data.get(currentRecord);
    }
    
    /**
     * Retrieves the record located at the specified `index` location.
     * 
     * @param index the index of the record to retrieve
     * @return the record specified by `index`
     * @throws IndexOutOfBoundsException in the event the specified `index`
     *          is invalid
     */
    public T get(int index) throws IndexOutOfBoundsException {
        if (index < 0 || index > (data.size() - 1)) {
            throw new IndexOutOfBoundsException(index + " is out of bounds");
        }
        return data.get(index);
    }
    
    /**
     * Retrieves the record with the specified unique `id` value. This 
     * method involves knowledge of the underlying data model, so must be 
     * implemented by the subclass.
     * 
     * @param id the unique ID of the record to retrieve
     * @return the record with the supplied unique `id` value or `null`
     *          if no record matches
     * @throws IllegalArgumentException if the specified `id` is less than
     *          or equal to zero
     */
    public abstract T get(long id);
    
    /**
     * Retrieves the total count of records in the table.
     * 
     * @return the number of records in the table
     */
    public int getRecordCount() {
        return data.size();
    }
    
    /**
     * Returns an array of all of the records in the table.
     * 
     * <dl><dt><strong><em>Developer's Note</em></strong>:</dt>
     * <dd>Subclasses should create their own custom version of this method that
     * <em>does not</em> take a class as a parameter, such as:
     * ```java
     * public MyTableModel getDataAsArray() {
     *     super getDataAsArray(MyTableModel.class);
     * }
     * ```
     * This way, the rest of the application does not need to specify the data
     * model class to the subclass in order to get an array of the records. In
     * an effort to enforce this policy, this method is declared with the scope
     * of } protected}, so that it is not accidentally exposed to an 
     * application via the subclass.</dd></dl>
     * 
     * @param cls the class type to which the data needs to be cast
     * @return an array of all records
     * @throws IllegalArgumentException if } cls} is `null`
     */
    protected T[] getDataAsArray(Class<T> cls) {
        if (cls == null) {
            throw new IllegalArgumentException("null cls");
        }
        T[] brokers = (T[]) Array.newInstance(cls, getRecordCount());
        
        int index = 0;
        for (T b : data) {
            brokers[index++] = b;
        }
        
        return brokers;
    }
    
    public boolean hasNext() {
        return currentRecord < data.size() - 1;
    }
    
    /**
     * Moves the record pointer to the last record in the table and retrieves the
     * record located at that position.
     * 
     * @return the last record in the table
     */
    public T last() {
        currentRecord = data.size() - 1;
        return data.get(currentRecord);
    }
    
    /**
     * Moves the record pointer to the next record in the table and retrieves 
     * the record located at that position. This method is guaranteed to never
     * move the record pointer beyond the last record in the table.
     * 
     * @return the next record in the table
     */
    public T next() {
        if (currentRecord != (data.size() - 1)) {
            currentRecord++;
        } else {
            currentRecord = data.size() - 1;
        }
        
        return data.get(currentRecord);
    }
    
    /**
     * Moves the record pointer to the previous record in the table and retrieves
     * the record located at that position. This method is guaranteed to never
     * mover the record pointer prior to the first record in the table.
     * 
     * @return the previous record in the table
     */
    public T previous() {
        if (currentRecord > 0) {
            currentRecord--;
        } else {
            currentRecord = 0;
        }
        
        return data.get(currentRecord);
    }
    
    /**
     * Updates an existing record in the table with new values. This method
     * requires knowledge of the underlying table model, so must be implemented
     * by the subclass.
     * <p>
     * <strong><em>Note</em></strong>: Data validation should be performed by 
     * the subclass so that no record is updated with `null` data.</p>
     * <dl>
     * <dt><strong><em>Developer's Note</em></strong>:</dt>
     * <dd>If desired by the subclass, the protected `store` method may be
     * called from this method to store the data immediately to disk. If this
     * action is taken, the data file will need to be created within this method
     * and passed to the `store` method.</dd></dl>
     * 
     * @param record the record whose values to update
     * @return `true` upon successful update; `false upon failure
     * @throws IllegalArgumentException in the event the supplied `record`
     *          is `null`
     */
    public abstract boolean update(T record);
    
    /**
     * Updates a list of existing records in the table with new values. This 
     * method requires knowledge of the underlying table model, so must be
     * implemented by the subclass.
     * <p>
     * <strong><em>Note</em></strong>: Data validation should be performed by 
     * the subclass so that no record is updated with `null` data, and the
     * supplied list itself is not `null`.</p>
     * <dl>
     * <dt><strong><em>Developer's Note</em></strong>:</dt>
     * <dd>If desired by the subclass, the protected `store` method may be
     * called from this method to store the data immediately to disk. If this
     * action is taken, the data file will need to be created within this method
     * and passed to the `store` method.</dd></dl>
     * 
     * @param records the list of records to update
     * @return the count of successful updates
     * @throws IllegalArgumentException in the event the supplied list of 
     *          `records` is `null`, or if any one of the records in the
     *          list is `null`
     */
    public final int update(List<T> records) {
        if (records == null) {
            throw new IllegalArgumentException("null records list");
        }
        
        int count = 0;
        for (T record : records) {
            if (update(record)) {
                count++;
            }
        }
        
        return count;
    }
    
    /**
     * Opens the data access connection. Prior to calling any other method of
     * this class, the data needs to have a connection established by calling
     * this method. Once the data connection is opened, it will remain open
     * until such time as the `close()` method is called.
     * <p>
     * If the data file does not exist, for instance the first time the
     * application is executed, then this method simply creates the file and
     * returns. Once this method has been called, the methods for adding records
     * may then be called to populate the data file with the data.</p>
     * 
     * @throws IOException in the event an input/output error occurs.
     */
    public void open() throws IOException {
        if (!dataFile.exists()) {
            dataFile.createNewFile();
            // Since the data file was just created, there is nothing in it, so
            //+ we will just return.
            return;
        }
        
        loadData();
    }
    
    /**
     * Stores the data from the array list to the data file. This method needs
     * to have the knowledge of the specific data model object, so must be
     * implemented at the subclass level. This method is called from the
     * `close()` method.
     * 
     * @throws IOException in the event an input/output error occurs
     */
    private void store() throws IOException {
        FileOutputStream fos = new FileOutputStream(dataFile);
        ObjectOutputStream out = new ObjectOutputStream(fos);
        
        out.writeObject(data);
        out.flush();
        
        out.close();
        fos.close();
    }
    
    /**
     * Loads the data from the data file specified to the constructor. This 
     * method needs to have the knowledge of the specific data model object, so
     * must be implemented at the subclass level. This method is called from the
     * `open()` method.
     * 
     * @throws IOException in the event an input/output error occurs
     */
    private void loadData() throws IOException {
        FileInputStream fis = new FileInputStream(dataFile);
        ObjectInputStream in = new ObjectInputStream(fis);
        
        try {
            // Read in the data and cast it to our list object.
            List<T> inData = (ArrayList<T>) in.readObject();
            
            data.clear();
            
            inData.forEach(t -> {
                data.add(t);
            });
        } catch (ClassNotFoundException ex) {
            throw new IOException("unable to restore ArrayList<T>", ex);
        } finally {
            in.close();
            fis.close();
        }
    }

}
