/*
 * Copyright (C) 2022 GS United Labs
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * *****************************************************************************
 *  Project    :   NTOS
 *  Class      :   PerDiemTable.java
 *  Author     :   Sean Carrick
 *  Created    :   Apr 13, 2022
 *  Modified   :   Apr 13, 2022
 *  
 *  Purpose: See class JavaDoc for explanation
 *  
 *  Revision History:
 *  
 *  WHEN          BY                   REASON
 *  ------------  -------------------  -----------------------------------------
 *  Apr 13, 2022  Sean Carrick         Initial creation.
 * *****************************************************************************
 */

package com.gs.ntos.database.tables;

import com.gs.ntos.database.errors.DataStoreException;
import com.gs.ntos.database.errors.InvalidDataException;
import com.gs.ntos.database.errors.ReferentialIntegrityException;
import com.gs.ntos.database.models.PerDiem;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.Month;
import java.time.ZoneId;

/**
 *
 * @author Sean Carrick &lt;sean at gs-unitedlabs dot com&gt;
 * 
 * @version 1.0
 * @since 1.0
 */
public class PerDiemTable extends GenericTable<PerDiem> {
    
    public PerDiemTable () {
        super("pd", PerDiem.class);
    }

    @Override
    public boolean add(PerDiem record) {
        if (record == null) {
            return false;
        }
        if (doesRecordExistInTable(record)) {
            throw new InvalidDataException("The record with the ID " + record.getId()
                    + " already exists in the table. Call update(PerDiem) "
                            + "instead.");
        }
        
        boolean result = records.add(record);
        
        try {
            save();
        } catch (DataStoreException ex) {
            logger.error(ex, "Attempting to save data after new record added: "
                    + "{0}", record);
        }
        
        return result;
    }

    @Override
    public boolean delete(PerDiem record) throws ReferentialIntegrityException {
        if (record == null) {
            return false;
        }
        if (!doesRecordExistInTable(record)) {
            return false;
        }
        
        int idx = records.indexOf(record);
        boolean result = records.remove(record);
        
        if (idx >= records.size() && idx == getCurrentRecordNumber()){
            setCurrentRecordNumber(records.size() - 1);
        } else {
            setCurrentRecordNumber(idx);
        }
        
        return result;
    }

    @Override
    protected boolean doesRecordExistInTable(PerDiem record) {
        for (PerDiem p : records) {
            if (p.getId() == record.getId()) {
                return true;
            }
        }
        
        return false;
    }

    @Override
    public boolean update(PerDiem record) {
        if (record == null) {
            return false;
        }
        if (!doesRecordExistInTable(record)) {
            throw new InvalidDataException("Record with ID " + record.getId()
                    + " does not exist in the table. Call add(PerDiem) "
                    + "instead.");
        }
        
        for (PerDiem t : records) {
            if (t.getId() == record.getId()) {
                int idx = records.indexOf(t);
                records.remove(idx);
                records.add(idx, record);
                return true;
            }
        }
        
        return false;
    }
    
    /**
     * Retrieves the total number of days on tour for the specified period. If
     * the `startDate` is `null`, no exception is thrown and it is set to the 
     * first day of the year. If the `endDate` is `null`, no exception is thrown
     * and it is set to the last day of the year.
     * 
     * @param startDate the start date for the period
     * @param endDate the end date for the period
     * @return the total number of days out for the specified period.
     */
    public double getTotalDaysOutForPeriod(LocalDateTime startDate,
            LocalDateTime endDate) {
        if (startDate == null) {
            startDate = LocalDateTime.of(
                    LocalDate.now(ZoneId.systemDefault()).getYear(), 
                    Month.JANUARY, 1, 0, 0, 0);
        }
        if (endDate == null) {
            endDate = LocalDateTime.of(
                    LocalDate.now(ZoneId.systemDefault()).getYear(), 
                    Month.DECEMBER, 31, 23, 59, 59);
        }
        
        double totalDaysOut = 0.0d;
        
        for (PerDiem p : records) {
            if ((p.getStartDate().isEqual(startDate) 
                    || p.getStartDate().isAfter(startDate))
                    && (p.getEndDate().isEqual(endDate) 
                    || p.getEndDate().isBefore(endDate))) {
                totalDaysOut += p.getDaysOut();
            }
        }
        
        return totalDaysOut;
    }
    
    /**
     * Returns the total number of days spent on the road in the specified `year`.
     * 
     * If the `year` value is 2017 or earlier, an `IllegalArgumentException`
     * will be thrown.
     * 
     * @param year the year in question
     * @return the total number of days out for the entire year
     */
    public double getTotalDaysOutForYear(int year) {
        if (year <= 2017) {
            throw new IllegalArgumentException("a year from 2017 and earlier is "
                    + "not valid");
        }
        LocalDateTime start = LocalDateTime.of(year, Month.JANUARY, 1, 0, 0, 0);
        LocalDateTime end = LocalDateTime.of(year, Month.DECEMBER, 31, 23, 59, 59);
        
        double totalDaysOut = 0.0d;
        
        for (PerDiem p : records) {
            if ((p.getStartDate().equals(start) || p.getStartDate().isAfter(start))
                    && p.getEndDate().isBefore(end) || p.getEndDate().equals(end)) {
                totalDaysOut += p.getDaysOut();
            }
        }
        
        return totalDaysOut;
    }
    
    /**
     * Returns the total number of days spent on the road for the first quarter
     * of the specified `year`.
     * 
     * If the `year` value is 2017 or earlier, an `IllegalArgumentException`
     * will be thrown.
     * 
     * @param year the year in question
     * @return the total number of days out for the first quarter
     */
    public double getTotalDaysOutForFirstQuarter(int year) {
        if (year <= 2017) {
            throw new IllegalArgumentException("a year from 2017 and earlier is "
                    + "not valid");
        }
        LocalDateTime start = LocalDateTime.of(year, Month.JANUARY, 1, 0, 0, 0);
        LocalDateTime end = LocalDateTime.of(year, Month.MARCH, 31, 23, 59, 59);
        
        return getTotalDaysOutForPeriod(start, end);
    }
    
    /**
     * Returns the total number of days spent on the road for the second quarter
     * of the specified `year`.
     * 
     * If the `year` value is 2017 or earlier, an `IllegalArgumentException`
     * will be thrown.
     * 
     * @param year the year in question
     * @return the total number of days out for the second quarter
     */
    public double getTotalDaysOutForSecondQuarter(int year) {
        if (year <= 2017) {
            throw new IllegalArgumentException("a year from 2017 and earlier is "
                    + "not valid");
        }
        LocalDateTime start = LocalDateTime.of(year, Month.APRIL, 1, 0, 0, 0);
        LocalDateTime end = LocalDateTime.of(year, Month.JUNE, 30, 23, 59, 59);
        
        return getTotalDaysOutForPeriod(start, end);
    }
    
    /**
     * Returns the total number of days spent on the road for the third quarter
     * of the specified `year`.
     * 
     * If the `year` value is 2017 or earlier, an `IllegalArgumentException`
     * will be thrown.
     * 
     * @param year the year in question
     * @return the total number of days out for the third quarter
     */
    public double getTotalDaysOutForThirdQuarter(int year) {
        if (year <= 2017) {
            throw new IllegalArgumentException("a year from 2017 and earlier is "
                    + "not valid");
        }
        LocalDateTime start = LocalDateTime.of(year, Month.JULY, 1, 0, 0, 0);
        LocalDateTime end = LocalDateTime.of(year, Month.SEPTEMBER, 30, 23, 59, 59);
        
        return getTotalDaysOutForPeriod(start, end);
    }
    
    /**
     * Returns the total number of days spent on the road for the fourth quarter
     * of the specified `year`.
     * 
     * If the `year` value is 2017 or earlier, an `IllegalArgumentException`
     * will be thrown.
     * 
     * @param year the year in question
     * @return the total number of days out for the fourth quarter
     */
    public double getTotalDaysOutForFourthQuarter(int year) {
        if (year <= 2017) {
            throw new IllegalArgumentException("a year from 2017 and earlier is "
                    + "not valid");
        }
        LocalDateTime start = LocalDateTime.of(year, Month.OCTOBER, 1, 0, 0, 0);
        LocalDateTime end = LocalDateTime.of(year, Month.DECEMBER, 31, 23, 59, 59);
        
        return getTotalDaysOutForPeriod(start, end);
    }
    
    /**
     * Calculates the total amount of per diem that may be claimed on the income
     * tax return for the owner/operator. The claimable amount is *not allowed 
     * to be* deducted, only **80% is allowed**. However, this calculation is
     * the first step in calculating how much the owner/operator gets to 
     * deduct for per diem rates.
     * 
     * If the `year` value is 2017 or earlier, an `IllegalArgumentException`
     * will be thrown.
     * 
     * @param year the calendar year for which per diem is to be calculated.
     * @param firstAllowance this is the taxing authority's per diem rate for
     *          quarters 1-3
     * @param secondAllowance this is the taxing authority's per diem rate for
     *          the fourth quarter
     * @return the total dollar amount of claimable per dien for the calendar
     *          year
     */
    public double getTotalClaimablePerDiemAmountForYear(int year, 
            double firstAllowance, double secondAllowance) {
        if (year <= 2017) {
            throw new IllegalArgumentException("a year from 2017 and earlier is "
                    + "not valid");
        }
        double claimableDays = getTotalDaysOutForFirstQuarter(year);
        claimableDays += getTotalDaysOutForSecondQuarter(year);
        claimableDays += getTotalDaysOutForThirdQuarter(year);
        
        double claimableAmount = claimableDays * firstAllowance;
        
        claimableDays = getTotalDaysOutForFourthQuarter(year);
        
        claimableAmount += (claimableDays * secondAllowance);
        
        return claimableAmount;
    }

}
