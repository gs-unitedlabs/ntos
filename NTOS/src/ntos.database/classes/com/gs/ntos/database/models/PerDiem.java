/*
 * Copyright (C) 2022 GS United Labs
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * *****************************************************************************
 *  Project    :   ntos.database
 *  Class      :   PerDiem.java
 *  Author     :   Sean Carrick
 *  Created    :   Mar 17, 2022 @ 2:32:25 PM
 *  Modified   :   Mar 17, 2022
 *  
 *  Purpose: See JavaDoc comments.
 *  
 *  Revision History:
 *  
 *  WHEN          BY                   REASON
 *  ------------  -------------------  ------------------------------------------
 *  Mar 17, 2022  Sean Carrick         Initial creation.
 * *****************************************************************************
 */

package com.gs.ntos.database.models;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

/**
 * This Java Bean defines the data required for storing and tracking <em>per
 * diem</em> for tax purposes.
 * <p>
 * The data fields in this Bean are declared as } protected} in case the
 * situation arises in the future that this Bean needs to be extended. In that
 * situation, the subclass will be able to access the data fields directly, and
 * the documentation should describe the length and requirements of the data.</p>
 *
 * @author Sean Carrick &lt;sean at gs-unitedlabs dot com&gt;
 * 
 * @version 0.1.0
 * @since 0.1.0
 */
public class PerDiem implements SelfStorage<PerDiem>, Serializable {

    private static final long serialVersionUID = 3189045215204243684L;
    
    private final DateTimeFormatter sdf = DateTimeFormatter.ofPattern("MM/dd/yyyy HH:mm:ss.SSS");
    
    /** Required and must be unique. For internal use only, so no setter */
    protected long id;
    /** Required and must be unique. Length is 23 characters maximum. */
    protected LocalDateTime startDate;
    /** Required and must be unique. Length is 23 characters maximum. */
    protected LocalDateTime endDate;
    /** Required and should be calculated using DateUtils methods. */
    protected float daysOut;
    
    /**
     * Creates a new, default } PerDiem} instance.
     */
    public PerDiem () {
        id = System.currentTimeMillis();
    }
    
    /**
     * Retrieves the unique ID value for this tour of duty.
     * 
     * @return the unique ID value
     */
    public long getId() {
        return id;
    }
    
    private void setId(long id) {
        this.id = id;
    }

    /**
     * Retrieves the value of the tour of duty start date as a `LocalDateTime`
     * object.
     * 
     * @return the start date
     */
    public LocalDateTime getStartDate() {
        return startDate;
    }
    
    /**
     * Retrieves the value of the tour of duty start date as a string.
     * 
     * @return the start date
     */
    public String getStartDateAsString() {
        return sdf.format(startDate);
    }

    /**
     * Sets the value of the tour of duty start date as a string.
     * 
     * @param startDate the start date
     * @throws IllegalArgumentException in the event `startDate` is blank,
     *          empty, or `null`
     */
    public void setStartDate(String startDate) {
        if (startDate == null || startDate.isBlank() || startDate.isEmpty()) {
            throw new IllegalArgumentException("blank, empty, or null startDate");
        }
        this.startDate = LocalDateTime.parse(startDate, sdf);
    }
    
    /**
     * Sets the value of the tour of duty start date with a `LocalDateTime`
     * object
     * 
     * @param startDate the start date and time
     */
    public void setStartDate(LocalDateTime startDate) {
        if (startDate == null) {
            throw new IllegalArgumentException("null startDate");
        }
        
        this.startDate = startDate;
    }

    /**
     * Retrieves the value of the tour of duty end date as a `LocalDateTime`
     * object.
     * 
     * @return the end date
     */
    public LocalDateTime getEndDate() {
        return endDate;
    }
    
    /**
     * Retrieves the value of the tour of duty end date as a string.
     * 
     * @return the end date
     */
    public String getEndDateAsString() {
        return sdf.format(endDate);
    }

    /**
     * Sets the value of the tour of duty end date as a string.
     * 
     * @param endDate the end date
     * @throws IllegalArgumentException in the event `endDate` is blank,
     *          empty, or `null`
     */
    public void setEndDate(String endDate) {
        if (endDate == null || endDate.isBlank() || endDate.isEmpty()) {
            throw new IllegalArgumentException("blank, empty, or null endDate");
        }
        if (!verifyEndDateIsValid(LocalDateTime.parse(endDate))) {
            throw new IllegalStateException("endDate MUST BE AFTER startDate");
        }
        
        this.endDate = LocalDateTime.parse(endDate, sdf);
    }
    
    public void setEndDate(LocalDateTime endDate) {
        if (endDate == null) {
            throw new IllegalArgumentException("null endDate");
        }
        if (!verifyEndDateIsValid(endDate)) {
            throw new IllegalStateException("endDate MUST BE AFTER startDate");
        }
        
        this.endDate = endDate;
    }

    /**
     * Retrieves the number of days that make up the tour of duty.
     * 
     * @return the number of days total
     */
    public float getDaysOut() {
        return daysOut;
    }

    /**
     * Sets the total number of days that make up the tour of duty.
     * 
     * @param daysOut the number of days total
     * @throws IllegalArgumentException in the event `daysOut` is not
     *          greater than zero
     */
    public void setDaysOut(float daysOut) {
        if (daysOut <= 0.0f) {
            throw new IllegalArgumentException(daysOut + " <= zero");
        }
        this.daysOut = daysOut;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        
        sb.append("[").append(getStartDate()).append("] to [");
        sb.append(getEndDate()).append("] for [").append(getDaysOut());
        sb.append(" days out]@").append(Integer.toHexString(hashCode()));
        
        return sb.toString();
    }
    
    private boolean verifyEndDateIsValid(LocalDateTime endDate) {
        return endDate.isAfter(startDate);
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null || !(obj instanceof PerDiem)) {
            return false;
        }
        
        PerDiem other = (PerDiem) obj;
        
        return (getStartDate().equals(other.getStartDate())) 
                && (getEndDate().equals(other.getEndDate()))
                && getDaysOut() == other.getDaysOut();
    }

    @Override
    public native int hashCode();

    @Override
    public String store() {
        StringBuilder sb = new StringBuilder();
        
        sb.append(getId()).append("~").append(getStartDate()).append("~");
        sb.append(getEndDate()).append("~").append(getDaysOut());
        
        return sb.toString() + "\n";
    }

    @Override
    public PerDiem restore(String line) {
        String[] fields = line.split("~");
        
        setId(Long.valueOf(fields[0]));
        setStartDate(fields[1]);
        setEndDate(fields[2]);
        setDaysOut(Float.valueOf(fields[3]));
        
        return this;
    }
    
    

}
