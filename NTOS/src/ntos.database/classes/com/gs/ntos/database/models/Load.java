/*
 * Copyright (C) 2022 GS United Labs
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * *****************************************************************************
 *  Project    :   ntos.database
 *  Class      :   Load.java
 *  Author     :   Sean Carrick
 *  Created    :   Mar 21, 2022
 *  Modified   :   Mar 21, 2022
 *  
 *  Purpose: See class JavaDoc for explanation
 *  
 *  Revision History:
 *  
 *  WHEN          BY                   REASON
 *  ------------  -------------------  -----------------------------------------
 *  Mar 21, 2022  Sean Carrick         Initial creation.
 * *****************************************************************************
 */

package com.gs.ntos.database.models;

import com.gs.ntos.database.errors.DataStoreException;
import com.gs.ntos.database.tables.StopsTable;
import com.gs.ntos.support.TerminalErrorPrinter;
import java.io.Serializable;
import java.text.ParseException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Date;

/**
 * This Java Bean defines the data that is required to be collected for a load.
 * <p>
 * The data fields in this Bean are declared as } protected} in case the
 * situation arises in the future that this Bean needs to be extended. In that
 * situation, the subclass will be able to access the data fields directly, and
 * the documentation should describe the length and requirements of the data.</p>
 *
 * @author Sean Carrick &lt;sean at gs-unitedlabs dot com&gt;
 * 
 * @version 0.1.0
 * @since 0.1.0
 */
public class Load implements SelfStorage<Load>, Serializable {

    private static final long serialVersionUID = 8673376198227195590L;
    
    private final DateTimeFormatter sdf = DateTimeFormatter.ofPattern("MM/dd/yyyy");
    
    private long id;    // For internal use only, no setter.
    private String orderNumber;
    private String tripNumber;
    private double grossRevenue;
    private int dispatchedDistance;
    private long brokerId;
    private String commodity;
    private boolean hazMat;
    private boolean tarped;
    private int tarpHeight;
    private boolean ltl;
    private boolean twic;
    private boolean team;
    private boolean signatureAndTalley;
    private boolean topCustomer;
    private boolean rampsRequired;
    private int weight;
    private int pieceCount;
    private String bolNumber;
    private String referenceNumber;
    private LocalDate dispatched;
    private boolean active;
    private boolean completed;
    private long startingOdometer;
    private long endingOdometer;
    
    public Load () {
        id = System.currentTimeMillis();
        endingOdometer = -1L;
        startingOdometer = -1L;
    }

    public String getOrderNumber() {
        return orderNumber;
    }

    public void setOrderNumber(String orderNumber) {
        this.orderNumber = orderNumber;
    }

    public String getTripNumber() {
        return tripNumber;
    }

    public void setTripNumber(String tripNumber) {
        this.tripNumber = tripNumber;
    }

    public double getGrossRevenue() {
        return grossRevenue;
    }

    public void setGrossRevenue(double grossRevenue) {
        this.grossRevenue = grossRevenue;
    }

    public int getDispatchedDistance() {
        return dispatchedDistance;
    }

    public void setDispatchedDistance(int distance) {
        this.dispatchedDistance = distance;
    }

    public long getBrokerId() {
        return brokerId;
    }

    public void setBrokerId(long brokerId) {
        this.brokerId = brokerId;
    }

    public String getCommodity() {
        return commodity;
    }

    public void setCommodity(String commodity) {
        this.commodity = commodity;
    }

    public boolean isHazMat() {
        return hazMat;
    }

    public void setHazMat(boolean hazMat) {
        this.hazMat = hazMat;
    }

    public boolean isTarped() {
        return tarped;
    }

    public void setTarped(boolean tarped) {
        this.tarped = tarped;
    }

    public boolean isLtl() {
        return ltl;
    }

    public void setLtl(boolean ltl) {
        this.ltl = ltl;
    }

    public boolean isTwic() {
        return twic;
    }

    public void setTwic(boolean twic) {
        this.twic = twic;
    }

    public boolean isTeam() {
        return team;
    }

    public void setTeam(boolean team) {
        this.team = team;
    }

    public boolean isSignatureAndTalley() {
        return signatureAndTalley;
    }

    public void setSignatureAndTalley(boolean signatureAndTalley) {
        this.signatureAndTalley = signatureAndTalley;
    }

    public boolean isTopCustomer() {
        return topCustomer;
    }

    public void setTopCustomer(boolean topCustomer) {
        this.topCustomer = topCustomer;
    }

    public boolean isRampsRequired() {
        return rampsRequired;
    }

    public void setRampsRequired(boolean rampsRequired) {
        this.rampsRequired = rampsRequired;
    }

    public int getWeight() {
        return weight;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }

    public int getPieceCount() {
        return pieceCount;
    }

    public void setPieceCount(int pieceCount) {
        this.pieceCount = pieceCount;
    }

    public String getBolNumber() {
        return bolNumber;
    }

    public void setBolNumber(String bolNumber) {
        this.bolNumber = bolNumber;
    }

    public String getReferenceNumber() {
        return referenceNumber;
    }

    public void setReferenceNumber(String referenceNumber) {
        this.referenceNumber = referenceNumber;
    }

    public long getId() {
        return id;
    }
    
    private void setId(long id) {
        this.id = id;
    }

    public int getTarpHeight() {
        return tarpHeight;
    }

    public void setTarpHeight(int tarpHeight) {
        this.tarpHeight = tarpHeight;
    }

    public LocalDate getDispatched() {
        return dispatched;
    }
    
    public String getDispatchedAsString() {
        return sdf.format(dispatched);
    }

    public void setDispatched(LocalDate dispatched) {
        this.dispatched = dispatched;
    }
    
    public void setDispatched(String dispatched) {
        this.dispatched = LocalDate.parse(dispatched, sdf);
    }
    
    public boolean isActive() {
        return active;
    }
    
    public void setActive(boolean active) {
        this.active = active;
    }
    
    public boolean isCompleted() {
        return completed;
    }
    
    public void setCompleted(boolean completed) {
        this.completed = completed;
    }
    
    public long getStartingOdometer() {
        return startingOdometer;
    }
    
    public void setStartingOdometer(long startingOdometer) {
        this.startingOdometer = startingOdometer;
    }
    
    public long getEndingOdometer() {
        return endingOdometer;
    }
    
    public void setEndingOdometer(long endingOdometer) {
        this.endingOdometer = endingOdometer;
    }
    
    /**
     * Retrieves the total number of loaded miles driven for this `Load`. If the
     * load has not been completed nor if the starting nor ending odometer values
     * have been set, this method will return zero.
     * 
     * To discover the number of deadhead miles between two loads, for example
     * between this load and the previous load, use the 
     * `getDeadHeadMilesBetweenConsecutiveLoads()` method of the `LoadsTable`.
     * 
     * @return the total loaded miles driven, or zero if the load is not completed.
     * 
     * @see com.gs.ntos.database.tables.LoadsTable#getDeadHeadMilesBetweenConsecutiveLoads(String, String);
     */
    public long getTotalLoadedDistanceForThisLoad() {
        if (!isCompleted() || endingOdometer == -1L || startingOdometer == -1L) {
            return 0L;
        }
        
        return endingOdometer - startingOdometer;
    }
    
    /**
     * Retrieves the `Stop` record for the specified `stopNumber`.
     * 
     * @param stopNumber the stopNumber of interest
     * @return the `Stop` record, if found, or `null` if not
     */
    public Stop getStopFor(int stopNumber) {
        StopsTable table = new StopsTable();
        Stop[] onLoad = null;
        try {
            table.connect();
            onLoad = table.getStopsForLoad(this);
            table.close();
        } catch (DataStoreException ex) {
            TerminalErrorPrinter.print(ex, "Attempting to get the stops for the "
                    + "current load.");
        }
        
        if (stopNumber < 1 || stopNumber > onLoad.length) {
            throw new IndexOutOfBoundsException();
        }
        
        for (Stop s : onLoad) {
            if (s.getStopNumber() == stopNumber) {
                return s;
            }
        }
        
        return null;
    }

    @Override
    public String store() {
        StringBuilder sb = new StringBuilder();
        
        sb.append(getId()).append("~").append(getOrderNumber()).append("~");
        sb.append(getTripNumber()).append("~").append(sdf.format(getDispatched()));
        sb.append("~").append(getGrossRevenue()).append("~");
        sb.append(getDispatchedDistance()).append("~").append(getCommodity());
        sb.append("~").append(getWeight()).append("~").append(getPieceCount());
        sb.append("~").append(getReferenceNumber()).append("~");
        sb.append(isTarped()).append("~").append(getTarpHeight()).append("~");
        sb.append(isHazMat()).append("~").append(isTeam()).append("~");
        sb.append(isTwic()).append("~").append(isTopCustomer()).append("~");
        sb.append(isLtl()).append("~").append(isRampsRequired()).append("~");
        sb.append(isSignatureAndTalley()).append("~").append(getBrokerId());
        sb.append("~").append(getBolNumber()).append("~").append(getStartingOdometer());
        sb.append("~").append(getEndingOdometer()).append("~");
        sb.append(isActive()).append("~").append(isCompleted());
        
        return sb.toString() + "\n";
    }

    @Override
    public Load restore(String line) {
        String[] fields = line.split("~");
        
        setId(Long.valueOf(fields[0]));
        setOrderNumber(fields[1]);
        setTripNumber(fields[2]);
        setDispatched(LocalDate.parse(fields[3], sdf));
        setGrossRevenue(Double.valueOf(fields[4]));
        setDispatchedDistance(Integer.valueOf(fields[5]));
        setCommodity(fields[6]);
        setWeight(Integer.valueOf(fields[7]));
        setPieceCount(Integer.valueOf(fields[8]));
        setReferenceNumber(fields[9]);
        setTarped(Boolean.parseBoolean(fields[10]));
        setTarpHeight(Integer.valueOf(fields[11]));
        setHazMat(Boolean.parseBoolean(fields[12]));
        setTeam(Boolean.parseBoolean(fields[13]));
        setTwic(Boolean.parseBoolean(fields[14]));
        setTopCustomer(Boolean.parseBoolean(fields[15]));
        setLtl(Boolean.parseBoolean(fields[16]));
        setRampsRequired(Boolean.parseBoolean(fields[17]));
        setSignatureAndTalley(Boolean.parseBoolean(fields[18]));
        setBrokerId(Long.valueOf(fields[19]));
        setBolNumber(fields[20]);
        setStartingOdometer(Long.valueOf(fields[21]));
        setEndingOdometer(Long.valueOf(fields[22]));
        setActive(Boolean.parseBoolean(fields[23]));
        setCompleted(Boolean.parseBoolean(fields[24]));
        
        return this;
    }

}
