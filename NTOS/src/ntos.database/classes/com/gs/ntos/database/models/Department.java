/*
 * Copyright (C) 2022 GS United Labs
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * *****************************************************************************
 *  Project    :   ntos.database
 *  Class      :   Department.java
 *  Author     :   Sean Carrick
 *  Created    :   Mar 21, 2022
 *  Modified   :   Mar 21, 2022
 *  
 *  Purpose: See class JavaDoc for explanation
 *  
 *  Revision History:
 *  
 *  WHEN          BY                   REASON
 *  ------------  -------------------  -----------------------------------------
 *  Mar 21, 2022  Sean Carrick         Initial creation.
 * *****************************************************************************
 */

package com.gs.ntos.database.models;

/**
 * This Java Bean defines the data that is required to be collected for 
 * departments.
 * <p>
 * The data fields in this Bean are declared as } protected} in case the
 * situation arises in the future that this Bean needs to be extended. In that
 * situation, the subclass will be able to access the data fields directly, and
 * the documentation should describe the length and requirements of the data.</p>
 *
 * @author Sean Carrick &lt;sean at gs-unitedlabs dot com&gt;
 * 
 * @version 0.1.0
 * @since 0.1.0
 */
public class Department {
    
    private long id;
    private String deptName;
    private String description;
    
    public Department () {
        id = System.currentTimeMillis();
    }

    /**
     * Retrieves the unique ID for this } Department}.
     * 
     * @return the ID
     */
    public long getId() {
        return id;
    }

    /**
     * Sets the unique ID for this } Department}. Typically, this method is
     * only used when populating fields from a data store, as an automatically
     * generated unique ID value is created when the object is initialized.
     * 
     * @param id the unique ID
     * @throws IllegalArgumentException if the specified `id` is less than
     *          or equal to zero, or if it already exists in the data store
     */
    public void setId(long id) {
        if (id <= 0L) {
            throw new IllegalArgumentException(id + " <= 0");
        }
        // Validation of whether the id already exists in the table will have to
        //+ be performed in the DataTable subclass when the record is being 
        //+ added to the table.
        
        this.id = id;
    }

    /**
     * Retrieves the name of the } Department}.
     * 
     * @return the department name
     */
    public String getDeptName() {
        return deptName;
    }

    /**
     * Sets the name of the } Department}. This field is required and has a
     * maximum length of 40 characters. If the } deptName} value is longer
     * than that, it will be truncated.
     * 
     * @param deptName the name of the } Department}
     * @throws IllegalArgumentException if } deptName} is `null`, 
     *          blank, or empty
     */
    public void setDeptName(String deptName) {
        if (deptName == null || deptName.isBlank() || deptName.isEmpty()) {
            throw new IllegalArgumentException("null, blank, or empty deptName");
        } else if (deptName.length() > 40) {
            deptName = deptName.substring(0, 40);
        }
        
        this.deptName = deptName;
    }

    /**
     * Retrieves the description of the } Department}.
     * 
     * @return the description
     */
    public String getDescription() {
        return description;
    }

    /**
     * Sets a description for the } Department}. This field is optional,
     * but `null` values are not allowed: if a `null` value is
     * supplied, it will be converted to an empty string. There is a maximum of
     * one thousand (1,000) characters allowed in this field, so if the specified
     * } description} is longer than that, it will be truncated.
     * 
     * @param description the description of the } Department}
     */
    public void setDescription(String description) {
        if (description == null) {
            description = "";
        } else if (description.length() > 1000) {
            description = description.substring(0, 1000);
        }
        this.description = description;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        
        sb.append(getDeptName()).append("@").append(hashCode());
        sb.append("[id=").append(getId()).append("]");
        
        return sb.toString();
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null || !(obj instanceof Department)) {
            return false;
        }
        
        Department other = (Department) obj;
        
        return (getId() == other.getId())
                && (getDeptName().equals(other.getDeptName()))
                && (getDescription().equals(other.getDescription()))
                && (hashCode() == other.hashCode());
    }

    @Override
    public int hashCode() {
        int hash = Long.valueOf(getId()).hashCode();
        hash += getDeptName().hashCode();
        hash += getDescription().hashCode();
        
        return hash;
    }

}
