/*
 * Copyright (C) 2022 GS United Labs
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * *****************************************************************************
 *  Project    :   NTOS
 *  Class      :   BrokersTable.java
 *  Author     :   Sean Carrick
 *  Created    :   Apr 13, 2022
 *  Modified   :   Apr 13, 2022
 *  
 *  Purpose: See class JavaDoc for explanation
 *  
 *  Revision History:
 *  
 *  WHEN          BY                   REASON
 *  ------------  -------------------  -----------------------------------------
 *  Apr 13, 2022  Sean Carrick         Initial creation.
 * *****************************************************************************
 */

package com.gs.ntos.database.tables;

import com.gs.ntos.database.errors.DataStoreException;
import com.gs.ntos.database.errors.InvalidDataException;
import com.gs.ntos.database.errors.ReferentialIntegrityException;
import com.gs.ntos.database.models.Broker;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 *
 * @author Sean Carrick &lt;sean at gs-unitedlabs dot com&gt;
 * 
 * @version 1.0
 * @since 1.0
 */
public class BrokersTable extends GenericTable<Broker> {
    
    public BrokersTable () {
        super("brkrs", Broker.class);
    }

    @Override
    public boolean add(Broker record) {
        if (record == null) {
            return false;
        }
        if (doesRecordExistInTable(record)) {
            throw new InvalidDataException("The record with the ID " + record.getId()
                    + " already exists in the table. Call update(Broker) "
                            + "instead.");
        }
        
        boolean result = records.add(record);
        
        try {
            save();
        } catch (DataStoreException ex) {
            logger.error(ex, "Attempting to save data after new record added: "
                    + "{0}", record);
        }
        
        return result;
    }

    @Override
    public boolean delete(Broker record) throws ReferentialIntegrityException {
        if (record == null) {
            return false;
        }
        if (!doesRecordExistInTable(record)) {
            return false;
        }
        if (isRecordReferenced(record)){
            throw new ReferentialIntegrityException("Broker/Agent, "
                    + record.getCompany()+ ", has foreign key references.\n\n"
                    + "These references must be deleted before this account "
                    + "type record can be deleted.");
        }
        
        int idx = records.indexOf(record);
        boolean result = records.remove(record);
        
        if (idx >= records.size() && idx == getCurrentRecordNumber()){
            setCurrentRecordNumber(records.size() - 1);
        } else {
            setCurrentRecordNumber(idx);
        }
        
        return result;
    }

    @Override
    protected boolean doesRecordExistInTable(Broker record) {
        for (Broker b : records) {
            if (b.getId() == record.getId()) {
                return true;
            }
        }
        
        return false;
    }
    
    public Broker get(long id) {
        for (Broker b : records) {
            if (b.getId() == id) {
                return b;
            }
        }
        
        return null;
    }
    
    /**
     * Retrieves all `Broker` records that are located in a city with the name
     * specified in `city`. This does not mean that all returned `Broker`s are
     * in the same state, just that they are located in a city with the given
     * city name. This method is guaranteed to never return `null`.
     * 
     * @param city the name of the city to find
     * @return an array of `Broker` records matching the `city` criteria; or an
     *          empty `Broker` array if no matches are found
     */
    public Broker[] getBrokerInCity(String city) {
        List<Broker> found = new ArrayList<>();
        
        for (Broker b : records) {
            if (b.getCity().equalsIgnoreCase(city)) {
                found.add(b);
            }
        }
        
        return !found.isEmpty() ? found.toArray(new Broker[found.size()])
                : Collections.<Broker>emptyList().toArray(new Broker[0]);
    }
    
    /**
     * Retrieves all `Broker` records that are located in a state with the name
     * specified in `state`. This method is guaranteed to never return `null`.
     * 
     * @param state the name of the state to find
     * @return an array of `Broker` records matching the `state` criteria; or an
     *          empty `Broker` array if no matches are found
     */
    public Broker[] getBrokerInState(String state) {
        List<Broker> found = new ArrayList<>();
        
        for (Broker b : records) {
            if (b.getCity().equalsIgnoreCase(state)) {
                found.add(b);
            }
        }
        
        return !found.isEmpty() ? found.toArray(new Broker[found.size()])
                : Collections.<Broker>emptyList().toArray(new Broker[0]);
    }
    
    /**
     * Retrieves all `Broker` records that are located in a company with the name
     * specified in `company`. This does not mean that all returned `Broker`s are
     * in the same city or state, just that they are with a company with the given
     * company name. This method is guaranteed to never return `null`.
     * 
     * @param company the name of the company to find
     * @return an array of `Broker` records matching the `company` criteria; or an
     *          empty `Broker` array if no matches are found
     */
    public Broker[] getBrokerWithCompany(String company) {
        List<Broker> found = new ArrayList<>();
        
        for (Broker b : records) {
            if (b.getCity().equalsIgnoreCase(company)) {
                found.add(b);
            }
        }
        
        return !found.isEmpty() ? found.toArray(new Broker[found.size()])
                : Collections.<Broker>emptyList().toArray(new Broker[0]);
    }

    @Override
    protected boolean isRecordReferenced(Broker record) {
        LoadsTable table = new LoadsTable();
        boolean referenced = false;
        
        try {
            table.connect();
            referenced = table.isBrokerReferenceByLoad(record);
            table.close();
        } catch (DataStoreException ex) {
            logger.error(ex, "Attempting to see if Broker ({0}) is referenced "
                    + "by the loads table: {1}", record, table);
        }
        
        return referenced;
    }

    @Override
    protected void save() throws DataStoreException {
        super.save();
        program.firePropertyChange("brokersAvailable", !encryptedFile.exists(), 
                encryptedFile.exists());
    }

    @Override
    public boolean update(Broker record) {
        if (record == null) {
            return false;
        }
        if (!doesRecordExistInTable(record)) {
            throw new InvalidDataException("Record with ID " + record.getId()
                    + " does not exist in the table. Call add(Broker) "
                    + "instead.");
        }
        
        for (Broker b : records) {
            if (b.getId() == record.getId()) {
                int idx = records.indexOf(b);
                records.remove(idx);
                records.add(idx, record);
                return true;
            }
        }
        
        return false;
    }

}
