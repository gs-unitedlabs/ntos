/*
 * Copyright (C) 2022 GS United Labs
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * *****************************************************************************
 *  Project    :   ntos.database
 *  Class      :   TransactionCodeValidator.java
 *  Author     :   Sean Carrick
 *  Created    :   Mar 23, 2022
 *  Modified   :   Mar 23, 2022
 *  
 *  Purpose: See class JavaDoc for explanation
 *  
 *  Revision History:
 *  
 *  WHEN          BY                   REASON
 *  ------------  -------------------  -----------------------------------------
 *  Mar 23, 2022  Sean Carrick         Initial creation.
 * *****************************************************************************
 */

package com.gs.ntos.database.validators;

import com.gs.ntos.database.errors.DataStoreException;
import com.gs.ntos.database.models.TransactionCode;
import com.gs.ntos.database.tables.TransactionCodesTable;
import com.gs.ntos.support.TerminalErrorPrinter;

/**
 *
 * @author Sean Carrick &lt;sean at gs-unitedlabs dot com&gt;
 * 
 * @version 0.1.0
 * @since 0.1.0
 */
public class TransactionCodeValidator {
    
    private final TransactionCodesTable table;
    private final long txCodeId;
    private TransactionCode[] allTxCodes;
    
    public TransactionCodeValidator (long txCodeId) {
        this.txCodeId = txCodeId;
        table = new TransactionCodesTable();
        
        try {
            table.connect();
            allTxCodes = table.getAllRecords();
            table.close();
        } catch (DataStoreException ex) {
            TerminalErrorPrinter.print(ex, "Attempting to get a Tx codes.");
        }
    }
    
    public boolean isPresent() {
        for (TransactionCode t : allTxCodes) {
            if (t.getId() == txCodeId) {
                return true;
            }
        }
        
        return false;
    }

}
