/*
 * Copyright (C) 2022 GS United Labs
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * *****************************************************************************
 *  Project    :   ntos.database
 *  Class      :   AccountTypes.java
 *  Author     :   Sean Carrick
 *  Created    :   Mar 19, 2022
 *  Modified   :   Mar 19, 2022
 *  
 *  Purpose: See class JavaDoc for explanation
 *  
 *  Revision History:
 *  
 *  WHEN          BY                   REASON
 *  ------------  -------------------  -----------------------------------------
 *  Mar 19, 2022  Sean Carrick         Initial creation.
 * *****************************************************************************
 */

package com.gs.ntos.database.models;

import java.io.Serializable;

/**
 * This Java Bean defines the data that is required to be collected for account
 * types.
 * <p>
 * The data fields in this Bean are declared as } protected} in case the
 * situation arises in the future that this Bean needs to be extended. In that
 * situation, the subclass will be able to access the data fields directly, and
 * the documentation should describe the length and requirements of the data.</p>
 *
 * @author Sean Carrick &lt;sean at gs-unitedlabs dot com&gt;
 * 
 * @version 0.1.0
 * @since 0.1.0
 */
public class AccountType implements SelfStorage<AccountType>, Serializable {

    private static final long serialVersionUID = 8424169051402391481L;
    
    /** Required. Must be unique */
    private long id;
    /** Required. Length is thirty (30) characters maximum. */
    private String typeName;
    /** `true` indicates credit account; `false` indicates debit account. */
    private boolean creditAccount;
    
    /**
     * Constructs a new `AccountType` object. This constructor establishes
     * a unique ID for the new object.
     */
    public AccountType () {
        id = System.currentTimeMillis();
    }

    /**
     * Retrieves the unique ID for this `AccountType` object. 
     * 
     * @return the unique ID
     */
    public long getId() {
        return id;
    }

    /**
     * Sets the ID for the `AccountType`. This is typically only used when
     * loading a record from a data source.
     * 
     * @param id the unique ID
     * @throws IllegalArgumentException in the event the supplied `id` is
     *          less than or equal to zero
     */
    private void setId(long id) {
        // TODO: Improve validation once the AccountTypesTable is fixed.
        if (id <= 0) {
            throw new IllegalArgumentException(id + " <= 0");
        }
        this.id = id;
    }

    /**
     * Retrieves the name of this `AccountType`.
     * 
     * @return the type name
     */
    public String getTypeName() {
        return typeName;
    }

    /**
     * Sets the name of this `AccountType`. This field is <strong>required
     * </strong> and <em>should be</em> unique, but does not have to be. This
     * field has a maximum length of thirty (30) characters. If the specified
     * } typeName} is greater than thirty characters in length, it will be
     * truncated.
     * 
     * @param typeName the type name
     * @throws IllegalArgumentException in the event } typeName} is blank,
     *          empty, or `null`
     */
    public void setTypeName(String typeName) {
        // TODO: Improve validation once the AccountTypesTable is fixed.
        if (typeName == null || typeName.isBlank() || typeName.isEmpty()) {
            throw new IllegalArgumentException("blank, empty, or null typeName");
        } else if (typeName.length() > 30) {
            typeName = typeName.substring(0, 30);
        }
        this.typeName = typeName;
    }

    /**
     * Determines whether this account type is a *Credit Account* or not. If this
     * account type is a Credit Account (returns `true`), then credits decrease
     * the value of the account and *debits* increase its value.
     * 
     * @return `true` if a credit account; `false` if a debit account
     * 
     * @see #setCreditAccount(boolean) 
     */
    public boolean isCreditAccount() {
        return creditAccount;
    }

    /**
     * Sets whether this account type is a *Credit Account* or not. If this 
     * account type is a Credit Account (set to `true`), then credits decrease
     * the value of this account and *debits* increase its value.
     * 
     * @param creditAccount `true` if a credit account; `false` if a debit account
     * 
     * @see #isCreditAccount() 
     */
    public void setCreditAccount(boolean creditAccount) {
        this.creditAccount = creditAccount;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        
        sb.append(getTypeName()).append("@").append(hashCode());
        sb.append("[id=").append(getId()).append("]");
        
        return sb.toString();
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null || !(obj instanceof AccountType)) {
            return false;
        }
        
        AccountType other = (AccountType) obj;
        
        return (getId() == other.getId())
                && (getTypeName().equals(other.getTypeName()))
                && (hashCode() == other.hashCode());
    }

    @Override
    public int hashCode() {
        int hash = Long.valueOf(getId()).hashCode();
        hash += (getTypeName() == null) ? 255 : getTypeName().hashCode();
        hash += ((Boolean) isCreditAccount()).hashCode();
        
        return hash;
    }

    @Override
    public String store() {
        StringBuilder sb = new StringBuilder();
        
        sb.append(getId()).append("~").append(getTypeName()).append("~");
        sb.append(isCreditAccount());
        
        return sb.toString() + "\n";
    }

    @Override
    public AccountType restore(String line) {
        String[] fields = line.split("~");
        
        setId(Long.valueOf(fields[0]));
        setTypeName((fields[1] == null) ? "[UNSPECIFIED]" : fields[1]);
        setCreditAccount((fields[2] == null) ? Boolean.FALSE 
                : Boolean.parseBoolean(fields[2]));
        
        return this;
    }

}
