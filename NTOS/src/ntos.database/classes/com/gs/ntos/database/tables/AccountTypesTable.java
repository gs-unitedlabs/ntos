/*
 * Copyright (C) 2022 GS United Labs
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * *****************************************************************************
 *  Project    :   ntos.database
 *  Class      :   AccountTypesTable.java
 *  Author     :   Sean Carrick
 *  Created    :   Mar 19, 2022
 *  Modified   :   Mar 19, 2022
 *  
 *  Purpose: See class JavaDoc for explanation
 *  
 *  Revision History:
 *  
 *  WHEN          BY                   REASON
 *  ------------  -------------------  -----------------------------------------
 *  Mar 19, 2022  Sean Carrick         Initial creation.
 * *****************************************************************************
 */

package com.gs.ntos.database.tables;

import com.gs.ntos.database.errors.DataStoreException;
import com.gs.ntos.database.errors.InvalidDataException;
import com.gs.ntos.database.errors.ReferentialIntegrityException;
import com.gs.ntos.database.models.Account;
import com.gs.ntos.database.models.AccountType;

/**
 * Data access object (DAO) that provides access and manipulation of the data
 * in the data store for the accounting system's account types.
 * <p>
 * Account types are a way of categorizing the accounts found in the Chart of
 * Accounts within an accounting system. The standard account types are:</p>
 * <table border="1" cellpadding="4%">
 * <tr>
 * <th>Type Name</th>
 * <th>Description</th>
 * </tr>
 * <tr>
 * <td>Expense</td>
 * <td>A type of account from which money is removed for the costs of doing
 * business. These deductions may or may not be tax deductible.</td>
 * </tr>
 * <tr>
 * <td>Income</td>
 * <td>A type of account from which money is added into the company's bank
 * accounts. These additions are taxable.</td>
 * </tr>
 * <tr>
 * <td>Liability</td>
 * <td>A type of account for long-term expenses. Expenses that fall into this
 * category are such things as mortgages, leases, vehicle purchases, etc.</td>
 * </tr>
 * <tr>
 * <td>Asset</td>
 * <td>A type of account that contains those items of value that the company
 * owns and increase the company's net worth and value. These types of items
 * are typically not liquid.</td>
 * </tr>
 * </table>
 * <p>
 * By having all of the accounts in the Chart of Accounts for the accounting
 * system, it is easier to discuss whether an entry in an account is a debit or
 * a credit. The terms &quot;debit&quot; and &quot;credit&quot; do not mean what
 * a non-accountant may think they mean. For more information on debit and credit,
 * see the NTOS Accounting Primer.</p>
 *
 * @author Sean Carrick &lt;sean at gs-unitedlabs dot com&gt;
 * 
 * @version 0.1.0
 * @since 0.1.0
 */
public class AccountTypesTable extends GenericTable<AccountType> {
    
    
    /**
     * Constructs a new `DataTable` based upon the account types table.
     * This object instance provides all required functionality for creating,
     * reading, updating, and deleting the records in the account type data
     * store.
     * <p>
     * The requirement of an `DesktopProgram` to be provided to the
     * `DataTable` subclass is so that the data is stored in the proper
     * location on disk, based upon the operating system on which the
     * application is being executed.</p>
     * 
     * @see com.gs.application.Application
     * @see #open() 
     * @see #close() 
     */
    public AccountTypesTable () {
        super("accttypes", AccountType.class);
    }

    @Override
    public boolean add(AccountType record) {
        if (record == null) {
            return false;
        }
        if (doesRecordExistInTable(record)) {
            throw new InvalidDataException("The record with the ID " + record.getId()
                    + " already exists in the table. Call update(AccountType) "
                            + "instead.");
        }
        
        boolean result = records.add(record);
        
        try {
            save();
        } catch (DataStoreException ex) {
            logger.error(ex, "Attempting to save data after new record added: "
                    + "{0}", record);
        }
        
        return result;
    }

    @Override
    public boolean delete(AccountType record) throws ReferentialIntegrityException {
        if (record == null) {
            return false;
        }
        if (!doesRecordExistInTable(record)) {
            return false;
        }
        if (isRecordReferenced(record)){
            throw new ReferentialIntegrityException("Account type, "
                    + record.getTypeName() + ", has foreign key references.\n\n"
                    + "These references must be deleted before this account "
                    + "type record can be deleted.");
        }
        
        int idx = records.indexOf(record);
        boolean result = records.remove(record);
        
        if (idx >= records.size() && idx == getCurrentRecordNumber()){
            setCurrentRecordNumber(records.size() - 1);
        } else {
            setCurrentRecordNumber(idx);
        }
        
        return result;
    }

    @Override
    protected boolean doesRecordExistInTable(AccountType record) {
        for (AccountType t : records) {
            if (t.getId() == record.getId()) {
                return true;
            }
        }
        
        return false;
    }

    @Override
    protected boolean isRecordReferenced(AccountType record) {
        AccountsTable accounts = new AccountsTable();
        Account[] accts = null;
        try {
            accounts.connect();
            accts = accounts.getAccountsForType(record);
            accounts.close();
        } catch (DataStoreException ex) {
            logger.error(ex, "Attempting to connect to the Accounts table {0}", 
                    accounts);
        }
        
        return accts.length > 0;
    }

    @Override
    public boolean update(AccountType record) {
        if (record == null) {
            return false;
        }
        if (!doesRecordExistInTable(record)) {
            throw new InvalidDataException("Record with ID " + record.getId()
                    + " does not exist in the table. Call add(AccountType) "
                    + "instead.");
        }
        
        for (AccountType t : records) {
            if (t.getId() == record.getId()) {
                int idx = records.indexOf(t);
                records.remove(idx);
                records.add(idx, record);
                return true;
            }
        }
        
        return false;
    }
    
    /**
     * Retrieves the name of the `AccountType` that has the specified
     * `id`. If no record has the specified `id`, then an empty
     * string is returned. If the specified `id` is less than or equal to
     * zero, an `IndexOutOfBoundsException` will be thrown.
     * 
     * @param id the unique ID of the `AccountType` for which the name is
     *          wanted
     * @return the name of the `AccountType` that has the supplied `id`
     * @throws IllegalArgumentException if the specified `id` is less than
     *          or equal to zero
     */
    public String getAccountTypeName(long id) {
        if (id <= 0L) {
            throw new IllegalArgumentException(id + " <= 0");
        }
        for (AccountType record : records) {
            if (record.getId() == id) {
                return record.getTypeName();
            }
        }
        
        return "";
    }

}
