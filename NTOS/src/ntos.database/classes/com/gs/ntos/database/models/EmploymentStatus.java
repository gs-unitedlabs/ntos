/*
 * Copyright (C) 2022 GS United Labs
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * *****************************************************************************
 *  Project    :   ntos.database
 *  Class      :   EmploymentStatus.java
 *  Author     :   Sean Carrick
 *  Created    :   Mar 21, 2022
 *  Modified   :   Mar 21, 2022
 *  
 *  Purpose: See class JavaDoc for explanation
 *  
 *  Revision History:
 *  
 *  WHEN          BY                   REASON
 *  ------------  -------------------  -----------------------------------------
 *  Mar 21, 2022  Sean Carrick         Initial creation.
 * *****************************************************************************
 */

package com.gs.ntos.database.models;

/**
 * This Java Bean defines the data that is required to be collected for 
 * employment statuses.
 * <p>
 * The data fields in this Bean are declared as } protected} in case the
 * situation arises in the future that this Bean needs to be extended. In that
 * situation, the subclass will be able to access the data fields directly, and
 * the documentation should describe the length and requirements of the data.</p>
 *
 * @author Sean Carrick &lt;sean at gs-unitedlabs dot com&gt;
 * 
 * @version 0.1.0
 * @since 0.1.0
 */
public class EmploymentStatus {
    
    private long id;
    private String status;
    private String description;
    
    public EmploymentStatus () {
        id = System.currentTimeMillis();
    }

    /**
     * Retrieves the unique ID for this } EmploymentStatus}.
     * 
     * @return the unique ID
     */
    public long getId() {
        return id;
    }

    /**
     * Sets the unique ID for this } EmploymentStatus}. Typically, this method is
     * only used when populating fields from a data store, as an automatically
     * generated unique ID value is created when the object is initialized.
     * 
     * @param id the unique ID
     * @throws IllegalArgumentException if the specified `id` is less than
     *          or equal to zero, or if it already exists in the data store
     */
    public void setId(long id) {
        if (id <= 0L) {
            throw new IllegalArgumentException(id + " <= 0");
        }
        // Validation of whether the id already exists in the table will have to
        //+ be performed in the DataTable subclass when the record is being 
        //+ added to the table.
        
        this.id = id;
    }

    /**
     * Retrieves the human-readable status for this } EmploymentStatus}
     * 
     * @return the status
     */
    public String getStatus() {
        return status;
    }

    /**
     * Sets the human-readable status for this } EmploymentStatus}. This is
     * a required field, so `null` values are not allowed. This field has
     * a maximum length of 30 characters. If the specified `status` is
     * longer than that, it will be truncated.
     * 
     * @param status the status
     * @throws IllegalArgumentException if `status` is blank, empty or
     *          `null`
     */
    public void setStatus(String status) {
        if (status == null || status.isBlank() || status.isEmpty()) {
            throw new IllegalArgumentException("blank, empty, or null status");
        } else if (status.length() > 30) {
            status = status.substring(0, 30);
        }
        
        this.status = status;
    }

    /**
     * Retrieves the description of this } EmploymentStatus}
     * 
     * @return the description
     */
    public String getDescription() {
        return description;
    }

    /**
     * Sets the description of this } EmploymentStatus}. This field is not
     * required, but `null` values are not allowed. If the specified
     * } description} is `null`, it will be converted to an empty
     * string. The maximum length of this field is 500 characters. If the 
     * specified } description} is longer than that, it will be truncated.
     * 
     * @param description the } EmploymentStatus} description
     */
    public void setDescription(String description) {
        if (description == null) {
            description = "";
        } else if (description.length() > 500) {
            description = description.substring(0, 500);
        }
        this.description = description;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        
        sb.append(getStatus()).append("@").append(hashCode());
        sb.append("[id=").append(getId()).append("]");
        
        return sb.toString();
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null || !(obj instanceof EmploymentStatus)) {
            return false;
        }
        
        EmploymentStatus other = (EmploymentStatus) obj;
        
        return (getId() == other.getId())
                && (getStatus().equals(other.getStatus()))
                && (getDescription().equals(other.getDescription()))
                && (hashCode() == other.hashCode());
    }

    @Override
    public int hashCode() {
        int hash = Long.valueOf(getId()).hashCode();
        hash += getStatus().hashCode();
        hash += getDescription().hashCode();
        
        return hash;
    }

}
