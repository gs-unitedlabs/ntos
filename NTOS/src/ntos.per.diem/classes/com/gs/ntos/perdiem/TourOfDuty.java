/*
 * Copyright (C) 2022 GS United Labs
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * *****************************************************************************
 *  Project    :   ntos.per.diem
 *  Class      :   TourOfDuty.java
 *  Author     :   Sean Carrick
 *  Created    :   Mar 18, 2022 @ 12:11:25 PM
 *  Modified   :   Mar 18, 2022
 *  
 *  Purpose: See JavaDoc comments.
 *  
 *  Revision History:
 *  
 *  WHEN          BY                   REASON
 *  ------------  -------------------  ------------------------------------------
 *  Mar 18, 2022  Sean Carrick         Initial creation.
 * *****************************************************************************
 */
package com.gs.ntos.perdiem;

import com.gs.ntos.database.tables.PerDiemTable;
import com.gs.ntos.api.Program;
import com.gs.ntos.database.errors.DataStoreException;
import com.gs.ntos.database.models.PerDiem;
import com.gs.ntos.enums.Level;
import com.gs.ntos.enums.SysExits;
import com.gs.ntos.logging.Logger;
import com.gs.ntos.support.MessageBox;
import com.gs.ntos.utils.DateUtils;
import com.gs.ntos.utils.FileUtils;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 *
 * @author Sean Carrick &lt;sean at gs-unitedlabs dot com&gt;
 *
 * @version 0.1.0
 * @since 0.1.0
 */
public class TourOfDuty {

    private final Logger logger;
    private final Program program;
    private final SimpleDateFormat sdf;
    private final String tourFileName;
    private final PerDiemTable table;

    public TourOfDuty(Program program) {
        logger = new Logger(program, getClass().getCanonicalName(), Level.TRACE);
        this.program = program;
        sdf = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss.SSS");
        tourFileName = "pd_tod.ntdb";
        table = new PerDiemTable();

        try {
            table.connect();
        } catch (DataStoreException ex) {
            logger.error(ex, "Attempting to open the table {0}", table);
        }
    }
    
    /**
     * Starts a tour of duty. This method simply gets the current date and time
     * in the format "MM/dd/yyyy HH:mm:ss.SSS", and stores it to a file in the
     * operating system's default application data file storage location:
     * <table border="1" cellpadding="4%">
     * <tr>
     * <th>Operating System</th>
     * <th>Application Data Storage Location</th>
     * </tr>
     * <tr>
     * <td>Microsoft Windows</td>
     * <td>C:\${user.home}\AppData\Roaming\${application.vendorId}\${application.id}\data\</td>
     * </tr>
     * <tr>
     * <td>Apple MacOS X</td>
     * <td>${user.home}/Library/Application Data/${application.name}/data/</td>
     * </tr>
     * <tr>
     * <td>Linux and Solaris</td>
     * <td>${user.home}/.${application.id}/data/</td>
     * </tr>
     * </table>
     * <p>
     * In the above examples, the value `${user.home}` refers to the value
     * returned from the Java `System.getProperty("user.home")` method call.
     * </p>
     * 
     * @return the `SysExits` constant with which the application should
     *          exit
     * 
     * @see com.gs.ntos.enums.SysExits
     * @see #endTour() 
     */
    public int startTour() {
        logger.entering("TourOfDuty.startTour()");

        String startDate = sdf.format(new Date());
        logger.debug("Recording the Tour Of Duty startDate as {0}", startDate);

        if (!FileUtils.getApplicationDataDirectory().exists()) {
            FileUtils.getApplicationDataDirectory().mkdirs();
        }

        File store = new File(FileUtils.getApplicationDataDirectory(),
                tourFileName);

        if (store.exists()) {
            startDate = getStartDate(store);
            logger.log(Level.WARNING, "Already on a tour of duty which began:"
                    + "\n\n{0}", startDate);
            MessageBox.showWarning("You are on a tour of duty wich began:\n\n"
                    + startDate + "\n\nThis tour of duty needs to be ended\n"
                    + "before beginning a new tour of duty.",
                    "Invalid Operation");
            
            logger.exiting("Returning exit status {0} [{1}]", 
                    SysExits.EX_USAGE.toString(), SysExits.EX_USAGE.toInt());
            return TD_ON_TOUR_CANT_START;
        }
        
        try (BufferedWriter out = new BufferedWriter(new FileWriter(store))) {
            out.write(startDate);
            out.flush();
            logger.debug("Tour of duty successfully started.");
            
            MessageBox.showInfo("Your tour of duty has been started\nas of: "
                    + startDate, "NTOS Per Diem Tracker");
        } catch (IOException ex) {
            logger.error(ex, "Error writing startDate ({0}) to file {1}",
                    startDate, store);
            MessageBox.showError(ex, "Unable to store the start date.", program);
            
            logger.exiting("Returning exit status {0} [{1}].", 
                    SysExits.EX_IOERR.toString(), SysExits.EX_IOERR.toInt());
            return TD_CANNOT_WRITE;
        }

        logger.exiting("TourOfDuty.startTour(): Status: {0} [{1}]",
                SysExits.EX_OK.toString(), SysExits.EX_OK.toInt());
        return TD_SUCCESS;
    }
    
    /**
     * Ends a tour of duty. This method reads in the file that contains the tour
     * start date and time, calculates the total number of days our, then stores
     * all three values (start and end dates/times, and days out) in the database.
     * 
     * @return the `SysExits` constant with which the application should
     *          exit
     * 
     * @see com.gs.ntos.enums.SysExits
     * @see #startTour() 
     */
    public int endTour() {
        logger.entering("TourOfDuty.endTour()");
        
        String startDate = null;
        String endDate = sdf.format(new Date());
        
        File inFile = new File(FileUtils.getApplicationDataDirectory(),
                tourFileName);
        
        if (!inFile.exists()) {
            logger.log(Level.WARNING, "Not on a tour of duty.");
            MessageBox.showWarning("You are not on a tour of duty.\n"
                    + "Use Start Tour of Duty to start\none.", "Invalid Operation");
            logger.exiting("Returning status {0} [{1}]", 
                    SysExits.EX_USAGE.toString(), SysExits.EX_USAGE.toInt());
            return TD_NOT_ON_TOUR_CANT_END;
        }
        
        startDate = getStartDate(inFile);
        logger.debug("Retrieved the tour of duty start date: {0}", startDate);
        
        float daysOut = DateUtils.calculateDifferenceInDays(startDate, endDate);
        logger.debug("Total Days Out = {0}", daysOut);
        
        if (storeTour(startDate, endDate, daysOut)) {
            inFile.delete();
        } else {
            return TD_DATABASE_ERROR;
        }
        
        MessageBox.showInfo("You tour of duty has been ended.\n\nDays Out: "
                + daysOut + "\n", "NTOS Per Diem Tracker");
        
        logger.exiting("TourOfDuty.endTour(): Returning {0} [{1}]",
                SysExits.EX_OK.toString(), SysExits.EX_OK.toInt());
        return TD_SUCCESS;
    }
    
    /**
     * Shows both versions of the <em>Per Diem Report</em>, one using the 
     * standard deduction calculation method, and the other using the exact time
     * deduction calculation method. After reviewing both reports, the user will
     * want to print the one that give him/her the largest deductible amount for
     * <em>per diem</em> to take to his/her accountant or tax preparer.
     * <p>
     * In order to run the standard deduction calculation report, we will need 
     * to have a way of recalculating the days out. This should be pretty simple,
     * as all we should need to do is subtract `1.xxxx` from the `daysOut` total
     * and then add back `1.50`, which is the standard deduction for the two
     * days where the user left home and returned.</p>
     * <p>
     * This will need to be done for all tours of duty in the tax year, as the
     * law is clear that the same calculation deduction method <strong>must</strong>
     * be used for the entire year. The IRS does not allow the drivers to use
     * the exact time method for only the tours of duty where it benefits them 
     * and then use the standard deduction method for the tours of duty where
     * exact time does not.</p>
     * <p>
     * It is because of the above mentioned law regarding <em>per diem</em>
     * deduction calculations that we will generate both reports. This way the
     * driver <strong>can</strong> use whichever method gives them the greatest
     * deductible <em>per diem</em> amount for the entire tax year.</p>
     * 
     * @return the `SysExits` constant that denotes the status of the method
     */
    public SysExits showReports() {
        MessageBox.showWarning("Need to implement TourOfDuty.showReports()", 
                "Missing Code");
        return SysExits.EX_OK;
    }
    
    private boolean storeTour(String startDate, String endDate, float daysOut) {
        logger.entering("TourOfDuty.storeTour(String startDate={0}, String "
                + "endDate={1}, float daysOut={2})", startDate, endDate, daysOut);
        
        boolean results = false;
        
        NumberFormat fmt = new DecimalFormat("##0.0000");
        PerDiem p = new PerDiem();
        p.setStartDate(startDate);
        p.setEndDate(endDate);
        p.setDaysOut(daysOut);
        
        table.add(p);
        
        try {
            table.close();
            
            results = true;
        } catch (DataStoreException ex) {
            logger.error(ex, "Unable to close the database table: {0}", table);
            MessageBox.showError(ex, "Database Error", program);
            results = false;
        }
        
        logger.exiting("TourOfDuty.storeTour(): Returning {0}", results);
        return results;
    }
    
    private String getStartDate(File inFile) {
        logger.entering("TourOfDuty.getStartDate(File inFile={0})", inFile);
        
        if (inFile == null) {
            throw new IllegalArgumentException("null inFile");
        }
        
        String startDate = null;
        try (BufferedReader in = new BufferedReader(new FileReader(inFile))) {
            startDate = in.readLine();
        } catch (IOException ex) {
            logger.error(ex, "Unable to read in the startDate from {0}", inFile);
        }
        
        return (startDate == null) ? "" : startDate;
    }
    
    public static final int TD_SUCCESS = 0;
    public static final int TD_FILE_NOT_FOUND = 1;
    public static final int TD_CANNOT_WRITE = 2;
    public static final int TD_DATABASE_ERROR = 3;
    public static final int TD_ON_TOUR_CANT_START = 4;
    public static final int TD_NOT_ON_TOUR_CANT_END = 5;
    
}
