/*
 * Copyright (C) 2022 GS United Labs
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * *****************************************************************************
 *  Project    :   LoadAccounting
 *  Class      :   Launcher.java
 *  Author     :   Sean Carrick
 *  Created    :   Mar 29, 2022
 *  Modified   :   Mar 29, 2022
 *  
 *  Purpose: See class JavaDoc for explanation
 *  
 *  Revision History:
 *  
 *  WHEN          BY                   REASON
 *  ------------  -------------------  -----------------------------------------
 *  Mar 29, 2022  Sean Carrick         Initial creation.
 * *****************************************************************************
 */

package com.gs.ntos.manager;

import com.gs.ntos.NTOSProgram;

/**
 *
 * @author Sean Carrick &lt;sean at gs-unitedlabs dot com&gt;
 * 
 * @version 0.1.0
 * @since 0.1.0
 */
public class Launcher {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        new NTOSProgram().launch(args);
    }

}
