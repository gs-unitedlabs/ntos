/*
 * Copyright (C) 2022 GS United Labs
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * *****************************************************************************
 *  Project    :   LoadAccounting
 *  Class      :   module-info.java
 *  Author     :   Sean Carrick
 *  Created    :   Mar 29, 2022
 *  Modified   :   Mar 29, 2022
 *  
 *  Purpose: See class JavaDoc for explanation
 *  
 *  Revision History:
 *  
 *  WHEN          BY                   REASON
 *  ------------  -------------------  -----------------------------------------
 *  Mar 29, 2022  Sean Carrick         Initial creation.
 * *****************************************************************************
 */

module ntos.utils {
    requires java.base;
    requires java.desktop;
    
    exports com.gs.ntos.api;
    exports com.gs.ntos.enums;
    exports com.gs.ntos.errors;
    exports com.gs.ntos.filters;
    exports com.gs.ntos.formatters;
    exports com.gs.ntos.logging;
    exports com.gs.ntos.support;
    exports com.gs.ntos.utils;
    exports com.gs.ntos.validators;
    exports com.gs.ntos.verifiers;
}
