/*
 * Copyright (C) 2022 GS United Labs
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * *****************************************************************************
 *  Project    :   NTA_Utils
 *  Class      :   EmailValidator.java
 *  Author     :   Sean Carrick
 *  Created    :   Mar 7, 2022 @ 9:30:48 PM
 *  Modified   :   Mar 7, 2022
 *  
 *  Purpose:
 *  
 *  Revision History:
 *  
 *  WHEN         BY                   REASON
 *  -----------  -------------------  ------------------------------------------
 *  Mar 7, 2022  Sean Carrick         Initial creation.
 * *****************************************************************************
 */

package com.gs.ntos.verifiers;

import java.awt.Color;
import java.awt.Font;
import java.awt.SystemColor;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.swing.InputVerifier;
import javax.swing.JComponent;
import javax.swing.JTextField;

/**
 *
 * @author Sean Carrick &lt;sean at gs-unitedlabs dot com&gt;
 *
 * @version 1.0.0
 * @since 1.0.0
 */
public class EmailInputVerifier extends InputVerifier {
    
    private String regex;
    
    private Color dfltBG = SystemColor.text;
    private Color dfltFG = SystemColor.textText;
    private Font stdFont = new Font("Dialog", Font.PLAIN, 11);
    
    private Color errBG = new Color(255, 204, 204);
    private Color errFG = new Color(220, 7, 15);
    private Font errFont = new Font("Dialog", Font.BOLD + Font.ITALIC, 11);

    public EmailInputVerifier () {
        regex = "^[\\w!#$%&’*+/=?}{|}~^-]+(?:\\.[\\w!#$%&’*+/=?}{|}~^-]+)*@(?:[a-zA-Z0-9-]+\\.)+[a-zA-Z]{2,6}$";
    }

    @Override
    public boolean verify(JComponent input) {
        JTextField fld = null;
        boolean matches = false;
        if (input instanceof JTextField) {
            fld = (JTextField) input;
        }
        
        if (fld != null) {
            Pattern pattern = Pattern.compile(regex);
            Matcher matcher = pattern.matcher(fld.getText());
            
            matches = matcher.matches();
        } else {
            matches = false;
        }
        
        if (matches) {
            fld.setBackground(dfltBG);
            fld.setForeground(dfltFG);
            fld.setFont(stdFont);
        } else {
            fld.setBackground(errBG);
            fld.setForeground(errFG);
            fld.setFont(errFont);
        }
        
        return matches;
    }

}
