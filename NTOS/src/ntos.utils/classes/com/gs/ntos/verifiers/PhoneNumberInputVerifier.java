/*
 * Copyright (C) 2022 GS United Labs
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * *****************************************************************************
 *  Project    :   NTA_Utils
 *  Class      :   EmailValidator.java
 *  Author     :   Sean Carrick
 *  Created    :   Mar 7, 2022 @ 9:30:48 PM
 *  Modified   :   Mar 7, 2022
 *  
 *  Purpose:
 *  
 *  Revision History:
 *  
 *  WHEN         BY                   REASON
 *  -----------  -------------------  ------------------------------------------
 *  Mar 7, 2022  Sean Carrick         Initial creation.
 * *****************************************************************************
 */

package com.gs.ntos.verifiers;

import java.awt.Color;
import java.awt.Font;
import java.awt.SystemColor;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.swing.InputVerifier;
import javax.swing.JComponent;
import javax.swing.JTextField;

/**
 *
 * @author Sean Carrick &lt;sean at gs-unitedlabs dot com&gt;
 *
 * @version 1.0.0
 * @since 1.0.0
 */
public class PhoneNumberInputVerifier extends InputVerifier {
    
    private final String regex10;
    private final String regex12;
    private final String regex14;
    private final String regexI12L;
    
    private Color dfltBG = SystemColor.text;
    private Color dfltFG = SystemColor.textText;
    private Font stdFont = new Font("Dialog", Font.PLAIN, 11);
    
    private Color errBG = new Color(255, 204, 204);
    private Color errFG = new Color(220, 7, 15);
    private Font errFont = new Font("Dialog", Font.BOLD + Font.ITALIC, 11);

    public PhoneNumberInputVerifier () {
        regex10 = "^\\d{10}$";
        regex12 = "^(\\d{3}[- .]?){2}\\d{4}$";
        regex14 = "^((\\(\\d{3}\\))|\\d{3})[- .]?\\d{3}[- .]?\\d{4}$";
        regexI12L = "^(\\+\\d{1,3}( )?)?((\\(\\d{3}\\))|\\d{3})[- .]?\\d{3}[- .]?\\d{4}$";
    }

    @Override
    public boolean verify(JComponent input) {
        if (input == null) {
            return false;
        }
        JTextField fld = null;
        
        boolean matches10 = false;
        boolean matches12 = false;
        boolean matches14 = false;
        boolean matchesI12L = false;
        
        if (input instanceof JTextField) {
            fld = (JTextField) input;
        }
        
        if (fld != null) {
            Pattern pat10 = Pattern.compile(regex10);
            Pattern pat12 = Pattern.compile(regex12);
            Pattern pat14 = Pattern.compile(regex14);
            Pattern patI12L = Pattern.compile(regexI12L);
            Matcher match10 = pat10.matcher(fld.getText());
            Matcher match12 = pat12.matcher(fld.getText());
            Matcher match14 = pat12.matcher(fld.getText());
            Matcher matchI12L = patI12L.matcher(fld.getText());
            
            matches10 = match10.matches();
            matches12 = match12.matches();
            matches14 = match14.matches();
            matchesI12L = matchI12L.matches();
        } else {
            matches10 = false;
            matches12 = false;
            matches14 = false;
            matchesI12L = false;
        }
        
        if (matches10 || matches12 || matches14 || matchesI12L) {
            fld.setBackground(dfltBG);
            fld.setForeground(dfltFG);
            fld.setFont(stdFont);
        } else {
            fld.setBackground(errBG);
            fld.setForeground(errFG);
            fld.setFont(errFont);
        }
        
        return matches10 || matches12 || matches14 || matchesI12L;
    }

}
