/*
 * Copyright (C) 2022 GS United Labs
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * *****************************************************************************
 *  Project    :   NTA_Utils
 *  Class      :   Level.java
 *  Author     :   Sean Carrick
 *  Created    :   Mar 7, 2022 @ 9:41:50 PM
 *  Modified   :   Mar 7, 2022
 *  
 *  Purpose:
 *  
 *  Revision History:
 *  
 *  WHEN         BY                   REASON
 *  -----------  -------------------  ------------------------------------------
 *  Mar 7, 2022  Sean Carrick         Initial creation.
 * *****************************************************************************
 */

package com.gs.ntos.enums;

/**
 *
 * @author Sean Carrick &lt;sean at gs-unitedlabs dot com&gt;
 *
 * @version 0.1.0
 * @since 0.1.0
 */
public enum Level {
    
    ALL(-1),
    TRACE(0),
    DEBUG(1),
    CONFIG(2),
    INFO(3),
    WARNING(4),
    ERROR(5),
    CRITICAL(6),
    OFF(99);
    
    private final int status;
    
    Level(int status) {
        this.status = status;
    }
    
    public int intValue() {
        return status;
    }
    
    @Override
    public String toString() {
        switch(status) {
            case -1:
                return "All";
            case 0:
                return "Trace";
            case 1:
                return "Debugging";
            case 2:
                return "Configuration";
            case 3:
                return "Informational";
            case 4:
                return "Warning";
            case 5:
                return "Error";
            case 6:
                return "Critical";
            default:
                return "Off";
        }
    }
    
    /**
     * Compares the provided } other} } Level} to this one numerically. 
     * 
     * @param other the } Level} to which this one should be compared
     * @return zero (} 0}) if this } Level} is equal to } other};
     *          returns a value less than zero (} < 0}) if this } Level}
     *          is less than } other}; and, returns a value greater than
     *          zero (} > 0}) if this } Level} is greater than } 
     *          other}
     */
    public int compare(Level other) {
        return Integer.compare(status, other.intValue());
    }
}
