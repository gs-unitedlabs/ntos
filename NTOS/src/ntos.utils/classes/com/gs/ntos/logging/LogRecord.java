/*
 * Copyright (C) 2022 GS United Labs
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * *****************************************************************************
 *  Project    :   NTA_Utils
 *  Class      :   LogRecord.java
 *  Author     :   Sean Carrick
 *  Created    :   Mar 7, 2022 @ 9:40:13 PM
 *  Modified   :   Mar 7, 2022
 *  
 *  Purpose:
 *  
 *  Revision History:
 *  
 *  WHEN         BY                   REASON
 *  -----------  -------------------  ------------------------------------------
 *  Mar 7, 2022  Sean Carrick         Initial creation.
 * *****************************************************************************
 */

package com.gs.ntos.logging;

import com.gs.ntos.support.Sequencer;
import com.gs.ntos.enums.Level;
import java.time.Instant;
import java.util.ResourceBundle;

/**
 * The `LogRecord` class provides the means by which to send detailed messages
 * to the `Logger` messaging methods. By using the `LogRecord` to record all of
 * the data to be logged, the log messages will be detailed and provide better
 * understanding of the inner workings of a running application.
 * <p>
 * The typical class will define a single `LogRecord` to be used throughout that
 * class. The common way of accomplishing this is:</p>
 * }}`java public class MyClass {
 *
 * private Logger logger; private final LogRecord record = new
 * LogRecord(MyClass.class.getSimpleName()); private final Application app; //
 * ...other required fields...
 *
 * public MyClass(Application application) { // other parameters can be listed
 * after Application. logger = Logger.getLogger(application);
 * record.setSourceMethodName("MyClass (Constructor)"); record.setParameters(new
 * Object[] {application}); logger.enter(record); app = application;
 *
 *         // the rest of the class' initialization.
 *
 * logger.exit(record); }
 *
 * public void doSomething(String name, int id, float pay) { // Let's assume
 * that name == Joe, id == 5, and pay == 5.5 record.setParameters(new Object[]
 * {name, id, pay}); record.setSourceMethodName("doSomething");
 * logger.enter(record);
 *
 *         // Based upon the assumed values above, calc would then equal 27.5 float
 * calc = pay * id; record.setMessage("When multiplying pay (%2f) by id (%d),
 * the " + "product is %2f"); record.setParameters(new Object[] {pay, id,
 * calc}); logger.debug(record);
 *
 *         // the rest of the method...
 *
 * logger.exit(record); }
 *
 * }
 * }}}
 * <p>
 * As can be seen in the example above, a format string may be set to the
 * `message` property of the `LogRecord`, then the parameters need to be set to
 * hold the parameters to the format string. A call to the above `doSomething`
 * message will have the following three messages entered into the log file:</p>
 * <pre>
 * Sat Oct 23 20:41:49 CDT 2021: ENTERING MyClass.doSomething(Joe, 5, 5.5)
 * Sat Oct 23 20:41:51 CDT 2021: DEBUG: When multiplying pay (5.5) by id (5), the product is 27.5
 * Sat Oct 23 20:41:53 CDT 2021: EXITING MyClass.doSomething()
 * </pre>
 * <p>
 * The `enter` and `exit` methods place the words `ENTERING` and `EXITING`,
 * respectively, after the message timestamp, but do not have colons (:) after
 * them. For all other messaging methods, there will be a colon after the method
 * description: `DEBUG:}, `CONFIG:}, `INFO:}, `WARNING:}, `ERROR:}, and
 * `CRITICAL:}. Also, unless a newline character is placed into the raw message
 * text, the log message will be on a single line. However, if the
 * }--formatted-logs` command-line switch is passed to the application, then the
 * log messages will be wrapped at eighty (80) characters, taking into account
 * any newlines that may be in the message text. Also, when the
 * }--formatted-logs` switch is present, each message will have a header and a
 * footer, and the message body will be wrapped in such a way that no word will
 * be split over multiple lines, unless that word has a hyphen in it and the
 * part of the word to the left of the hyphen will fit on one line, while
 * leaving that part of the word to the right for the next line.</p>
 *
 * @author Sean Carrick &lt;sean at gs-unitedlabs dot com&gt;
 * 
 * @version 0.1.0
 * @since 0.1.0
 */
public class LogRecord {
    
    private Instant instant;
    private Level level;
    private String message;
    private Object[] params;
    private ResourceBundle bundle;
    private String bundleName;
    private String sourceClass;
    private String sourceMethod;
    private Thread thread;
    private Throwable thrown;
    
    /**
     * Creates a new instance of the `LogRecord` class for the specified source
     * class, and sets the initial instant to the time this `LogRecord` is
     * instantiated.
     * 
     * @param sourceClass the class creating the instance of the log record
     */
    public LogRecord (String sourceClass) {
        this.sourceClass = sourceClass;
        this.instant = Instant.now();
    }

    /**
     * Gets the instant that the event occurred.
     *
     * @return the event instant
     */
    public Instant getInstant() {
        return (instant == null) ? Instant.now() : instant;
    }

    /**
     * Sets the instant that the event occurred.
     *
     * @param instant the event instant
     */
    public void setInstant(Instant instant) {
        this.instant = (instant == null) ? Instant.now() : instant;
    }

    /**
     * Get logging message level, for example `LevelCRITICAL`.
     *
     * @return the currently set logging message level
     */
    public Level getLevel() {
        return (level == null) ? Level.INFO : level;
    }

    /**
     * Sets the logging message level, for example `LevelCRITICAL`.
     *
     * @param level the new logging message level
     */
    public void setLevel(Level level) {
        this.level = (level == null) ? Level.INFO : level;
    }

    /**
     * Gets the "raw" log message, before localization or formatting.
     *
     * @return the raw log message
     */
    public String getMessage() {
        return (message == null || message.isBlank() || message.isEmpty())
                ? "[no message available]" : message;
    }

    /**
     * Sets the "raw" log message, before localization or formatting.
     *
     * @param message the raw log message &mdash; may include `String.format`
     * tokens, provided that those tokens match up to the parameters array
     */
    public void setMessage(String message) {
        this.message = message;
    }

    /**
     * Gets the array of parameters as an `Object[]}.
     *
     * @return an `Object` array of parameters
     */
    public Object[] getParameters() {
        return (params == null) ? new Object[]{} : params;
    }

    /**
     * Sets the specified array of `Object`s as the parameters.
     *
     * @param parameters an array of `Object`s
     */
    public void setParameters(Object[] parameters) {
        this.params = parameters;
    }

    /**
     * Gets the `ResourceBundle` associated with this `LogRecord`.
     *
     * @return the associated `ResourceBundle`
     */
    public ResourceBundle getResourceBundle() {
        return bundle;
    }

    /**
     * Sets the `ResourceBundle` to associate with this `LogRecord`
     *
     * @param resourceBundle the `ResourceBundle` to associate
     */
    public void setResourceBundle(ResourceBundle resourceBundle) {
        this.bundle = bundle;
    }

    /**
     * Gets the name of the `ResourceBundle` associated with this `LogRecord`.
     *
     * @return the associated `ResourceBundle`'s name
     */
    public String getResourceBundleName() {
        return (bundleName == null || bundleName.isBlank() || bundleName.isEmpty())
                ? "[no bundle named]" : bundleName;
    }

    /**
     * Sets the name of the `ResourceBundle` associated with this `LogRecord`.
     *
     * @param resourceBundleName the associated `ResourceBundle`'s name
     */
    public void setResourceBundleName(String resourceBundleName) {
        this.bundleName = resourceBundleName;
    }
    
    /**
     * Gets the sequence number for this `LogRecord`. The default behavior of
     * this method is to use the 
     * `com.gs.spi.logging.support.Sequencer.getNextSequence()} method to return
     * the next available sequence number. 
     *
     * @return the sequence number
     */
    public long getSequenceNumber() {
        return Sequencer.getNextSequence();
    }

    /**
     * Gets the name of the (alleged) source class. "Alleged" because this will
     * only have the name of the source class if it is explicitly set at
     * runtime.
     *
     * @return the source class name
     */
    public String getSourceClassName() {
        return (sourceClass == null || sourceClass.isBlank() || sourceClass.isEmpty()) 
                ? "[source class not set]" : sourceClass;
    }

    /**
     * Set the name of the source class for this `LogRecord`;
     * @param sourceClassName 
     */
    public void setSourceClassName(String sourceClassName) {
        this.sourceClass = sourceClassName;
    }

    /**
     * Gets the name of the source method for this `LogRecord`.
     *
     * @return the source method's name
     */
    public String getSourceMethodName() {
        return (sourceMethod == null || sourceMethod.isBlank() || sourceMethod.isEmpty()) 
                ? "[source method not set]" : sourceMethod;
    }

    /**
     * Sets the name of the source method for this `LogRecord`.
     *
     * @param sourceMethodName the source method's name
     */
    public void setSourceMethodName(String sourceMethodName) {
        this.sourceMethod = sourceMethodName;
    }

    /**
     * Gets the thread in which this `LogRecord` is used.
     *
     * @return the thread
     */
    public Thread getThread() {
        return (thread == null) ? Thread.currentThread() : thread;
    }

    /**
     * Sets the ID of the thread in which this `LogRecord` is used.
     *
     * @param thread the current thread
     */
    public void setThread(Thread thread) {
        this.thread = (thread == null) ? Thread.currentThread() : thread;
    }

    /**
     * Gets the `Throwable` associated with this `LogRecord`.
     *
     * @return the associated `Throwable`
     */
    public Throwable getThrown() {
        return thrown;
    }

    /**
     * Sets the `Throwable` to be associated with this `LogRecord`.
     *
     * @param thrown the associated `Throwable`
     */
    public void setThrown(Throwable thrown) {
        this.thrown = thrown;
    }

}
