/*
 * Copyright (C) 2022 GS United Labs
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * *****************************************************************************
 *  Project    :   NTOS
 *  Class      :   Logger.java
 *  Author     :   Sean Carrick
 *  Created    :   Mar 29, 2022
 *  Modified   :   Mar 29, 2022
 *  
 *  Purpose: See class JavaDoc for explanation
 *  
 *  Revision History:
 *  
 *  WHEN          BY                   REASON
 *  ------------  -------------------  -----------------------------------------
 *  Mar 29, 2022  Sean Carrick         Initial creation.
 * *****************************************************************************
 */

package com.gs.ntos.logging;

import com.gs.ntos.api.Program;
import com.gs.ntos.enums.Level;
import com.gs.ntos.support.TerminalErrorPrinter;
import com.gs.ntos.utils.FileUtils;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

/**
 * The `Logger` class provides a method for applications to log information
 * to the standard out and standard error, as well as storing certain messages
 * to a file, while running. By having a simple method of logging these messages,
 * the application developers can see what is going on inside of the application
 * while it is running, in real time, as well as after the application exits.
 * 
 * It is important to note that the way this particular logger works is that
 * *all* messages are logged to standard out, with the exception of 
 * messages at the `Level.WARNING` level or higher, which are logged to
 * standard error. Regardless of the setting of `Logger.getLevel`, all of
 * the messages will be logged to standard out/err. This point cannot be
 * emphasized enough.
 * 
 * The `Logger.setLevel()` property only controls which messages are stored
 * in the log file. By allowing for this setting, the log file size can be
 * regulated, so that the logs do not grow to extremely large sizes in the
 * production environment. Therefore, only messages logged at the `Level`
 * set for this `Logger`, or higher, will be written to the log file.
 *
 * @author Sean Carrick &lt;sean at gs-unitedlabs dot com&gt;
 * 
 * @version 1.0
 * @since 1.0
 */
public class Logger {
    
    private final Program program;
    private final String logName;
    private final File logFile;
    
    private Level level;
    private boolean formatted;
    
    /**
     * Constructs an instance of the `Logger` class for the specified `Program`
     * and `className`. This constructor defaults to the level `Level.INFO`. If
     * `className` is blank, empty, or `null`,  the log file name will be set as
     * `program.getProgramId() + "-NULL.log"`. Otherwise, the log name will be
     * set as `program.getProgramId() + "-" + className + ".log"`. The program
     * or class may retrieve the log file name at anytime by calling the method
     * `getLogFileName()`.
     * 
     * @see #Logger(com.gs.ntos.api.Program, java.lang.String, com.gs.ntos.enums.Level) 
     * @see #getLogFileName()
     * 
     * @param program the `Program` for which the log is being created
     * @param className the name of the class which is creating the log file
     */
    public Logger (Program program, String className) {
        this(program, className, Level.INFO);
    }
    
    /**
     * Constructs an instance of the `Logger` class for the specified `Program`
     * and `className`, set at the specified `level` for log file output. If
     * `className` is blank, empty, or `null`, the log file name will be set as
     * `program.getProgramId() + -NULL.log". Otherwise, the log file name will 
     * be set as `program.getProgramId() + "-" + className + ".log"`. The program
     * or class may retrieve the log file name at anytime by calling the method
     * `getLogFileName()`.
     * 
     * @see #Logger(com.gs.ntos.api.Program, java.lang.String) 
     * @see #getLogFileName() 
     * 
     * @param program the `Program` for which the log is being created
     * @param className the name of the class which is creating the log file
     * @param level the `Level` at which log messages should be written to the
     *          log file
     * @throws IllegalArgumentException if `level` is `null`
     */
    public Logger (Program program, String className, Level level) {
        if (level == null) {
            throw new IllegalArgumentException("null level");
        }
        
        this.program = program;
        
        if (className == null || className.isBlank() || className.isEmpty()) {
            className = "NULL";
        }
        this.logName = program.getProgramId() + "-" + className + ".log";
        
        this.level = level;
        
        formatted = false;
        
        logFile = new File(FileUtils.getApplicationLogDirectory(), logName);
        
        if (logFile.exists()) {
            logFile.delete();
        }
    }
    
    /**
     * Retrieves the name of the log file that was constructed when the `Logger`
     * was initialized. The value returned is *only* the **base name** of the
     * log file.
     * 
     * @return the name of the log file
     * 
     * @see #getLogFilePath()
     */
    public String getLogFileName() {
        return logFile.getName();
    }
    
    /**
     * Retrieves the name and full path of the log file that was constructed
     * when the `Logger` was initialized.
     * 
     * @return the full path and name of the log file
     * 
     * @see #getLogFileName() 
     */
    public String getLogFilePath() {
        return logFile.getAbsolutePath();
    }
    
    /**
     * Retrieves the current logging `Level` that is set for this `Logger`.
     * 
     * @return the current level
     * 
     * @see #setLevel(com.gs.ntos.enums.Level) 
     */
    public Level getLevel() {
        return level;
    }
    
    /**
     * Sets the `Level` for this `Logger` to use. The `level` setting determines
     * the verbosity of the log output to the log file. No matter this setting,
     * all messages are logged to the standard out, and to the standard error 
     * for messages at `Level.WARNING`, `Level.ERROR`, and `Level.CRITICAL`.
     * Furthermore, regardless of the `Logger`'s level setting, all error and
     * critical error messages are logged to the log file as well.
     * 
     * @param level the `Level` constant to which this logger's output should be
     *          set
     * @throws IllegalArgumentException if `level` is `null`
     * 
     * @see #getLevel() 
     */
    public void setLevel(Level level) {
        if (level == null) {
            throw new IllegalArgumentException("null level");
        }
        
        this.level = level;
    }
    
    /**
     * Convenience method for logging `Level.CONFIG` messages.
     * 
     * The `message` string may be tokenized (`{0}`, `{1}`, etc.) and the 
     * values for the tokens may be passed via the `params` parameter. The
     * `params` will be used in the order they are supplied, so make sure that
     * the location of the related token in the message correlates to the 
     * position of the parameter in the `params` array.

     * @param message the message to log
     * @param params the token parameters for the message
     * @throws IllegalArgumentException if `message` is blank, empty, or `null`
     */
    public void config(String message, Object... params) {
        message = "[CONFIG] " + message;
        log(Level.CONFIG, message, params);
    }
    
    /**
     * Logs critical error messages. A critical error is any error that causes
     * the program to exit abnormally. If the program is able to recover from 
     * the error, then it ***is not*** a *critical* error, and should be logged
     * using the `error` method.
     * 
     * The `message` string may be tokenized (`{0}`, `{1}`, etc.) and the 
     * values for the tokens may be passed via the `params` parameter. The
     * `params` will be used in the order they are supplied, so make sure that
     * the location of the related token in the message correlates to the 
     * position of the parameter in the `params` array.
     * 
     * @param ex the `Exception` that was thrown
     * @param message the message to log with the error; may be `null`
     * @param params the token parameters for the message
     * @throws IllegalArgumentException if `ex` is `null`
     * 
     * @see #error(java.lang.Exception, java.lang.String, java.lang.Object...) 
     */
    public void critical(Exception ex, String message, Object... params) {
        checkParameters(ex);
        
        // TODO: Implement Logger.critical method.
    }
    
    /**
     * A convenience method for logging debugging (`Level.DEBUG`) messages.
     * 
     * The `message` string may be tokenized (`{0}`, `{1}`, etc.) and the 
     * values for the tokens may be passed via the `params` parameter. The
     * `params` will be used in the order they are supplied, so make sure that
     * the location of the related token in the message correlates to the 
     * position of the parameter in the `params` array.
     * 
     * @param message the message to log
     * @param params the token parameters for the message
     * @throws IllegalArgumentException if `message` is blank, empty, or `null`
     * 
     * @see #log(java.lang.String, java.lang.Object...) 
     * @see #log(com.gs.ntos.enums.Level, java.lang.String, java.lang.Object...) 
     */
    public void debug(String message, Object... params) {
        message = "[DEBUG] " + message;
        log(Level.DEBUG, message, params);
    }
    
    /**
     * A convenience method for logging debugging (`Level.TRACE`) messages when
     * entering a method.
     * 
     * The `message` string may be tokenized (`{0}`, `{1}`, etc.) and the 
     * values for the tokens may be passed via the `params` parameter. The
     * `params` will be used in the order they are supplied, so make sure that
     * the location of the related token in the message correlates to the 
     * position of the parameter in the `params` array.
     * 
     * @param message the message to log
     * @param params the token parameters for the message
     * @throws IllegalArgumentException if `message` is blank, empty, or `null`
     * 
     * @see #log(java.lang.String, java.lang.Object...) 
     * @see #log(com.gs.ntos.enums.Level, java.lang.String, java.lang.Object...) 
     */
    public void entering(String message, Object... params) {
        message = "[ENTERING] " + message;
        log(Level.TRACE, message, params);
    }
    
    /**
     * Logs non-critical error messages. A non-critical error is any error that
     * the program can either recover from, or that does not affect the overall
     * operation of the program. These types of messages *should be* logged, so 
     * that the development team can look into them in order to fix them. For 
     * the most part, these types of errors are typically referred to as "bugs".
     * 
     * Messages logged using this message will always be written to the log file,
     * regardless of the `Level` that is set, unless the level is set to
     * `Level.OFF`.
     * 
     * @param ex the `Exception` which was thrown
     * @param message the message to log with the exception
     * @param params the token parameters for the message
     * @throws IllegalArgumentException if `ex` is `null`, or if the `message`
     *          is blank, empty, or `null`
     * 
     * @see #critical(java.lang.Exception, java.lang.String, java.lang.Object...) 
     */
    public void error(Exception ex, String message, Object... params) {
        checkParameters(ex);
        
        StringBuilder sb = new StringBuilder();
        sb.append("Exception: ").append(ex.getClass().getSimpleName());
        sb.append("\nMessage: ").append(ex.getMessage()).append("\n");
        sb.append("Stacktrace:\n\n");
        
        for (Throwable t : ex.getSuppressed()) {
            sb.append(t.getClass().toString()).append(": ");
            sb.append(t.getMessage()).append("\n");
            for (StackTraceElement e : t.getStackTrace()) {
                sb.append("\t").append(e.toString());
            }
        }
        
        message += "\n" + "-".repeat(60) + "\n" + sb.toString();
        message = "[ERROR] " + message;
        
        log(Level.ERROR, message, params);
    }
    
    /**
     * A convenience method for logging debugging (`Level.TRACE`) messages when
     * exiting a method.
     * 
     * The `message` string may be tokenized (`{0}`, `{1}`, etc.) and the 
     * values for the tokens may be passed via the `params` parameter. The
     * `params` will be used in the order they are supplied, so make sure that
     * the location of the related token in the message correlates to the 
     * position of the parameter in the `params` array.
     * 
     * @param message the message to log
     * @param params the token parameters for the message
     * @throws IllegalArgumentException if `message` is blank, empty, or `null`
     * 
     * @see #log(java.lang.String, java.lang.Object...) 
     * @see #log(com.gs.ntos.enums.Level, java.lang.String, java.lang.Object...) 
     */
    public void exiting(String message, Object... params) {
        message = "[EXITING] " + message;
        log(Level.TRACE, message, params);
    }
    
    /**
     * A convenience method for logging informational (`Level.INFO`) messages.
     * 
     * The `message` string may be tokenized (`{0}`, `{1}`, etc.) and the 
     * values for the tokens may be passed via the `params` parameter. The
     * `params` will be used in the order they are supplied, so make sure that
     * the location of the related token in the message correlates to the 
     * position of the parameter in the `params` array.
     * 
     * @param message the message to log
     * @param params the token parameters for the message
     * @throws IllegalArgumentException if `message` is blank, empty, or `null`
     * 
     * @see #log(java.lang.String, java.lang.Object...) 
     * @see #log(com.gs.ntos.enums.Level, java.lang.String, java.lang.Object...) 
     */
    public void info(String message, Object... params) {
        message = "[INFO] " + message;
        log(Level.INFO, message, params);
    }
    
    /**
     * A convenience method for logging informational (`Level.INFO`) messages.
     * 
     * The `message` string may be tokenized (`{0}`, `{1}`, etc.) and the 
     * values for the tokens may be passed via the `params` parameter. The
     * `params` will be used in the order they are supplied, so make sure that
     * the location of the related token in the message correlates to the 
     * position of the parameter in the `params` array.
     * 
     * @param message the message to log
     * @param params the token parameters for the message
     * @throws IllegalArgumentException if `message` is blank, empty, or `null`
     * 
     * @see #config(java.lang.String, java.lang.Object...) 
     * @see #debug(java.lang.String, java.lang.Object...) 
     * @see #entering(java.lang.String, java.lang.Object...) 
     * @see #error(java.lang.Exception, java.lang.String, java.lang.Object...) 
     * @see #exiting(java.lang.String, java.lang.Object...) 
     * @see #info(java.lang.String, java.lang.Object...) 
     * @see #warning(java.lang.String, java.lang.Object...) 
     * @see #log(com.gs.ntos.enums.Level, java.lang.String, java.lang.Object...) 
     */
    public void log(String message, Object... params) {
        log(Level.INFO, message, params);
    }
    
    /**
     * Logs the specified `message` at the given `level`. The message will be
     * logged to standard out (or standard error if the `level` is `Level.WARNING`
     * or higher), regardless of the `level` specified. However, the `message`
     * will only be logged to the log file if the specified `level` is greater
     * than or equal to the `Level` at which the logger is set.
     * 
     * The `message` string may be tokenized (`{0}`, `{1}`, etc.) and the 
     * values for the tokens may be passed via the `params` parameter. The
     * `params` will be used in the order they are supplied, so make sure that
     * the location of the related token in the message correlates to the 
     * position of the parameter in the `params` array.
     * 
     * @param level the `Level` setting for the message
     * @param message the message to log
     * @param params any token parameters for the message
     * @throws IllegalArgumentException if `level` is `null`, or if `message` is
     *          blank, empty, or `null`
     * 
     * @see com.gs.ntos.enums.Level
     * @see #log(java.lang.String, java.lang.Object...) 
     * @see #config(java.lang.String, java.lang.Object...) 
     * @see #debug(java.lang.String, java.lang.Object...) 
     * @see #entering(java.lang.String, java.lang.Object...) 
     * @see #error(java.lang.Exception, java.lang.String, java.lang.Object...) 
     * @see #exiting(java.lang.String, java.lang.Object...) 
     * @see #info(java.lang.String, java.lang.Object...) 
     * @see #warning(java.lang.String, java.lang.Object...) 
     */
    public void log(Level level, String message, Object... params) {
        checkParameters(level, message);
        
        int x = 0;
        for (Object o : params) {
            message = message.replace("{" + x + "}", o == null ? "NULL" : o.toString());
            x++;
        }
        
        if (level.compare(Level.WARNING) >= 0) {
            System.err.println(message);
        } else {
            System.out.println(message);
        }
        
        if (level.intValue() != Level.OFF.intValue()
                && (level.intValue() == Level.ALL.intValue()
                || level.intValue() >= getLevel().intValue())) {
            writeMessage(message);
        }
    }
    
    /**
     * A convenience method for logging warning (`Level.WARNING`) messages.
     * 
     * The `message` string may be tokenized (`{0}`, `{1}`, etc.) and the 
     * values for the tokens may be passed via the `params` parameter. The
     * `params` will be used in the order they are supplied, so make sure that
     * the location of the related token in the message correlates to the 
     * position of the parameter in the `params` array.
     * 
     * @param message the message to log
     * @param params the token parameters for the message
     * @throws IllegalArgumentException if `message` is blank, empty, or `null`
     * 
     * @see #log(java.lang.String, java.lang.Object...) 
     * @see #log(com.gs.ntos.enums.Level, java.lang.String, java.lang.Object...) 
     */
    public void warning(String message, Object... params) {
        message = "[WARNING] " + message;
        log(Level.WARNING, message, params);
    }
    
    /* Checks the parameters passed to the logging methods to make sure that the
     * Level setting is not null and that the message is not blank, empty, nor
     * null. Throws an IllegalArgumentException if any of the above tests fail.
     */
    private void checkParameters(Level level, String message) {
        if (level == null) {
            throw new IllegalArgumentException("null level");
        }
        if (message == null || message.isBlank() || message.isEmpty()) {
            throw new IllegalArgumentException("blank, empty, or null message");
        }
    }
    
    private void checkParameters(Exception ex) {
        if (ex == null) {
            throw new IllegalArgumentException("null Exception ex");
        }
    }
    
    /* Writes the log message to the log file. This method should only be called
     * from the log(Level, String, Object...) method, and only if the logging
     * level is set to Level.ALL or the level value passed in is greater than or
     * equal to the logger's level setting.
     */
    private void writeMessage(String message) {
        if (!message.endsWith("\n")) {
            message += "\n";
        }
        
        try {
            FileWriter fw = new FileWriter(logFile, true);
            BufferedWriter out = new BufferedWriter(fw, message.length());
            
            out.write(message);
            
            out.flush();
            out.close();
            fw.close();
        } catch (IOException ex) {
            TerminalErrorPrinter.print(ex, "Attempting to write the log message "
                    + "to the file: " + logFile.getAbsolutePath());
        }
    }

}
