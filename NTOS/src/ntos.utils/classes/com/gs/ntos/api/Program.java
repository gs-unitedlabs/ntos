/*
 * Copyright (C) 2022 GS United Labs
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * *****************************************************************************
 *  Project    :   ntos.utils
 *  Class      :   Program.java
 *  Author     :   Sean Carrick
 *  Created    :   Mar 18, 2022 @ 9:19:50 AM
 *  Modified   :   Mar 18, 2022
 *  
 *  Purpose: See JavaDoc comments.
 *  
 *  Revision History:
 *  
 *  WHEN          BY                   REASON
 *  ------------  -------------------  ------------------------------------------
 *  Mar 18, 2022  Sean Carrick         Initial creation.
 * *****************************************************************************
 */

package com.gs.ntos.api;

import com.gs.ntos.enums.SysExits;
import java.util.EventObject;
import javax.swing.SwingUtilities;

/**
 * The `Program` interface defines the data that every program should
 * expose for the ability of saving program-specific data to the host system's
 * disk(s). By implementing this interface, the main class of a program will be
 * required to define certain properties of the program which can be used by the
 * various helper utilities, such as `FileUtils` and `SessionStorage`.
 *
 * @author Sean Carrick &lt;sean at gs-unitedlabs dot com&gt;
 * 
 * @version 0.1.0
 * @since 0.1.0
 */
public interface Program {
    
    /**
     * Retrieves the program's name.
     * 
     * @return name of the program
     */
    String getProgramName();
    
    /**
     * Retrieves the full title of the program.
     * 
     * @return title of the program
     */
    String getProgramTitle();
    
    /**
     * Retrieves the program's ID.
     * 
     * @return ID for the program
     */
    String getProgramId();
    
    /**
     * Retrieves the program's vendor.
     * 
     * @return vendor of the program
     */
    String getVendorName();
    
    /**
     * Retrieves the program's vendor ID.
     * 
     * @return vendor ID of the program
     */
    String getVendorId();
    
    /**
     * Retrieves the program's version.
     * 
     * @return version of the program
     */
    String getVersion();
    
    /**
     * Returns the parent } DesktopProgram} for this `Program`. The
     * default implementation returns `null`.
     * 
     * @return the parent } DesktopProgram}
     */
    public default DesktopProgram getParent() {
        return null;
    }
    
    /**
     * The launching of the program should be used to get the program onto the
     * Event Dispatching Thread (EDT), and to call `initialize`, `startup`, and
     * `ready`. This is exactly what the default implementation does for you.
     * <p>
     * <strong><em>Note</em></strong>: The default implementation also sets the
     * look and feel for the program to a native look and feel for the operating
     * system on which it is running.</p>
     * 
     * @param args the command line arguments to the program
     */
    default void launch(String[] args) {
        Runnable startOnEDT = () -> {
            initialize(args);
            startup();
            ready();
        };
        
        SwingUtilities.invokeLater(startOnEDT);
    }
    
    /**
     * The state of the program launching where initialization takes place, such
     * as parsing any command line parameter that may have been passed.
     * 
     * @param args the command line parameters
     */
    void initialize(String[] args);
    
    /**
     * The state of the program launching that prepares the GUI to be shown to 
     * the user of the program.
     */
    void startup();
    
    /**
     * The state of the program launching where the GUI has been prepared and
     * shown to the user, but additional setup and initialization still needs to
     * be completed. The default implementation does nothing.
     */
    default void ready() {}
    
    /**
     * The state of the program where required cleanup takes place prior to the
     * program exiting back to the operating system. The default implementation
     * does nothing.
     */
    default void shutdown() {}
    
    /**
     * The method by which the program exits back to the operating system. This
     * version of `exit` is typically used for normal exits, without 
     * requiring information to be passed from the desktop environment because
     * it was called by a user clicking on a button.
     * <p>
     * The default implementation, simply calls `exit(SysExits.EX_OK)`.</p>
     */
    default void exit() {
        exit(SysExits.EX_OK);
    }
    
    /**
     * The method by which the program exits back to the operating system. This
     * version of `exit` is typically used for normal exits that were
     * initiated by the desktop environment, such as when the user clicks on the
     * windowing system's close button on the window frame. `EventObject`
     * may be `null`.
     * <p>
     * The default implementation simply calls `exit(evt, SysExits.EX_OK)`.
     * </p>
     * 
     * @param evt the `EventObject` that initiated the exit
     */
    default void exit(EventObject evt) {
        exit(evt, SysExits.EX_OK);
    }
    
    /**
     * The method by which the program exits back to the operating system. This
     * version of `exit` is typically used for abnormal exits that need to
     * report a status code to the operating system, such as the user canceling
     * an operation, a critical operating system file not being found, or a
     * protocol error in distributed or client/server programs. `SysExits`
     * may be `null`.
     * <p>
     * The default implementation simply calls `exit(null, status)`.</p>
     * 
     * @param status the status as one of the `SysExits` constants
     */
    default void exit(SysExits status) {
        exit(null, status);
    }
    
    /**
     * The method by which the program exits back to the operating system. This
     * version of `exit` is called by the other three versions. Once called,
     * if `EventObject` is `null`, it is ignored. If `SysExits`
     * is `null`, then `SysExits.EX_OK` is passed to the operating
     * system.
     * <p>
     * Unlike the other three `exit()` methods, this one provides no 
     * default behavior and must be overridden by the implementing class. It is
     * suggested that any and all primary and/or secondary windows (excluding
     * temporary dialogs) use the {@link ExitListener ExitListener} interface
     * and that this `exit()` method loop through all registered exit
     * listeners to query the `canExit()` method results to make sure that
     * all windows are prepared to exit.</p>
     * <p>
     * <strong><em>Note</em></strong>: The `ExitListener` interface is not
     * limited to being used only by GUI components. Other `Program`s may
     * implement the interface as well, and register themselves with the main
     * program. This way, "sub-programs" will be able to abort an exit just as
     * a GUI component is able.</p>
     * 
     * @param evt the event that triggered the exit (may be `null`)
     * @param status the `SysExits` constant that state the status of the
     *          exit (may be `null`)
     */
    void exit(EventObject evt, SysExits status);
    
    /**
     * Ends the program by calling `Runtime.getRuntime().exit(status.toInt())`.
     * This method may be overridden if some other method of exiting is preferred,
     * but this method of ending the `Program` is valid with `launch()`
     * putting the program onto the Event Dispatching Thread (EDT).
     * <p>
     * Throws an `IllegalArgumentException` in the event that `status`
     * is `null`.</p>
     * 
     * @param status the `SysExits` constant for the program termination
     *          status, such as `SysExits.EX_OK` for normal exit or
     *          `SysExits.EX_OPCANCEL` for the user cancelling an operation
     * @throws IllegalArgumentException in the event that `status` is
     *          `null`
     */
    default void end(SysExits status) {
        if (status == null) {
            throw new IllegalArgumentException("null status");
        }
        Runtime.getRuntime().exit(status.toInt());
    }
    
    /**
     * Registers an `ExitListener` with the program. If the specified
     * `listener` is `null`, no exception is thrown and no action is
     * taken.
     * <p>
     * If a listener is registered more than once, it will be called as many
     * times as it is added.</p>
     * <p>
     * The default implementation does nothing. It was just provided so that
     * implementing classes are not required to implement this method if the
     * program is not using the `ExitListener` interface.</p>
     * 
     * @param listener the `ExitListener` to register
     * 
     * @see #removeExitListener(com.gs.ntos.api.ExitListener)
     */
    default void addExitListener(ExitListener listener) {}
    
    /**
     * Unregisters an `ExitListener` from the program. If the specified
     * `listener` does not exist, no exception is thrown and no action is
     * taken.
     * <p>
     * If a listener was added more than once, it will be called one less time.
     * </p><p>
     * The default implementation does nothing. It was just provided so that
     * implementing classes are not required to implement this method if the
     * program is not using the `ExitListener` interface.</p>
     *
     * @param listener 
     */
    default void removeExitListener(ExitListener listener) {}
    
}
