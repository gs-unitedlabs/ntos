/*
 * Copyright (C) 2022 GS United Labs
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * *****************************************************************************
 *  Project    :   NTOS
 *  Class      :   UpdateTask.java
 *  Author     :   Sean Carrick
 *  Created    :   Mar 25, 2022
 *  Modified   :   Mar 25, 2022
 *  
 *  Purpose: See class JavaDoc for explanation
 *  
 *  Revision History:
 *  
 *  WHEN          BY                   REASON
 *  ------------  -------------------  -----------------------------------------
 *  Mar 25, 2022  Sean Carrick         Initial creation.
 * *****************************************************************************
 */

package com.gs.ntos.api;

import com.gs.ntos.utils.NetUtils;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

/**
 *
 * @author Sean Carrick &lt;sean at gs-unitedlabs dot com&gt;
 * 
 * @version 0.1.0
 * @since 0.1.0
 */
public class UpdateTask {
    
    /* This *MUST* remain above the declarations of the private static final
     * variables due to forward reference errors if we place it at the bottom 
     * of the class.
     */
    private static final Runnable updateChecker = () -> {
        // TODO: Implement the updateChecker runnable.
    };
    
    private static final ScheduledExecutorService updater 
            = Executors.newScheduledThreadPool(1);
    private static final ScheduledFuture<?> updateHandler 
            = updater.scheduleAtFixedRate(updateChecker, 1, 1, TimeUnit.HOURS);
    private static final List<String> modsToUpdate = new ArrayList<>();
    private static final List<String> updatedMods = new ArrayList<>();
    
    private UpdateTask () {
    }
    
    public static boolean areUpdatesAvailable() {
        // TODO: Implement areUpdatesAvailable().
        
        return false;
    }
    
    public static void performUpdates() {
        // TODO: Implement performUpdates().
    }
    
    private static boolean isInternetAvailable() {
        return NetUtils.isNetworkConnectionAvailable();
    }
    
    private static void downloadUpdates() {
        // TODO: ImplementDownloadUpdates().
    }
    
    private static void installUpdates() {
        // TODO: Implement installUpdates().
    }

}
