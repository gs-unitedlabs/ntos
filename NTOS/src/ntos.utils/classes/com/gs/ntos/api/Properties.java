/*
 * Copyright (C) 2022 GS United Labs
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * *****************************************************************************
 *  Project    :   NTOS
 *  Class      :   Properties.java
 *  Author     :   Sean Carrick
 *  Created    :   Mar 28, 2022
 *  Modified   :   Mar 28, 2022
 *  
 *  Purpose: See class JavaDoc for explanation
 *  
 *  Revision History:
 *  
 *  WHEN          BY                   REASON
 *  ------------  -------------------  -----------------------------------------
 *  Mar 28, 2022  Sean Carrick         Initial creation.
 * *****************************************************************************
 */

package com.gs.ntos.api;

import com.gs.ntos.enums.Level;
import com.gs.ntos.logging.Logger;
import com.gs.ntos.utils.FileUtils;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

/**
 * The `Properties` class is a custom method of storing properties for a `Program`,
 * especially for `DesktopProgram` subclasses. The `Properties` class specifies
 * two different kinds of properties that may be accessed by a program: system
 * and user.
 * <p>
 * <strong>System properties</strong>: These are properties that affect the
 * program regardless of the executing user. These properties include such items
 * as defaults the program may use, storage locations, etc. Typically, these
 * properties are set a single time and remain relatively unchanged.</p>
 * <p>
 * <strong>User properties</strong>: These are properties that are specific to
 * each user of the program. These properties include such items as the size and
 * location of the various windows and components that make up a program, the
 * look and feel of the program, etc. Typically, these properties have the
 * potential of being changed quite often.</p>
 * <p>
 * The `Properties` class provides a way of retrieving <em>user</em> properties
 * as `Object`s, as well as specific intrinsic and object types. The <em>system
 * </em> properties are only retrieved and set using `Object`s as the value. The
 * reason the `Properties` class is set up in this manner is, as mentioned above,
 * the system properties do not typically change very often, so when they do, it
 * is required that the implementing program perform <em>explicity</em> casting
 * of the values. For the user properties, since they are retrieved and set more
 * often, the convenience methods have been provided to make the developers'
 * lives easier.</p>
 * <p>
 * Another manner in which we have tried to make the developers' lives easier is
 * to not add the word "User" to the method names of the getters and setters for
 * the user properties. Since those properties will be accessed more often, we
 * have simply named those methods as `getProperty*()`, where the `*` indicates
 * the various specific return types methods.</p>
 * <dl>
 * <dt><strong><em>Developer's Note</em></strong>:</dt>
 * <dd>This <code>Properties</code> class stores the properties to file each 
 * time a property is set, be that a system or a user property. This method is 
 * to be sure that all properties are saved each time a property is changed, so
 * that if there is a system crash after a property has been modified, the
 * modification is not lost.</dt></dl>
 *
 * @author Sean Carrick &lt;sean at gs-unitedlabs dot com&gt;
 * 
 * @version 1.0
 * @since 1.0
 */
public class Properties {
    
    private final Logger logger;
    private final DesktopProgram program;
    private final Map<String, Object> system;
    private final Map<String, Object> user;
    
    public Properties (DesktopProgram program) {
        logger = new Logger(program, getClass().getCanonicalName(), Level.WARNING);
        this.program = program;
        system = new ConcurrentHashMap<>();
        user = new ConcurrentHashMap<>();
        
        loadProperties();
    }
    
    /**
     * Retrieves the value for the specified `key`. If the key does not exist,
     * no exception is thrown and null is returned.
     * 
     * @param key the key for the value, typically the property name
     * @return the value for the specified `key` or `null` if it does not exist
     * @throws NullPointerException if `key` is `null`
     * 
     * @see #getSystemProperty(java.lang.String, java.lang.Object)
     * @see #setSystemProperty(java.lang.String, java.lang.Object) 
     */
    public Object getSystemProperty(String key) {
        checkKey(key);
        
        return system.get(key);
    }
    
    /**
     * Retrieves the value for the specified `key`. If the key has not been set,
     * then the specified default `value` is returned.
     * 
     * @param key the key for the value, typically the property name
     * @param value a default value in case the property has not been set
     * @return the value for the specified `key`, or the default `value` if the
     *          property has not been set
     * @throws NullPointerException if `key` is `null`
     * 
     * @see #getSystemProperty(java.lang.String) 
     * @see #setSystemProperty(java.lang.String, java.lang.Object) 
     */
    public Object getSystemProperty(String key, Object value) {
        checkKey(key);
        
        return system.get( key) == null ? value 
                : system.get(key);
    }
    
    /**
     * Sets the `value` for the specified `key` in the system properties. 
     * 
     * @param key the key for the value, typically the property name
     * @param value the value to be set for the given `key`
     * @throws NullPointerException if the specified `key` or `value` is `null`
     * 
     * @see #getSystemProperty(java.lang.String) 
     * @see #getSystemProperty(java.lang.String, java.lang.Object) 
     */
    public void setSystemProperty(String key, Object value) {
        checkKey(key);
        
        if (value == null) {
            throw new NullPointerException("null value");
        }
        system.put(key, value);
        
        storeProperties();
    }
    
    /**
     * Retrieves the value stored for the specified `key`.
     * 
     * @param key the key for the value, typically the property name
     * @return the value represented by the specified `key`, or `null` if not set
     * @throws NullPointerException if the specified `key` is null
     */
    public Object getProperty(String key) {
        checkKey(key);
        
        return user.get(key);
    }
    
    /**
     * Retrieves the value stored for the specified `key`. If `key` has not been
     * set, the default `value` is returned.
     * 
     * @param key the key for the value, typically the property name
     * @param value a default value in case the property has not been set
     * @return the value associated with `key`, or the default `value` supplied
     * @throws NullPointerException if the specified `key` is `null`
     */
    public Object getProperty(String key, Object value) {
        checkKey(key);
        
        return user.get(key) == null ? value : user.get(key);
    }
    
    /**
     * Sets the property with the specified `key` to the specified `value`.
     * 
     * @param key the key to set, typically the property name
     * @param value the value to store for the specified `key`
     * @throws NullPointerException if `key` is `null`
     */
    public void setProperty(String key, Object value) {
        checkKey(key);
        
        user.put(key, value);
        
        storeProperties();
    }
    
    /**
     * Convenience method to retrieve the value of the specified `key` as a
     * {@link java.lang.String String} value.
     * 
     * @param key the key for the value, typically the property name
     * @return the value as a string
     * @throws NullPointerException if `key` is `null`
     */
    public String getPropertyAsString(String key) {
        checkKey(key);
        
        return user.get(key).toString();
    }
    
    /**
     * Convenience method to retrieve the value of the specified `key` as a
     * {@link java.lang.String String) value. If `key` is not set, the default
     * `value` will be returned.
     * 
     * @param key the key for the value, typically the property name
     * @param value the default value to return if `key` is not set
     * @return the value specified by `key`, or the specified default `value`
     * @throws NullPointerException if `key` is `null`
     */
    public String getPropertyAsString(String key, String value) {
        checkKey(key);
        
        return user.get(key) == null ? value : user.get(key).toString();
    }
    
    /**
     * Sets the value of the specified `key` to the string specified in `value`.
     * 
     * @param key the key to set, typically the property name
     * @param value the value to set for the specified `key`
     * @throws NullPointerException if `key` is `null`
     */
    public void setProperty(String key, String value) {
        checkKey(key);
        
        user.put(key, value);
        
        storeProperties();
    }
    
    /**
     * Convenience method to retrieve the value of the specified `key` as a
     * `boolean` value.
     * 
     * @param key the key for the value, typically the property name
     * @return `true` if the `key` is set to one of: "true", "yes", "accepted",
     *          or "ok"; `false` otherwise, including if the key's value is `null`
     * @throws NullPointerException if `key` is `null`
     */
    public boolean getPropertyAsBoolean(String key) {
        checkKey(key);
        
        String val = getPropertyAsString(key);
        
        if (val != null) {
            switch (val.toLowerCase()) {
                case "true":
                case "yes":
                case "accepted":
                case "ok":
                    return true;
                default:
                    return false;
            }
        } else {
            return false;
        }
    }
    
    /**
     * Convenience method to retrieve the value of the specified `key` as a
     * `boolean` value. If `key` is not set, the the default `value` is returned.
     * 
     * @param key the key for the value, typically the property name
     * @param value the value to return if `key` is not set
     * @return the `key`'s value or the default `value` if `key` is not set
     * @throws NullPointerException if `key` is `null`
     */
    public boolean getPropertyAsBoolean(String key, boolean value) {
        checkKey(key);
        
        String val = getPropertyAsString(key);
        boolean result = false;
        
        if (val != null) {
            switch (val.toLowerCase()) {
                case "true":
                case "yes":
                case "accepted":
                case "ok":
                    result = true;
                    break;
                default:
                    result = value;
                    break;
            }
        } else {
            result = value;
        }
        
        return result;
    }
    
    /**
     * Sets the value of the specified `key` to the `boolean` value specified in
     * `value`.
     * 
     * @param key the key to set, typically the property name
     * @param value the boolean value to store for the `key`
     * @throws NullPointerException if `key` is `null`
     */
    public void setProperty(String key, boolean value) {
        checkKey(key);
        
        user.put(key, value);
        
        storeProperties();
    }
    
    /**
     * Convenience method to retrieve the value of the specified `key` as an
     * `Integer` value.
     * 
     * @param key the key for the value, typically the property name
     * @return the `key`'s value or `null` if `key` is not set
     * @throws NullPointerException if `key` is `null`
     */
    public Integer getPropertyAsInteger(String key) {
        checkKey(key);
        
        try {
            int val = Integer.parseInt(user.get(key).toString());
            return val;
        } catch (NumberFormatException ex) {
            return null;
        }
    }
    
    /**
     * Convenience method to retreive the value of the specified `key` as an
     * `Integer` value. If `key` is not set, the default `value` will be returned.
     * 
     * @param key the key for the value, typically the property name
     * @param value the default value to return if `key` is not set
     * @return the value for `key`, or the default `value` if `key` is not set
     * @throws NullPointerException if `key` is `null`
     */
    public Integer getPropertyAsInteger(String key, int value) {
        checkKey(key);
        
        String val = getPropertyAsString(key);
        
        if (val != null) {
            try {
                int i = Integer.parseInt(val);
                return i;
            } catch (NumberFormatException ex) {
                return value;
            }
        } else {
            return value;
        }
    }
    
    /**
     * Sets the value for the specified `key` to the `int` value supplied.
     * 
     * @param key the key to set, typically the property name
     * @param value the `int` value to store for the `key`
     * @throws NullPointerException if `key` is `null`
     */
    public void setProperty(String key, int value) {
        checkKey(key);
        
        user.put(key, value);
        
        storeProperties();
    }
    
    private void checkKey(String key) {
        if (key == null || key.isBlank() || key.isEmpty()) {
            throw new NullPointerException("null key");
        }
    }
    
    private void storeProperties() {
        try (BufferedWriter out = new BufferedWriter(new FileWriter(
                getSystemFile()))) {
            Set<String> keys = system.keySet();
            
            for (String key : keys) {
                String property = key + "=" + system.get(key);
                
                out.write(property + "\n");
            }
        } catch (IOException ex) {
            logger.error(ex, "Attempting to store system properties to: {0}", 
                    getSystemFile());
        }
        
        try (BufferedWriter out = new BufferedWriter(new FileWriter(
                getUserFile()))) {
            Set<String> keys = user.keySet();
            
            for (String key : keys) {
                String property = key + "=" + user.get(key);
                
                out.write(property + "\n");
            }
        } catch (IOException ex) {
            logger.error(ex, "Attempting to store user properties to: {0}", 
                    getUserFile());
        }
    }
    
    private void loadProperties() {
        try (BufferedReader in = new BufferedReader(new FileReader(
                getSystemFile()))) {
            String line = null;
            
            while ((line = in.readLine()) != null) {
                if (line.contains("=")) {
                    String[] split = line.split("=");
                    
                    if (split.length == 2) {
                        system.put(split[0], split[1]);
                    }
                }
            }
        } catch (IOException ex) {
            logger.warning("Error: {0} while attempting to load system "
                    + "properties from: {1}", ex.getMessage(), getSystemFile());
        }

        try (BufferedReader in = new BufferedReader(new FileReader(
                getUserFile()))) {
            String line = null;
            
            while ((line = in.readLine()) != null) {
                if (line.contains("=")) {
                    String[] split = line.split("=");
                    
                    if (split.length == 2) {
                        user.put(split[0], split[1]);
                    }
                }
            }
        } catch (IOException ex) {
            logger.warning("Error: {0} while attempting to load user "
                    + "properties from: {1}", ex.getMessage(), getUserFile());
        }
    }
    
    private File getSystemFile() {
        StringBuilder sb = new StringBuilder();
        
        sb.append(program.getProgramId()).append(".cfg");
        
        return new File(FileUtils.getApplicationConfigDirectory(), sb.toString());
    }
    
    private File getUserFile() {
        String userFile = "user-" + System.getProperty("user.name");
        userFile += ".cfg";
        return new File(FileUtils.getApplicationConfigDirectory(), userFile);
    }

}
