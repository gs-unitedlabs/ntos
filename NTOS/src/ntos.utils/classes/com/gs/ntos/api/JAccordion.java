/*
 * Copyright (C) 2022 GS United Labs
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * *****************************************************************************
 *  Project    :   LoadAccounting
 *  Class      :   JAccordion.java
 *  Author     :   Sean Carrick
 *  Created    :   Mar 29, 2022
 *  Modified   :   Mar 29, 2022
 *  
 *  Purpose: See class JavaDoc for explanation
 *  
 *  Revision History:
 *  
 *  WHEN          BY                   REASON
 *  ------------  -------------------  -----------------------------------------
 *  Mar 29, 2022  Sean Carrick         Initial creation.
 * *****************************************************************************
 */

package com.gs.ntos.api;

import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;
import javax.swing.Icon;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JPanel;

/**
 * A component that provides a look similar to the old Outlook Bar. It is 
 * similar to a `JTabbedPane`, except instead of maintaining tabs, it uses
 * Outlook-style bars to control the visible component. Furthermore, this 
 * component differs from the SwingLabs `JXTaskPaneContainer` and `JXTaskPane`
 * in that these panels overlap and hide each other, instead of simply stacking
 * one below the other as the `JXTaskPane`s do.
 * 
 * ***Attribution***: I found this code online at 
 * <a href="http://www.java2s.com/Code/Java/Swing-Components/AJOutlookBarprovidesacomponentthatissimilartoaJTabbedPanebutinsteadofmaintainingtabs.htm">
 * www.java2s.com</a>, but there is no author attribution on it. So, for whomever
 * wrote this ingenious class, we are giving you a big shout-out!
 *
 * @author Sean Carrick &lt;sean at gs-unitedlabs dot com&gt;
 * 
 * @version 1.0
 * @since 1.0
 */
public class JAccordion extends JPanel implements ActionListener {

    private static final long serialVersionUID = 7696311412522125140L;
    
    /**
     * `topPanel` displays the button on the top of the Outlook bar.
     */
    private JPanel topPanel = new JPanel(new GridLayout(1, 1));
    /**
     * `bottomPanel` displays the button on the bottom of the Outlook bar.
     */
    private JPanel bottomPanel = new JPanel(new GridLayout(1, 1));
    /**
     * `bars` is a `LinkedHashMap` that preserves the order of the bars.
     */
    private Map<String, BarInfo> bars = new LinkedHashMap<>();
    /**
     * The currently visible bar (zero-based index).
     */
    private int visibleBar = 0;
    /**
     * A place-holder for the currently visible component.
     */
    private JComponent visibleComponent = null;
    
    /**
     * Creates a new Outlook-style bar, after which, calls to `addBar()` should
     * be made for each bar required.
     */
    public JAccordion () {
        initComponents();
    }
    
    private void initComponents() {
        setLayout(new BorderLayout());
        add(topPanel, BorderLayout.NORTH);
        add(bottomPanel, BorderLayout.SOUTH);
    }
    
    /**
     * Adds the specified `component` to the Outlook bar and sets the bar's 
     * name. If `name` is blank, empty, or `null`, it will be set to the value
     * of `component.getName()`. After this, if `name` is still blank, empty, or
     * `null` because the name property was not set, `name` will be set to
     * `component.getClass().getSimpleName()`.
     * 
     * @param name the name for the bar component
     * @param component the component to be displayed as the bar
     * @throws IllegalArgumentException if `component` is `null`
     */
    public void addBar(String name, JComponent component) {
        if (component == null) {
            throw new IllegalArgumentException("null component");
        }
        if (name == null || name.isBlank() || name.isEmpty()) {
            name = component.getName();
            if (name == null || name.isBlank() || name.isEmpty()) {
                name = component.getClass().getSimpleName();
            }
        }
        
        BarInfo barInfo = new BarInfo(name, component);
        barInfo.getButton().addActionListener(this);
        bars.put(name, barInfo);
        render();
    }
    
    /**
     * Adds the specified `component` to the Outlook bar and sets the bar's name.
     * If `name` is blank, empty, or `null`, it will be set to the value of
     * `component.getName()`. After this, if `name` is still blank, empty, or
     * `null because the name property was not set, `name` will be set to
     * `component.getClass().getSimpleName()`.
     * 
     * @param name the name for the bar component
     * @param component the component to be displayed as the bar
     * @param icon the icon to display in the Outlook bar; if `icon` is `null`,
     *          no exception will be thrown
     * @throws IllegalArgumentException if `component` is `null`
     */
    public void addBar(String name, JComponent component, Icon icon) {
        BarInfo barInfo = new BarInfo(name, component, icon);
        barInfo.getButton().addActionListener(this);
        bars.put(name, barInfo);
        render();
    }
    
    /**
     * Removes the bar with the specified `name` from the Outlook bar.
     * 
     * @param name name of the bar to remove
     */
    public void removeBar(String name) {
        bars.remove(name);
        render();
    }
    
    /**
     * Retrieves the index of the visible bar.
     * 
     * @return the visible bar's index
     */
    public int getVisibleBarIndex() {
        return visibleBar;
    }
    
    /**
     * Sets the currently visible bar. The `visibleBar` parameter must be greater
     * than zero and less than the count of bars currently added.
     * 
     * @param visibleBar the index of the bar to make visible
     * @throws IndexOutOfBoundsException
     */
    public void setVisibleBar(int visibleBar) {
        if (visibleBar < 0 || visibleBar >= bars.size()) {
            throw new IndexOutOfBoundsException(visibleBar + " is less than "
                    + "zero or greater than the number of available bars");
        }
        
        this.visibleBar = visibleBar;
    }
    
    /**
     * Rebuilds the Outlook bar component. The top and bottom bars are completely
     * rebuilt, then the intervening bars are readded and the appropriate bar is
     * set as the visible bar.
     */
    public void render() {
        // Compute the number of bars.
        int totalBars = bars.size();
        int topBars = visibleBar + 1;
        int bottomBars = totalBars - topBars;
        
        // Get an iterator to walk through all of the bars.
        Iterator<String> itr = bars.keySet().iterator();
        
        // Render the top bars: remove all components, reset the GridLayout to 
        //+ hold the correct number of bars, add the bars, and "validate" it to
        //+ cause it to re-layout its components.
        topPanel.removeAll();
        GridLayout topLayout = (GridLayout) topPanel.getLayout();
        topLayout.setRows(topBars);
        BarInfo barInfo = null;
        
        for (int i = 0; i < topBars; i++) {
            String barName = (String) itr.next();
            barInfo = (BarInfo) bars.get(barName);
            topPanel.add(barInfo.getButton());
        }
        
        topPanel.validate();
        
        // Render the center component: remove the current component (if any) and
        //+ then put the visible component in the center of this panel.
        if (visibleComponent != null) {
            remove(visibleComponent);
        }
        visibleComponent = barInfo.getComponent();
        add(visibleComponent, BorderLayout.CENTER);
        
        // Render the bottom bars: remove all components, reset the GridLayout
        //+ to hold the correct number of bars, add the bars, and "validate" it
        //+ to cause it to re-layout its components.
        bottomPanel.removeAll();
        GridLayout bottomLayout = (GridLayout) bottomPanel.getLayout();
        bottomLayout.setRows(bottomBars);
        for (int i = 0; i < bottomBars; i++) {
            String barName = (String) itr.next();
            barInfo = (BarInfo) bars.get(barName);
            bottomPanel.add(barInfo.getButton());
        }
        
        bottomPanel.validate();
        
        // Validate all of our components: causes this container to re-layout
        //+ its subcomponents.
        validate();
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        int currentBar = 0;
        for (Iterator<String> i = bars.keySet().iterator(); i.hasNext();) {
            String barName = (String) i.next();
            BarInfo barInfo = (BarInfo) bars.get(barName);
            if (barInfo.getButton() == e.getSource()) {
                // Found the selected button.
                visibleBar = currentBar;
                render();
                return;
            }
            
            currentBar++;
        }
    }
    
    class BarInfo {
        /**
         * The name of this bar.
         */
        private String name;
        /**
         * The JButton that implements the Outlook bar itself.
         */
        private JButton button; 
        /**
         * The component that is the body of the Outlook bar.
         */
        private JComponent component;
        
        /**
         * Constructs a new `BarInfo` instance with the specified `name` and
         * `component`.
         * 
         * @param name name for the bar
         * @param component component that makes up the body of the bar
         * @throws IllegalArgumentException if `name` is blank, empty, or `null`;
         *          or if `component` is `null`
         */
        public BarInfo(String name, JComponent component) {
            if (name == null || name.isBlank() || name.isEmpty()) {
                throw new IllegalArgumentException("blank, empty, or null name");
            }
            if (component == null) {
                throw new IllegalArgumentException("null component");
            }
            this.name = name;
            this.component = component;
            button = new JButton(name);
        }
        
        /**
         * Constructs a new `BarInfo` instance with the specified `name`, 
         * `component` and `icon`.
         * 
         * @param name name for the bar
         * @param component component that makes up the body of the bar
         * @param icon an icon to display next to the name
         * @throws IllegalArgumentException if `name` is blank, empty, or `null`,
         *          if `component` or `icon` is `null`
         */
        public BarInfo(String name, JComponent component, Icon icon) {
            this(name, component);
            if (icon == null) {
                throw new IllegalArgumentException("null icon");
            }
            
            button = new JButton(name, icon);
            button.setIconTextGap(15);
        }
        
        /**
         * Retrieves the name of this bar.
         * 
         * @return the bar's name
         */
        public String getName() {
            return name;
        }
        
        /**
         * Sets the name for this bar.
         * 
         * @param name the bar's name
         * @throws IllegalArgumentException if `name` is blank, empty, or `null`
         */
        public void setName(String name) {
            if (name == null || name.isBlank() || name.isEmpty()) {
                throw new IllegalArgumentException("blank, empty, or null name");
            }
            
            this.name = name;
        }
        
        /**
         * Retrieves this bar's button.
         * 
         * @return the bar's button
         */
        public JButton getButton() {
            return button;
        }
        
        /**
         * Retrieves the component that makes up this bar's body.
         * 
         * @return the bar's component
         */
        public JComponent getComponent() {
            return component;
        }
    }

}
