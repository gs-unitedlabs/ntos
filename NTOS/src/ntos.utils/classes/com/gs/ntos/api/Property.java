/*
 * Copyright (C) 2022 GS United Labs
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * *****************************************************************************
 *  Project    :   ntos.utils
 *  Class      :   Property.java
 *  Author     :   Sean Carrick
 *  Created    :   Mar 17, 2022 @ 8:52:53 PM
 *  Modified   :   Mar 17, 2022
 *  
 *  Purpose: See JavaDoc comments.
 *  
 *  Revision History:
 *  
 *  WHEN          BY                   REASON
 *  ------------  -------------------  ------------------------------------------
 *  Mar 17, 2022  Sean Carrick         Initial creation.
 * *****************************************************************************
 */

package com.gs.ntos.api;

import java.awt.Component;

/**
 * Defines the `sessionState` property. The value of this property is the
 * GUI state that should be preserved across sessions for the specified component.
 * The type of sessionState values just like those supported by
 * {@link java.beans.XMLEncoder XMLEncoder} and
 * {@link java.beans.XMLDecoder XMLDecoder}, for example beans (null
 * constructor, read/write properties), primitives, and Collections.
 *
 * @author Sean Carrick &lt;sean at gs-unitedlabs dot com&gt;
 * 
 * @version 0.1.0
 * @since 0.1.0
 */
public interface Property {
    
    /**
     * Return the value of the `sessionState` property, typically a Java
     * bean or a Collection that defines the `Component` state that should
     * be preserved across program sessions. This value will be stored with
     * {@link java.beans.XMLEncoder XMLEncoder}, loaded with
     * {@link java.beans.XMLDecoder XMLDecoder}, and passed to
     * } setSessionState} to restore the component's state.
     * 
     * @param c the Component
     * @return the `sessionState` object for Component `c`
     * 
     * @see #setSessionState(java.awt.Component, java.lang.Object) 
     */
    Object getSessionState(Component c);
    
    /**
     * Restore Component `c`'s `sessionState` from the specified
     * object.
     * 
     * @param c the Component
     * @param state the value of the `sessionState` from the specified
     *          object
     * 
     * @see #getSessionState(java.awt.Component) 
     */
    void setSessionState(Component c, Object state);
    
}
