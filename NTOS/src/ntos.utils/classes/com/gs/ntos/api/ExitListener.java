/*
 * Copyright (C) 2022 GS United Labs
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * *****************************************************************************
 *  Project    :   ntos.utils
 *  Class      :   ExitListener.java
 *  Author     :   Sean Carrick
 *  Created    :   Mar 17, 2022 @ 5:51:30 PM
 *  Modified   :   Mar 17, 2022
 *  
 *  Purpose: See JavaDoc comments.
 *  
 *  Revision History:
 *  
 *  WHEN          BY                   REASON
 *  ------------  -------------------  -----------------------------------------
 *  Mar 17, 2022  Sean Carrick         Initial creation.
 * *****************************************************************************
 */

package com.gs.ntos.api;

import java.util.EventObject;

/**
 * Give the Application a chance to veto an attempt to exit/quit. An
 * `ExitListener`'s` `canExit` method should return false if
 * there are pending decisions that the user must make before the app exits.
 * A typical `ExitListener` would prompt the user with a modal dialog.
 *
 * The `eventObject` argument will be the the value passed to
 * {@link com.gs.ntos.api.Program#exit(EventObject) `exit()}}. It may be `null`.
 *
 * The `willExit` method is called after the exit has been confirmed.
 * An `ExitListener` that's going to perform some cleanup work should do so 
 * in `willExit`.
 *
 * `ExitListeners` run on the event dispatching thread.
 *
 * @author Sean Carrick &lt;sean at gs-unitedlabs dot com&gt;
 * 
 * @version 0.1.0
 * @since 0.1.0
 */
public interface ExitListener {
    
    /**
     * Allows a window to cancel the exiting of the application.
     * 
     * @param event the value passed into `exit(EventObject)` and may be
     *          `null`
     * @return  `true` if the window is ready to exit; `false` if not
     */
    boolean canExit(EventObject event);

    /**
     * Called after the exit has been confirmed. In other words, all windows
     * have responded to calls to their } canExit} method with `true`.
     * This method allows the window to perform some cleanup work, such as 
     * saving its session state to file.
     * 
     * @param event the value passed into `exit(EventObject)` and may be
     *          `null`
     */
    void willExit(EventObject event);

}
