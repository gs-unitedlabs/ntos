/*
 * Copyright (C) 2022 GS United Labs
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * *****************************************************************************
 *  Project    :   ntos.utils
 *  Class      :   DesktopProgram.java
 *  Author     :   Sean Carrick
 *  Created    :   Mar 19, 2022
 *  Modified   :   Mar 19, 2022
 *  
 *  Purpose: See class JavaDoc for explanation
 *  
 *  Revision History:
 *  
 *  WHEN          BY                   REASON
 *  ------------  -------------------  -----------------------------------------
 *  Mar 19, 2022  Sean Carrick         Initial creation.
 * *****************************************************************************
 */

package com.gs.ntos.api;

import com.gs.ntos.enums.Level;
import com.gs.ntos.enums.SysExits;
import com.gs.ntos.logging.Logger;
import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Container;
import java.awt.Frame;
import java.awt.Window;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import java.awt.event.HierarchyEvent;
import java.awt.event.HierarchyListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.IOException;
import java.util.ArrayList;
import java.util.EventObject;
import java.util.List;
import javax.swing.JComponent;
import javax.swing.JDesktopPane;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JInternalFrame;
import javax.swing.RootPaneContainer;

/**
 *
 * @author Sean Carrick &lt;sean at gs-unitedlabs dot com&gt;
 * 
 * @version 0.1.0
 * @since 0.1.0
 */
public abstract class DesktopProgram extends AbstractBean implements Program {
    
    private final SessionStorage sessionStorage;
    
    private static DesktopProgram instance;
    
    protected final Logger logger;
    protected final Properties props;
    protected final List<ExitListener> exitListeners;
    
    private JFrame mainFrame = null;
    
    public DesktopProgram () {
        logger = new Logger(this, getClass().getCanonicalName(), Level.TRACE);
        sessionStorage = new SessionStorage(this);
        props = new Properties(this);
        exitListeners = new ArrayList<>();
        instance = this;
    }
    
    public static DesktopProgram getInstance() {
        return instance;
    }

    @Override
    public void exit(EventObject evt, SysExits status) {
        // First, loop through all of the registered ExitListeners and make sure
        //+ they are all ready to exit.
        for (ExitListener el : exitListeners) {
            if (!el.canExit(evt)) {
                return;
            }
        }
        
        try {
            exitListeners.forEach(listener -> {
                try {
                    listener.willExit(evt);
                } catch (Exception e) {
                    logger.log(Level.WARNING, "ExitListener.willExit() failed:"
                            + "\nException: {0}\nMessage: {1}", 
                            e.getClass().getSimpleName(), e.getMessage());
                }
            });
            
            shutdown();
        } catch (Exception ex) {
            logger.log(Level.WARNING, "Unexpected error in DesktopProgram."
                    + "shutdown()\nException: {0}\nMessage: {1}", 
                    ex.getClass().getSimpleName(), ex.getMessage());
        } finally {
            end(status);
        }
    }
    
    /**
     * Retrieves the program's {@link java.util.prefs.Properties Properties} 
     * object. This method may be used to get the `Properties` object to read
     * or set a property(ies). However, the convenience methods may also be used
     * for getting and setting properties.
     * <p>
     * The advantage of getting the system `Properties` object is that the
     * various properties can be set or obtained using various data types. The
     * provided convenience methods only allow string values to be set or
     * obtained.</p>
     * 
     * @return the `Program`'s system `Properties` object
     * 
     * @see #getProperty(java.lang.String) 
     * @see #getSystemProperty(java.lang.String, java.lang.String) 
     * @see #setSystemProperty(java.lang.String, java.lang.String) 
     */
    public Properties getProperties() {
        return props;
    }
    
    /**
     * A convenience method for getting the value of a system property.
     * 
     * @param propertyName the property name for which to get the value
     * @param defaultValue a default value in case the property is not set
     * @return the value of the property named by `propertyName`, or
     *          `null` if the property is not found
     * @throws IllegalArgumentException if either `propertyName` or `defaultValue`
     *          are blank, empty, or null
     * 
     * @see #getProperties() 
     * @see #getSystemProperty(java.lang.String, java.lang.String) 
     * @see #setSystemProperty(java.lang.String, java.lang.String) 
     */
    public Object getSystemProperty(String propertyName, String defaultValue) {
        if (propertyName == null || propertyName.isBlank() 
                || propertyName.isEmpty()) {
            throw new IllegalArgumentException("blank, empty, or null propertyName");
        }
        if (defaultValue == null || defaultValue.isBlank() 
                || defaultValue.isEmpty()) {
            throw new IllegalArgumentException("blank, empty, or null defaultValue");
        }
        return props.getSystemProperty(propertyName, defaultValue);
    }
    
    /**
     * A convenience method for getting the value of a user property.
     * 
     * @param propertyName the property name for which to get the value
     * @param defaultValue a default value in case the property is not set
     * @return the value of the property named by `propertyName`, or
     *          `null` if the property is not found
     * @throws IllegalArgumentException if either `propertyName` or `defaultValue`
     *          are blank, empty, or null
     * 
     * @see #getProperties() 
     * @see #getSystemProperty(java.lang.String, java.lang.String) 
     * @see #setSystemProperty(java.lang.String, java.lang.String) 
     */
    public Object getProperty(String propertyName, String defaultValue) {
        if (propertyName == null || propertyName.isBlank() 
                || propertyName.isEmpty()) {
            throw new IllegalArgumentException("blank, empty, or null propertyName");
        }
        if (defaultValue == null || defaultValue.isBlank() 
                || defaultValue.isEmpty()) {
            throw new IllegalArgumentException("blank, empty, or null defaultValue");
        }
        return props.getProperty(propertyName, defaultValue);
    }
    
    /**
     * A convenience method for setting a system `Properties`' value. If 
     * `propertyName` is blank, empty, or `null`, no exception is thrown and no
     * action is taken. If `value` is blank, empty, or `null`, but `propertyName`
     * is provided, then the named property will be set to blank, empty, or 
     * `null`.
     * 
     * @param propertyName the name of the property to set
     * @param value the value to which the `propertyName` should be set
     * 
     * @see #getProperties() 
     * @see #getProperty(java.lang.String) 
     * @see #getSystemProperty(java.lang.String, java.lang.String) 
     */
    public void setSystemProperty(String propertyName, String value) {
        if (propertyName == null || propertyName.isBlank() 
                || propertyName.isEmpty()) {
            return;
        }
        props.setSystemProperty(propertyName, value);
    }
    
    /**
     * A convenience method for setting a user `Properties`' value. If 
     * `propertyName` is blank, empty, or `null`, no exception is thrown and no
     * action is taken. If `value` is blank, empty, or `null`, but `propertyName`
     * is provided, then the named property will be set to blank, empty, or 
     * `null`.
     * 
     * @param propertyName the name of the property to set
     * @param value the value to which the `propertyName` should be set
     * 
     * @see #getSystemProperties() 
     * @see #getSystemProperty(java.lang.String, java.lang.String) 
     * @see #getUserProperties() 
     * @see #getUserProperty(java.lang.String, java.lang.String) 
     */
    public void setUserProperty(String propertyName, String value) {
        if (propertyName == null || propertyName.isBlank() 
                || propertyName.isEmpty()) {
            return;
        }
        props.setProperty(propertyName, value);
    }
    
    /**
     * Retrieves the main window for this } DesktopProgram}. If the
     * } mainFrame} has not been set, a new } JFrame} is created and
     * initialized with the title } getProgramTitle()}.
     * 
     * @return the } DesktopProgram}'s main window
     */
    public JFrame getMainFrame() {
        if (mainFrame == null) {
            mainFrame = new JFrame(getProgramTitle());
        }
        
        return mainFrame;
    }
    
    /**
     * Sets a window as the } DesktopProgram}'s main window. If the
     * } mainFrame} has already been set, this method throws an
     * } IllegalStateException}.
     * 
     * @param mainFrame the } JFrame} to use as the main window for this
     *          } DesktopProgram}
     */
    public void setMainFrame(JFrame mainFrame) {
//        if (mainFrame != null) {
//            throw new IllegalStateException("mainFrame has already been set");
//        }
        
        this.mainFrame = mainFrame;
    }
    
    /**
     * Retrieves the `SessionStorage` instance for this 
     * } DesktopProgram}.
     * 
     * @return the `SessionStorage` object
     */
    public SessionStorage getSessionStorage() {
        return sessionStorage;
    }

    @Override
    public void removeExitListener(ExitListener listener) {
        exitListeners.remove(listener);
    }

    @Override
    public void addExitListener(ExitListener listener) {
        exitListeners.add(listener);
    }
    
    protected void initRootPaneContainer(RootPaneContainer c) {
        JComponent rootPane = c.getRootPane();
        // These initializations are only done once
        Object k = "DesktopProgram.initRootPaneContainer";
        if (rootPane.getClientProperty(k) != null) {
            return;
        }
        rootPane.putClientProperty(k, Boolean.TRUE);
        // Inject resources
        Container root = rootPane.getParent();
        // If this is the mainFrame, then close == exit
        JFrame mFrame = getMainFrame();
        if (c == mFrame) {
            mFrame.addWindowListener(new JFrameListener());
            mFrame.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
        } else if (root instanceof Window) { // close == save session state
            Window window = (Window) root;
            window.addHierarchyListener(new SecondaryWindowListener());
        }
        // If this is a JFrame monitor "normal" (not maximized) bounds
        if (root instanceof JFrame) {
            root.addComponentListener(new FrameBoundsListener());
        }
        // If the window's bounds don't appear to have been set, do it
        if (root instanceof Window) {
            Window window = (Window) root;
            if (!root.isValid() || (root.getWidth() == 0)
                    || (root.getHeight() == 0)) {
                window.pack();
            }
            if (!window.isLocationByPlatform() && (root.getX() == 0)
                    && (root.getY() == 0)) {
                Component owner = window.getOwner();
                if (owner == null) {
                    owner = (window != mFrame) ? mFrame : null;
                }
                window.setLocationRelativeTo(owner);  // center the window
            }
        }
        // Restore session state
        if (root instanceof Window) {
            String filename = sessionFilename((Window) root);
            if (filename != null) {
                try {
                    sessionStorage.restore(root, filename);
                } catch (IOException e) {
                    String msg = String.format("couldn't restore sesssion [%s]\n"
                            + "Reason: %s", filename, e);
                    logger.log(Level.WARNING, msg);
                }
            }
        }
    }

    /**
     * Show the specified component in the {@link #getMainFrame main frame}.
     * Typical applications will call this method after constructing their main
     * GUI panel in the `startup` method.
     *
     * Before the main frame is made visible, the properties of all of the
     * components in the hierarchy are restored from saved session state (if any) with {@link
     * SessionStorage#restore SessionStorage.restore}. When the application
     * shuts down, session state is saved.
     *
     * Note that the name of the lazily created main frame (see
     * {@link #getMainFrame getMainFrame}) is set by default. Session state is
     * only saved for top level windows with a valid name and then only for
     * component descendants that are named.
     *
     * Throws an IllegalArgumentException if `c` is null
     *
     * @param c the main frame's contentPane child
     */
    protected void show(JComponent c) {
        if (c == null) {
            throw new IllegalArgumentException("null JComponent");
        }
        JFrame f = getMainFrame();
        f.getContentPane().add(c, BorderLayout.CENTER);
        initRootPaneContainer(f);
        f.setVisible(true);
    }
    
    public void show(JInternalFrame f, JDesktopPane d) {
        if (f == null) {
            throw new IllegalArgumentException("null JInternalFrame");
        }
        if (d == null) {
            throw new IllegalArgumentException("null JDesktopPane");
        }
        
        d.add(f);
        restoreInternalFrameSession(f);
        f.setVisible(true);
    }

    /**
     * Initialize and show the JDialog.
     *
     * This method is intended for showing "secondary" windows, like message
     * dialogs, about boxes, and so on. Unlike the `mainFrame`, dismissing a
     * secondary window will not exit the application.
     *
     * Session state is only automatically saved if the specified JDialog has a
     * name, and then only for component descendants that are named.
     *
     * Throws an IllegalArgumentException if `c` is null
     *
     * @param c the main frame's contentPane child
     * @see #show(JComponent)
     * @see #show(JFrame)
     */
    public void show(JDialog c) {
        if (c == null) {
            throw new IllegalArgumentException("null JDialog");
        }
        initRootPaneContainer(c);
        restore(c);
        c.setVisible(true);
    }

    /**
     * Initialize and show the secondary JFrame.
     *
     * This method is intended for showing "secondary" windows, like message
     * dialogs, about boxes, and so on. Unlike the `mainFrame`, dismissing a
     * secondary window will not exit the application.
     *
     * Session state is only automatically saved if the specified JFrame has a
     * name, and then only for component descendants that are named.
     *
     * Throws an IllegalArgumentException if `c` is null
     * 
     * @param c the `javax.swing.JFrame` to show
     *
     * @see #show(JComponent)
     * @see #show(JDialog)
     */
    public void show(JFrame c) {
        if (c == null) {
            throw new IllegalArgumentException("null JFrame");
        }
        initRootPaneContainer(c);
        restore(c);
        c.setVisible(true);
    }
    
    public void save(Window window) {
        saveSession(window);
    }
    
    public void save(JInternalFrame window) {
        saveInternalFrameSession(window);
    }
    
    public void restore(Window window) {
        try {
            sessionStorage.restore(window, sessionFilename(window));
        } catch (IOException ex) {
                String msg = String.format("couldn't restore session\nReason: "
                        + "%s", ex);
                logger.log(Level.WARNING, msg);
        }
    }
    
    public void restore(JComponent window) {
        try {
            sessionStorage.restore(window, sessionFilename(window));
        } catch (IOException ex) {
                String msg = String.format("couldn't restore session\nReason: "
                        + "%s", ex);
                logger.log(Level.WARNING, msg);
        }
    }
    
    public void firePropertyChange(String propertyName, Object oldValue,
            Object newValue) {
        super.firePropertyChange(propertyName, oldValue, newValue);
    }

    protected void saveSession(Window window) {
        String filename = sessionFilename(window);
        if (filename != null) {
            try {
                sessionStorage.save(window, filename);
            } catch (IOException ex) {
                String msg = String.format("couldn't save session\nReason: %s", 
                        ex);
                logger.log(Level.WARNING, msg);
            }
        }
    }
    
    protected void saveSession(JComponent window) {
        String filename = sessionFilename(window);
        if (filename != null) {
            try {
                sessionStorage.save(window, filename);
            } catch (IOException ex) {
                String msg = String.format("couldn't save session\nReason: %s", 
                        ex);
                logger.log(Level.WARNING, msg);
            }
        }
    }
    
    protected void saveInternalFrameSession(JInternalFrame window) {
            try {
                sessionStorage.save(window, sessionFilename(window));
            } catch (IOException ex) {
                String msg = String.format("couldn't save session\nReason: %s", 
                        ex);
                logger.log(Level.WARNING, msg);
            }
    }
    
    protected void restoreInternalFrameSession(JInternalFrame window) {
        try {
            sessionStorage.restore(window, sessionFilename(window));
        } catch (IOException ex) {
                String msg = String.format("couldn't restore session\nReason: "
                        + "%s", ex);
                logger.log(Level.WARNING, msg);
        }
    }
    
    private String sessionFilename(Window window) {
        if (window == null) {
            return null;
        } else {
            String name = window.getName();
            
            if (name == null) {
                name = window.getClass().getCanonicalName();
            }
            return (name == null) ? null : name + ".session.xml";
        }
    }

    private String sessionFilename(JComponent window) {
        if (window == null) {
            return null;
        } else {
            String name = window.getName();
            
            if (name == null) {
                name = window.getClass().getCanonicalName();
            }
            return (name == null) ? null : name + ".session.xml";
        }
    }

    /* In order to properly restore a maximized JFrame, we need to 
     * record it's normal (not maximized) bounds.  They're recorded
     * under a rootPane client property here, so that they've can be 
     * session-saved by WindowProperty#getSessionState().
     */
    private static class FrameBoundsListener implements ComponentListener {

        private void maybeSaveFrameSize(ComponentEvent e) {
            if (e.getComponent() instanceof JFrame) {
                JFrame f = (JFrame) e.getComponent();
                if ((f.getExtendedState() & Frame.MAXIMIZED_BOTH) == 0) {
                    String clientPropertyKey = "WindowState.normalBounds";
                    f.getRootPane().putClientProperty(clientPropertyKey, f.getBounds());
                }
            }
        }

        @Override
        public void componentResized(ComponentEvent e) {
            maybeSaveFrameSize(e);
        }

        /* BUG: on Windows XP, with JDK6, this method is called once when the 
         * frame is a maximized, with x,y=-4 and getExtendedState() == 0.
         */
        @Override
        public void componentMoved(ComponentEvent e) {
            /* maybeSaveFrameSize(e); */ }

        @Override
        public void componentHidden(ComponentEvent e) {
        }

        @Override
        public void componentShown(ComponentEvent e) {
        }
    }

    private class JFrameListener extends WindowAdapter {

        @Override
        public void windowOpened(WindowEvent e) {
            restore(mainFrame);
        }
        
        @Override
        public void windowClosing(WindowEvent e) {
            save(mainFrame);
            exit(e);
        }
    }

    /* Although it would have been simpler to listen for changes in
     * the secondary window's visibility per either a
     * PropertyChangeEvent on the "visible" property or a change in
     * visibility per ComponentListener, neither listener is notified
     * if the secondary window is disposed.
     * HierarchyEvent.SHOWING_CHANGED does report the change in all
     * cases, so we use that.
     */
    private class SecondaryWindowListener implements HierarchyListener {

        @Override
        public void hierarchyChanged(HierarchyEvent e) {
            if ((e.getChangeFlags() & HierarchyEvent.SHOWING_CHANGED) != 0) {
                if (e.getSource() instanceof Window) {
                    Window secondaryWindow = (Window) e.getSource();
                    if (!secondaryWindow.isShowing()) {
                        saveSession(secondaryWindow);
                    }
                }
            }
        }
    }

}
