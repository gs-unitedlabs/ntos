/*
 * Copyright (C) 2022 GS United Labs
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * *****************************************************************************
 *  Project    :   NTOS
 *  Class      :   NumericalFormatters.java
 *  Author     :   Sean Carrick
 *  Created    :   Apr 6, 2022
 *  Modified   :   Apr 6, 2022
 *  
 *  Purpose: See class JavaDoc for explanation
 *  
 *  Revision History:
 *  
 *  WHEN          BY                   REASON
 *  ------------  -------------------  -----------------------------------------
 *  Apr 6, 2022  Sean Carrick         Initial creation.
 * *****************************************************************************
 */

package com.gs.ntos.formatters;

import com.gs.ntos.api.DesktopProgram;
import com.gs.ntos.logging.Logger;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.ParseException;

/**
 *
 * @author Sean Carrick &lt;sean at gs-unitedlabs dot com&gt;
 * 
 * @version 1.0
 * @since 1.0
 */
public class NumericalFormatters {
    
    private static final Logger logger = new Logger(DesktopProgram.getInstance(),
            NumericalFormatters.class.getSimpleName());
    
    private NumericalFormatters () {}
    
    public static double doubleFormatter(String value) {
        NumberFormat nf = null;
        Number pay = null;
        try {
            nf = new java.text.DecimalFormat("#,##0.00");
            pay = nf.parse(value);
        } catch (ParseException ex) {
            logger.warning("Could not parse revenueField value: {0}", value);
        }
        
        return pay == null ? 0.0d : pay.doubleValue();
    }
    
    public static int integerFormatter(String value) {
        NumberFormat nf = null;
        Number number = null;
        try {
            nf = NumberFormat.getCurrencyInstance();
            number = nf.parse(value);
        } catch (ParseException ex) {
            logger.warning("Could not parse revenueField value: {0}", value);
        }
        
        return number == null ? 0 : number.intValue();
    }
    
    public static String currencyFormatter(String value) {
        NumberFormat nf = null;
        Number number = null;
        try {
            nf = new java.text.DecimalFormat("#,##0");
            number = nf.parse(value);
        } catch (ParseException ex) {
            logger.warning("Could not parse revenueField value: {0}", value);
        }
        
        return number == null ? "0.00" : ((DecimalFormat) nf).format(value);
    }

}
