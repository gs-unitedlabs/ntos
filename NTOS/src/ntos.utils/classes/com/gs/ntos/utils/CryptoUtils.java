/*
 * Copyright (C) 2022 GS United Labs
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * *****************************************************************************
 *  Project    :   NTOS
 *  Class      :   CryptoUtils.java
 *  Author     :   Sean Carrick
 *  Created    :   Apr 13, 2022
 *  Modified   :   Apr 13, 2022
 *  
 *  Purpose: See class JavaDoc for explanation
 *  
 *  Revision History:
 *  
 *  WHEN          BY                   REASON
 *  ------------  -------------------  -----------------------------------------
 *  Apr 13, 2022  Sean Carrick         Initial creation.
 * *****************************************************************************
 */

package com.gs.ntos.utils;

import com.gs.ntos.errors.CryptoException;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.security.InvalidKeyException;
import java.security.Key;
import java.security.NoSuchAlgorithmException;
import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.SecretKeySpec;

/**
 *
 * @author Sean Carrick &lt;sean at gs-unitedlabs dot com&gt;
 * 
 * @version 1.0
 * @since 1.0
 */
public class CryptoUtils {
    
    private static final String ALGORITHM = "AES";
    private static final String TRANSFORMATION = "AES";
    
    private CryptoUtils () {}
    
    public static void encrypt(String key, File inputFile, File outputFile)
            throws CryptoException {
        doCrypto(Cipher.ENCRYPT_MODE, key, inputFile, outputFile);
    }
    
    public static void decrypt(String key, File inputFile, File outputFile)
            throws CryptoException {
        doCrypto(Cipher.DECRYPT_MODE, key, inputFile, outputFile);
    }
    
    private static void doCrypto(int cipherMode, String key, File inputFile, 
            File outputFile) throws CryptoException {
        try {
            Key secretKey = new SecretKeySpec(
                    truncateOrPadIfNecessary(key).getBytes(), ALGORITHM);
            Cipher cipher = Cipher.getInstance(TRANSFORMATION);
            cipher.init(cipherMode, secretKey);
            
            FileInputStream in = new FileInputStream(inputFile);
            byte[] inputBytes = new byte[(int) inputFile.length()];
            in.read(inputBytes);
            
            byte[] outputBytes = cipher.doFinal(inputBytes);
            
            FileOutputStream out = new FileOutputStream(outputFile);
            out.write(outputBytes);
            
            in.close();
            out.close();
        } catch (NoSuchPaddingException
                | NoSuchAlgorithmException
                | InvalidKeyException
                | BadPaddingException
                | IllegalBlockSizeException
                | IOException ex) {
            throw new CryptoException("Encryption/Decryption Error: "
                    + ex.getMessage(), ex);
        }
    }
    
    private static String truncateOrPadIfNecessary(String string) {
        if (string.length() > 16) {
            string = string.substring(string.length() - 16);
        } else if (string.length() < 16) {
            string = padString(string, 16 - string.length());
        }
        
        return string;
    }
    
    private static String padString(String string, int padding) {
        return string + "a".repeat(padding);
    }

}
