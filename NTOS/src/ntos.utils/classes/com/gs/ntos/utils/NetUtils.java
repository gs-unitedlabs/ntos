/*
 * Copyright (C) 2022 GS United Labs
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * *****************************************************************************
 *  Project    :   NTA_Utils
 *  Class      :   NetUtils.java
 *  Author     :   Sean Carrick
 *  Created    :   Mar 7, 2022 @ 9:35:45 PM
 *  Modified   :   Mar 7, 2022
 *  
 *  Purpose:
 *  
 *  Revision History:
 *  
 *  WHEN          BY                   REASON
 *  ------------  -------------------  ------------------------------------------
 *  Mar 07, 2022  Sean Carrick         Initial creation.
 *  Mar 31, 2022  Sean Carrick         Updated the canPing() method to use a
 *                                     command string variable, instead of the
 *                                     hard-coded "ping -c 5 + siteToPing" 
 *                                     command. This is because I don't know if
 *                                     the Windows version of ping works the
 *                                     same way as ping on Linux and MacOS. I
 *                                     *believe* that the ping command on Windows
 *                                     takes no parameters. However, if we do 
 *                                     not provide the `-c 5` parameter to ping
 *                                     on Linux and MacOS, the `exec()` call will
 *                                     never return, since ping would require a
 *                                     CTRL+C to terminate.
 *  
 *                                     I also commented out the MessageBox
 *                                     statements in the methods, since we don't
 *                                     want those popping up in a production
 *                                     environment for users to see.
 * *****************************************************************************
 */

package com.gs.ntos.utils;

import com.gs.ntos.api.Program;
import com.gs.ntos.support.MessageBox;
import java.io.IOException;
import java.net.URL;
import java.net.URLConnection;

/**
 *
 * @author Sean Carrick &lt;sean at pekinsoft dot com&gt;
 *
 * @version 1.2
 * @since 1.2
 */
public class NetUtils {

    private NetUtils() {
        // Preventing initialization
    }

    /**
     * An improved version of determining if the local computer is connected to
     * the internet. This version simply attempts to connect to a website. If
     * the internet is reachable, it returns `true`, otherwise `false`
     * is returned.
     * <p>
     * For best results, use this method in conjunction with the `canPing`
     * method to be absolutely positive the internet is reachable.</p>
     *
     * @return `true` if the internet is available and reachable;
     *          `false` otherwise.
     *
     * @see #canPing
     */
    public static boolean isNetworkConnectionAvailable() {
        try {
            URL url = new URL("https://duckduckgo.com");
            URLConnection connection = url.openConnection();
            connection.connect();

//            MessageBox.showInfo("Internet connection successful!", "Internet Available");
            return true;
        } catch (IOException ex) {
//            MessageBox.showWarning("Internet connection failed!", "Internet Unavailable");
            return false;
        }
    }

    public static boolean canPing(String siteToPing, Program program) {
        String osName = System.getProperty("os.name");
        String command = "ping -c 5 " + siteToPing;
        
        // I am not sure if the ping command on Windows takes the same parameters
        //+ in the same way as on Linux and MacOS, so I am going to only use the
        //+ command `ping` on Windows, without the parameter for the number of
        //+ pings to send. On Linux and MacOS, if the count paramter is not
        //+ provided, `ping` will run until CTRL+C is pressed, so the exec()
        //+ method would never return if we don't include the count parameter.
        
        if (osName.toLowerCase().contains("win")) {
            command = "ping " + siteToPing;
        }
        
        try {
            Process p = Runtime.getRuntime().exec(command);
            int x = p.waitFor();
            if (x == 0) {
//                MessageBox.showInfo("Ping successful!", "Site Reachable");
                return true;
            } else {
//                MessageBox.showWarning("Ping failed!", "Site Unreachable");
                return false;
            }
        } catch (Exception ex) {
            MessageBox.showError(ex, "Error: " + ex.getClass().getSimpleName(),
                    program);
            return false;
        }
    }
}
