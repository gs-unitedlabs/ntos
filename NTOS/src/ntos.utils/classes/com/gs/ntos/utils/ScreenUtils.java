/*
 * Copyright (C) 2022 GS United Labs
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * *****************************************************************************
 *  Project    :   NTA_Utils
 *  Class      :   ScreenUtils.java
 *  Author     :   Jiří Kovalský
 *  Created    :   Mar 08, 2020
 *  Modified   :   Apr 01, 2022
 *  
 *  Purpose: See class JavaDoc comments.
 *  
 *  Revision History:
 *  
 *  WHEN          BY                  REASON
 *  ------------  ------------------- ------------------------------------------
 *  Mar 08, 2020  Jiří Kovalský        Initial creation.
 *  Nov 13, 2020  Jiří Kovalský       Contributed the getCenterPoint method for
 *                                    centering windows on their parent.
 *  Feb 13, 2021  Sean Carrick        Introduced null container for centering a
 *                                    window on the screen to getCenterPoint.
 *  Apr 01, 2022  Sean Carrick        Added the getCenterPoint(Container,
 *                                    Container) method to allow for centering a
 *                                    child window or dialog on the screen that
 *                                    the parent window is showing. This is used
 *                                    for proper window placement in a multi-
 *                                    display environment.
 *
 *                                    I also added the getCurrentScreenNumber()
 *                                    method to allow a program to determine on
 *                                    which screen number the given window is
 *                                    located. The only parameter is the window's
 *                                    getLocationOnScreen() Point.
 * *****************************************************************************
 */

package com.gs.ntos.utils;

import java.awt.Container;
import java.awt.Dimension;
import java.awt.GraphicsConfiguration;
import java.awt.GraphicsDevice;
import java.awt.GraphicsEnvironment;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.Toolkit;

/**
 * This class provides static utility methods that concentrate on using the 
 * screen (or desktop) effectively. Currently, there are three `public static`
 * methods available:
 * 
 * * `getCenterPoint(Dimension, Dimension)` which centers the child `Dimension`
 *    within the constraints of the parent `Dimension`. This method also allows
 *    for the child `Dimension` to be centered on the screen (without taking 
 *    into account *multi-screen* environments) if the parent supplied is `null`.
 * * `getCenterPoint(Container, Container, boolean)` which centers the child
 *    `Container` on screen or on the parent `Container` if the `boolean` value
 *    is `true`. This method differs from the `getCenterPoint(Dimension, Dimension)`
 *    method in that this method takes into consideration a multi-screen
 *    environment.
 * * `getCurrentScreenNumber(Point)` which returns the screen number for the
 *    screen upon which the window whose `java.awt.Point` is provided. The 
 *    `Point` needs to be the windows `getLocationOnScreen()` return value, from
 *    which the current screen number is able to be determined. This method works
 *    for multi-screen environments that are set up horizontally or vertically,
 *    as well as if there are more than just two screens.
 * 
 * With these methods, applications will be able to use the desktop and the
 * various available screens efficiently.
 * 
 * <dl><dt>Class Information</dt>
 * <dd>This class was originally started by Jiří Kovalský, one of the two 
 * founding partners of GS United Labs. The increases in functionality have been
 * provided by the other founding partner, Sean Carrick. Even so, much credit
 * goes to Jiří for getting this class started, as Sean never even thought of
 * creating a method to center a window at all. He just kept reinventing the 
 * wheel whenever he needed to center a window within a program.</dd></dl>
 *
 * @author Jiří Kovalský &lt;jiri dot kovalsky at centrum dot cz&gt;
 * 
 * @since 1.0
 * @version 1.7
 */
public class ScreenUtils {
    

    /**
     * Calculates central position of the window within its container.
     * <p>If the specified `window` is a top-level window or dialog,
     * `null` can be specified for the provided `container` to
     * center the window or dialog on the screen.</p>
     *
     * <dl>
     * <dt>Contributed By</dt>
     * <dd>Jiří Kovalský &lt;jiri dot kovalsky at centrum dot cz&gt;</dd>
     * <dt>Updated Feb 13, 2021</dt>
     * <dd>Update allows the specified `container` to be set to
     *      `null` to allow centering a top-level window or dialog on the
     *      screen.<br><br>
     *      Sean Carrick &lt;sean at pekinsoft dot com&gt;</dd>
     * </dl>
     *
     * @param container Dimensions of parent container where window will be
     * located. If `null` is supplied, then the `window` will be centered on 
     * the screen.
     * @param window Dimensions of child window which will be displayed within
     * its parent container.
     * @return Location of top left corner of window to be displayed in the
     * center of its parent container.
     */
    public static Point getCenterPoint(Dimension container, Dimension window) {
        if (container != null) {
            int x = container.width / 2;
            int y = container.height / 2;
            x = x - (window.width / 2);
            y = y - (window.height / 2);
            x = x < 0 ? 0 : x;
            y = y < 0 ? 0 : y;
            return new Point(x, y);
        } else {
            int x = Toolkit.getDefaultToolkit().getScreenSize().width / 2;
            int y = Toolkit.getDefaultToolkit().getScreenSize().height / 2;
            x = x - (window.width / 2);
            y = y - (window.width / 2);
            x = x < 0 ? 0 : x;
            y = y < 0 ? 0 : y;
            return new Point(x, y);
        }
    }
    
    /**
     * Calculates central position of the window within its container.
     * 
     * If the specified `child` is a top-level window or dialog,
     * `null` can be specified for the provided `parent` to
     * center the window or dialog on the screen.
     *
     * This version of `getCenterPoint` requires that `java.awt.Container`s be
     * provided so that the `java.awt.GraphicsConfiguration` can be taken into
     * account for systems that are using multiple virtual screens (i.e., using
     * multiple monitors). By taking this into account, the `child` container
     * will be displayed on the same virtual screen as the `parent` container.
     * 
     * <dl><dt>Contributed By</dt>
     * <dd>Sean Carrick &lt;sean at gs-unitedlabs dot com&gt;</dd>
     * <dt>Contributed on April 01, 2022</dt>
     * <dd>I had used <code>getCenterPoint(Dimension, Dimension)</code> to center
     * an Options dialog due to the <code>SessionStorage</code> not workig for
     * an unknown reason (TBD later). The Options dialog did indeed center on
     * the screen, but on the extension monitor on my laptop, when the primary
     * program's main window was on the laptop screen. I felt this should not
     * happen, so created this version of 
     * <code>getCenterPoint(Container, Container)</code> in order to not have
     * this happen. It just seemed to be unprofessional for an application to
     * place its windows and dialogs willy-nilly, without paying attention to
     * which screen the main window is on when displaying child windows.</dd>
     * </dl>
     * 
     * In essence, this method gets the `java.awt.GraphicsDevice` on which the
     * `parent` window is displayed, and then centers the `child` window on that
     * graphics device. 
     * 
     * @param parent the parent (top-level) window or dialog
     * @param child the child (secondary) window or dialog
     * @param centerOnParent `true` to center the `child` over the `parent`;
     *          `false` to center the `child` on the screen
     * @return the `java.awt.Point` at which to place the `child` to have it
     *          properly centered
     * @throws IllegalArgumentException if `parent` or `child` is `null`
     */
    public static Point getCenterPoint(Container parent, Container child, 
            boolean centerOnParent) {
        if (parent == null) {
            throw new IllegalArgumentException("null parent");
        }
        if (child == null) {
            throw new IllegalArgumentException("null child");
        }
        
        if (centerOnParent) {
            return centerOnParent(parent, child);
        } else { 
            return centerOnParentsScreen(parent, child);
        }
    }
    
    /**
     * Determines on which screen number in a multi-screen environment the current
     * window is located. The only required parameter is the current window's
     * `getLocationOnScreen()` `java.awt.Point` value. Using this `Point`, this
     * method will determine upon which screen the window is located, regardless
     * of whether the multi-screen setup is horizontal (left and right), vertical
     * (top and bottom), or if there are more than just two screens.
     * 
     * @param locationOnScreen the `java.util.Point` at which the window is
     *          located
     * @return the current screen number (zero-based)
     * @throws IllegalArgumentException if `locationOnScreen` is `null`
     */
    public static int getCurrentScreenNumber(Point locationOnScreen) {
        if (locationOnScreen == null) {
            throw new IllegalArgumentException("null locationOnScreen");
        } else if (locationOnScreen.x < Toolkit.getDefaultToolkit().getScreenSize().width
                && locationOnScreen.y < Toolkit.getDefaultToolkit().getScreenSize().height) {
            return 0;
        }
        GraphicsEnvironment ge = GraphicsEnvironment.getLocalGraphicsEnvironment();
        GraphicsDevice[] devices = ge.getScreenDevices();
        Rectangle virtualBounds = new Rectangle();
        
        for (int j = 0; j < devices.length; j++) {
            GraphicsDevice gd = devices[j];
            GraphicsConfiguration[] gc = gd.getConfigurations();
            for (int i = 0; i < gc.length; i++) {
                virtualBounds = virtualBounds.union(gc[i].getBounds());
            }
        }
        
        int screenWidth = virtualBounds.width / devices.length;
        int screenHeight = virtualBounds.height / devices.length;
        int screenNumber = 0;
        
        for (int x = 0; x < devices.length; x++) {
            if (locationOnScreen.x > (screenWidth)) {
                screenWidth *= x + 1;
                screenNumber = x;
            } else if (locationOnScreen.y > screenHeight) {
                screenHeight *= x + 1;
                screenNumber = x;
            }
        }
        
        return screenNumber;
    }
    
    private static Point centerOnParent(Container parent, Container child) {
        Rectangle virtualBounds = new Rectangle();
        GraphicsEnvironment ge = GraphicsEnvironment.getLocalGraphicsEnvironment();
        GraphicsDevice[] devices = ge.getScreenDevices();

        for (int j = 0; j < devices.length; j++) {
            GraphicsDevice gd = devices[j];
            GraphicsConfiguration[] gc = gd.getConfigurations();
            for (int i=0; i < gc.length; i++) {
                virtualBounds = virtualBounds.union(gc[i].getBounds());
            }
        }
        
        int locX = (parent.getWidth() - child.getWidth()) / 2;
        locX += parent.getLocationOnScreen().x;
        
        int locY = (parent.getHeight() - child.getHeight()) / 2;
        locY += parent.getLocationOnScreen().y;
                
        Point position = new Point(locX, locY);
        
        return position;
    }
    
    private static Point centerOnParentsScreen(Container parent, Container child) {
        Rectangle virtualBounds = new Rectangle();
        GraphicsEnvironment ge = GraphicsEnvironment.getLocalGraphicsEnvironment();
        GraphicsDevice[] devices = ge.getScreenDevices();
        Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();

        for (int j = 0; j < devices.length; j++) {
            GraphicsDevice gd = devices[j];
            GraphicsConfiguration[] gc = gd.getConfigurations();
            for (int i=0; i < gc.length; i++) {
                virtualBounds = virtualBounds.union(gc[i].getBounds());
            }
        }
        
        boolean right = parent.getLocationOnScreen().x > screenSize.width;
        boolean bottom = parent.getLocationOnScreen().y > screenSize.height;
        
        int shiftX = screenSize.width;
        int shiftY = screenSize.height;
        
        int locX = (screenSize.width - child.getWidth()) / 2;
        int locY = (screenSize.height - child.getHeight()) / 2;
        
        if (right) {
            locX += shiftX;
        }
        if (bottom) {
            locY += shiftY;
        }
                
        Point position = new Point(locX, locY);
        
        return position;
    }

}
