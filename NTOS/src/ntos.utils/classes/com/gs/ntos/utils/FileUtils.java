/*
 * Copyright (C) 2022 GS United Labs
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * *****************************************************************************
 *  Project    :   NTA_Utils
 *  Class      :   FileUtils.java
 *  Author     :   Sean Carrick
 *  Created    :   Mar 7, 2022 @ 9:31:56 PM
 *  Modified   :   Mar 7, 2022
 *  
 *  Purpose:
 *  
 *  Revision History:
 *  
 *  WHEN         BY                   REASON
 *  -----------  -------------------  ------------------------------------------
 *  Mar 7, 2022  Sean Carrick         Initial creation.
 * *****************************************************************************
 */

package com.gs.ntos.utils;

import com.gs.ntos.api.Program;
import com.gs.ntos.errors.LSException;
import java.awt.Rectangle;
import java.beans.DefaultPersistenceDelegate;
import java.beans.Encoder;
import java.beans.ExceptionListener;
import java.beans.Expression;
import java.beans.XMLDecoder;
import java.beans.XMLEncoder;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.JarURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.util.Enumeration;
import java.util.Map;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;

/**
 * The `FileUtils` class provide numerous static methods for manipulating
 * physical files.
 *
 * @author Sean Carrick &lt;sean at gs-unitedlabs dot com&gt;
 *
 * @version 1.0
 * @since 1.0
 */
public class FileUtils {
    
    private static ProgramInfo program = new ProgramInfo();
    
    //<editor-fold defaultstate="collapsed" desc="Constructor(s)">
    private FileUtils () {
        // Privatized to prevent this class from being instantiated.
    }
    //</editor-fold>

    /**
     * Copies the file } toCopy} to the destination file } destFile}.
     * 
     * @param toCopy the file to copy
     * @param destFile the new location of the copied file
     * @return `true` upon success; `false` upon failure
     */
    public static boolean copyFile(final File toCopy, final File destFile) {
        try {
            return FileUtils.copyStream(new FileInputStream(toCopy), 
                                                new FileOutputStream(destFile));
        } catch (final FileNotFoundException e) {
            e.printStackTrace();
        }
        
        return false;
    }
    
    /**
     * Copies all files in the } sourceDir} to the destination } 
     * destDir}. 
     * 
     * @param sourceDir the folder that contains the files to copy
     * @param destDir the destination folder for the copied files
     * @return `true` upon success; `false` upon failure
     */
    private static boolean copyFilesRecursively(final File sourceDir, 
                                                final File destDir) {
        assert destDir.isDirectory();
        
        if ( !sourceDir.isDirectory() ) {
            return FileUtils.copyFile(sourceDir, new File(destDir, sourceDir.getName()));
        } else {
            final File newDestDir = new File(destDir, sourceDir.getName());
            
            if ( !newDestDir.exists() && !newDestDir.mkdir() ) {
                return false;
            }
            
            for ( final File child : sourceDir.listFiles() ) {
                if ( !FileUtils.copyFilesRecursively(child, newDestDir)) {
                    return false;
                }
            }
        }
        
        return true;
    }
    
    public static boolean copyJarResourcesRecursively(final File destDir,
            final JarURLConnection jarConnection) throws IOException {
        final JarFile jarFile = jarConnection.getJarFile();
        
        for (final Enumeration<JarEntry> e = jarFile.entries(); 
                                                        e.hasMoreElements();) {
            final JarEntry entry = e.nextElement();
            
            if (entry.getName().startsWith(jarConnection.getEntryName())) {
                final String filename = StringUtils.removeStart(entry.getName(),
                        jarConnection.getEntryName());
                
                final File f = new File(destDir, filename);
                
                if (!entry.isDirectory()) {
                    final InputStream entryInputStream = jarFile.getInputStream(entry);
                    
                    if (!FileUtils.copyStream(entryInputStream, f)) {
                        return false;
                    }
                    
                    entryInputStream.close();
                } else {
                    if ( !FileUtils.ensureDirectoryExists(f)) {
                        throw new IOException("Could not create directory: " +
                                f.getAbsolutePath());
                    }
                }
            }
        }
        
        return true;
    }
    
    public static boolean copyResourcesRecursively(final URL originUrl,
                                                   final File destination) {
        try {
            final URLConnection urlConnection = originUrl.openConnection();
            
            if ( urlConnection instanceof JarURLConnection) {
                return FileUtils.copyJarResourcesRecursively(destination, 
                        (JarURLConnection) urlConnection);
            } else {
                return FileUtils.copyFilesRecursively(new File(
                                                      originUrl.getPath()), 
                                                      destination);
            }
        } catch ( final IOException e) {
            e.printStackTrace();
        }
        
        return false;
    }
    
    private static boolean copyStream(final InputStream is, final File f) {
        try {
            return FileUtils.copyStream(is, new FileOutputStream(f));
        } catch ( final FileNotFoundException e ) {
            e.printStackTrace();
        }
        
        return false;
    }
    
    private static boolean copyStream(final InputStream is,
                                      final OutputStream os) {
        try {
            final byte[] buf = new byte[1024];
            
            int len = 0;
            while ( (len = is.read(buf)) > 0 ) {
                os.write(buf);
            }
            
            is.close();
            os.close();
            return true;
        } catch ( final IOException e ) {
            e.printStackTrace();
        }
        
        return false;
    }
    
    /**
     * Provides a means of determining if the given directory exists, and is not
     * a regular file. If } f} is a regular file, `false` is returned.
     * If } f} does not exist, the directory is created. Whether or not the
     * directory is created, `true` is returned if the directory exists.
     * 
     * @param f the file to verify, or create
     * @return `false` if the file is a regular file; `true` if the 
     * directory did not exist, but was created, or already exists; `false`
     * otherwise
     */
    public static boolean ensureDirectoryExists(final File f) {
        if (!f.isDirectory()) {
            return false;
        } else if (f.isDirectory()) {
            if (!f.exists()) {
                return f.mkdirs();
            } else {
                return true;
            }
        }
        
        return false ;
    }
    
    /**
     * Gets the OS-specific location for the home folder of a given application.
     * This method makes sure to follow the "rules" of the operating system as
     * to where the application stores its files.
     * <p>
     * Applications can make whatever folder structure needed on the user's PC
     * hard drive. However, operating systems tend to desire that applications
     * do not make folders willy-nilly on the hard drive. Therefore, each 
     * operating system vendor has determined locations for third-party 
     * application developers to store their application's files. The OS-specific
     * locations are detailed in the table below.
     *  This directory is created if it does not exist.</p>
     * <table>
     * <caption>OS-Specific Application Top-Level Folder Locations</caption>
     * <tr>
     * <th>OS</th><th>Application Folder Location</th></tr>
     * <tr><td>Microsoft Windows<sup>&trade;</sup></td>
     * <td>} ${user.home}\\AppData\\Roaming\\${application.vendor}\\${application.name}\\}</td></tr>
     * <tr><td>Apple Mac OS-X<sup>&trade;</sup></td>
     * <td>} ${user.home}/Library/Application Data/${application.name}/}</td></tr>
     * <tr><td>Linux and Solaris</td>
     * <td>} ${user.home}/.${application.name}/}</td></tr>
     * </table>
     * <p>
     * In order to follow these guidelines, this method has been created. It 
     * checks the system upon which the Java Virtual Machine (JVM) is running to
     * get its name, then builds the application folder path for that OS in a
     * `java.lang.String` and returns it to the calling class.</p>
     * 
     * @return the full path to the application's OS-specific folder location as
     *          a } java.io.File} object
     */
    public static File getApplicationDirectory() {
        String osName = System.getProperty("os.name").toLowerCase();
        String userHome = System.getProperty("user.home");
        String location = "";
        File appHome = null;

        if (osName.contains("win")) {
            location = System.getenv("APPDATA") + File.separator;
            appHome = new File(location + program.getVendorId() + File.separator
                    + program.getProgramId() + File.separator);
        } else if (osName.contains("mac")) {
            location = "Library" + File.separator + "Preferences" + File.separator;
            appHome = new File(userHome + File.separator + location + program.getProgramId() + File.separator);
        } else {
            location = userHome + File.separator + "." + program.getProgramId();
            appHome = new File(location + File.separator);
        }
        
        if (!appHome.exists()) {
            appHome.mkdirs();
        }
        
        return appHome;
    }
    
    /**
     * A convenience method for getting a standard data directory in which to
     * store files from an application.
     * <p>
     * For applications that wish to store their data in a separate directory
     * from any other files the application may create, this method will get a
     * directory named "data", with the application's home directory prepended
     * to it. The path returned is described in the table below, by OS.
     *  This directory is created if it does not exist.</p>
     * <table>
     * <caption>OS-Specific Application Data Folder Locations</caption>
     * <tr>
     * <th>OS</th><th>Application Data Folder Location</th></tr>
     * <tr><td>Microsoft Windows<sup>&trade;</sup></td>
     * <td>} ${user.home}\\AppData\\Roaming\\${application.vendor}\\${application.name}\\data\\}</td></tr>
     * <tr><td>Apple Mac OS-X<sup>&trade;</sup></td>
     * <td>} ${user.home}/Library/Application Data/${application.name}/data/}
     * </td></tr>
     * <tr><td>Linux and Solaris</td>
     * <td>} ${user.home}/.${application.name}/data/}</td></tr>
     * </table>
     * 
     * @return the full path to the application's OS-specific folder location as
     *          a } java.io.File} object
     */
    public static File getApplicationDataDirectory() {
        File dataDir = new File(getApplicationDirectory() + File.separator
                + "data" + File.separator);
        
        if (!dataDir.exists()) {
            dataDir.mkdirs();
        }
        
        return dataDir;
    }
    
    /**
     * A convenience method for getting a standard configuration file directory
     * in which applications settings and configurations may be stored.
     * <p>
     * For applications that wish to store their config files in a separate directory
     * from any other files the application may create, this method will get a
     * directory named "etc", with the application's home directory prepended
     * to it. The path returned is described in the table below, by OS.
     *  This directory is created if it does not exist.</p>
     * <table>
     * <caption>OS-Specific Application Configuration Folder Locations</caption>
     * <tr>
     * <th>OS</th><th>Application Configuration Folder Location</th></tr>
     * <tr><td>Microsoft Windows<sup>&trade;</sup></td>
     * <td>} ${user.home}\\AppData\\Roaming\\${application.vendor}\\${application.name}\\etc\\}</td></tr>
     * <tr><td>Apple Mac OS-X<sup>&trade;</sup></td>
     * <td>} ${user.home}/Library/Preferences/${application.name}/}</td></tr>
     * <tr><td>Linux and Solaris</td>
     * <td>} ${user.home}/.${application.name}/etc/}</td></tr>
     * </table>
     * 
     * @return the full path to the application's OS-specific folder location as
     *          a } java.io.File} object
     */
    public static File getApplicationConfigDirectory() {
        File config = new File(getApplicationDirectory() + File.separator
                + "etc" + File.separator);
        
        if (!config.exists()) {
            config.mkdirs();
        }
        
        return config;
    }
    
    /**
     * A convenience method for getting a standard log file directory in which
     * applications may store log files.
     * <p>
     * For applications that wish to store their log files in a separate directory
     * from any other files the application may create, this method will get a
     * directory named "var/log" for Linux applications, with the application's 
     * home directory prepended to it. However, other operating systems have very
     * specific locations where they want log files created. The path returned 
     * for each OS is described in the table below, by OS.
     *  This directory is created if it does not exist.</p>
     * <table>
     * <caption>OS-Specific Application Log Folder Locations</caption>
     * <tr>
     * <th>OS</th><th>Application Log Folder Location</th></tr>
     * <tr><td>Microsoft Windows<sup>&trade;</sup></td>
     * <td>} %SystemRoot%\\System32\\Config\\${application.name}\\}</td></tr>
     * <tr><td>Apple Mac OS-X<sup>&trade;</sup></td>
     * <td>} ${user.home}/Library/Logs/${application.name}/}</td></tr>
     * <tr><td>Linux and Solaris</td>
     * <td>} /var/log/${application.name}} or 
     * } ${user.home}/.${application.name}}, if the system logs directory 
     * is not writable. Typically, an application that is installed system wide
     * will have write access to the } /var/log/${application.name}} folder.
     * If a user installs the application just for his/herself, then the logs
     * will be stored in the } ${user.home}/.${application.name}/var/log/}
     * folder.</td></tr>
     * </table>
     * 
     * @return the full path to the application's OS-specific folder location as
     *          a } java.io.File} object
     */
    public static File getApplicationLogDirectory() {
        String logDir = System.getProperty("system.root") + File.separator;
        
        if (System.getProperty("os.name").toLowerCase().contains("win")) {
            logDir = System.getenv("SystemRoot") + File.separator + "System32"
                    + File.separator + "Config" + File.separator + program.getProgramId()
                    + File.separator;
        } else if (System.getProperty("os.name").toLowerCase().contains("mac")) {
            logDir = System.getProperty("user.home") + File.separator + "Library"
                    + File.separator + "Logs" + File.separator + program.getProgramId()
                    + File.separator;
        } else {
            // Attempt the /var/log folder on Linux and Solaris first.
            logDir += "var" + File.separator + "log" + File.separator;
            
            // Check to see if we can write to it.
            File logDirFile = new File(logDir);
            if (!logDirFile.canWrite()) {
                logDir = getApplicationDirectory() + File.separator
                        + "var" + File.separator + "log" + File.separator;
            }
        }
        
        File logs = new File(logDir);
        
        if (!logs.exists()) {
            logs.mkdirs();
        }
        
        return logs;
    }
    
    /**
     * A convenience method for getting a standard error log file directory in 
     * which applications may store error log files.
     * <p>
     * For applications that wish to store their error log files in a separate 
     * directory from any other files the application may create, this method 
     * will get a directory named "var/err" for Linux applications, with the 
     * application's home directory prepended to it. However, other operating 
     * systems have very specific locations where they want log files created.
     * The path returned for each OS is described in the table below, by OS.
     *  This directory is created if it does not exist.</p>
     * <table>
     * <caption>OS-Specific Application Error Log Folder Locations</caption>
     * <tr>
     * <th>OS</th><th>Application Error Log Folder Location</th></tr>
     * <tr><td>Microsoft Windows<sup>&trade;</sup></td>
     * <td>} %SystemRoot%\\System32\\Config\\${application.name}\\err\\}</td></tr>
     * <tr><td>Apple Mac OS-X<sup>&trade;</sup></td>
     * <td>} ${user.home}/Library/Logs/${application.name}/err/}</td></tr>
     * <tr><td>Linux and Solaris</td>
     * <td>} /var/log/${application.name}/err/} or 
     * } ${user.home}/.${application.name}/var/err/}, if the system logs directory 
     * is not writable. Typically, an application that is installed system wide
     * will have write access to the } /var/log/${application.name}} folder.
     * If a user installs the application just for his/herself, then the logs
     * will be stored in the } ${user.home}/.${application.name}/var/log/}
     * folder.</td></tr>
     * </table>
     * 
     * @return the full path to the application's OS-specific folder location as
     *          a } java.io.File} object
     */
    public static File getApplicationErrorDirectory() {
        File err = new File(getApplicationLogDirectory() + File.separator
                + "err" + File.separator);
        
        if (!err.exists()) {
            err.mkdirs();
        }
        
        return err;
    }
    
    /**
     * A convenience method for getting a standard system file directory in 
     * which applications may store system files.
     * <p>
     * For applications that wish to store their system files in a separate 
     * directory from any other files the application may create, this method 
     * will get a directory named "sys", with the application's home directory 
     * prepended to it. This directory is created if it does not exist.</p>
     * <table>
     * <caption>OS-Specific Application System Folder Locations</caption>
     * <tr>
     * <th>OS</th><th>Application System Folder Location</th></tr>
     * <tr><td>Microsoft Windows<sup>&trade;</sup></td>
     * <td>} ${user.home}\\AppData\\Roaming\\${application.vendor}\\${application.name}\\sys\\}</td></tr>
     * <tr><td>Apple Mac OS-X<sup>&trade;</sup></td>
     * <td>} ${user.home}/Library/Application Data/${application.name}/sys/}</td></tr>
     * <tr><td>Linux and Solaris</td>
     * <td>} ${user.home}/.${application.name}/sys/}</td></tr>
     * </table>
     * 
     * @return the full path to the application's OS-specific folder location as
     *          a } java.io.File} object
     */
    public static File getApplicationSystemDirectory() {
        File sys = new File(getApplicationDirectory() + File.separator 
                + "sys" + File.separator);
        
        if (!sys.exists()) {
            sys.mkdirs();
        }
        
        return sys;
    }
    
    private static boolean persistenceDelegatesInitialized = false;
    public static void save(Program program, Map<String, Object> map, 
            String filename) throws IOException {
        AbortExceptionListener el = new AbortExceptionListener();
        XMLEncoder e = null;
        
        /* Buffer the XMLEncoder's output so that decoding errors doesn't cause
         * us to trash the current version of the specified path.
         */
        ByteArrayOutputStream bst = new ByteArrayOutputStream();
        
        try {
            e = new XMLEncoder(bst);
            if (!persistenceDelegatesInitialized) {
                e.setPersistenceDelegate(Rectangle.class, new RectanglePD());
                persistenceDelegatesInitialized = true;
            }
            
            e.setExceptionListener(el);
            e.writeObject(map);
        } finally {
            if (e != null) { e.close(); }
        }
        
        if (el.exception != null) {
            throw new LSException("save failed \"" + filename + "\"", el.exception);
        }
        
        OutputStream ost = null;
        
        try {  
            ost = openOutputFile(filename);
            ost.write(bst.toByteArray());
        } finally {
            if (ost != null) { ost.close(); }
        }
    }
    
    public static Object load(Program program, String filename)
            throws IOException {
        InputStream ist = null;
        try {
            ist = openInputFile(filename);
        } catch (IOException e) {
            return null;
        }
        AbortExceptionListener el = new AbortExceptionListener();
        try (XMLDecoder d = new XMLDecoder(ist)) {
            d.setExceptionListener(el);
            Object bean = d.readObject();
            if (el.exception != null) {
                throw new LSException("load failed \"" + filename + "\"", el.exception);
            }
            return bean;
        }
    }
    
    private static InputStream openInputFile(String filename) throws IOException {
        File path = new File(getApplicationConfigDirectory(), filename);
        try {
            return new BufferedInputStream(new FileInputStream(path));
        } catch (IOException ex) {
            throw new LSException("couldn't open input file \"" + filename + "\"", ex);
        }
    }
    
    private static OutputStream openOutputFile(String filename) throws IOException {
        File dir = getApplicationConfigDirectory();
        
        if (!dir.isDirectory()) {
            if (!dir.mkdirs()) {
                throw new LSException("couldn't create directory " + dir);
            }
        }
        
        File path = new File(dir, filename);
        try {
            return new BufferedOutputStream(new FileOutputStream(path));
        } catch (IOException ex) {
            throw new LSException("couldn't open output file \"" + filename + "\"", ex);
        }
    }
    
    /**
     * Calculates the total size of a directory on disk. This method returns the
     * size of the directory in <em>terabytes</em>.
     * <dl><dt><em>Developer's Note</em>:</dt>
     * <dd>We included this method just for giggles, looking at the way disk
     * sizes and software distribution sizes are going. Also, we believed that
     * this would allow us to be smart alecks and be able to show a really small
     * size for our software...we will just be putting the &quot;TB&quot; very
     * small...</dd></dl>
     * 
     * @param folder the folder whose size should be calculated
     * @return the size of the folder and its contents on disk in terabytes
     * 
     * @see #getFolderSizeInBytes(java.io.File) 
     * @see #getFolderSizeInKilobytes(java.io.File) 
     * @see #getFolderSizeInMegabytes(java.io.File) 
     * @see #getFolderSizeInGigabytes(java.io.File) 
     */
    public static long getFolderSizeInTerabytes(File folder) {
        return getFolderSizeInGigabytes(folder) / 1024;
    }
    
    /**
     * Calculates the total size of a directory on disk. This method returns the
     * size of the directory in <em>gigabytes</em>.
     * 
     * @param folder the folder whose size should be calculated
     * @return  the size of the folder and its contents in gigabytes
     * 
     * @see #getFolderSizeInBytes(java.io.File) 
     * @see #getFolderSizeInKilobytes(java.io.File) 
     * @see #getFolderSizeInMegabytes(java.io.File) 
     * @see #getFolderSizeInTerabytes(java.io.File) 
     */
    public static long getFolderSizeInGigabytes(File folder) {
        return getFolderSizeInMegabytes(folder) / 1024;
    }
    
    /**
     * Calculates the total size of a directory on disk. This method returns the
     * size of the directory in <em>megabytes</em>.
     * 
     * @param folder the folder whose size should be calculated
     * @return the size of the folder and its contents in megabytes
     * 
     * @see #getFolderSizeInBytes(java.io.File) 
     * @see #getFolderSizeInKilobytes(java.io.File) 
     * @see #getFolderSizeInGigabytes(java.io.File) 
     * @see #getFolderSizeInTerabytes(java.io.File) 
     */
    public static long getFolderSizeInMegabytes(File folder) {
        return getFolderSizeInKilobytes(folder) / 1024;
    }
    
    /**
     * Calculates the total size of a directory on disk. This method returns the
     * size of the directory on disk in <em>kilobytes</em>.
     * 
     * @param folder the folder whose size should be calculated
     * @return the size of the folder and its contents in kilobytes
     * 
     * @see #getFolderSizeInBytes(java.io.File) 
     * @see #getFolderSizeInMegabytes(java.io.File) 
     * @see #getFolderSizeInGigabytes(java.io.File) 
     * @see #getFolderSizeInTerabytes(java.io.File) 
     */
    public static long getFolderSizeInKilobytes(File folder) {
        return getFolderSizeInBytes(folder) / 1024;
    }
    
    /**
     * Calculates the total size of a directory on disk. This method returns the
     * size of the folder on disk in <em>bytes</em>.
     * 
     * @param folder the folder whose size should be calculated
     * @return the size of the folder and its contents in bytes
     * 
     * @see #getFolderSizeInKilobytes(java.io.File) 
     * @see #getFolderSizeInMegabytes(java.io.File) 
     * @see #getFolderSizeInGigabytes(java.io.File) 
     * @see #getFolderSizeInTerabytes(java.io.File) 
     */
    public static long getFolderSizeInBytes(File folder) {
        long length = 0L;
        
        File[] files = folder.listFiles();
        
        for (File f : files) {
            if (f.isFile()) {
                length += getFileSizeInBytes(f);
            } else {
                length += getFolderSizeInBytes(f);
            }
        }
        
        return length;
    }
    
    /**
     * Calculates the size of a file on disk.
     * 
     * @param file the file whose size should be calculated
     * @return the file's size in bytes
     */
    public static long getFileSizeInBytes(File file) {
        return file.length();
    }
    
    /* There are some (old) Java classes that aren't proper beans.  Rectangle
     * is one of these.  When running within the secure sandbox, writing a 
     * Rectangle with XMLEncoder causes a security exception because 
     * DefaultPersistenceDelegate calls Field.setAccessible(true) to gain
     * access to private fields.  This is a workaround for that problem.
     * A bug has been filed, see JDK bug ID 4741757  
     */
    private static class RectanglePD extends DefaultPersistenceDelegate {
	public RectanglePD() {
	    super(new String[]{"x", "y", "width", "height"});
	}
        @Override
	protected Expression instantiate(Object oldInstance, Encoder out) {
	    Rectangle oldR = (Rectangle)oldInstance;
	    Object[] constructorArgs = new Object[]{
		oldR.x, oldR.y, oldR.width, oldR.height
	    };
	    return new Expression(oldInstance, oldInstance.getClass(), "new", constructorArgs);
	}
    }

    /* If an exception occurs in the XMLEncoder/Decoder, we want
     * to throw an IOException.  The exceptionThrow listener method
     * doesn't throw a checked exception so we just set a flag
     * here and check it when the encode/decode operation finishes
     */
    private static class AbortExceptionListener implements ExceptionListener {
	public Exception exception = null;
        @Override
	public void exceptionThrown(Exception e) {
	    if (exception == null) { exception = e; }
	}
    }
    
    private static class ProgramInfo {
        
        private String programId;
        private String programName;
        private String programTitle;
        private String vendorId;
        private String vendorName;
        
        public ProgramInfo() {
            programId = "ntos";
            programName = "NTOS";
            programTitle = "Northwind Truckers' Operating System" + " (" 
                    + programId + ")";
            vendorId = "GS";
            vendorName = "GS United Labs";
        }

        public String getProgramId() {
            return programId;
        }

        public String getProgramName() {
            return programName;
        }

        public String getProgramTitle() {
            return programTitle;
        }

        public String getVendorId() {
            return vendorId;
        }

        public String getVendorName() {
            return vendorName;
        }
        
    }

}
