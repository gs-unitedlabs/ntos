/*
 * Copyright (C) 2022 GS United Labs
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * *****************************************************************************
 *  Project    :   ntos.utils
 *  Class      :   DateUtils.java
 *  Author     :   Sean Carrick
 *  Created    :   Mar 9, 2022 @ 12:54:07 PM
 *  Modified   :   Mar 9, 2022
 *  
 *  Purpose:
 *  
 *  Revision History:
 *  
 *  WHEN         BY                   REASON
 *  -----------  -------------------  ------------------------------------------
 *  Mar 9, 2022  Sean Carrick         Initial creation.
 * *****************************************************************************
 */

package com.gs.ntos.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 *
 * @author Sean Carrick &lt;sean at gs-unitedlabs dot com&gt;
 * 
 * @version 0.1.0
 * @since 0.1.0
 */
public class DateUtils {
    
    private DateUtils () {}
    
    public static long calculateDifferenceInMillis(String ago, String later) {
        SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
        long millis = 0L;
        
        try {
            Date earlier = sdf.parse(ago);
            Date laterOn = sdf.parse(later);
            
            millis = laterOn.getTime() - earlier.getTime();
        } catch (ParseException ex) {
            System.err.println(ex + "\n\n" + String.format("Unable to parse the "
                    + "dates represented by ago=%s and later=%s", ago, later));
        }
        
        return millis;
    }
    
    public static long calculateDifferenceInSeconds(String ago, String later) {
        return (calculateDifferenceInMillis(ago, later) / 1000);
    }
    
    public static float calculateDifferenceInMinutes(String ago, String later) {
        return (calculateDifferenceInSeconds(ago, later) / 60);
    }
    
    public static float calculateDifferenceInHours(String ago, String later) {
        return (calculateDifferenceInMinutes(ago, later) / 60);
    }
    
    public static float calculateDifferenceInDays(String ago, String later) {
        return (calculateDifferenceInHours(ago, later) / 24);
    }
    
    public static float calculateDifferenceInYears(String ago, String later) {
        return calculateDifferenceInDays(ago, later) / 365;
    }

}
