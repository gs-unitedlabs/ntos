/*
 * Copyright (C) 2022 GS United Labs
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * *****************************************************************************
 *  Project    :   NTA_Utils
 *  Class      :   Sequencer.java
 *  Author     :   Sean Carrick
 *  Created    :   Mar 8, 2022 @ 9:10:10 AM
 *  Modified   :   Mar 8, 2022
 *  
 *  Purpose:
 *  
 *  Revision History:
 *  
 *  WHEN         BY                   REASON
 *  -----------  -------------------  ------------------------------------------
 *  Mar 8, 2022  Sean Carrick         Initial creation.
 * *****************************************************************************
 */

package com.gs.ntos.support;

/**
 * The `Sequencer` class is a package-private utility class for the `GSLogRecord`
 * interface's default `getSequenceNumber` method.
 *
 * @author Sean Carrick &lt;sean at gs-unitedlabs dot com&gt;
 * 
 * @version 1.0
 * @since 1.0
 */
public class Sequencer {
    
    private static long sequence = 0;

    private Sequencer() {
        
    }
    
    /**
     * Retrieves the next available value in the sequence. This method will only
     * return positive values greater than zero (`0`), i.e., `1L`, `2L`, etc.
     * 
     * @return the next available sequence value
     */
    public static long getNextSequence() {
        return ++sequence;
    }

}
