/*
 * Copyright (C) 2022 GS United Labs
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * *****************************************************************************
 *  Project    :   NTA_Utils
 *  Class      :   EmailValidator.java
 *  Author     :   Sean Carrick
 *  Created    :   Mar 7, 2022 @ 9:30:48 PM
 *  Modified   :   Mar 7, 2022
 *  
 *  Purpose:
 *  
 *  Revision History:
 *  
 *  WHEN         BY                   REASON
 *  -----------  -------------------  ------------------------------------------
 *  Mar 7, 2022  Sean Carrick         Initial creation.
 * *****************************************************************************
 */

package com.gs.ntos.validators;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 *
 * @author Sean Carrick &lt;sean at gs-unitedlabs dot com&gt;
 *
 * @version 1.0.0
 * @since 1.0.0
 */
public class PostalCodeValidator {
    
    private final String canadian;
    private final String us;

    public PostalCodeValidator () {
        canadian  = "^(?!.*[DFIOQU])[A-VXY][0-9][A-Z] ?[0-9][A-Z][0-9]$";
        us = "^[0-9]{5}(?:-[0-9]{4})?$";
    }

    public boolean validate(String input) {
        if (input == null || input.isBlank() || input.isEmpty()) {
            return false;
        }
        boolean canadianMatches = false;
        boolean usMatches = false;
        
        Pattern canadianPattern = Pattern.compile(canadian);
        Pattern usPattern = Pattern.compile(us);
        Matcher canadianMatcher = canadianPattern.matcher(input);
        Matcher usMatcher = usPattern.matcher(input);

        canadianMatches = canadianMatcher.matches();
        usMatches = usMatcher.matches();
        
        return canadianMatches || usMatches;
    }

}
