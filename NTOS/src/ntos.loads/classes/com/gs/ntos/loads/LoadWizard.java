/*
 * Copyright (C) 2022 GS United Labs
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * *****************************************************************************
 *  Project    :   NTOS
 *  Class      :   LoadWizard.java
 *  Author     :   Sean Carrick
 *  Created    :   Apr 3, 2022
 *  Modified   :   Apr 3, 2022
 *  
 *  Purpose: See class JavaDoc for explanation
 *  
 *  Revision History:
 *  
 *  WHEN          BY                   REASON
 *  ------------  -------------------  -----------------------------------------
 *  Apr 3, 2022  Sean Carrick         Initial creation.
 * *****************************************************************************
 */

package com.gs.ntos.loads;

import com.gs.ntos.api.DesktopProgram;
import com.gs.ntos.api.ExitListener;
import com.gs.ntos.enums.Level;
import com.gs.ntos.logging.Logger;
import com.gs.ntos.support.MessageBox;
import com.gs.ntos.verifiers.NotEmptyInputVerifier;
import java.awt.CardLayout;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.RenderingHints;
import java.awt.event.ActionEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.IOException;
import java.util.EventObject;
import java.util.HashMap;
import java.util.Map;
import java.util.ResourceBundle;
import javax.imageio.ImageIO;
import javax.swing.AbstractAction;
import javax.swing.ImageIcon;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;

/**
 *
 * @author Sean Carrick
 */
public class LoadWizard extends javax.swing.JDialog 
        implements ExitListener, PropertyChangeListener, DocumentListener {
    
    private final LoadWizard instance;
    private final Logger logger;
    private final ResourceBundle bundle;
    private final DesktopProgram program;
    private final Map<String, Object> settings;
    
    private final Color required;
    private final Color partlyRequired;
    private final Color accepted;
    
    private final String[] steps;
    private int currentStep;
    
    private boolean backAvailable;
    private boolean nextAvailable;
    private boolean finishAvailable;
    
    /** Creates new form LoadWizard */
    public LoadWizard(DesktopProgram program) {
        super(program.getMainFrame());
        
        logger = new Logger(program, getClass().getSimpleName(), (Level)
                program.getProperties().getSystemProperty("logging.level", 
                Level.ALL));
        this.program = program;
        bundle = ResourceBundle.getBundle("com/gs/ntos/loads/resources/Bundle");
        settings = new HashMap<>();
        
        back = new BackAction();
        cancel = new CancelAction();
        finish = new FinishAction();
        help = new HelpAction();
        next = new NextAction();
        adapter = new LoadWizardAdapter();
        
        required = new Color(1.0f, 0.8f, 0.8f);
        partlyRequired = new Color(1.0f, 0.878f, 0.698f);
        accepted = new Color(0.8f, 0.922f, 0.839f);
        
        steps = new String[] {
            "Welcome to the Load Wizard",
            "General Load Information",
            "Broker/Agent Information"
        };
                
        initComponents();
        
        currentStep = 0;
        changeStep();
        
        instance = this;
    }
    
    public boolean isBackAvailable() {
        return backAvailable;
    }
    
    public void setBackAvailable(boolean backAvailable) {
        boolean oldValue = isBackAvailable();
        this.backAvailable = backAvailable;
        
        program.firePropertyChange("backAvailable", oldValue, isBackAvailable());
    }
    
    public boolean isNextAvailable() {
        return nextAvailable;
    }
    
    public void setNextAvailable(boolean nextAvailable) {
        boolean oldValue = isNextAvailable();
        this.nextAvailable = nextAvailable;
        
        program.firePropertyChange("nextAvailable", oldValue, isNextAvailable());
    }
    
    public boolean canFinish() {
        return finishAvailable;
    }
    
    public void setCanFinish(boolean finishAvailable) {
        boolean oldValue = canFinish();
        this.finishAvailable = finishAvailable;
        
        program.firePropertyChange("finishAvailable", oldValue, canFinish());
    }

    @Override
    public boolean canExit(EventObject event) {
        return true;
    }

    @Override
    public void willExit(EventObject event) {
        adapter.windowClosing(new WindowEvent(this, WindowEvent.WINDOW_CLOSING));
    }

    @Override
    public void propertyChange(PropertyChangeEvent evt) {
        switch (evt.getPropertyName()) {
            case "backAvailable":
                back.setEnabled((Boolean) evt.getNewValue());
                break;
            case "nextAvailable":
                next.setEnabled((Boolean) evt.getNewValue());
                break;
            case "finishAvailable":
                finish.setEnabled((Boolean) evt.getNewValue());
                break;
        }
    }

    @Override
    public void insertUpdate(DocumentEvent e) {
        changedUpdate(e);
    }

    @Override
    public void removeUpdate(DocumentEvent e) {
        changedUpdate(e);
    }

    @Override
    public void changedUpdate(DocumentEvent e) {
        switch (e.getDocument().getProperty("owner").toString()) {
            case "orderNumber":
                if (orderNumberField.getText().length() > 0) {
                    orderNumberField.setBackground(accepted);
                    settings.put("orderNumber", orderNumberField.getText());
                } else {
                    orderNumberField.setBackground(required);
                    if (settings.get("orderNumber") != null) {
                        settings.remove("orderNumber");
                    }
                }
                orderNumberField.select(orderNumberField.getText().length(), 
                        orderNumberField.getText().length());
                break;
            case "tripNumber":
                if (tripNumberField.getText().length() > 0) {
                    tripNumberField.setBackground(accepted);
                    settings.put("tripNumber", tripNumberField.getText());
                } else {
                    tripNumberField.setBackground(required);
                    if (settings.get("tripNumber") != null) {
                        settings.remove("tripNumber");
                    }
                }
                tripNumberField.select(orderNumberField.getText().length(), 
                        tripNumberField.getText().length());
                break;
            case "revenue":
                if (((Double) revenueField.getValue()) > 0) {
                    revenueField.setBackground(accepted);
                    settings.put("revenue", (Double) revenueField.getValue());
                } else {
                    revenueField.setBackground(required);
                    if (settings.get("revenue") != null) {
                        settings.remove("revenue");
                    }
                }
//                revenueField.select(0, 0); /*orderNumberField.getText().length(), 
//                        revenueField.getText().length());*/
                break;
            case "mileage":
                if (mileageField.getText().length() > 0) {
                    mileageField.setBackground(accepted);
                    settings.put("mileage", (Integer) mileageField.getValue());
                } else {
                    mileageField.setBackground(required);
                    if (settings.get("mileage") != null) {
                        settings.remove("mileage");
                    }
                }
                mileageField.select(0, 0); /*orderNumberField.getText().length(), 
                        mileageField.getText().length());*/
                break;
        }
        
        
    }

    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        sidebarPanel = new SidebarPanel();
        bottomPanel = new javax.swing.JPanel();
        navigationSeparator = new javax.swing.JSeparator();
        commandPanel = new javax.swing.JPanel();
        helpButton = new javax.swing.JButton();
        backButton = new javax.swing.JButton();
        nextButton = new javax.swing.JButton();
        finishButton = new javax.swing.JButton();
        cancelButton = new javax.swing.JButton();
        tipLabel = new javax.swing.JLabel();
        wizardPanel = new javax.swing.JPanel();
        welcomePanel = new javax.swing.JPanel();
        welcomeLabel = new javax.swing.JLabel();
        loadPanel = new javax.swing.JPanel();
        generalInfoPanel = new javax.swing.JPanel();
        orderNumberLabel = new javax.swing.JLabel();
        orderNumberField = new javax.swing.JTextField();
        tripNumberLabel = new javax.swing.JLabel();
        tripNumberField = new javax.swing.JTextField();
        revenueLabel = new javax.swing.JLabel();
        revenueField = new javax.swing.JFormattedTextField();
        mileageLabel = new javax.swing.JLabel();
        mileageField = new javax.swing.JFormattedTextField();
        commodityLabel = new javax.swing.JLabel();
        weightLabel = new javax.swing.JLabel();
        weightField = new javax.swing.JFormattedTextField();
        pieceField = new javax.swing.JSpinner();
        pieceLabel = new javax.swing.JLabel();
        commodityField = new javax.swing.JTextField();
        xtraInfoPanel = new javax.swing.JPanel();
        hazMatCheckbox = new javax.swing.JCheckBox();
        teamCheckbox = new javax.swing.JCheckBox();
        topCheckbox = new javax.swing.JCheckBox();
        twicCheckbox = new javax.swing.JCheckBox();
        ltlCheckbox = new javax.swing.JCheckBox();
        rampsCheckbox = new javax.swing.JCheckBox();
        sigAndTalleyCheckbox = new javax.swing.JCheckBox();
        tarpedCheckbox = new javax.swing.JCheckBox();
        tarpHeightLabel = new javax.swing.JLabel();
        tarpHeightField = new javax.swing.JSpinner();
        brokerPanel = new javax.swing.JPanel();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle(bundle.getString("Form.title")); // NOI18N
        setIconImage(new ImageIcon(getClass().getResource(bundle.getString("Form.icon"))).getImage());
        setModalExclusionType(java.awt.Dialog.ModalExclusionType.APPLICATION_EXCLUDE);
        setName("LoadWizard"); // NOI18N

        sidebarPanel.setMinimumSize(new java.awt.Dimension(198, 361));
        sidebarPanel.setName("sidebarPanel"); // NOI18N
        sidebarPanel.setPreferredSize(new java.awt.Dimension(250, 400));

        javax.swing.GroupLayout sidebarPanelLayout = new javax.swing.GroupLayout(sidebarPanel);
        sidebarPanel.setLayout(sidebarPanelLayout);
        sidebarPanelLayout.setHorizontalGroup(
            sidebarPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 250, Short.MAX_VALUE)
        );
        sidebarPanelLayout.setVerticalGroup(
            sidebarPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 515, Short.MAX_VALUE)
        );

        getContentPane().add(sidebarPanel, java.awt.BorderLayout.LINE_START);

        bottomPanel.setName("bottomPanel"); // NOI18N
        bottomPanel.setLayout(new java.awt.BorderLayout());

        navigationSeparator.setName("navigationSeparator"); // NOI18N
        bottomPanel.add(navigationSeparator, java.awt.BorderLayout.PAGE_START);

        commandPanel.setMinimumSize(new java.awt.Dimension(100, 63));
        commandPanel.setName("commandPanel"); // NOI18N
        commandPanel.setPreferredSize(new java.awt.Dimension(865, 63));

        helpButton.setName("helpButton"); // NOI18N

        backButton.setAction(back);
        backButton.setName("backButton"); // NOI18N

        nextButton.setAction(next);
        nextButton.setName("nextButton"); // NOI18N

        finishButton.setAction(finish);
        finishButton.setName("finishButton"); // NOI18N

        cancelButton.setAction(cancel);
        cancelButton.setMinimumSize(new java.awt.Dimension(64, 58));
        cancelButton.setName("cancelButton"); // NOI18N

        tipLabel.setText(bundle.getString("LoadWizard.tipLabel.text")); // NOI18N
        tipLabel.setName("tipLabel"); // NOI18N

        javax.swing.GroupLayout commandPanelLayout = new javax.swing.GroupLayout(commandPanel);
        commandPanel.setLayout(commandPanelLayout);
        commandPanelLayout.setHorizontalGroup(
            commandPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(commandPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(helpButton)
                .addGap(18, 18, 18)
                .addComponent(tipLabel, javax.swing.GroupLayout.DEFAULT_SIZE, 725, Short.MAX_VALUE)
                .addGap(18, 18, 18)
                .addComponent(backButton)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(nextButton)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(finishButton)
                .addGap(18, 18, 18)
                .addComponent(cancelButton, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        commandPanelLayout.setVerticalGroup(
            commandPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(commandPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(commandPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(commandPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(helpButton)
                        .addComponent(backButton)
                        .addComponent(nextButton)
                        .addComponent(finishButton)
                        .addComponent(cancelButton, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(tipLabel))
                .addContainerGap(41, Short.MAX_VALUE))
        );

        bottomPanel.add(commandPanel, java.awt.BorderLayout.CENTER);

        getContentPane().add(bottomPanel, java.awt.BorderLayout.PAGE_END);

        wizardPanel.setName("wizardPanel"); // NOI18N
        wizardPanel.setLayout(new java.awt.CardLayout());

        welcomePanel.setName("welcomePanel"); // NOI18N

        welcomeLabel.setText(bundle.getString("LoadWizard.welcomeLabel.text")); // NOI18N
        welcomeLabel.setVerticalAlignment(javax.swing.SwingConstants.TOP);
        welcomeLabel.setName("welcomeLabel"); // NOI18N

        javax.swing.GroupLayout welcomePanelLayout = new javax.swing.GroupLayout(welcomePanel);
        welcomePanel.setLayout(welcomePanelLayout);
        welcomePanelLayout.setHorizontalGroup(
            welcomePanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(welcomePanelLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(welcomeLabel, javax.swing.GroupLayout.DEFAULT_SIZE, 591, Short.MAX_VALUE)
                .addContainerGap())
        );
        welcomePanelLayout.setVerticalGroup(
            welcomePanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(welcomePanelLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(welcomeLabel, javax.swing.GroupLayout.DEFAULT_SIZE, 491, Short.MAX_VALUE)
                .addContainerGap())
        );

        wizardPanel.add(welcomePanel, "step0");

        loadPanel.setName("loadPanel"); // NOI18N

        generalInfoPanel.setBorder(javax.swing.BorderFactory.createTitledBorder(bundle.getString("LoadWizard.generalInfoPanel.border.title"))); // NOI18N
        generalInfoPanel.setName("generalInfoPanel"); // NOI18N

        orderNumberLabel.setFont(new java.awt.Font("Noto Sans", 1, 13)); // NOI18N
        orderNumberLabel.setText(bundle.getString("LoadWizard.orderNumberLabel.text")); // NOI18N
        orderNumberLabel.setName("orderNumberLabel"); // NOI18N

        orderNumberField.setBackground(required);
        orderNumberField.setText(bundle.getString("LoadWizard.orderNumberField.text")); // NOI18N
        orderNumberField.setName("orderNumberField"); // NOI18N
        orderNumberField.setInputVerifier(new NotEmptyInputVerifier());

        orderNumberField.getDocument().putProperty("owner", "orderNumber");
        orderNumberField.getDocument().addDocumentListener(this);

        tripNumberLabel.setFont(new java.awt.Font("Noto Sans", 1, 13)); // NOI18N
        tripNumberLabel.setText(bundle.getString("LoadWizard.tripNumberLabel.text")); // NOI18N
        tripNumberLabel.setName("tripNumberLabel"); // NOI18N

        tripNumberField.setBackground(required);
        tripNumberField.setText(bundle.getString("LoadWizard.tripNumberField.text")); // NOI18N
        tripNumberField.setName("tripNumberField"); // NOI18N
        tripNumberField.setInputVerifier(new NotEmptyInputVerifier());

        tripNumberField.getDocument().putProperty("owner", "tripNumber");
        tripNumberField.getDocument().addDocumentListener(this);

        revenueLabel.setFont(new java.awt.Font("Noto Sans", 1, 13)); // NOI18N
        revenueLabel.setText(bundle.getString("LoadWizard.revenueLabel.text")); // NOI18N
        revenueLabel.setName("revenueLabel"); // NOI18N

        revenueField.setBackground(required);
        revenueField.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.NumberFormatter(new java.text.DecimalFormat("#,##0.00"))));
        revenueField.setHorizontalAlignment(javax.swing.JTextField.TRAILING);
        revenueField.setText(bundle.getString("LoadWizard.revenueField.text")); // NOI18N
        revenueField.setName("revenueField"); // NOI18N
        revenueField.setInputVerifier(new NotEmptyInputVerifier());

        revenueField.getDocument().putProperty("owner", "revenue");
        revenueField.getDocument().addDocumentListener(this);

        mileageLabel.setFont(new java.awt.Font("Noto Sans", 1, 13)); // NOI18N
        mileageLabel.setText(bundle.getString("LoadWizard.mileageLabel.text")); // NOI18N
        mileageLabel.setName("mileageLabel"); // NOI18N

        mileageField.setBackground(required);
        mileageField.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.NumberFormatter(new java.text.DecimalFormat("#,##0"))));
        mileageField.setHorizontalAlignment(javax.swing.JTextField.TRAILING);
        mileageField.setText(bundle.getString("LoadWizard.mileageField.text")); // NOI18N
        mileageField.setName("mileageField"); // NOI18N
        mileageField.setInputVerifier(new NotEmptyInputVerifier());

        mileageField.getDocument().putProperty("owner", "mileage");
        mileageField.getDocument().addDocumentListener(this);

        commodityLabel.setText(bundle.getString("LoadWizard.commodityLabel.text")); // NOI18N
        commodityLabel.setName("commodityLabel"); // NOI18N

        weightLabel.setText(bundle.getString("LoadWizard.weightLabel.text")); // NOI18N
        weightLabel.setName("weightLabel"); // NOI18N

        weightField.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.NumberFormatter(new java.text.DecimalFormat("#,##0.00"))));
        weightField.setHorizontalAlignment(javax.swing.JTextField.TRAILING);
        weightField.setText(bundle.getString("LoadWizard.weightField.text")); // NOI18N
        weightField.setName("weightField"); // NOI18N

        pieceField.setModel(new javax.swing.SpinnerNumberModel(1, 1, 5000, 1));
        pieceField.setName("pieceField"); // NOI18N

        pieceLabel.setText(bundle.getString("LoadWizard.pieceLabel.text")); // NOI18N
        pieceLabel.setName("pieceLabel"); // NOI18N

        commodityField.setText(bundle.getString("LoadWizard.commodityField.text")); // NOI18N
        commodityField.setName("commodityField"); // NOI18N

        javax.swing.GroupLayout generalInfoPanelLayout = new javax.swing.GroupLayout(generalInfoPanel);
        generalInfoPanel.setLayout(generalInfoPanelLayout);
        generalInfoPanelLayout.setHorizontalGroup(
            generalInfoPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(generalInfoPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(generalInfoPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(generalInfoPanelLayout.createSequentialGroup()
                        .addGroup(generalInfoPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(weightLabel)
                            .addComponent(commodityLabel))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(generalInfoPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(generalInfoPanelLayout.createSequentialGroup()
                                .addComponent(weightField, javax.swing.GroupLayout.PREFERRED_SIZE, 89, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(pieceLabel)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(pieceField, javax.swing.GroupLayout.PREFERRED_SIZE, 77, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addComponent(commodityField)))
                    .addGroup(generalInfoPanelLayout.createSequentialGroup()
                        .addGroup(generalInfoPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addGroup(generalInfoPanelLayout.createSequentialGroup()
                                .addGap(12, 12, 12)
                                .addComponent(revenueLabel)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(revenueField))
                            .addGroup(generalInfoPanelLayout.createSequentialGroup()
                                .addComponent(orderNumberLabel)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(orderNumberField, javax.swing.GroupLayout.PREFERRED_SIZE, 178, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addGroup(generalInfoPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(generalInfoPanelLayout.createSequentialGroup()
                                .addGap(18, 18, 18)
                                .addComponent(tripNumberLabel)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(tripNumberField, javax.swing.GroupLayout.PREFERRED_SIZE, 178, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(generalInfoPanelLayout.createSequentialGroup()
                                .addGap(34, 34, 34)
                                .addComponent(mileageLabel)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(mileageField, javax.swing.GroupLayout.PREFERRED_SIZE, 89, javax.swing.GroupLayout.PREFERRED_SIZE)))))
                .addContainerGap())
        );
        generalInfoPanelLayout.setVerticalGroup(
            generalInfoPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(generalInfoPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(generalInfoPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(orderNumberLabel)
                    .addComponent(orderNumberField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(tripNumberLabel)
                    .addComponent(tripNumberField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(generalInfoPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(revenueLabel)
                    .addComponent(revenueField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(mileageLabel)
                    .addComponent(mileageField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(generalInfoPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(commodityLabel)
                    .addComponent(commodityField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(generalInfoPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(weightLabel)
                    .addComponent(weightField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(pieceField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(pieceLabel))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        xtraInfoPanel.setBorder(javax.swing.BorderFactory.createTitledBorder(bundle.getString("LoadWizard.xtraInfoPanel.border.title"))); // NOI18N
        xtraInfoPanel.setName("xtraInfoPanel"); // NOI18N

        hazMatCheckbox.setText(bundle.getString("LoadWizard.hazMatCheckbox.text")); // NOI18N
        hazMatCheckbox.setName("hazMatCheckbox"); // NOI18N

        teamCheckbox.setText(bundle.getString("LoadWizard.teamCheckbox.text")); // NOI18N
        teamCheckbox.setName("teamCheckbox"); // NOI18N

        topCheckbox.setText(bundle.getString("LoadWizard.topCheckbox.text")); // NOI18N
        topCheckbox.setName("topCheckbox"); // NOI18N

        twicCheckbox.setText(bundle.getString("LoadWizard.twicCheckbox.text")); // NOI18N
        twicCheckbox.setName("twicCheckbox"); // NOI18N

        ltlCheckbox.setText(bundle.getString("LoadWizard.ltlCheckbox.text")); // NOI18N
        ltlCheckbox.setName("ltlCheckbox"); // NOI18N

        rampsCheckbox.setText(bundle.getString("LoadWizard.rampsCheckbox.text")); // NOI18N
        rampsCheckbox.setName("rampsCheckbox"); // NOI18N

        sigAndTalleyCheckbox.setText(bundle.getString("LoadWizard.sigAndTalleyCheckbox.text")); // NOI18N
        sigAndTalleyCheckbox.setName("sigAndTalleyCheckbox"); // NOI18N

        tarpedCheckbox.setText(bundle.getString("LoadWizard.tarpedCheckbox.text")); // NOI18N
        tarpedCheckbox.setName("tarpedCheckbox"); // NOI18N
        tarpedCheckbox.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                tarpedCheckboxItemStateChanged(evt);
            }
        });

        tarpHeightLabel.setText(bundle.getString("LoadWizard.tarpHeightLabel.text")); // NOI18N
        tarpHeightLabel.setName("tarpHeightLabel"); // NOI18N

        tarpHeightField.setModel(new javax.swing.SpinnerNumberModel(0, 0, 8, 2));
        tarpHeightField.setName("tarpHeightField"); // NOI18N

        javax.swing.GroupLayout xtraInfoPanelLayout = new javax.swing.GroupLayout(xtraInfoPanel);
        xtraInfoPanel.setLayout(xtraInfoPanelLayout);
        xtraInfoPanelLayout.setHorizontalGroup(
            xtraInfoPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(xtraInfoPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(xtraInfoPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(xtraInfoPanelLayout.createSequentialGroup()
                        .addComponent(hazMatCheckbox)
                        .addGap(18, 18, 18)
                        .addComponent(teamCheckbox))
                    .addComponent(sigAndTalleyCheckbox))
                .addGap(18, 18, 18)
                .addGroup(xtraInfoPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(xtraInfoPanelLayout.createSequentialGroup()
                        .addComponent(topCheckbox)
                        .addGap(18, 18, 18)
                        .addComponent(twicCheckbox))
                    .addGroup(xtraInfoPanelLayout.createSequentialGroup()
                        .addComponent(tarpedCheckbox)
                        .addGap(18, 18, 18)
                        .addComponent(tarpHeightLabel)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(tarpHeightField)))
                .addGap(18, 18, 18)
                .addComponent(ltlCheckbox)
                .addGap(18, 18, 18)
                .addComponent(rampsCheckbox)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        xtraInfoPanelLayout.setVerticalGroup(
            xtraInfoPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(xtraInfoPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(xtraInfoPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(hazMatCheckbox)
                    .addComponent(teamCheckbox)
                    .addComponent(topCheckbox)
                    .addComponent(twicCheckbox)
                    .addComponent(ltlCheckbox)
                    .addComponent(rampsCheckbox))
                .addGap(18, 18, 18)
                .addGroup(xtraInfoPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(sigAndTalleyCheckbox)
                    .addComponent(tarpedCheckbox)
                    .addComponent(tarpHeightLabel)
                    .addComponent(tarpHeightField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout loadPanelLayout = new javax.swing.GroupLayout(loadPanel);
        loadPanel.setLayout(loadPanelLayout);
        loadPanelLayout.setHorizontalGroup(
            loadPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(loadPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(loadPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(xtraInfoPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(generalInfoPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(71, Short.MAX_VALUE))
        );
        loadPanelLayout.setVerticalGroup(
            loadPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(loadPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(generalInfoPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(xtraInfoPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(180, Short.MAX_VALUE))
        );

        tarpHeightLabel.setVisible(tarpedCheckbox.isSelected());
        tarpHeightField.setVisible(tarpedCheckbox.isSelected());

        wizardPanel.add(loadPanel, "step1");

        brokerPanel.setName("brokerPanel"); // NOI18N

        javax.swing.GroupLayout brokerPanelLayout = new javax.swing.GroupLayout(brokerPanel);
        brokerPanel.setLayout(brokerPanelLayout);
        brokerPanelLayout.setHorizontalGroup(
            brokerPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 615, Short.MAX_VALUE)
        );
        brokerPanelLayout.setVerticalGroup(
            brokerPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 515, Short.MAX_VALUE)
        );

        wizardPanel.add(brokerPanel, "step2");

        getContentPane().add(wizardPanel, java.awt.BorderLayout.CENTER);
        program.addExitListener(this);
        program.addPropertyChangeListener("backAvailable", this);
        program.addPropertyChangeListener("nextAvailable", this);
        program.addPropertyChangeListener("finishAvailable", this);
        addWindowListener(adapter);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void tarpedCheckboxItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_tarpedCheckboxItemStateChanged
        tarpHeightLabel.setVisible(tarpedCheckbox.isSelected());
        tarpHeightField.setVisible(tarpedCheckbox.isSelected());
    }//GEN-LAST:event_tarpedCheckboxItemStateChanged

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton backButton;
    private javax.swing.JPanel bottomPanel;
    private javax.swing.JPanel brokerPanel;
    private javax.swing.JButton cancelButton;
    private javax.swing.JPanel commandPanel;
    private javax.swing.JTextField commodityField;
    private javax.swing.JLabel commodityLabel;
    private javax.swing.JButton finishButton;
    private javax.swing.JPanel generalInfoPanel;
    private javax.swing.JCheckBox hazMatCheckbox;
    private javax.swing.JButton helpButton;
    private javax.swing.JPanel loadPanel;
    private javax.swing.JCheckBox ltlCheckbox;
    private javax.swing.JFormattedTextField mileageField;
    private javax.swing.JLabel mileageLabel;
    private javax.swing.JSeparator navigationSeparator;
    private javax.swing.JButton nextButton;
    private javax.swing.JTextField orderNumberField;
    private javax.swing.JLabel orderNumberLabel;
    private javax.swing.JSpinner pieceField;
    private javax.swing.JLabel pieceLabel;
    private javax.swing.JCheckBox rampsCheckbox;
    private javax.swing.JFormattedTextField revenueField;
    private javax.swing.JLabel revenueLabel;
    private javax.swing.JPanel sidebarPanel;
    private javax.swing.JCheckBox sigAndTalleyCheckbox;
    private javax.swing.JSpinner tarpHeightField;
    private javax.swing.JLabel tarpHeightLabel;
    private javax.swing.JCheckBox tarpedCheckbox;
    private javax.swing.JCheckBox teamCheckbox;
    private javax.swing.JLabel tipLabel;
    private javax.swing.JCheckBox topCheckbox;
    private javax.swing.JTextField tripNumberField;
    private javax.swing.JLabel tripNumberLabel;
    private javax.swing.JCheckBox twicCheckbox;
    private javax.swing.JFormattedTextField weightField;
    private javax.swing.JLabel weightLabel;
    private javax.swing.JLabel welcomeLabel;
    private javax.swing.JPanel welcomePanel;
    private javax.swing.JPanel wizardPanel;
    private javax.swing.JPanel xtraInfoPanel;
    // End of variables declaration//GEN-END:variables

    private void changeStep() {
        ((CardLayout) wizardPanel.getLayout()).show(wizardPanel, "step" + currentStep);
        
        update(getGraphics());
    }
    
    private final BackAction back;
    private class BackAction extends AbstractAction {

        private static final long serialVersionUID = 3439435409062560658L;

        public BackAction() {
            super(bundle.getString("BackAction.text"));
            
            initAction();
        }
        
        private void initAction() {
            putValue(SMALL_ICON, new ImageIcon(getClass().getResource(
                    bundle.getString("BackAction.smallIcon"))));
            putValue(DISPLAYED_MNEMONIC_INDEX_KEY, Integer.valueOf(
                    bundle.getString("BackAction.mnemonicIndex")));
            putValue(SHORT_DESCRIPTION, bundle.getString("BackAction.shortDescription"));
        }
        
        @Override
        public void actionPerformed(ActionEvent e) {
            currentStep--;
            changeStep();
        }
        
    }
    
    private final NextAction next;
    private class NextAction extends AbstractAction {

        private static final long serialVersionUID = 4287734709791430808L;
        
        public NextAction() {
            super(bundle.getString("NextAction.text"));
            
            initAction();
        }
        
        private void initAction() {
            putValue(SMALL_ICON, new ImageIcon(getClass().getResource(
                    bundle.getString("NextAction.smallIcon"))));
            putValue(DISPLAYED_MNEMONIC_INDEX_KEY, Integer.valueOf(
                    bundle.getString("NextAction.mnemonicIndex")));
            putValue(SHORT_DESCRIPTION, bundle.getString("NextAction.shortDescription"));
        }

        @Override
        public void actionPerformed(ActionEvent e) {
            currentStep++;
            changeStep();
        }
        
    }
    
    private final FinishAction finish;
    private class FinishAction extends AbstractAction {

        private static final long serialVersionUID = -7446963206784288340L;
        
        public FinishAction() {
            super(bundle.getString("FinishAction.text"));
            
            initAction();
        }
        
        private void initAction() {
            putValue(SMALL_ICON, new ImageIcon(getClass().getResource(
                    bundle.getString("FinishAction.smallIcon"))));
            putValue(DISPLAYED_MNEMONIC_INDEX_KEY, Integer.valueOf(
                    bundle.getString("FinishAction.mnemonicIndex")));
            putValue(SHORT_DESCRIPTION, bundle.getString("FinishAction.shortDescription"));
        }

        @Override
        public void actionPerformed(ActionEvent e) {
            // Handle storing of load to database.
            
            cancelButton.getAction().actionPerformed(e);
        }
        
    }
    
    private final CancelAction cancel;
    private class CancelAction extends AbstractAction {

        private static final long serialVersionUID = -6089139508449217744L;
        
        public CancelAction() {
            super(bundle.getString("CancelAction.text"));
            
            initAction();
        }
        
        private void initAction() {
            putValue(SMALL_ICON, new ImageIcon(getClass().getResource(
                    bundle.getString("CancelAction.smallIcon"))));
            putValue(DISPLAYED_MNEMONIC_INDEX_KEY, Integer.valueOf(
                    bundle.getString("CancelAction.mnemonicIndex")));
            putValue(SHORT_DESCRIPTION, 
                    bundle.getString("CancelAction.shortDescription"));
        }

        @Override
        public void actionPerformed(ActionEvent e) {
                willExit(e);
        }
        
    }
    
    private final HelpAction help;
    private class HelpAction extends AbstractAction {

        private static final long serialVersionUID = 9195556010988315538L;
        
        public HelpAction() {
            super(bundle.getString("HelpAction.text"));
            
            initAction();
        }
        
        private void initAction() {
            putValue(SMALL_ICON, new ImageIcon(getClass().getResource(
                    bundle.getString("HelpAction.smallIcon"))));
            putValue(DISPLAYED_MNEMONIC_INDEX_KEY, Integer.valueOf(
                    bundle.getString("HelpAction.mnemonicIndex")));
            putValue(SHORT_DESCRIPTION, 
                    bundle.getString("HelpAction.shortDescription"));
        }

        @Override
        public void actionPerformed(ActionEvent e) {
            throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        }
        
    }
    
    private final LoadWizardAdapter adapter;
    private class LoadWizardAdapter extends WindowAdapter {

        @Override
        public void windowClosed(WindowEvent e) {
            
        }
        
        @Override
        public void windowClosing(WindowEvent e) {
            if (currentStep > 0) {
                int choice = MessageBox.askQuestion("Load Wizard is not "
                        + "complete.\n\nDo you wish to cancel anyway?", 
                        "Confirm Cancel", false);
                
                if (choice == MessageBox.YES_OPTION) {
                    program.save(instance);
                    currentStep = 0;    // Prevent second confirmation.
                    dispose();
                }
            } else {
                program.save(instance);
                dispose();
            }
        }

        @Override
        public void windowOpened(WindowEvent e) {
            setBackAvailable(false);
            setNextAvailable(true);
            setCanFinish(false);
            backButton.getAction().setEnabled(isBackAvailable());
            nextButton.getAction().setEnabled(isNextAvailable());
            finishButton.getAction().setEnabled(canFinish());
            tipLabel.setText("");
        }
        
    }

    private class SidebarPanel extends javax.swing.JPanel {

        private static final long serialVersionUID = -5120849133825965095L;
        
        private final java.awt.Font stdFont;
        
        private java.awt.image.BufferedImage bgImage;
        
        public SidebarPanel() {
            super();
            
            try {
                bgImage = ImageIO.read(getClass().getResource(
                        "/com/gs/ntos/loads/resources/icons/LoadWizard.png"));
            } catch (IOException ex) {
                logger.error(ex, "Attempting to read in the BufferedImage");
            }
            stdFont = getFont();
        }

        @Override
        public void paint(Graphics g) {
            super.paint(g);
            
            java.awt.Graphics2D g2d = (java.awt.Graphics2D) g;
            
            g2d.drawImage(bgImage, 0, 0, getWidth(), getHeight(), this);
            
            g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING, 
                    RenderingHints.VALUE_ANTIALIAS_ON);
            
            for (int x = 0; x < steps.length; x++) {
                if (x == currentStep) {
                    g2d.setColor(new Color(0.0f, 0.0f, 0.8f)); // Highlight Color
                    g2d.setFont(new java.awt.Font("Dialog", Font.BOLD, 13));
                } else {
                    g2d.setColor(new Color(0.0f, 0.0f, 0.4f)); // Standard Color
                    g2d.setFont(stdFont);
                }
                
                g2d.drawString((x + 1) + ". " + steps[x], 5, 15 * (x + 1));
            }
        }
        
    }
    
}
