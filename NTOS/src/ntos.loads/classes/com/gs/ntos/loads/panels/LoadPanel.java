/*
 * Copyright (C) 2022 GS United Labs
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * *****************************************************************************
 *  Project    :   NTOS
 *  Class      :   LoadPanel.java
 *  Author     :   Sean Carrick
 *  Created    :   Apr 3, 2022
 *  Modified   :   Apr 3, 2022
 *  
 *  Purpose: See class JavaDoc for explanation
 *  
 *  Revision History:
 *  
 *  WHEN          BY                   REASON
 *  ------------  -------------------  -----------------------------------------
 *  Apr 3, 2022  Sean Carrick         Initial creation.
 * *****************************************************************************
 */
package com.gs.ntos.loads.panels;

import com.gs.ntos.filters.DoubleFilter;
import com.gs.ntos.filters.IntegerFilter;
import com.gs.ntos.loads.support.NotBlankVerifier;
import com.gs.ntos.verifiers.NotEmptyInputVerifier;
import java.awt.Color;
import java.awt.Component;
import java.awt.SystemColor;
import java.util.ResourceBundle;
import javax.swing.text.PlainDocument;
import org.netbeans.spi.wizard.WizardController;
import org.netbeans.spi.wizard.WizardPage;

/**
 *
 * @author Sean Carrick
 */
public class LoadPanel extends WizardPage  {

    private static final long serialVersionUID = 7614684253270394560L;
    
    private final ResourceBundle bundle;
    private final Color accepted;

    /**
     * Creates new form LoadPanel
     */
    public LoadPanel() {
        bundle = ResourceBundle.getBundle("com/gs/ntos/loads/resources/Bundle");
        accepted = new Color(176, 229, 212);
        
        initComponents();
        
        orderNumberField.requestFocusInWindow();
        
        setForwardNavigationMode(WizardController.MODE_CAN_CONTINUE);
    }
    
    public static final String getDescription() {
        return "General Load Information";
    }

    @Override
    protected String validateContents(Component component, Object event) {
        // Set the flags to our settings.
        if (orderNumberField.getText().length() == 0) {
            return "Order Number is required.";
        } else {
            orderNumberField.setBackground(accepted);
        }
        if (tripNumberField.getText().length() == 0) {
            return "Trip Number is required.";
        } else {
            tripNumberField.setBackground(accepted);
        }
        if (revenueField.getText().length() == 0) {
            return "Gross Revenue is required and must be greater than zero.";
        } else {
            revenueField.setBackground(accepted);
        }
        if (mileageField.getText().length() == 0
                || Integer.parseInt(mileageField.getText()) <= 0) {
            return "Mileage is required and must be greater than zero.";
        } else {
            mileageField.setBackground(accepted);
        }
        if (commodityField.getText().length() > 0) {
            commodityField.setBackground(accepted);
        }
        if (weightField.getText().length() > 0) {
            if (Integer.parseInt(weightField.getText()) < 1) {
                weightField.setBackground(new Color(255, 193, 193));
                return "Weight, if specified, must be greater than zero";
            }
            weightField.setBackground(accepted);
        }
        if (pieceField.getText().length() > 0) {
            if (Integer.parseInt(pieceField.getText()) < 1) {
                pieceField.setBackground(new Color(255, 193, 193));
                return "Pieces, if specified, must be greater than zero";
            }
            pieceField.setBackground(accepted);
        }
        if (hazMatCheckbox.isSelected()) {
            hazMatCheckbox.setBackground(accepted);
        } else {
            hazMatCheckbox.setBackground(SystemColor.text);
        }
        if (teamCheckbox.isSelected()) {
            teamCheckbox.setBackground(accepted);
        } else {
            teamCheckbox.setBackground(SystemColor.text);
        }
        if (twicCheckbox.isSelected()) {
            twicCheckbox.setBackground(accepted);
        } else {
            twicCheckbox.setBackground(SystemColor.text);
        }
        if (rampsCheckbox.isSelected()) {
            rampsCheckbox.setBackground(accepted);
        } else {
            rampsCheckbox.setBackground(SystemColor.text);
        }
        if (tarpedCheckbox.isSelected()) {
            tarpedCheckbox.setBackground(accepted);
            if (((Integer) tarpHeightField.getValue()) > 0) {
                tarpHeightField.setBackground(accepted);
            } else {
                tarpHeightField.setBackground(new Color(255, 193, 193));
                return "If Tarped is selected, Tarp Height must be specified";
            }
        } else {
            tarpedCheckbox.setBackground(SystemColor.text);
        }
        if (ltlCheckbox.isSelected()) {
            ltlCheckbox.setBackground(accepted);
        } else {
            ltlCheckbox.setBackground(SystemColor.text);
        }
        if (topCheckbox.isSelected()) {
            topCheckbox.setBackground(accepted);
        } else {
            topCheckbox.setBackground(SystemColor.text);
        }
        if (hazMatCheckbox.isSelected()) {
            hazMatCheckbox.setBackground(accepted);
        } else {
            hazMatCheckbox.setBackground(SystemColor.text);
        }
        if (sigAndTalleyCheckbox.isSelected()) {
            sigAndTalleyCheckbox.setBackground(accepted);
        } else {
            sigAndTalleyCheckbox.setBackground(SystemColor.text);
        }
        
        putWizardData("orderNumber", orderNumberField.getText());
        putWizardData("tripNumber", tripNumberField.getText());
        putWizardData("grossPay", revenueField.getText());
        putWizardData("loadedMiles", mileageField.getText());
        putWizardData("commodity", commodityField.getText());
        putWizardData("weight", weightField.getText());
        putWizardData("pieces", pieceField.getText());
        putWizardData("hazMat", hazMatCheckbox.isSelected());
        putWizardData("team", teamCheckbox.isSelected());
        putWizardData("twic", twicCheckbox.isSelected());
        putWizardData("ramps", rampsCheckbox.isSelected());
        putWizardData("top", topCheckbox.isSelected());
        putWizardData("sigTalley", sigAndTalleyCheckbox.isSelected());
        putWizardData("tarped", tarpedCheckbox.isSelected());
        putWizardData("tarpHeight", tarpHeightField.getValue());
        putWizardData("ltl", ltlCheckbox.isSelected());
        
        return null;
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        generalInfoPanel = new javax.swing.JPanel();
        orderNumberLabel = new javax.swing.JLabel();
        orderNumberField = new javax.swing.JTextField();
        tripNumberLabel = new javax.swing.JLabel();
        tripNumberField = new javax.swing.JTextField();
        revenueLabel = new javax.swing.JLabel();
        mileageLabel = new javax.swing.JLabel();
        commodityLabel = new javax.swing.JLabel();
        weightLabel = new javax.swing.JLabel();
        pieceLabel = new javax.swing.JLabel();
        commodityField = new javax.swing.JTextField();
        revenueField = new javax.swing.JTextField();
        mileageField = new javax.swing.JTextField();
        weightField = new javax.swing.JTextField();
        pieceField = new javax.swing.JTextField();
        xtraInfoPanel = new javax.swing.JPanel();
        hazMatCheckbox = new javax.swing.JCheckBox();
        teamCheckbox = new javax.swing.JCheckBox();
        topCheckbox = new javax.swing.JCheckBox();
        twicCheckbox = new javax.swing.JCheckBox();
        ltlCheckbox = new javax.swing.JCheckBox();
        rampsCheckbox = new javax.swing.JCheckBox();
        sigAndTalleyCheckbox = new javax.swing.JCheckBox();
        tarpedCheckbox = new javax.swing.JCheckBox();
        tarpHeightLabel = new javax.swing.JLabel();
        tarpHeightField = new javax.swing.JSpinner();

        setName("Form"); // NOI18N

        generalInfoPanel.setBorder(javax.swing.BorderFactory.createTitledBorder(bundle.getString("LoadPanel.generalInfoPanel.border.title"))); // NOI18N
        generalInfoPanel.setName("generalInfoPanel"); // NOI18N

        orderNumberLabel.setDisplayedMnemonic(bundle.getString("LoadPanel.orderNumberLabel.mnemonic").charAt(0));
        orderNumberLabel.setFont(new java.awt.Font("Noto Sans", 1, 13)); // NOI18N
        orderNumberLabel.setLabelFor(orderNumberField);
        orderNumberLabel.setText(bundle.getString("LoadPanel.orderNumberLabel.text")); // NOI18N
        orderNumberLabel.setName("orderNumberLabel"); // NOI18N

        orderNumberField.setBackground(new java.awt.Color(255, 193, 193));
        orderNumberField.setText(bundle.getString("LoadPanel.orderNumberField.text")); // NOI18N
        orderNumberField.setName("orderNumberField"); // NOI18N
        orderNumberField.setInputVerifier(new NotEmptyInputVerifier());

        tripNumberLabel.setDisplayedMnemonic(bundle.getString("LoadPanel.tripNumberLabel.mnemonic").charAt(0));
        tripNumberLabel.setFont(new java.awt.Font("Noto Sans", 1, 13)); // NOI18N
        tripNumberLabel.setLabelFor(tripNumberField);
        tripNumberLabel.setText(bundle.getString("LoadPanel.tripNumberLabel.text")); // NOI18N
        tripNumberLabel.setName("tripNumberLabel"); // NOI18N

        tripNumberField.setBackground(new java.awt.Color(255, 193, 193));
        tripNumberField.setText(bundle.getString("LoadPanel.tripNumberField.text")); // NOI18N
        tripNumberField.setName("tripNumberField"); // NOI18N
        tripNumberField.setInputVerifier(new NotEmptyInputVerifier());

        revenueLabel.setDisplayedMnemonic(bundle.getString("LoadPanel.revenueLabel.mnemonic").charAt(0));
        revenueLabel.setFont(new java.awt.Font("Noto Sans", 1, 13)); // NOI18N
        revenueLabel.setText(bundle.getString("LoadPanel.revenueLabel.text")); // NOI18N
        revenueLabel.setName("revenueLabel"); // NOI18N

        mileageLabel.setDisplayedMnemonic(bundle.getString("LoadPanel.mileageLabel.mnemonic").charAt(0));
        mileageLabel.setFont(new java.awt.Font("Noto Sans", 1, 13)); // NOI18N
        mileageLabel.setLabelFor(mileageField);
        mileageLabel.setText(bundle.getString("LoadPanel.mileageLabel.text")); // NOI18N
        mileageLabel.setName("mileageLabel"); // NOI18N

        commodityLabel.setDisplayedMnemonic(bundle.getString("LoadPanel.commodityLabel.mnemonic").charAt(0));
        commodityLabel.setLabelFor(commodityField);
        commodityLabel.setText(bundle.getString("LoadPanel.commodityLabel.text")); // NOI18N
        commodityLabel.setName("commodityLabel"); // NOI18N

        weightLabel.setDisplayedMnemonic(bundle.getString("LoadPanel.weightLabel.mnemonic").charAt(0));
        weightLabel.setLabelFor(weightField);
        weightLabel.setText(bundle.getString("LoadPanel.weightLabel.text")); // NOI18N
        weightLabel.setName("weightLabel"); // NOI18N

        pieceLabel.setDisplayedMnemonic(bundle.getString("LoadPanel.piecesLabel.mnemonic").charAt(0));
        pieceLabel.setLabelFor(pieceField);
        pieceLabel.setText(bundle.getString("LoadPanel.piecesLabel.text")); // NOI18N
        pieceLabel.setName("pieceLabel"); // NOI18N

        commodityField.setText(bundle.getString("LoadPanel.commodityField.text")); // NOI18N
        commodityField.setName("commodityField"); // NOI18N

        revenueField.setBackground(new java.awt.Color(255, 193, 193));
        revenueField.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        revenueField.setText(bundle.getString("LoadPanel.revenueField.text")); // NOI18N
        revenueField.setName("revenueField"); // NOI18N
        PlainDocument revDoc = (PlainDocument) revenueField.getDocument();
        revDoc.setDocumentFilter(new DoubleFilter());
        revenueField.setInputVerifier(new NotEmptyInputVerifier());

        mileageField.setBackground(new java.awt.Color(255, 193, 193));
        mileageField.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        mileageField.setText(bundle.getString("LoadPanel.mileageField.text")); // NOI18N
        mileageField.setName("mileageField"); // NOI18N
        PlainDocument milDoc = (PlainDocument) mileageField.getDocument();
        milDoc.setDocumentFilter(new IntegerFilter());
        mileageField.setInputVerifier(new NotBlankVerifier());

        weightField.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        weightField.setText(bundle.getString("LoadPanel.weightField.text")); // NOI18N
        weightField.setName("weightField"); // NOI18N
        PlainDocument wDoc = (PlainDocument) weightField.getDocument();
        wDoc.setDocumentFilter(new IntegerFilter());

        pieceField.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        pieceField.setText(bundle.getString("LoadPanel.pieceField.text")); // NOI18N
        pieceField.setName("pieceField"); // NOI18N
        PlainDocument pDoc = (PlainDocument) pieceField.getDocument();
        pDoc.setDocumentFilter(new IntegerFilter());

        javax.swing.GroupLayout generalInfoPanelLayout = new javax.swing.GroupLayout(generalInfoPanel);
        generalInfoPanel.setLayout(generalInfoPanelLayout);
        generalInfoPanelLayout.setHorizontalGroup(
            generalInfoPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(generalInfoPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(generalInfoPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(generalInfoPanelLayout.createSequentialGroup()
                        .addGroup(generalInfoPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(weightLabel)
                            .addComponent(commodityLabel))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(generalInfoPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(generalInfoPanelLayout.createSequentialGroup()
                                .addComponent(weightField, javax.swing.GroupLayout.PREFERRED_SIZE, 92, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(pieceLabel)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(pieceField, javax.swing.GroupLayout.PREFERRED_SIZE, 78, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addComponent(commodityField)))
                    .addGroup(generalInfoPanelLayout.createSequentialGroup()
                        .addGap(12, 12, 12)
                        .addGroup(generalInfoPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(revenueLabel)
                            .addComponent(orderNumberLabel))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(generalInfoPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(generalInfoPanelLayout.createSequentialGroup()
                                .addComponent(orderNumberField, javax.swing.GroupLayout.PREFERRED_SIZE, 178, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(tripNumberLabel))
                            .addGroup(generalInfoPanelLayout.createSequentialGroup()
                                .addComponent(revenueField, javax.swing.GroupLayout.PREFERRED_SIZE, 130, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(mileageLabel)))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(generalInfoPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(tripNumberField, javax.swing.GroupLayout.PREFERRED_SIZE, 178, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(mileageField, javax.swing.GroupLayout.PREFERRED_SIZE, 91, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addContainerGap())
        );
        generalInfoPanelLayout.setVerticalGroup(
            generalInfoPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(generalInfoPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(generalInfoPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(orderNumberLabel)
                    .addComponent(orderNumberField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(tripNumberLabel)
                    .addComponent(tripNumberField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(generalInfoPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(revenueLabel)
                    .addComponent(mileageLabel)
                    .addComponent(revenueField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(mileageField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(generalInfoPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(commodityLabel)
                    .addComponent(commodityField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(generalInfoPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(weightLabel)
                    .addComponent(pieceLabel)
                    .addComponent(weightField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(pieceField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(13, Short.MAX_VALUE))
        );

        xtraInfoPanel.setBorder(javax.swing.BorderFactory.createTitledBorder(bundle.getString("LoadPanel.additionalInfoPanel.border.title"))); // NOI18N
        xtraInfoPanel.setName("xtraInfoPanel"); // NOI18N

        hazMatCheckbox.setMnemonic(bundle.getString("LoadPanel.hazMatCheckbox.mnemonic").charAt(0));
        hazMatCheckbox.setText(bundle.getString("LoadPanel.hazMatCheckbox.text")); // NOI18N
        hazMatCheckbox.setName("hazMatCheckbox"); // NOI18N

        teamCheckbox.setMnemonic(bundle.getString("LoadPanel.teamCheckbox.mnemonic").charAt(0));
        teamCheckbox.setText(bundle.getString("LoadPanel.teamCheckbox.text")); // NOI18N
        teamCheckbox.setName("teamCheckbox"); // NOI18N

        topCheckbox.setMnemonic(bundle.getString("LoadPanel.topCheckbox.mnemonic").charAt(0));
        topCheckbox.setText(bundle.getString("LoadPanel.topCheckbox.text")); // NOI18N
        topCheckbox.setName("topCheckbox"); // NOI18N

        twicCheckbox.setMnemonic(bundle.getString("LoadPanel.twicCheckbox.mnemonic").charAt(0));
        twicCheckbox.setText(bundle.getString("LoadPanel.twicCheckbox.text")); // NOI18N
        twicCheckbox.setName("twicCheckbox"); // NOI18N

        ltlCheckbox.setMnemonic(bundle.getString("LoadPanel.ltlCheckbox.mnemonic").charAt(0));
        ltlCheckbox.setText(bundle.getString("LoadPanel.ltlCheckbox.text")); // NOI18N
        ltlCheckbox.setName("ltlCheckbox"); // NOI18N

        rampsCheckbox.setMnemonic(bundle.getString("LoadPanel.rampsCheckbox.mnemonic").charAt(0));
        rampsCheckbox.setText(bundle.getString("LoadPanel.rampsCheckbox.text")); // NOI18N
        rampsCheckbox.setName("rampsCheckbox"); // NOI18N

        sigAndTalleyCheckbox.setMnemonic(bundle.getString("LoadPanel.sigAndTalleyCheckbox.mnemonic").charAt(0));
        sigAndTalleyCheckbox.setText(bundle.getString("LoadPanel.sigAndTalleyCheckbox.text")); // NOI18N
        sigAndTalleyCheckbox.setName("sigAndTalleyCheckbox"); // NOI18N

        tarpedCheckbox.setMnemonic(bundle.getString("LoadPanel.tarpedCheckbox.mnemonic").charAt(0));
        tarpedCheckbox.setText(bundle.getString("LoadPanel.tarpedCheckbox.text")); // NOI18N
        tarpedCheckbox.setName("tarpedCheckbox"); // NOI18N
        tarpedCheckbox.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                tarpedCheckboxItemStateChanged(evt);
            }
        });

        tarpHeightLabel.setDisplayedMnemonic(bundle.getString("LoadPanel.tarpHeightLabel.mnemonic").charAt(0));
        tarpHeightLabel.setLabelFor(tarpHeightField);
        tarpHeightLabel.setText(bundle.getString("LoadPanel.tarpHeightLabel.text")); // NOI18N
        tarpHeightLabel.setName("tarpHeightLabel"); // NOI18N

        tarpHeightField.setModel(new javax.swing.SpinnerNumberModel(0, 0, 8, 2));
        tarpHeightField.setName("tarpHeightField"); // NOI18N

        javax.swing.GroupLayout xtraInfoPanelLayout = new javax.swing.GroupLayout(xtraInfoPanel);
        xtraInfoPanel.setLayout(xtraInfoPanelLayout);
        xtraInfoPanelLayout.setHorizontalGroup(
            xtraInfoPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(xtraInfoPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(xtraInfoPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(xtraInfoPanelLayout.createSequentialGroup()
                        .addComponent(hazMatCheckbox)
                        .addGap(18, 18, 18)
                        .addComponent(teamCheckbox))
                    .addComponent(sigAndTalleyCheckbox))
                .addGap(18, 18, 18)
                .addGroup(xtraInfoPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(xtraInfoPanelLayout.createSequentialGroup()
                        .addComponent(topCheckbox)
                        .addGap(18, 18, 18)
                        .addComponent(twicCheckbox))
                    .addGroup(xtraInfoPanelLayout.createSequentialGroup()
                        .addComponent(tarpedCheckbox)
                        .addGap(18, 18, 18)
                        .addComponent(tarpHeightLabel)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(tarpHeightField)))
                .addGap(18, 18, 18)
                .addComponent(ltlCheckbox)
                .addGap(18, 18, 18)
                .addComponent(rampsCheckbox)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        xtraInfoPanelLayout.setVerticalGroup(
            xtraInfoPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(xtraInfoPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(xtraInfoPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(hazMatCheckbox)
                    .addComponent(teamCheckbox)
                    .addComponent(topCheckbox)
                    .addComponent(twicCheckbox)
                    .addComponent(ltlCheckbox)
                    .addComponent(rampsCheckbox))
                .addGap(18, 18, 18)
                .addGroup(xtraInfoPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(sigAndTalleyCheckbox)
                    .addComponent(tarpedCheckbox)
                    .addComponent(tarpHeightLabel)
                    .addComponent(tarpHeightField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(generalInfoPanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(xtraInfoPanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(generalInfoPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(xtraInfoPanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );

        tarpHeightLabel.setVisible(tarpedCheckbox.isSelected());
        tarpHeightField.setVisible(tarpedCheckbox.isSelected());
    }// </editor-fold>//GEN-END:initComponents

    private void tarpedCheckboxItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_tarpedCheckboxItemStateChanged
        tarpHeightLabel.setVisible(tarpedCheckbox.isSelected());
        tarpHeightField.setVisible(tarpedCheckbox.isSelected());
    }//GEN-LAST:event_tarpedCheckboxItemStateChanged

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JTextField commodityField;
    private javax.swing.JLabel commodityLabel;
    private javax.swing.JPanel generalInfoPanel;
    private javax.swing.JCheckBox hazMatCheckbox;
    private javax.swing.JCheckBox ltlCheckbox;
    private javax.swing.JTextField mileageField;
    private javax.swing.JLabel mileageLabel;
    private javax.swing.JTextField orderNumberField;
    private javax.swing.JLabel orderNumberLabel;
    private javax.swing.JTextField pieceField;
    private javax.swing.JLabel pieceLabel;
    private javax.swing.JCheckBox rampsCheckbox;
    private javax.swing.JTextField revenueField;
    private javax.swing.JLabel revenueLabel;
    private javax.swing.JCheckBox sigAndTalleyCheckbox;
    private javax.swing.JSpinner tarpHeightField;
    private javax.swing.JLabel tarpHeightLabel;
    private javax.swing.JCheckBox tarpedCheckbox;
    private javax.swing.JCheckBox teamCheckbox;
    private javax.swing.JCheckBox topCheckbox;
    private javax.swing.JTextField tripNumberField;
    private javax.swing.JLabel tripNumberLabel;
    private javax.swing.JCheckBox twicCheckbox;
    private javax.swing.JTextField weightField;
    private javax.swing.JLabel weightLabel;
    private javax.swing.JPanel xtraInfoPanel;
    // End of variables declaration//GEN-END:variables

}
