/*
 * Copyright (C) 2022 GS United Labs
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * *****************************************************************************
 *  Project    :   NTOS
 *  Class      :   LocalFieldValidation.java
 *  Author     :   Sean Carrick
 *  Created    :   Apr 4, 2022
 *  Modified   :   Apr 4, 2022
 *  
 *  Purpose: See class JavaDoc for explanation
 *  
 *  Revision History:
 *  
 *  WHEN          BY                   REASON
 *  ------------  -------------------  -----------------------------------------
 *  Apr 4, 2022  Sean Carrick         Initial creation.
 * *****************************************************************************
 */

package com.gs.ntos.loads.panels;

import java.awt.Color;
import javax.swing.text.JTextComponent;

/**
 *
 * @author Sean Carrick &lt;sean at gs-unitedlabs dot com&gt;
 * 
 * @version 1.0
 * @since 1.0
 */
public class LocalFieldValidation {
    
    /**
     * Optional value. This field specifies that the field value does not need
     * to be provided.
     */
    public static final int VALUE_OPTIONAL = -1;
    
    /**
     * Required value. This field specifies that the field value must be 
     * provided.
     */
    public static final int VALUE_REQUIRED = 0;
    
    /**
     * Partly required value. This field specifies that one of a selection of
     * fields must provide a value, but not all of the selection of fields need
     * to have values provided.
     */
    public static final int VALUE_PARTLY = 1;
    
    private static final Color REQUIRED = new Color(1.0f, 0.8f, 0.8f);
    private static final Color ACCEPTED = new Color(0.8f, 0.922f, 0.839f);
    private static final Color PART_REQUIRED = new Color(1.0f, 0.878f, 0.698f);
    
    private final JTextComponent[] components;
    private final int requirement;
    private final int min;
    private final int max;
    
    /**
     * Constructs a new `LocalFieldValidation` instance for the specified field(s),
     * requirement(s), and other field information. When constructing an instance
     * of the `LocalFieldValidation` class, any and all fields need to be
     * provided through this constructor, and the requirements for the fields
     * need to be specified in the same order. For instance, if this validation
     * class is for a single `JTextComponent` which requires a value to be
     * provided, this constructor would be called as:
     * ```java
     * LocalFieldValidation validator = new LocalFieldValidation(
     *         new JTextComponent[] {myTextField}, 
     *         LocalFieldValidation.REQUIRED);
     * ```
     * By calling this constructor in this manner, two arrays of a single item
     * each will be created. However, if there are multiple fields, of which at
     * least one must have a value provided, this constructor should be called as:
     * ```java
     * LocalFieldValidation validator = new LocalFieldValidation(
     *         new JTextComponent[] {myPhoneFIeld, myEmailField},
     *         JTextComponent.PART_REQUIRED);
     * ```
     * A `LocalFieldValidation` instance declared in this manner will return
     * `true` from the `validateFields()` method if one of the supplied fields
     * has data provided. This method does not attempt to determine the type of
     * data the field contains, so other classes will need to be used to validate
     * the data itself, such as email addresses, phone numbers, etc.
     * 
     * @param components an array of the `JTextComponent`s to validate
     * @param requirement the requirement of the data
     * @param min the minimum length of the data, if any
     * @param max the maximum length of the data, if any
     * 
     * @see #validateField(javax.swing.text.JTextComponent() 
     * 
     * @see com.gs.ntos.validators.EmailValidator
     * @see com.gs.ntos.validators.PhoneNumberValidator
     * @see com.gs.ntos.validators.PostalCodeValidator
     * @see com.gs.ntos.validators.StateOrProvinceValidator
     */
    public LocalFieldValidation (JTextComponent[] components, int requirement,
            int min, int max) {
        this.components = components;
        this.requirement = requirement;
        this.max = max;
        this.min = min;
    }
    
    /**
     * Performs simple validation on the fields this `LocalFieldValidation`
     * class was create using. Visual clues will be provided to the user, such
     * as changing the background color of the fields to a light red or light
     * orange for required and partly-required fields that have no value, 
     * respectively. Also, the background color will be changed to light green
     * for required or partly-required fields that have data provided to show 
     * that those fields have been accepted.
     * 
     * @return `true` if data is provided for all fields; `false` if any field
     *          has not had data provided if all fields are required
     */
    public boolean validateField() {
        boolean valid = true;   // Let's be positive here.
        
        if (min > 0) {
            for (JTextComponent c : components) {
                valid = c.getText().length() > min;
            }
            
            if (!valid) {
                return valid;
            }
        }
        
        if (max > 0) {
            for (JTextComponent c : components) {
                valid = c.getText().length() > max;
                
                if (valid) {
                    c.setText(c.getText().substring(0, max));
                    valid = true;
                }
            }
        }
        
        switch (requirement) {
            case VALUE_REQUIRED:
                for (JTextComponent c : components) {
                    if (c.getText() == null || c.getText().isBlank()
                            || c.getText().isEmpty() || c.getText().length() == 0) {
                        c.setBackground(REQUIRED);
                        return false;
                    } else {
                        c.setBackground(ACCEPTED);
                        valid = true;
                    }
                }
                break;
            case VALUE_PARTLY:
                int countWithData = 0;
                for (JTextComponent c : components) {
                    if (c.getText() != null && !c.getText().isBlank()
                            && !c.getText().isEmpty())
                        countWithData ++;
                }
                
                valid = countWithData > 0;
                
                if (!valid) {
                    for (JTextComponent c : components) {
                        c.setBackground(PART_REQUIRED);
                    }
                } else {
                    for (JTextComponent c : components) {
                        c.setBackground(ACCEPTED);
                    }
                }
        }
        
        return valid;
    }

}
