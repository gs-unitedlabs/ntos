/*
 * Copyright (C) 2022 GS United Labs
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * *****************************************************************************
 *  Project    :   NTOS
 *  Class      :   SummaryPanel.java
 *  Author     :   Sean Carrick
 *  Created    :   Apr 8, 2022
 *  Modified   :   Apr 8, 2022
 *  
 *  Purpose: See class JavaDoc for explanation
 *  
 *  Revision History:
 *  
 *  WHEN          BY                   REASON
 *  ------------  -------------------  -----------------------------------------
 *  Apr 8, 2022  Sean Carrick         Initial creation.
 * *****************************************************************************
 */

package com.gs.ntos.loads.panels;

import com.gs.ntos.api.DesktopProgram;
import com.gs.ntos.database.models.Broker;
import com.gs.ntos.database.models.Customer;
import com.gs.ntos.database.models.Stop;
import com.gs.ntos.database.tables.CustomersTable;
import com.gs.ntos.enums.Level;
import com.gs.ntos.logging.Logger;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;
import org.netbeans.spi.wizard.WizardPage;

/**
 *
 * @author Sean Carrick
 */
public class SummaryPanel extends WizardPage {

    private static final long serialVersionUID = 3181469093100666882L;
    
    private final DesktopProgram program = DesktopProgram.getInstance();
    private final ResourceBundle bundle;
    private final Logger logger;
    private final CustomersTable customers;
    
    private Map map;
    private Customer customer;

    /** Creates new form SummaryPanel */
    public SummaryPanel(Map map) {
        logger = new Logger(program, getClass().getSimpleName(), (Level)
                program.getProperties().getSystemProperty("logging.level", 
                        Level.ALL));
        bundle = ResourceBundle.getBundle("com/gs/ntos/loads/resources/Bundle");
        customers = new CustomersTable();
        this.map = map;
        
        initComponents();
        
        initSummary();
    }

    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane1 = new javax.swing.JScrollPane();
        summaryEditor = new javax.swing.JEditorPane();

        jScrollPane1.setViewportView(summaryEditor);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 736, Short.MAX_VALUE)
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 407, Short.MAX_VALUE)
                .addContainerGap())
        );
    }// </editor-fold>//GEN-END:initComponents

    /* This method takes care of reading all of the input that was gathered from
     * the `settings` map and creating a summary report of what was set for the
     * load's information. By having this summary report, the user will be able
     * to review the information supplied and to go back and change things that
     * are not correct, prior to actually finishing the wizard and having the
     * data written to the database tables.
     */
    private void initSummary() {
        StringBuilder sb = new StringBuilder();
        
        // Get our numbers (gross pay, miles, weight, and pieces) and parse them
        //+ to numerical values.
        int miles = 0, pieces = 0;
        double weight = 0.0d;
        double pay = 0.0f;
        
        try {
            miles = Integer.parseInt(map.get("loadedMiles").toString());
            pieces = Integer.parseInt(map.get("pieces") != null 
                    ? map.get("pieces").toString() : "0");
            pay = Double.parseDouble(map.get("grossRevnue") != null 
                    ? map.get("grossRevenue").toString() : "0.00");
            weight = Double.parseDouble(map.get("weight") != null 
                    ? map.get("weight").toString() : "0.00");
        } catch (NumberFormatException ex) {
            logger.warning("Unable to parse a number: {0}", ex.getMessage());
        }
        
        sb.append("<html><body><h1 style=\"text-align: center\">");
        sb.append(bundle.getString("SummaryPanel.title")).append("</h1>");
        sb.append("<h2>").append(bundle.getString("SummaryPanel.overview.title"));
        sb.append("</h2><table><thead<tr><th>");
        sb.append(bundle.getString("SummaryPanel.table.header0")).append("</th>");
        sb.append("<th>").append(bundle.getString("SummaryPanel.table.header1"));
        sb.append("</th></tr></thead><tbody><tr><td>");
        sb.append(bundle.getString("SummaryPanel.orderNumber.text"));
        sb.append("</td><td>").append((String) map.get("orderNumber"));
        sb.append("</td></tr><tr><td>");
        sb.append(bundle.getString("SummaryPanel.tripNumber.text"));
        sb.append("</td><td>").append((String) map.get("tripNumber"));
        sb.append("</td></tr><tr><td>");
        sb.append(bundle.getString("SummaryPanel.grossPay.text"));
        sb.append("</td><td>").append(NumberFormat.getCurrencyInstance()
                .getCurrency().getSymbol());
        sb.append(DecimalFormat.getNumberInstance().format(
                Double.valueOf(map.get("grossPay").toString())));
        sb.append(" (").append(NumberFormat.getCurrencyInstance().getCurrency()
                .getCurrencyCode()).append(")");
        sb.append("</td></tr><tr><td>");
        sb.append(bundle.getString("SummaryPanel.mileage.text"));
        sb.append("</td><td>").append(
                DecimalFormat.getIntegerInstance().format(miles));
        sb.append("</td></tr><tr><td>");
        sb.append(bundle.getString("SummaryPanel.commodity.text"));
        sb.append("</td><td>").append((String) map.get("commodity"));
        sb.append("</td></tr><tr><td>");
        sb.append(bundle.getString("SummaryPanel.weight.text"));
        sb.append("</td><td>").append(
                DecimalFormat.getNumberInstance().format(weight));
        sb.append("</td></tr><tr><td>");
        sb.append(bundle.getString("SummaryPanel.pieces.text"));
        sb.append("</td><td>").append(
                DecimalFormat.getIntegerInstance().format(pieces));
        sb.append("</td></tr></tbody></table>");
        
        sb.append("<p>").append(bundle.getString("SummaryPanel.details.title"));
        sb.append("<br><br>");
        
        // Only print the data for the checkboxes if they are selected.
        if (Boolean.valueOf(map.get("hazMat").toString()) == true) {
            sb.append(bundle.getString("SummaryPanel.hazMat.text")).append("<br>");
        }
        if (Boolean.valueOf(map.get("team").toString()) == true) {
            sb.append(bundle.getString("SummaryPanel.team.text")).append("<br>");
        }
        if (Boolean.valueOf(map.get("ltl").toString()) == true) {
            sb.append(bundle.getString("SummaryPanel.ltl.text")).append("<br>");
            sb.append("<div style=\"float: right; background-color: ");
            sb.append("CornflowerBlue;\">");
            sb.append(bundle.getString("SummaryPanel.addStops.text"));
            sb.append("</div><br>");
        }
        if (Boolean.valueOf(map.get("top").toString()) == true) {
            sb.append(bundle.getString("SummaryPanel.top.text")).append("<br>");
        }
        if (Boolean.valueOf(map.get("twic").toString()) == true) {
            sb.append(bundle.getString("SummaryPanel.twic.text")).append("<br>");
        }
        if (Boolean.valueOf(map.get("sigTalley").toString()) == true) {
            sb.append(bundle.getString("SummaryPanel.sigTalley.text")).append("<br>");
        }
        if (Boolean.valueOf(map.get("ramps").toString()) == true) {
            sb.append(bundle.getString("SummaryPanel.ramps.text")).append("<br>");
        }
        if (Boolean.valueOf(map.get("tarped").toString()) == true) {
            sb.append(bundle.getString("SummaryPanel.tarped.text"));
            sb.append(map.get("tarpHeight").toString());
            sb.append(bundle.getString("SummaryPanel.tarpHeight.text"));
            sb.append("<br></p>");
        }
        
        sb.append("<h2>").append(bundle.getString("SummaryPanel.broker.title"));
        sb.append("</h2><p>").append(bundle.getString("SummaryPanel.address.text"));
        sb.append(((Broker) map.get("broker")).getPrintableAddress());
        sb.append("</p><h2>").append(bundle.getString("SummaryPanel.stops.title"));
        sb.append("</h2><p>");
        
        List<Customer> customers = (List<Customer>) map.get("stopCustomers");
        List<Stop> stops = (List<Stop>) map.get("stops");
        
        for (Customer c : customers) {
            Stop s = stops.get(customers.indexOf(c));
            sb.append(bundle.getString("SummaryPanel.address.text"));
            sb.append(bundle.getString("SummaryPanel.stop.text"));
            sb.append(customers.indexOf(c) + 1).append("<br>");
            sb.append(c.getPrintableAddress()).append("<br><table><thead><tr><th>");
            sb.append(bundle.getString("SummaryPanel.table2.header0"));
            sb.append("</th><th>").append(bundle.getString("SummaryPanel.table2.header1"));
            sb.append("</th><th>").append(bundle.getString("SummaryPanel.table2.header2"));
            sb.append("</th></tr></thead><tbody><tr><td>");
            sb.append(bundle.getString("SummaryPanel.early.text"));
            sb.append("</td><td>").append(s.getEarlyDate()).append("</td><td>");
            sb.append(s.getEarlyTime()).append("</td></tr><tr><td>");
            sb.append(bundle.getString("SummaryPanel.late.text"));
            sb.append("</td><td>").append(s.getLateDate()).append("</td><td>");
            sb.append(s.getLateTime()).append("</td></tr></table>");
            sb.append("<hr><br>");
        }
        
        // Place the built-up HTML page into our EditorPane, after setting the
        //+ appropriate properties of it.
        summaryEditor.setContentType("text/html");
        summaryEditor.setEditable(false);
        summaryEditor.setText(sb.toString());
        summaryEditor.select(0, 0);
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JEditorPane summaryEditor;
    // End of variables declaration//GEN-END:variables

}
