/*
 * Copyright (C) 2022 GS United Labs
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * *****************************************************************************
 *  Project    :   NTOS
 *  Class      :   CustomerSelectorDialog.java
 *  Author     :   Sean Carrick
 *  Created    :   Apr 6, 2022
 *  Modified   :   Apr 6, 2022
 *  
 *  Purpose: See class JavaDoc for explanation
 *  
 *  Revision History:
 *  
 *  WHEN          BY                   REASON
 *  ------------  -------------------  -----------------------------------------
 *  Apr 6, 2022  Sean Carrick         Initial creation.
 * *****************************************************************************
 */

package com.gs.ntos.loads;

import com.gs.ntos.api.DesktopProgram;
import com.gs.ntos.customer.NewCustomerDialog;
import com.gs.ntos.database.errors.DataStoreException;
import com.gs.ntos.database.models.Customer;
import com.gs.ntos.database.models.Stop;
import com.gs.ntos.database.tables.CustomersTable;
import com.gs.ntos.enums.Level;
import com.gs.ntos.logging.Logger;
import java.awt.event.ActionEvent;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.ResourceBundle;
import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.ImageIcon;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.text.JTextComponent;

/**
 *
 * @author Sean Carrick
 */
public class CustomerSelectorDialog extends javax.swing.JDialog
        implements DocumentListener {
    
    private final CustomerSelectorDialog instance;
    
    private final DesktopProgram program;
    private final Logger logger;
    private final ResourceBundle bundle;
    private final CustomersTable table;
    
    private final Action filter;
    private final Action cancel;
    private final Action help;
    private final Action save;
    private Date today;
    
    private Customer[] customers;
    private Customer customer;
    private Stop stop;
    
    private boolean cancelled;
    private boolean loading;
    
    /** Creates new form CustomerSelectorDialog */
    public CustomerSelectorDialog(DesktopProgram program) {
        super(program.getMainFrame(), true);
        this.program = program;
        logger = new Logger(program, getClass().getSimpleName(), (Level)
                program.getProperties().getSystemProperty("logging.level", 
                Level.ALL));
        bundle = ResourceBundle.getBundle("com/gs/ntos/loads/resources/Bundle");
        table = new CustomersTable();
        SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
        today = new Date();
        String td = sdf.format(today);
        try {
            today = sdf.parse(td);
        } catch (ParseException ex) {
            logger.error(ex, "Parsing today's date ({0}) from the string {1}", today, td);
        }
        
        filter = new FilterAction();
        cancel = new CancelAction();
        help = new HelpAction();
        save = new SaveAction();
        
        loading = true;
        
        initComponents();
        
        validateInput();
        
        cancelled = true;
        
        instance = this;
        
        loading = false;
    }
    
    public boolean isCancelled() {
        return cancelled;
    }

    @Override
    public void insertUpdate(DocumentEvent e) {
        changedUpdate(e);
    }

    @Override
    public void removeUpdate(DocumentEvent e) {
        changedUpdate(e);
    }

    @Override
    public void changedUpdate(DocumentEvent e) {
        switch (e.getDocument().getProperty("owner").toString()) {
            case "valueField":
                filter.setEnabled(e.getDocument().getLength() > 0
                        && (cityOption.isSelected() 
                        || companyOption.isSelected()
                        || stateOption.isSelected()));
                break;
            case "earlyTimeField":
                lateTimeField.setText(earlyTimeField.getText());
            default:
                validateInput();
        }
    }
    
    public Customer getSelectedCustomer() {
        return customer;
    }
    
    public Stop getStop() {
        return stop;
    }
    
    private void validateInput() {
        logger.entering("validateInput()");
        
        if (customerCombobox.getSelectedIndex() <= 1) {
            logger.debug("Validating the customerCombobox.selectedIndex = {0}",
                    customerCombobox.getSelectedIndex());
            tipLabel.setText(bundle.getString("CustomersSelectorDialog.problemText"));
        } else {
            logger.debug("Validating the customerCombobox.selectedIndex = {0}",
                    customerCombobox.getSelectedIndex());
            tipLabel.setText("");
        }
        
        logger.debug("customerCombobox.getSelectedIndex >= 1 ({0})", 
                customerCombobox.getSelectedIndex() >= 1);
        logger.debug("earlyDateField.isEditValid() = {0}", 
                earlyDateField.isEditValid());
        logger.debug("validDate(earlyDateField.getDate()) = {0}",
                validDate(earlyDateField.getDate()));
        logger.debug("validTime() = {0}",
                validTime());
        logger.debug("lateDateField.isEditValid() = {0}",
                lateDateField.isEditValid());
        logger.debug("validDate(lateDateField.getDate(), earlyDateField.getDate()) = {0}",
                validDate(lateDateField.getDate(), earlyDateField.getDate()));
        save.setEnabled((customerCombobox.getSelectedIndex() >= 1)
                && earlyDateField.isEditValid()
                && validDate(earlyDateField.getDate())
                && lateDateField.isEditValid()
                && validDate(lateDateField.getDate(), earlyDateField.getDate())
                && validTime());
        
        logger.exiting("validateInput()");
    }
    
    private boolean validTime() {
        
        if (!earlyTimeField.getText().equals("  :  ") 
                && !lateTimeField.getText().equals("  :  ")) {
            String hour = earlyTimeField.getText().split(":")[0].trim();
            String mins = earlyTimeField.getText().split(":")[1].trim();
            
            if (hour.length() != 2 || mins.length() != 2) {
                return false;
            }
            
            hour = lateTimeField.getText().split(":")[0].trim();
            mins = lateTimeField.getText().split(":")[1].trim();
            
            if (hour.length() != 2 | mins.length() != 2) {
                return false;
            }
            
            SimpleDateFormat sdf = new SimpleDateFormat("HH:mm");
            
            try {
                sdf.parse(earlyTimeField.getText());
                sdf.parse(lateTimeField.getText());
            } catch (ParseException ex) {
                return false;
            }
            return true;
        }
        
        return false;
    }
    
    private boolean validDate(Date date) {
        logger.entering("validDate(Date date = {0})", date);
        
        logger.debug("date.compareTo(today = {0}) = {1}", today, date == null ? "NULL" : date.compareTo(today));
        logger.exiting("validDate(Date date): RETURNING: {0}", date == null ? "NULL" : date.compareTo(today));
        return date != null ? date.compareTo(today) >= 0 : false;
    }
    
    private boolean validDate(Date lateDate, Date earlyDate) {
        logger.entering("validDate(Date lateDate = {0}, Date earlyDate = {1})", lateDate, earlyDate);
        
        logger.debug("lateDate.compareTo(earlyDate = {0}) = {1}", today, (lateDate == null || earlyDate == null) ? "NULL" : lateDate.compareTo(earlyDate));
        logger.exiting("validDate(Date lateDate, Date earlyDate): RETURNING: {0}", (lateDate == null || earlyDate == null) ? "NULL" : lateDate.compareTo(earlyDate));
        return (lateDate != null && earlyDate != null) ? validDate(earlyDate) 
                && lateDate.compareTo(earlyDate) >= 0
                : false;
    }
    
    private void loadCustomersList() {
        customerCombobox.removeAllItems();
        customerCombobox.addItem("Select Customer...");
        customerCombobox.addItem("Add New Customer...");
        
        for (Customer c : customers) {
            customerCombobox.addItem(c.getCompany() + " (" + c.getCity() 
                    + ", " + c.getStateOrProvince() + ")");
        }
    }

    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        filterButtonGroup = new javax.swing.ButtonGroup();
        bottomPanel = new javax.swing.JPanel();
        commandSeparator = new javax.swing.JSeparator();
        commandPanel = new javax.swing.JPanel();
        helpButton = new javax.swing.JButton();
        saveButton = new javax.swing.JButton();
        cancelButton = new javax.swing.JButton();
        centerPanel = new javax.swing.JPanel();
        tipLabel = new javax.swing.JLabel();
        stopPanel = new javax.swing.JPanel();
        selectionPanel = new javax.swing.JPanel();
        filterPanel = new javax.swing.JPanel();
        companyOption = new javax.swing.JRadioButton();
        cityOption = new javax.swing.JRadioButton();
        stateOption = new javax.swing.JRadioButton();
        valueLabel = new javax.swing.JLabel();
        valueField = new javax.swing.JTextField();
        filterButton = new javax.swing.JButton();
        customerLabel = new javax.swing.JLabel();
        customerCombobox = new javax.swing.JComboBox<>();
        earlyDateLabel = new javax.swing.JLabel();
        earlyDateField = new org.jdesktop.swingx.JXDatePicker();
        earlyTimeLabel = new javax.swing.JLabel();
        earlyTimeField = new javax.swing.JFormattedTextField();
        lateDateLabel = new javax.swing.JLabel();
        lateDateField = new org.jdesktop.swingx.JXDatePicker();
        lateTimeLabel = new javax.swing.JLabel();
        lateTimeField = new javax.swing.JFormattedTextField();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setName("Form"); // NOI18N

        bottomPanel.setName("bottomPanel"); // NOI18N
        bottomPanel.setLayout(new java.awt.BorderLayout());

        commandSeparator.setName("commandSeparator"); // NOI18N
        bottomPanel.add(commandSeparator, java.awt.BorderLayout.PAGE_START);

        commandPanel.setName("commandPanel"); // NOI18N

        helpButton.setAction(help);
        helpButton.setName("helpButton"); // NOI18N

        saveButton.setAction(save);
        saveButton.setName("saveButton"); // NOI18N

        cancelButton.setAction(cancel);
        cancelButton.setName("cancelButton"); // NOI18N

        javax.swing.GroupLayout commandPanelLayout = new javax.swing.GroupLayout(commandPanel);
        commandPanel.setLayout(commandPanelLayout);
        commandPanelLayout.setHorizontalGroup(
            commandPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(commandPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(helpButton)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 508, Short.MAX_VALUE)
                .addComponent(saveButton)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(cancelButton)
                .addContainerGap())
        );
        commandPanelLayout.setVerticalGroup(
            commandPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(commandPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(commandPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(helpButton)
                    .addComponent(saveButton)
                    .addComponent(cancelButton))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        bottomPanel.add(commandPanel, java.awt.BorderLayout.CENTER);

        getContentPane().add(bottomPanel, java.awt.BorderLayout.PAGE_END);

        centerPanel.setName("centerPanel"); // NOI18N
        centerPanel.setLayout(new java.awt.BorderLayout());

        tipLabel.setText(bundle.getString("CustomerSelectorDialog.tipLabel.text")); // NOI18N
        tipLabel.setName("tipLabel"); // NOI18N
        centerPanel.add(tipLabel, java.awt.BorderLayout.PAGE_END);

        stopPanel.setName("stopPanel"); // NOI18N

        selectionPanel.setBorder(javax.swing.BorderFactory.createTitledBorder(bundle.getString("CustomerSelectorDialog.selectionPanel.border.title"))); // NOI18N
        selectionPanel.setName("selectionPanel"); // NOI18N

        filterPanel.setBorder(javax.swing.BorderFactory.createTitledBorder(bundle.getString("CustomerSelectorDialog.filterPanel.border.title"))); // NOI18N
        filterPanel.setName("filterPanel"); // NOI18N

        filterButtonGroup.add(companyOption);
        companyOption.setMnemonic(bundle.getString("CustomerSelectorDialog.companyOption.mnemonic").charAt(0));
        companyOption.setText(bundle.getString("CustomerSelectorDialog.companyOption.text")); // NOI18N
        companyOption.setName("companyOption"); // NOI18N
        companyOption.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                enableFilterAction(evt);
            }
        });

        filterButtonGroup.add(cityOption);
        cityOption.setMnemonic(bundle.getString("CustomerSelectorDialog.cityOption.mnemonic").charAt(0));
        cityOption.setText(bundle.getString("CustomerSelectorDialog.cityOption.text")); // NOI18N
        cityOption.setName("cityOption"); // NOI18N
        cityOption.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                enableFilterAction(evt);
            }
        });

        filterButtonGroup.add(stateOption);
        stateOption.setMnemonic(bundle.getString("CustomerSelectorDialog.stateOption.mnemonic").charAt(0));
        stateOption.setText(bundle.getString("CustomerSelectorDialog.stateOption.text")); // NOI18N
        stateOption.setName("stateOption"); // NOI18N
        stateOption.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                enableFilterAction(evt);
            }
        });

        valueLabel.setDisplayedMnemonic(bundle.getString("CustomerSelectorDialog.valueLabel.mnemonic").charAt(0));
        valueLabel.setLabelFor(valueField);
        valueLabel.setText(bundle.getString("CustomerSelectorDialog.valueLabel.text")); // NOI18N
        valueLabel.setName("valueLabel"); // NOI18N

        valueField.setText(bundle.getString("CustomerSelectorDialog.valueField.text")); // NOI18N
        valueField.setName("valueField"); // NOI18N
        valueField.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                selectText(evt);
            }
        });

        filterButton.setAction(filter);
        filterButton.setText(bundle.getString("CustomerSelectorDialog.filterButton.text")); // NOI18N
        filterButton.setMaximumSize(new java.awt.Dimension(29, 29));
        filterButton.setMinimumSize(new java.awt.Dimension(29, 29));
        filterButton.setName("filterButton"); // NOI18N
        filterButton.setPreferredSize(new java.awt.Dimension(29, 29));

        javax.swing.GroupLayout filterPanelLayout = new javax.swing.GroupLayout(filterPanel);
        filterPanel.setLayout(filterPanelLayout);
        filterPanelLayout.setHorizontalGroup(
            filterPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(filterPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(companyOption)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(cityOption)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(stateOption)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(valueLabel)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(valueField, javax.swing.GroupLayout.DEFAULT_SIZE, 116, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(filterButton, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        filterPanelLayout.setVerticalGroup(
            filterPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(filterPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(filterPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(companyOption)
                    .addComponent(cityOption)
                    .addComponent(stateOption)
                    .addComponent(valueLabel)
                    .addComponent(valueField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(filterButton, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        customerLabel.setDisplayedMnemonic(bundle.getString("CustomerSelectorDialog.customerLabel.mnemonic").charAt(0));
        customerLabel.setLabelFor(customerCombobox);
        customerLabel.setText(bundle.getString("CustomerSelectorDialog.customerLabel.text")); // NOI18N
        customerLabel.setName("customerLabel"); // NOI18N

        customerCombobox.setName("customerCombobox"); // NOI18N
        customerCombobox.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                customerComboboxItemStateChanged(evt);
            }
        });

        javax.swing.GroupLayout selectionPanelLayout = new javax.swing.GroupLayout(selectionPanel);
        selectionPanel.setLayout(selectionPanelLayout);
        selectionPanelLayout.setHorizontalGroup(
            selectionPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(selectionPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(selectionPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(filterPanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(selectionPanelLayout.createSequentialGroup()
                        .addComponent(customerLabel)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(customerCombobox, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                .addContainerGap())
        );
        selectionPanelLayout.setVerticalGroup(
            selectionPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(selectionPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(filterPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addGroup(selectionPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(customerLabel)
                    .addComponent(customerCombobox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        earlyDateLabel.setDisplayedMnemonic(bundle.getString("CustomerSelectorDialog.earlyDateLabel.mnemonic").charAt(0));
        earlyDateLabel.setLabelFor(earlyDateField);
        earlyDateLabel.setText(bundle.getString("CustomerSelectorDialog.earlyDateLabel.text")); // NOI18N
        earlyDateLabel.setName("earlyDateLabel"); // NOI18N

        earlyDateField.setName("earlyDateField"); // NOI18N
        earlyDateField.addInputMethodListener(new java.awt.event.InputMethodListener() {
            public void caretPositionChanged(java.awt.event.InputMethodEvent evt) {
            }
            public void inputMethodTextChanged(java.awt.event.InputMethodEvent evt) {
                earlyDateFieldInputMethodTextChanged(evt);
            }
        });
        earlyDateField.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                earlyDateFieldActionPerformed(evt);
            }
        });

        earlyTimeLabel.setDisplayedMnemonic(bundle.getString("CustomerSelectorDialog.earlyTimeLabel.mnemonic").charAt(0));
        earlyTimeLabel.setLabelFor(earlyTimeField);
        earlyTimeLabel.setText(bundle.getString("CustomerSelectorDialog.earlyTimeLabel.text")); // NOI18N
        earlyTimeLabel.setName("earlyTimeLabel"); // NOI18N

        try {
            earlyTimeField.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.MaskFormatter("##:##")));
        } catch (java.text.ParseException ex) {
            ex.printStackTrace();
        }
        earlyTimeField.setName("earlyTimeField"); // NOI18N
        earlyTimeField.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                selectText(evt);
            }
        });
        earlyTimeField.addInputMethodListener(new java.awt.event.InputMethodListener() {
            public void caretPositionChanged(java.awt.event.InputMethodEvent evt) {
            }
            public void inputMethodTextChanged(java.awt.event.InputMethodEvent evt) {
                earlyTimeFieldInputMethodTextChanged(evt);
            }
        });
        earlyTimeField.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                earlyTimeFieldKeyReleased(evt);
            }
        });

        lateDateLabel.setDisplayedMnemonic(bundle.getString("CustomerSelectorDialog.lateDateLabel.mnemonic").charAt(0));
        lateDateLabel.setLabelFor(lateDateField);
        lateDateLabel.setText(bundle.getString("CustomerSelectorDialog.lateDateLabel.text")); // NOI18N
        lateDateLabel.setName("lateDateLabel"); // NOI18N

        lateDateField.setName("lateDateField"); // NOI18N
        lateDateField.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                lateDateFieldActionPerformed(evt);
            }
        });

        lateTimeLabel.setDisplayedMnemonic(bundle.getString("CustomerSelectorDialog.lateTimeLabel.mnemonic").charAt(0));
        lateTimeLabel.setLabelFor(lateTimeField);
        lateTimeLabel.setText(bundle.getString("CustomerSelectorDialog.lateTimeLabel.text")); // NOI18N
        lateTimeLabel.setName("lateTimeLabel"); // NOI18N

        try {
            lateTimeField.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.MaskFormatter("##:##")));
        } catch (java.text.ParseException ex) {
            ex.printStackTrace();
        }
        lateTimeField.setName("lateTimeField"); // NOI18N
        lateTimeField.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                selectText(evt);
            }
        });
        lateTimeField.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                lateTimeFieldKeyReleased(evt);
            }
        });

        javax.swing.GroupLayout stopPanelLayout = new javax.swing.GroupLayout(stopPanel);
        stopPanel.setLayout(stopPanelLayout);
        stopPanelLayout.setHorizontalGroup(
            stopPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(stopPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(stopPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(selectionPanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(stopPanelLayout.createSequentialGroup()
                        .addGroup(stopPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(lateDateLabel)
                            .addComponent(earlyDateLabel))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(stopPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(stopPanelLayout.createSequentialGroup()
                                .addComponent(earlyDateField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(earlyTimeLabel))
                            .addGroup(stopPanelLayout.createSequentialGroup()
                                .addComponent(lateDateField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(lateTimeLabel)))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(stopPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(lateTimeField, javax.swing.GroupLayout.DEFAULT_SIZE, 66, Short.MAX_VALUE)
                            .addComponent(earlyTimeField))))
                .addContainerGap())
        );
        stopPanelLayout.setVerticalGroup(
            stopPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(stopPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(selectionPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addGroup(stopPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(earlyDateLabel)
                    .addComponent(earlyDateField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(earlyTimeLabel)
                    .addComponent(earlyTimeField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(stopPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lateDateLabel)
                    .addComponent(lateDateField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lateTimeLabel)
                    .addComponent(lateTimeField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(51, Short.MAX_VALUE))
        );

        centerPanel.add(stopPanel, java.awt.BorderLayout.CENTER);
        try {
            table.connect();
            customers = table.getAllRecords();
            loadCustomersList();
        } catch (DataStoreException ex) {
            logger.error(ex, "Unable to open CustomersTable: {0}", table);
        }

        getContentPane().add(centerPanel, java.awt.BorderLayout.CENTER);
        setTitle(bundle.getString("CustomerSelectorDialog.title"));
        setIconImage(new ImageIcon(getClass().getResource(bundle.getString("CustomerSelectorDialog.icon"))).getImage());
        getRootPane().setDefaultButton(saveButton);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void enableFilterAction(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_enableFilterAction
        filter.setEnabled((companyOption.isSelected() 
                || cityOption.isSelected() 
                || stateOption.isSelected())
                && valueField.getText().length() > 0);
    }//GEN-LAST:event_enableFilterAction

    private void selectText(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_selectText
        if (evt.getSource() instanceof JTextComponent) {
            ((JTextComponent) evt.getSource()).selectAll();
        }
    }//GEN-LAST:event_selectText

    private void earlyDateFieldInputMethodTextChanged(java.awt.event.InputMethodEvent evt) {//GEN-FIRST:event_earlyDateFieldInputMethodTextChanged
        
        lateDateField.setDate(earlyDateField.getDate());
    }//GEN-LAST:event_earlyDateFieldInputMethodTextChanged

    private void customerComboboxItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_customerComboboxItemStateChanged
        switch (customerCombobox.getSelectedIndex()) {
            case 0:
                break;
            case 1:
                NewCustomerDialog dlg = new NewCustomerDialog(program, this, true);
                program.show(dlg);
                break;
            default:
                validateInput();
                if (!loading) {
                    customer = customers[customerCombobox.getSelectedIndex() - 2];
                }
        }

    }//GEN-LAST:event_customerComboboxItemStateChanged

    private void earlyTimeFieldInputMethodTextChanged(java.awt.event.InputMethodEvent evt) {//GEN-FIRST:event_earlyTimeFieldInputMethodTextChanged
        lateTimeField.setText(earlyTimeField.getText());
    }//GEN-LAST:event_earlyTimeFieldInputMethodTextChanged

    private void earlyDateFieldActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_earlyDateFieldActionPerformed
        lateDateField.setDate(earlyDateField.getDate());
        lateDateField.getEditor().setText(earlyDateField.getEditor().getText());
        validateInput();
    }//GEN-LAST:event_earlyDateFieldActionPerformed

    private void earlyTimeFieldKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_earlyTimeFieldKeyReleased
        lateTimeField.setText(earlyTimeField.getText());
        validateInput();
    }//GEN-LAST:event_earlyTimeFieldKeyReleased

    private void lateDateFieldActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_lateDateFieldActionPerformed
        validateInput();
    }//GEN-LAST:event_lateDateFieldActionPerformed

    private void lateTimeFieldKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_lateTimeFieldKeyReleased
        validateInput();
    }//GEN-LAST:event_lateTimeFieldKeyReleased

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JPanel bottomPanel;
    private javax.swing.JButton cancelButton;
    private javax.swing.JPanel centerPanel;
    private javax.swing.JRadioButton cityOption;
    private javax.swing.JPanel commandPanel;
    private javax.swing.JSeparator commandSeparator;
    private javax.swing.JRadioButton companyOption;
    private javax.swing.JComboBox<String> customerCombobox;
    private javax.swing.JLabel customerLabel;
    private org.jdesktop.swingx.JXDatePicker earlyDateField;
    private javax.swing.JLabel earlyDateLabel;
    private javax.swing.JFormattedTextField earlyTimeField;
    private javax.swing.JLabel earlyTimeLabel;
    private javax.swing.JButton filterButton;
    private javax.swing.ButtonGroup filterButtonGroup;
    private javax.swing.JPanel filterPanel;
    private javax.swing.JButton helpButton;
    private org.jdesktop.swingx.JXDatePicker lateDateField;
    private javax.swing.JLabel lateDateLabel;
    private javax.swing.JFormattedTextField lateTimeField;
    private javax.swing.JLabel lateTimeLabel;
    private javax.swing.JButton saveButton;
    private javax.swing.JPanel selectionPanel;
    private javax.swing.JRadioButton stateOption;
    private javax.swing.JPanel stopPanel;
    private javax.swing.JLabel tipLabel;
    private javax.swing.JTextField valueField;
    private javax.swing.JLabel valueLabel;
    // End of variables declaration//GEN-END:variables

    private class FilterAction extends AbstractAction {

        private static final long serialVersionUID = 8055766104854209128L;
        
        public FilterAction() {
            super(bundle.getString("FilterAction.text"));
            
            initAction();
        }
        
        private void initAction() {
            putValue(SMALL_ICON, new ImageIcon(getClass().getResource(
                    bundle.getString("FilterAction.smallIcon"))));
            putValue(SHORT_DESCRIPTION,
                    bundle.getString("FilterAction.shortDescription"));
        }

        @Override
        public void actionPerformed(ActionEvent e) {
            throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        }
        
    }
    
    private class SaveAction extends AbstractAction {

        private static final long serialVersionUID = -7232581254088203628L;

        public SaveAction() {
            super(bundle.getString("SaveAction.text"));
            
            initAction();
        }
        
        private void initAction() {
            putValue(SMALL_ICON, new ImageIcon(getClass().getResource(
                    bundle.getString("SaveAction.smallIcon"))));
            putValue(DISPLAYED_MNEMONIC_INDEX_KEY, Integer.valueOf(
                    bundle.getString("SaveAction.mnemonicIndex")));
            putValue(SHORT_DESCRIPTION, 
                    bundle.getString("SaveAction.shortDescription"));
        }
        
        @Override
        public void actionPerformed(ActionEvent e) {
            cancelled = false;
            stop = new Stop();
            stop.setCustomerId(customer.getId());
            earlyDateField.setFormats(new SimpleDateFormat(Stop.LOAD_DATE_FORMAT));
            stop.setEarlyDate(earlyDateField.getEditor().getText());
            stop.setEarlyTime(earlyTimeField.getText());
            lateDateField.setFormats(new SimpleDateFormat(Stop.LOAD_DATE_FORMAT));
            stop.setLateDate(lateDateField.getEditor().getText());
            stop.setLateTime(lateTimeField.getText());
            setVisible(false);
            try {
                table.close();
            } catch (DataStoreException ex) {
                logger.error(ex, "Attempting to close the table ({0}) after "
                        + "adding the data: {1}", table, customer);
            }
                        
            logger.exiting("CustomerSelectionDialog.SaveAction.actionPerformed(e).");
            instance.setVisible(false);
        }
        
    }
    
    private class CancelAction extends AbstractAction {

        private static final long serialVersionUID = -6089139508449217744L;
        
        public CancelAction() {
            super(bundle.getString("CancelAction.text"));
            
            initAction();
        }
        
        private void initAction() {
            putValue(SMALL_ICON, new ImageIcon(getClass().getResource(
                    bundle.getString("CancelAction.smallIcon"))));
            putValue(DISPLAYED_MNEMONIC_INDEX_KEY, Integer.valueOf(
                    bundle.getString("CancelAction.mnemonicIndex")));
            putValue(SHORT_DESCRIPTION, 
                    bundle.getString("CancelAction.shortDescription"));
        }

        @Override
        public void actionPerformed(ActionEvent e) {
            cancelled = true;
            instance.setVisible(false);
        }
        
    }
    
    private class HelpAction extends AbstractAction {

        private static final long serialVersionUID = 9195556010988315538L;
        
        public HelpAction() {
            super(bundle.getString("HelpAction.text"));
            
            initAction();
        }
        
        private void initAction() {
            putValue(SMALL_ICON, new ImageIcon(getClass().getResource(
                    bundle.getString("HelpAction.smallIcon"))));
            putValue(DISPLAYED_MNEMONIC_INDEX_KEY, Integer.valueOf(
                    bundle.getString("HelpAction.mnemonicIndex")));
            putValue(SHORT_DESCRIPTION, 
                    bundle.getString("HelpAction.shortDescription"));
        }

        @Override
        public void actionPerformed(ActionEvent e) {
            throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        }
        
    }
    
}
