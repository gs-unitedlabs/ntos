/*
 * Copyright (C) 2022 GS United Labs
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * *****************************************************************************
 *  Project    :   NTOS
 *  Class      :   StartLoadDialog.java
 *  Author     :   Sean Carrick
 *  Created    :   Apr 9, 2022
 *  Modified   :   Apr 9, 2022
 *  
 *  Purpose: See class JavaDoc for explanation
 *  
 *  Revision History:
 *  
 *  WHEN          BY                   REASON
 *  ------------  -------------------  -----------------------------------------
 *  Apr 9, 2022  Sean Carrick         Initial creation.
 * *****************************************************************************
 */

package com.gs.ntos.loads;

import com.gs.ntos.api.DesktopProgram;
import com.gs.ntos.enums.Level;
import com.gs.ntos.filters.IntegerFilter;
import com.gs.ntos.logging.Logger;
import com.gs.ntos.utils.ScreenUtils;
import com.gs.ntos.verifiers.NotEmptyInputVerifier;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.ResourceBundle;
import javax.swing.AbstractAction;
import static javax.swing.Action.DISPLAYED_MNEMONIC_INDEX_KEY;
import static javax.swing.Action.SHORT_DESCRIPTION;
import static javax.swing.Action.SMALL_ICON;
import javax.swing.ImageIcon;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.text.PlainDocument;

/**
 *
 * @author Sean Carrick
 */
public class StartLoadDialog extends javax.swing.JDialog 
        implements DocumentListener, PropertyChangeListener {
    
    private final DesktopProgram program = DesktopProgram.getInstance();
    private final Logger logger;
    private final ResourceBundle bundle;
    private final Color accepted = new Color(176, 229, 212);
    private final CancelAction cancel;
    private final SaveAction save;
    private final StartLoadDialog instance;
    
    private String orderNumber;
    private String tripNumber;
    private boolean cancelled;
    private boolean canSave;

    /**
     * Constructs a new default `StartLoadDialog`. This constructor requires the
     * methods `setOrderNumber()` and `setTripNumber()` to be called separately.
     * 
     * @param parent the parent window to this dialog
     * 
     * @see #StartLoadDialog(java.awt.Frame, java.lang.String, java.lang.String) 
     * @see #setOrderNumber(java.lang.String) 
     * @see #setTripNumber(java.lang.String) 
     */
    public StartLoadDialog(java.awt.Frame parent) {
        super(parent, true);
        logger = new Logger(program, getClass().getSimpleName(), (Level)
                program.getProperties().getSystemProperty("logging.level", 
                        Level.ALL));
        bundle = ResourceBundle.getBundle("com/gs/ntos/loads/resources/Bundle");
        
        cancel = new CancelAction();
        save = new SaveAction();
        
        initComponents();
        
        instance = this;
    }
    
    /**
     * Constructs a new instance of `StartLoadDialog` with the specified
     * `orderNumber` and `tripNumber` preset. This constructor does not require
     * the methods `setOrderNumber()` nor `setTripNumber()` to be called, as the
     * order and trip numbers are specified here.
     * 
     * @param parent the parent window to this dialog
     * @param orderNumber the order number for the load
     * @param tripNumber the trip number for the load
     * 
     * @see #StartLoadDialog(java.awt.Frame) 
     * @see #setOrderNumber(java.lang.String) 
     * @see #setTripNumber(java.lang.String) 
     */
    public StartLoadDialog(java.awt.Frame parent, String orderNumber, 
            String tripNumber) {
        this(parent);
        
        this.orderNumber = orderNumber;
        this.tripNumber = tripNumber;
    }
    
    private void validateContents() {
        if (odometerField.getText() == null || odometerField.getText().isBlank()
                || odometerField.getText().isEmpty() 
                || Integer.valueOf(odometerField.getText()) < 1) {
            tipLabel.setText(bundle.getString("StartLoadDialog.problem.text"));
            odometerField.setBackground(new Color(255, 193, 193));
        } else {
            odometerField.setBackground(accepted);
            tipLabel.setText(bundle.getString("StartLoadDialog.tipLabel.text"));
        }
    }
    
    public boolean isCancelled() {
        return cancelled;
    }
    
    public void setCancelled(boolean cancelled) {
        boolean oldValue = isCancelled();
        this.cancelled = cancelled;
        
        program.firePropertyChange("cancelled", oldValue, isCancelled());
    }
    
    public boolean canSave() {
        return canSave;
    }
    
    public void setCanSave(boolean canSave) {
        boolean oldValue = canSave();
        this.canSave = canSave;
        
        program.firePropertyChange("canSave", oldValue, canSave());
    }
    
    public void setOrderNumber(String orderNumber) {
        this.orderNumber = orderNumber;
        orderNumberField.setText(orderNumber);
    }
    
    public void setTripNumber(String tripNumber) {
        this.tripNumber = tripNumber;
        tripNumberField.setText(tripNumber);
    }

    @Override
    public void insertUpdate(DocumentEvent e) {
        changedUpdate(e);
    }

    @Override
    public void removeUpdate(DocumentEvent e) {
        changedUpdate(e);
    }

    @Override
    public void changedUpdate(DocumentEvent e) {
        validateContents();
    }

    @Override
    public void propertyChange(PropertyChangeEvent evt) {
        if ("canSave".equals(evt.getPropertyName())) {
            save.setEnabled((Boolean) evt.getNewValue());
        }
    }

    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        bottomPanel = new javax.swing.JPanel();
        jSeparator1 = new javax.swing.JSeparator();
        commandPanel = new javax.swing.JPanel();
        jButton1 = new javax.swing.JButton();
        jButton2 = new javax.swing.JButton();
        detailsPanel = new javax.swing.JPanel();
        orderNumberLabel = new javax.swing.JLabel();
        orderNumberField = new javax.swing.JTextField();
        tripNumberLabel = new javax.swing.JLabel();
        tripNumberField = new javax.swing.JTextField();
        odometerLabel = new javax.swing.JLabel();
        odometerField = new javax.swing.JTextField();
        tipLabel = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setName("Form"); // NOI18N

        bottomPanel.setName("bottomPanel"); // NOI18N
        bottomPanel.setLayout(new java.awt.BorderLayout());

        jSeparator1.setName("jSeparator1"); // NOI18N
        bottomPanel.add(jSeparator1, java.awt.BorderLayout.PAGE_START);

        commandPanel.setName("commandPanel"); // NOI18N

        jButton1.setText(bundle.getString("StartLoadDialog.jButton1.text")); // NOI18N
        jButton1.setName("jButton1"); // NOI18N

        jButton2.setText(bundle.getString("StartLoadDialog.jButton2.text")); // NOI18N
        jButton2.setName("jButton2"); // NOI18N

        javax.swing.GroupLayout commandPanelLayout = new javax.swing.GroupLayout(commandPanel);
        commandPanel.setLayout(commandPanelLayout);
        commandPanelLayout.setHorizontalGroup(
            commandPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, commandPanelLayout.createSequentialGroup()
                .addContainerGap(249, Short.MAX_VALUE)
                .addComponent(jButton1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jButton2)
                .addContainerGap())
        );
        commandPanelLayout.setVerticalGroup(
            commandPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(commandPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(commandPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jButton1)
                    .addComponent(jButton2))
                .addContainerGap(15, Short.MAX_VALUE))
        );

        bottomPanel.add(commandPanel, java.awt.BorderLayout.CENTER);

        getContentPane().add(bottomPanel, java.awt.BorderLayout.PAGE_END);

        detailsPanel.setName("detailsPanel"); // NOI18N

        orderNumberLabel.setText(bundle.getString("StartLoadDialog.orderNumberLabel.text")); // NOI18N
        orderNumberLabel.setName("orderNumberLabel"); // NOI18N

        orderNumberField.setEditable(false);
        orderNumberField.setText(orderNumber != null && !orderNumber.isBlank() ? orderNumber : "");
        orderNumberField.setFocusable(false);
        orderNumberField.setName("orderNumberField"); // NOI18N

        tripNumberLabel.setText(bundle.getString("StartLoadDialog.tripNumberLabel.text")); // NOI18N
        tripNumberLabel.setName("tripNumberLabel"); // NOI18N

        tripNumberField.setEditable(false);
        tripNumberField.setText(tripNumber != null && !tripNumber.isBlank() ? tripNumber : "");
        tripNumberField.setFocusable(false);
        tripNumberField.setName("tripNumberField"); // NOI18N

        odometerLabel.setText(bundle.getString("StartLoadDialog.odometerLabel.text")); // NOI18N
        odometerLabel.setName("odometerLabel"); // NOI18N

        odometerField.setBackground(new java.awt.Color(255, 193, 193));
        odometerField.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        odometerField.setText(bundle.getString("StartLoadDialog.odometerField.text")); // NOI18N
        odometerField.setName("odometerField"); // NOI18N
        PlainDocument doc = (PlainDocument) odometerField.getDocument();
        doc.setDocumentFilter(new IntegerFilter());
        odometerField.setInputVerifier(new NotEmptyInputVerifier());

        tipLabel.setFont(new java.awt.Font("Noto Sans", 1, 13)); // NOI18N
        tipLabel.setForeground(new java.awt.Color(0, 0, 255));
        tipLabel.setText(bundle.getString("StartLoadDialog.tipLabel.text")); // NOI18N
        tipLabel.setName("tipLabel"); // NOI18N

        javax.swing.GroupLayout detailsPanelLayout = new javax.swing.GroupLayout(detailsPanel);
        detailsPanel.setLayout(detailsPanelLayout);
        detailsPanelLayout.setHorizontalGroup(
            detailsPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(detailsPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(detailsPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(detailsPanelLayout.createSequentialGroup()
                        .addGroup(detailsPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(odometerLabel)
                            .addComponent(orderNumberLabel))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(detailsPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addGroup(detailsPanelLayout.createSequentialGroup()
                                .addComponent(orderNumberField, javax.swing.GroupLayout.PREFERRED_SIZE, 109, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(tripNumberLabel))
                            .addComponent(odometerField))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(tripNumberField, javax.swing.GroupLayout.PREFERRED_SIZE, 109, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 2, Short.MAX_VALUE))
                    .addComponent(tipLabel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        detailsPanelLayout.setVerticalGroup(
            detailsPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(detailsPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(detailsPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(orderNumberLabel)
                    .addComponent(orderNumberField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(tripNumberLabel)
                    .addComponent(tripNumberField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(detailsPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(odometerLabel)
                    .addComponent(odometerField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 32, Short.MAX_VALUE)
                .addComponent(tipLabel))
        );

        getContentPane().add(detailsPanel, java.awt.BorderLayout.CENTER);
        setTitle(bundle.getString("StartLoadDialog.title"));
        setIconImage(new ImageIcon(getClass().getResource(bundle.getString("StartLoadDialog.icon"))).getImage());
        program.addPropertyChangeListener("canSave", this);

        validateContents();

        setLocation(ScreenUtils.getCenterPoint(getParent(), this, true));

        pack();
    }// </editor-fold>//GEN-END:initComponents

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JPanel bottomPanel;
    private javax.swing.JPanel commandPanel;
    private javax.swing.JPanel detailsPanel;
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton2;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JTextField odometerField;
    private javax.swing.JLabel odometerLabel;
    private javax.swing.JTextField orderNumberField;
    private javax.swing.JLabel orderNumberLabel;
    private javax.swing.JLabel tipLabel;
    private javax.swing.JTextField tripNumberField;
    private javax.swing.JLabel tripNumberLabel;
    // End of variables declaration//GEN-END:variables
    
    private class SaveAction extends AbstractAction {

        private static final long serialVersionUID = -7232581254088203628L;

        public SaveAction() {
            super(bundle.getString("SaveAction.text"));
            
            initAction();
        }
        
        private void initAction() {
            putValue(SMALL_ICON, new ImageIcon(getClass().getResource(
                    bundle.getString("SaveAction.smallIcon"))));
            putValue(DISPLAYED_MNEMONIC_INDEX_KEY, Integer.valueOf(
                    bundle.getString("SaveAction.mnemonicIndex")));
            putValue(SHORT_DESCRIPTION, 
                    bundle.getString("SaveAction.shortDescription"));
        }
        
        @Override
        public void actionPerformed(ActionEvent e) {
            cancelled = false;
                        
            logger.exiting("StartLoadDialog.SaveAction.actionPerformed(e).");
            instance.setVisible(false);
        }
        
    }
    
    private class CancelAction extends AbstractAction {

        private static final long serialVersionUID = -6089139508449217744L;
        
        public CancelAction() {
            super(bundle.getString("CancelAction.text"));
            
            initAction();
        }
        
        private void initAction() {
            putValue(SMALL_ICON, new ImageIcon(getClass().getResource(
                    bundle.getString("CancelAction.smallIcon"))));
            putValue(DISPLAYED_MNEMONIC_INDEX_KEY, Integer.valueOf(
                    bundle.getString("CancelAction.mnemonicIndex")));
            putValue(SHORT_DESCRIPTION, 
                    bundle.getString("CancelAction.shortDescription"));
        }

        @Override
        public void actionPerformed(ActionEvent e) {
            cancelled = true;
            instance.setVisible(false);
        }
        
    }
    
}
