/*
 * Copyright (C) 2022 GS United Labs
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * *****************************************************************************
 *  Project    :   LoadAccounting
 *  Class      :   IntGreaterThanZeroVerifier.java
 *  Author     :   Sean Carrick
 *  Created    :   Mar 30, 2022
 *  Modified   :   Mar 30, 2022
 *  
 *  Purpose: See class JavaDoc for explanation
 *  
 *  Revision History:
 *  
 *  WHEN          BY                   REASON
 *  ------------  -------------------  -----------------------------------------
 *  Mar 30, 2022  Sean Carrick         Initial creation.
 * *****************************************************************************
 */

package com.gs.ntos.loads.support;

import javax.swing.InputVerifier;
import javax.swing.JComponent;
import javax.swing.JTextField;

/**
 *
 * @author Sean Carrick &lt;sean at gs-unitedlabs dot com&gt;
 * 
 * @version 1.0
 * @since 1.0
 */
public class IntGreaterThanZeroVerifier extends InputVerifier {
    
    public IntGreaterThanZeroVerifier () {
        
    }

    @Override
    public boolean verify(JComponent input) {
        int value = 0;
        
        if (input instanceof JTextField) {
            try {
                value = Integer.parseInt(((JTextField) input).getText());
            } catch (NumberFormatException ex) {
                return false;
            }
        }
        
        return value > 0;
    }

}
