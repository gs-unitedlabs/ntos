/*
 * Copyright (C) 2022 GS United Labs
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * *****************************************************************************
 *  Project    :   NTOS
 *  Class      :   LoadWizardPanelProvider.java
 *  Author     :   Sean Carrick
 *  Created    :   Sep 07, 2020
 *  Modified   :   Oct 12, 2020
 *  
 *  Purpose: See class JavaDoc for explanation
 *  
 *  Revision History:
 *  
 *  WHEN          BY                   REASON
 *  ------------  -------------------  -----------------------------------------
 *  Sep 07, 2020  Sean Carrick         Initial creation.
 *  Oct 10, 2020  Sean Carrick         Modified constructors to call each
 *                                     successive constructor until such time as
 *                                     the super() needed to be called to prevent
 *                                     repetition of the same code for
 *                                     initialization.
 *  Oct 12, 2020  Jiří Kovalský        Improved parsing of the truck.pay value
 *                                     which will work in all countries, not only
 *                                     in USA, if NTOS plans to target markets
 *                                     around the world.
 *  Apr 05, 2022  Sean Carrick         Ported class to the NTOS Project from the
 *                                     old LoadMaster Project.
 *  Apr 06, 2022  Sean Carrick         Moved Jiří's improved parsing to a static
 *                                     class in the ntos.utils module.
 * *****************************************************************************
 */

package com.gs.ntos.loads;

import com.gs.ntos.api.DesktopProgram;
import com.gs.ntos.database.errors.DataStoreException;
import com.gs.ntos.database.models.Broker;
import com.gs.ntos.database.models.Load;
import com.gs.ntos.database.models.Stop;
import com.gs.ntos.database.tables.BrokersTable;
import com.gs.ntos.database.tables.LoadsTable;
import com.gs.ntos.database.tables.StopsTable;
import com.gs.ntos.enums.Level;
import com.gs.ntos.formatters.NumericalFormatters;
import com.gs.ntos.loads.panels.BrokerPanel;
import com.gs.ntos.loads.panels.LoadPanel;
import com.gs.ntos.loads.panels.StopsPanel;
import com.gs.ntos.loads.panels.SummaryPanel;
import com.gs.ntos.loads.panels.WelcomePanel;
import com.gs.ntos.logging.Logger;
import com.gs.ntos.support.MessageBox;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;
import javax.swing.JCheckBox;
import javax.swing.JComponent;
import javax.swing.text.JTextComponent;
import org.netbeans.spi.wizard.WizardController;
import org.netbeans.spi.wizard.WizardPanelProvider;

/**
 *
 * @author Sean Carrick &lt;sean at gs-unitedlabs dot com&gt;
 * 
 * @version 1.0
 * @since 1.0
 */
public class LoadWizardPanelProvider extends WizardPanelProvider 
        implements ActionListener {
    
    private final DesktopProgram program;
    private final Logger logger;
    private final ResourceBundle bundle;
    
    private final LoadsTable loads;
    private final StopsTable stops;
    private final BrokersTable brokers;
    
    private Map<String, Object> settings;
    private WizardController controller;
    
    public LoadWizardPanelProvider (DesktopProgram program) {
        this(program, new String[] {
            "welcome", "load", "broker", "stops", "summary"},
            new String[] {
            "Welcome", "General Load Information", "Broker/Agent Information",
            "Stops/Customers Information", "Load Summary"});
    }
    
    public LoadWizardPanelProvider(DesktopProgram program, String[] steps,
            String[] descriptions) {
        this(program, steps, descriptions, "New Load Wizard");
    }
    
    public LoadWizardPanelProvider(DesktopProgram program, String[] steps,
            String[] descriptions, String title) {
        super(title, steps, descriptions);
        
        logger = new Logger(program, getClass().getSimpleName(), (Level)
                program.getProperties().getSystemProperty("logging.level", 
                Level.ALL));
        logger.entering("LoadWizardPanelProvider(program = {0}, steps = {1}, "
                + "descriptions = {2}, titel = {3}) :: Constructing...",
                program, (Object) steps, (Object) descriptions, title);
        this.program = program;
        bundle = ResourceBundle.getBundle("com/gs/ntos/loads/resources/Bundle");
        settings = new HashMap<>();
        
        loads = new LoadsTable();
        stops = new StopsTable();
        brokers = new BrokersTable();
    }
    
    @Override
    public void actionPerformed(ActionEvent e) {
        String name = null;
        Object value = null;
        
        if (e.getSource() instanceof JTextComponent) {
            name = ((JTextComponent) e.getSource()).getName();
            value = ((JTextComponent) e.getSource()).getText();
        } else if (e.getSource() instanceof JCheckBox) {
            name = ((JCheckBox) e.getSource()).getName();
            value = ((JCheckBox) e.getSource()).isSelected() ? Boolean.TRUE
                    : Boolean.FALSE;
        }
        
        if (name != null && value != null) {
            settings.put(name, value);
        }
    }
    
    @Override
    protected JComponent createPanel(WizardController wc, String string, Map map) {
        this.settings = map;
        this.controller = wc;
        
        switch (string) {
            case "welcome":
                controller.setForwardNavigationMode(WizardController.MODE_CAN_CONTINUE);
                return new WelcomePanel();
            case "load":
                controller.setForwardNavigationMode(WizardController.MODE_CAN_CONTINUE);
                return new LoadPanel();
            case "broker":
                controller.setForwardNavigationMode(WizardController.MODE_CAN_CONTINUE);
                return new BrokerPanel();
            case "stops":
                controller.setForwardNavigationMode(WizardController.MODE_CAN_CONTINUE_OR_FINISH);
                return new StopsPanel();
            case "summary":
                controller.setForwardNavigationMode(WizardController.MODE_CAN_FINISH);
                return new SummaryPanel(settings);
            default:
                throw new Error("Unknown ID " + string + ". Please check the "
                        + "spelling and try again.");
        }
    }
    
    @Override
    protected Object finish(Map settings) {
        Load load = new Load();
        load.setDispatchedDistance(Integer.valueOf(settings.get("loadedMiles").toString()));
        load.setSignatureAndTalley(Boolean.parseBoolean(settings.get("sigTalley").toString()));
        load.setCommodity(settings.get("commodity").toString());
        load.setHazMat(Boolean.parseBoolean(settings.get("hazMat").toString()));
        load.setLtl(Boolean.parseBoolean(settings.get("ltl").toString()));
        load.setOrderNumber(settings.get("orderNumber").toString());
        load.setRampsRequired(Boolean.parseBoolean(settings.get("ramps").toString()));
        
        /* Moved the improved numerical formatting of Jiří's to the ntos.utils
         * module class NumericalFormatters. As we add new types of numerical
         * formatting, we should place them in that class as static methods.
         */
        load.setGrossRevenue(NumericalFormatters.doubleFormatter(settings.get("grossPay").toString()));
        /* ****************************************************************** */
        load.setTarped(Boolean.parseBoolean(settings.get("tarped").toString()));
        load.setTarpHeight(Integer.parseInt(settings.get("tarpHeight").toString()));
        load.setTeam(Boolean.parseBoolean(settings.get("team").toString()));
        load.setTripNumber(settings.get("tripNumber").toString());
        load.setTwic(Boolean.parseBoolean(settings.get("twic").toString()));
        load.setTopCustomer(Boolean.parseBoolean(settings.get("top").toString()));
        load.setWeight(Integer.valueOf(settings.get("weight").toString()));
        load.setPieceCount(Integer.valueOf(settings.get("pieces").toString()));
        load.setBrokerId(((Broker) settings.get("broker")).getId());
        load.setDispatched(LocalDate.now(ZoneId.systemDefault()));
        loads.add(load);
        try {
            loads.close();
        } catch (DataStoreException ex) {
            logger.error(ex, "Attempting to close the LoadsTable", loads);
        }
        
        List<Stop> stopList = (List<Stop>) settings.get("stops");
        // Add the order number to each Stop.
        for (Stop s : stopList) {
            s.setOrderNumber(load.getOrderNumber());
        }
        // Save the stops to the table.
        stops.add(stopList);
        
        
        // Attempt to store the load first, then the stops, but do so in
        //+ separate try...catch blocks so that we can log any error by which
        //+ table it occurred on.
        try {
            stops.close();
        } catch (DataStoreException ex) {
            logger.error(ex, "Attempting to close the StopsTable: {0}", stops);
        }
        
        // Now, let's see if the user is on an active load.
        if (program.getProperty("load.current", "No Active Load")
                .equals("No Active Load")) {
            int response = MessageBox.askQuestion("Would you like to start this "
                    + "load?", "Start Load", false);
            if (response == MessageBox.YES_OPTION) {
                try {
                    loads.connect();
                    
                    load.setActive(true);
                    loads.update(load);
                    
                    program.getProperties().setProperty("load.current", load.getId());
                    program.getProperties().setProperty("load.current.stop", 0);
                    program.getProperties().setProperty("load.settled", false);
                    program.getProperties().setProperty("load.current.stopCount", 
                            stopList.size() * 2);
                    
                    loads.close();
                } catch (DataStoreException ex) {
                    logger.error(ex, "Attempting to reopen the loads table: {0}", 
                            loads);
                }
            }
        }
        
        return null;
    }

}
