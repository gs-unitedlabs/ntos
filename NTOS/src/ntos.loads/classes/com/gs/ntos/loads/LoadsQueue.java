/*
 * Copyright (C) 2022 GS United Labs
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * *****************************************************************************
 *  Project    :   NTOS
 *  Class      :   LoadsQueue.java
 *  Author     :   Sean Carrick
 *  Created    :   Apr 9, 2022
 *  Modified   :   Apr 9, 2022
 *  
 *  Purpose: See class JavaDoc for explanation
 *  
 *  Revision History:
 *  
 *  WHEN          BY                   REASON
 *  ------------  -------------------  -----------------------------------------
 *  Apr 9, 2022  Sean Carrick         Initial creation.
 * *****************************************************************************
 */

package com.gs.ntos.loads;

import com.gs.ntos.api.DesktopProgram;
import com.gs.ntos.api.ExitListener;
import com.gs.ntos.database.errors.DataStoreException;
import com.gs.ntos.database.models.Load;
import com.gs.ntos.database.models.Stop;
import com.gs.ntos.database.tables.LoadsTable;
import com.gs.ntos.database.tables.StopsTable;
import com.gs.ntos.enums.Level;
import com.gs.ntos.logging.Logger;
import com.gs.ntos.support.MessageBox;
import com.gs.ntos.utils.ScreenUtils;
import java.awt.Container;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.text.NumberFormat;
import java.time.format.DateTimeFormatter;
import java.util.Collections;
import java.util.EventObject;
import java.util.ResourceBundle;
import javax.imageio.ImageIO;
import javax.swing.AbstractAction;
import static javax.swing.Action.DISPLAYED_MNEMONIC_INDEX_KEY;
import static javax.swing.Action.SMALL_ICON;
import javax.swing.ImageIcon;
import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;
import javax.swing.JTable;
import javax.swing.UIManager;
import javax.swing.table.DefaultTableModel;
import org.netbeans.api.wizard.WizardDisplayer;
import org.netbeans.spi.wizard.Wizard;

/**
 *
 * @author Sean Carrick
 */
public class LoadsQueue extends javax.swing.JFrame implements ExitListener {
    
    private final LoadsQueue instance;
    private final DesktopProgram program = DesktopProgram.getInstance();
    private final Logger logger;
    private final ResourceBundle bundle;
    private final LoadsTable loads;
    
    /** Creates new form LoadsViewer */
    public LoadsQueue() {
        logger = new Logger(program, getClass().getSimpleName(), (Level)
                program.getProperties().getSystemProperty("logging.level", 
                        Level.ALL));
        bundle = ResourceBundle.getBundle("com/gs/ntos/loads/resources/Bundle");
        
        loads = new LoadsTable();
        
        try {
            loads.connect();
        } catch (DataStoreException ex) {
            logger.error(ex, "Attempting to open the loads table: {0}", loads);
        }
        
        editRecordAction = new EditRecordAction();
        newLoadAction = new NewLoadAction();
        
        initComponents();
        loadData();
        
        instance = this;
    }

    @Override
    public boolean canExit(EventObject event) {
        return true;
    }

    @Override
    public void willExit(EventObject event) {
        try {
            loads.close();
        } catch (DataStoreException ex) {
            logger.error(ex, "Attempting to close the loads table {0}", loads);
        }
    }

    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane1 = new javax.swing.JScrollPane();
        loadsTable = new javax.swing.JTable();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setType(java.awt.Window.Type.UTILITY);

        loadsTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Order #", "Trip #", "Gross Revenue", "Distance", "1st Stop Early Date", "1st Stop Early Time", "Last Stop Late Date", "Last Stop Late Time", "Active?"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class
            };
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false, false, false, false, false
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        loadsTable.addMouseListener(new TableMouseListener());
        jScrollPane1.setViewportView(loadsTable);
        if (loadsTable.getColumnModel().getColumnCount() > 0) {
            loadsTable.getColumnModel().getColumn(0).setHeaderValue(bundle.getString("CustomerSelectorDialog.table.header0")); // NOI18N
            loadsTable.getColumnModel().getColumn(1).setHeaderValue(bundle.getString("CustomerSelectorDialog.table.header1")); // NOI18N
            loadsTable.getColumnModel().getColumn(2).setHeaderValue(bundle.getString("CustomerSelectorDialog.table.header2")); // NOI18N
            loadsTable.getColumnModel().getColumn(3).setHeaderValue(bundle.getString("CustomerSelectorDialog.table.header3")); // NOI18N
            loadsTable.getColumnModel().getColumn(4).setHeaderValue(bundle.getString("CustomerSelectorDialog.table.header4")); // NOI18N
            loadsTable.getColumnModel().getColumn(5).setHeaderValue(bundle.getString("CustomerSelectorDialog.table.header5")); // NOI18N
            loadsTable.getColumnModel().getColumn(6).setHeaderValue(bundle.getString("CustomerSelectorDialog.table.header6")); // NOI18N
            loadsTable.getColumnModel().getColumn(7).setHeaderValue(bundle.getString("CustomerSelectorDialog.table.header7")); // NOI18N
            loadsTable.getColumnModel().getColumn(8).setHeaderValue(bundle.getString("CustomerSelectorDialog.table.header8")); // NOI18N
        }
        setTitle(bundle.getString("LoadsQueue.title"));
        setIconImage(new ImageIcon(getClass().getResource(bundle.getString("LoadsQueue.icon"))).getImage());
        addWindowListener(new LoadsQueueAdapter());

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 780, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 486, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTable loadsTable;
    // End of variables declaration//GEN-END:variables

    private void loadData() {
        logger.entering("LoadsQueue.loadData()");
        
        logger.debug("Getting the table model for the loadsTable (JTable)");
        DefaultTableModel model = (DefaultTableModel) loadsTable.getModel();
        logger.log(Level.TRACE, "Retrieved the table model: {0}", model);
        
        logger.config("Clearing all data from the table model...");
        model.setRowCount(0);
        
        logger.debug("Looping through all loads in the loads table: {0}", loads);
        loads.first();
        do {
            Load l = loads.get();
            logger.log(Level.TRACE, "Retrieved Load = {0}", l);
            logger.log(Level.TRACE, "Only adding the load to the table if it is "
                    + "not completed.");
            if (!l.isCompleted()) {
                StopsTable stops = new StopsTable();
                Stop[] stopsForLoad = Collections.<Stop>emptyList().toArray(new Stop[0]);
                try {
                    stops.connect();
                    stopsForLoad = stops.getStopsForLoad(l);
                    stops.close();
                } catch (DataStoreException ex) {
                    logger.error(ex, "Attempting to connect to stops table", stops);
                }
                
                logger.debug("Formatting gross revenue...");
                String pay = NumberFormat.getCurrencyInstance().format(l.getGrossRevenue());
                logger.debug("Formatting distance...");
                String distance = NumberFormat.getIntegerInstance().format(l.getDispatchedDistance());
                DateTimeFormatter df = DateTimeFormatter.ofPattern("MM/dd/yyyy");
                DateTimeFormatter tf = DateTimeFormatter.ofPattern("HH:mm");
                String[] row = new String[] {
                    l.getOrderNumber(),
                    l.getTripNumber(),
                    pay,
                    distance,
                    df.format(l.getStopFor(1).getEarlyDate()),
                    tf.format(l.getStopFor(1).getEarlyTime()),
                    df.format(l.getStopFor(stopsForLoad.length).getLateDate()),
                    tf.format(l.getStopFor(stopsForLoad.length).getLateTime()),
                    l.isActive() ? "Active" : ""
                };
                
                logger.log(Level.TRACE, "Adding row ({0}) to the table",
                        (Object) row);
                model.addRow(row);
            }
        } while (loads.hasNext());
        
        logger.config("Setting the updated model to the table's model", model);
        loadsTable.setModel(model);
        
        logger.exiting("LoadsQueue.loadData()");
    }
    
    private final EditRecordAction editRecordAction;
    private class EditRecordAction extends AbstractAction {

        private static final long serialVersionUID = -3174484450843162304L;

        public EditRecordAction() {
            super(bundle.getString("EditRecordAction.text"));
            
            initAction();
        }
        
        private void initAction() {
            putValue(SMALL_ICON, new ImageIcon(getClass().getResource(
                    bundle.getString("EditRecordAction.smallIcon"))));
            putValue(DISPLAYED_MNEMONIC_INDEX_KEY, Integer.valueOf(
                    bundle.getString("EditRecordAction.mnemonicIndex")));
            putValue(SMALL_ICON, new ImageIcon(getClass().getResource(
                    bundle.getString("EditRecordAction.smallIcon"))));
        }
        
        @Override
        public void actionPerformed(ActionEvent e) {
            logger.entering("EditRecordAction.actionPerformed(ActionEvent e = {0})", e);
            
            logger.debug("Getting the ID of the load in the selected row...");
            String order = loadsTable.getValueAt(loadsTable.getSelectedRow(), 
                    0).toString();
            logger.log(Level.TRACE, "The selected load's order number is {0}", order);
            long id = loads.getIdForLoadOrderNumber(order);
            logger.log(Level.TRACE, "ID for load with order number ({0}) is {1}.", 
                    order, id);
            Load load = null;
            if (id > 0L) {
                load = loads.get(id);
            }
            
            logger.log(Level.TRACE, "Load retrieved: {0}", load);
            if (load != null) {
                MessageBox.showWarning("Need to create Load Editor.", "Implementation Needed");
            }
            
            loadData();
            
            logger.exiting("EditRecordAction.actionPerformed(ActionEvent e)");
        }
        
    }
    
    private final NewLoadAction newLoadAction;
    private class NewLoadAction extends AbstractAction {

        private static final long serialVersionUID = -3174484450843162304L;

        public NewLoadAction() {
            super(bundle.getString("NewLoadAction.text"));
            
            initAction();
        }
        
        private void initAction() {
            putValue(SMALL_ICON, new ImageIcon(getClass().getResource(
                    bundle.getString("NewLoadAction.smallIcon"))));
            putValue(DISPLAYED_MNEMONIC_INDEX_KEY, Integer.valueOf(
                    bundle.getString("NewLoadAction.mnemonicIndex")));
            putValue(SMALL_ICON, new ImageIcon(getClass().getResource(
                    bundle.getString("NewLoadAction.smallIcon"))));
        }
        
        @Override
        public void actionPerformed(ActionEvent e) {
            try {
                BufferedImage img = ImageIO.read(new File(getClass().getResource(
                        bundle.getString("LoadWizard.sidebar.image")).toURI()));
                UIManager.put("wizard.sidebar.image", img);
            } catch (URISyntaxException
                    | IOException ex) {
                logger.error(ex, "Attempting to load the sidebar image.");
            }
            Wizard wizard = new LoadWizardPanelProvider(program, 
                    new String[] {
                        "welcome",
                        "load",
                        "broker",
                        "stops",
                        "summary"
                    }, new String[] {
                        "Welcome and Instructions",
                        "General Load Information",
                        "Broker/Agent Information",
                        "Stop/Customer Information",
                        "Load Summary"
                    }, "New Load Wizard").createWizard();
            Container throwAway = new Container();
            throwAway.setSize(900, 475);
            Point loc = ScreenUtils.getCenterPoint(instance, throwAway, false);
            
            WizardDisplayer.showWizard(wizard, new Rectangle(loc.x, loc.y, 
                    throwAway.getWidth(), throwAway.getHeight()));
            
            loadData();
        }
        
    }
    
    private class TableMouseListener extends MouseAdapter {

        @Override
        public void mouseReleased(MouseEvent e) {
            int r = loadsTable.rowAtPoint(e.getPoint());
            
            if (r >= 0 && r < loadsTable.getRowCount()) {
                editRecordAction.setEnabled(true);
                loadsTable.setRowSelectionInterval(r, r);
            } else if (r < loadsTable.getRowCount() || r >= loadsTable.getRowCount()) {
                editRecordAction.setEnabled(false);
                loadsTable.clearSelection();
            } else {
                loadsTable.clearSelection();
            }
            
            if (e.isPopupTrigger() && e.getComponent() instanceof JTable) {
                JPopupMenu popupMenu = new JPopupMenu();
                JMenuItem addMenuItem = new JMenuItem();
                addMenuItem.setAction(newLoadAction);
                addMenuItem.setName("addMenuItem");
                
                JMenuItem editMenuItem = new JMenuItem();
                editMenuItem.setAction(editRecordAction);
                editMenuItem.setName("editMenuItem");
                
                popupMenu.add(addMenuItem);
                popupMenu.add(editMenuItem);
                
                popupMenu.show(e.getComponent(), e.getX(), e.getY());
            }
        }

        @Override
        public void mousePressed(MouseEvent e) {
            int r = loadsTable.rowAtPoint(e.getPoint());
            
            if (r >= 0 && r < loadsTable.getRowCount()) {
                editRecordAction.setEnabled(true);
                loadsTable.setRowSelectionInterval(r, r);
            } else if (r < 0 || r >= loadsTable.getRowCount()) {
                editRecordAction.setEnabled(false);
                loadsTable.clearSelection();
            } else {
                loadsTable.clearSelection();
            }
            
            if (e.isPopupTrigger() && e.getComponent() instanceof JTable) {
                JPopupMenu popupMenu = new JPopupMenu();
                JMenuItem addMenuItem = new JMenuItem();
                addMenuItem.setAction(newLoadAction);
                addMenuItem.setName("addMenuItem");
                
                JMenuItem editMenuItem = new JMenuItem();
                editMenuItem.setAction(editRecordAction);
                editMenuItem.setName("editMenuItem");
                
                popupMenu.add(addMenuItem);
                popupMenu.add(editMenuItem);
                
                popupMenu.show(e.getComponent(), e.getX(), e.getY());
            }
        }
        
    }
    
    private class LoadsQueueAdapter extends WindowAdapter {

        @Override
        public void windowClosing(WindowEvent e) {
            program.save(instance);
        }

        @Override
        public void windowOpened(WindowEvent e) {
            super.windowOpened(e); //To change body of generated methods, choose Tools | Templates.
        }
        
    }
    
}
